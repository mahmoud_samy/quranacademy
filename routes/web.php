<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', fn () => view('welcome'));
Route::get('/privacy-policy', fn () => view('privacy-policy'));
Route::get('/terms', fn () => view('terms'));

Route::prefix('storage')->group(fn () => [
    Route::get('image/{image_id}', 'storage@getImageFromModelFolder')->name('image_Real_path'),
    Route::get('file/{file_id}', 'storage@getFileFromModelFolder')->name('file_Real_path'),
    Route::get('video/{video_id}', 'storage@getVideoFromModelFolder')->name('video_Real_path'),
]);

Route::get('schedule', fn () => response()->json(['session' => App\Models\session::schedule(),  'instructor' => App\Models\instructor::schedule()]))->name('schedule');
Route::get('soka',function (){
    $details = [
        'email' => 'ahmed.salah.fcih@gmail.com',
        'type' => 'welcome_registration',
        'title' => 'Mail from quran academy',
        'body' => 'This is for testing email using smtp'
    ];



    dispatch(new App\Jobs\SendEventsEmailJob($details));

    dd("Email is Sent.");
});
