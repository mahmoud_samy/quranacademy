<?php

use App\Models\instructor;
use App\Models\student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:students')->get('/students', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:instructors')->get('/instructors', function (Request $request) {
    return $request->user();
});

Route::delete('students/{email?}', function (Request $request, $email = null) {

    if (request()->get('password') !== env('API_REMOVE_USER_PASSWORD')) {
        abort(403);
    }

    if (!$email) {
        return null;
    }

    $student = student::where('email', $email)->firstOrFail();

    if ($student) {


        $images = App\Models\image::where('uplouder_id', $student->id)->where('uplouder_type', student::class)->get();

        foreach ($images as $image) {
            Storage::disk('images')->delete($image->id);
        }

        $images = App\Models\image::where('uplouder_id', $student->id)->where('uplouder_type', student::class)->delete();

        $student->delete();
    }

    return response()->json([
        'message' => 'delete student successfully',
        'code' => 200
    ]);
});

Route::delete('instructors/{email?}', function (Request $request, $email = null) {

    if (request()->get('password') !== env('API_REMOVE_USER_PASSWORD')) {
        abort(403);
    }

    if (!$email) {
        return null;
    }

    $instructor = instructor::where('email', $email)->firstOrFail();

    if ($instructor) {


        $images = App\Models\image::where('uplouder_id', $instructor->id)->where('uplouder_type', instructor::class)->get();

        foreach ($images as $image) {
            Storage::disk('images')->delete($image->id);
        }

        $files = App\Models\File::where('instructor_id', $instructor->id)->get();

        foreach ($files as $file) {
            Storage::disk('Files')->delete($file->id);
        }

        $images = App\Models\image::where('uplouder_id', $instructor->id)->where('uplouder_type', instructor::class)->delete();
        $files = App\Models\File::where('instructor_id', $instructor->id)->delete();

        $instructor->delete();
    }

    return response()->json([
        'message' => 'delete instructor successfully',
        'code' => 200
    ]);
});
