<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    Route::prefix('admin')->as('admin.')->group(function () {
        Route::get('dashboard', function () {
            return view('admin.dashboard');
        })->name('dashboard');

        Route::get('settings/emails',   [\App\Http\Controllers\Admin\EmailsController::class, "create"])->name('settings.emails.create');
        Route::post('settings/emails',   [\App\Http\Controllers\Admin\EmailsController::class, "store"])->name('settings.emails.store');
        Route::resource('users', \App\Http\Controllers\Admin\UserController::class)->except(['show', 'destroy']);
        Route::resource('students', \App\Http\Controllers\Admin\StudentController::class)->except(['show', 'destroy']);
        Route::resource('instructors', \App\Http\Controllers\Admin\InstructorController::class)->except(['destroy']);
        Route::resource('instructor-levels', \App\Http\Controllers\Admin\InstructorLevelController::class);
        Route::resource('sub-instructors', \App\Http\Controllers\Admin\SubInstructorController::class)->except(['show', 'create', 'edit', 'update', 'store', 'destroy']);
        Route::resource('sessions', \App\Http\Controllers\Admin\SessionController::class)->except(['show', 'destroy']);
        Route::resource('packages', \App\Http\Controllers\Admin\PackageController::class)->except(['show', 'destroy']);
        Route::resource('plans', \App\Http\Controllers\Admin\PlanController::class)->except(['show', 'destroy']);
        Route::resource('terms', \App\Http\Controllers\Admin\TermController::class)->except(['show', 'destroy']);
        Route::resource('promocodes', \App\Http\Controllers\Admin\PromocodeController::class)->except(['update', 'store', 'show', 'destroy']);
        Route::resource('countries', \App\Http\Controllers\Admin\CountryController::class)->except(['create', 'edit', 'update', 'store', 'show', 'destroy']);
    });
});
