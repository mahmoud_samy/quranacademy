@props(['name', 'label', 'hideLabel' => false, 'src' => null, 'maxWidth' => '20'])

@php
    $refName = str_replace(['[',']'], ['_',''], $name);
    $errName = str_replace(['[',']'], ['.',''], $name);
@endphp

<div x-data="{fileName: null}" class="col-span-6 sm:col-span-4">
    <!-- Profile Photo File Input -->
    <input type="file" class="hidden"
           {{ $attributes }}
           name="{{ $name }}"
           x-ref="{{ $refName }}"
           x-on:change="
                    fileName = $refs.{{ $refName }}.files[0].name;
            " />

    @if(!$hideLabel)
        <x-jet-label for="{{ $refName }}" value="{{ __($label) }}" />
    @endif

    @if($src)
        <!-- Current Profile {{ $label }} -->
        <div class="mt-2">
            {{ basename($src) }}
        </div>
    @endif

    <!-- New Profile {{ $label }} Preview -->
    <div class="mt-2">
        <span class="block" x-text="fileName"></span>
    </div>

    <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.{{ $refName }}.click()">
        {{ __("Select A New $label") }}
    </x-jet-secondary-button>

    <x-jet-input-error for="{{ $errName }}" class="mt-2" />
</div>
