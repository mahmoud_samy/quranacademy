@props([
    'label',
    'likeable',
    'selectedItem'
])

<button 
    wire:click="handleSelectItemFromCategoryList('{{$likeable}}')"
    class="block btnCategory w-full text-left px-2 py-3 text-sm font-semibold {{isset($selectedItem['id']) && $selectedItem['id'] == $likeable ? 'btnMybagTabActive' : ''}} text-gray-900 dark-mode:bg-gray-700 dark-mode:hover:bg-gray-600 dark-mode:focus:bg-gray-600 dark-mode:focus:text-white dark-mode:hover:text-white dark-mode:text-gray-200 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-300 focus:outline-none focus:shadow-outline" 
    >
    {{$label}}
</button>