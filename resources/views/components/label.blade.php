@props(['value'])

<label {{ $attributes->merge(['class' => 'block labelTitle']) }}>
    {{ $value ?? $slot }}
</label>