@props(['title' => 'Booth', 'records' => [], 'selected_records' => []])

<div class="sm:inline-flex divCardsSlider">
    @foreach ($records as $key => $row)
    <div class="mt-10 sm:mr-4 sm:mt-0 sm:w-1/3 sm:flex-shrink-0">
        <div>
            <h4 class="h4CardTitle">{{ $title . $row['id']}}</h4>
            <div class="p-4 border rounded-lg">
                <img class="object-cover w-full rounded-md h-60" src="{{$row['image']}}" alt="">
            </div>
            <div class="flex items-center justify-between mt-3">
                <label class="flex items-center">
                    @php
                        $checked = "";
                        if(in_array($row['id'], $selected_records))
                        {
                            $checked = 'checked';
                        }
                    @endphp
                    <input type="checkbox" wire:click="selectUnselectValue({{$row['id']}})" value="{{$row['id']}}" class="form-checkbox ck" {{ $checked }}>
                    <span class="ml-2 ckLabel">{{ __('Add to Library') }}</span>
                </label>
                <a href="" class="btnPreview">{{ __('Preview') }}</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
<link rel="stylesheet" type="text/css" href="{{ asset('js/slick/slick.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('js/slick/slick-theme.css') }}"/>
<style>
.h4CardTitle
{
    margin: 21px 0px 9px 0 !important;
    font-size: 15px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #666666;
}
.btnPreview
{
    width: 120px;
    height: 40px;
    padding: 7px 32px 12px 33px;
    border-radius: 6px;
    border-style: solid;
    border-width: 2px;
    font-size: 14px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #000000;
}

.ck
{
    position: relative;
    top: 2px;
}

.ckLabel
{
    margin: 4px 0 0 6px;
    font-size: 15px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #666666;
}

.slick-prev:before, .slick-next:before
{
    /*color: #e83d61 !important;*/
}

.slick-dots li.slick-active button:before
{
    /*color: #e83d61 !important;*/
}

.slick-dots
{
    position: static !important;
    bottom: 0px !important;
}

.slick-slide
{
    outline: none;
}
</style>
<script type="text/javascript" src="{{ asset('js/slick/slick.min.js') }}"></script>
<script>
    boothsSlider();

    function boothsSlider()
    {
        $('.divCardsSlider').slick({
            lazyLoad: 'ondemand',
            dots: ($('.slick-slide').length > 3)? true : false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            focusOnSelect: false,
            prevArrow:"<img class='a-left control-c prev slick-prev' src='<?= asset('images/arrow-' . ((app()->getLocale() == 'en')? 'left' : 'right') . '.png'); ?>'>",
            nextArrow:"<img class='a-right control-c next slick-next' src='<?= asset('images/arrow-' . ((app()->getLocale() == 'en')? 'right' : 'left') . '.png'); ?>'>",
            responsive: [
                {
                breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    }
</script>