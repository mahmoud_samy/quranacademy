@props([
    'leadingAddOn' => false,
    'leadingAddIcon' => false,
])

<div class="flex rounded-md shadow-sm">
    @if ($leadingAddOn)
        <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 text-gray-500 sm:text-sm spanSearchIcon">
            @if($leadingAddIcon)
                <x-dynamic-component :component="'icon.'.$leadingAddIcon" />
            @else
                {{ $leadingAddOn }}
            @endif
        </span>
    @endif

    <input {{ $attributes->merge(['class' => 'flex-1 form-input border-cool-gray-300 block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 border-l-0' . ($leadingAddOn ? ' rounded-none rounded-r-md' : '')]) }}/>
</div>