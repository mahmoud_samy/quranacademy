@props(['name', 'mimes' => null, 'minFileSize' => null, 'maxFileSize' => null, 'value' => [], 'multiple' => false])

@php
    if(!empty($mimes)) {
        $mimes = "allowedExtensions: '.".implode(',.', explode(',', $mimes))."',";
    }
    if(!empty($minFileSize)) {
        $minFileSize = "minFileSize: {$minFileSize},";
    }
    if(!empty($maxFileSize)) {
        $maxFileSize = "maxFileSize: {$maxFileSize},";
    }
    $files = [];
    if($value) {
        $uploads = \App\Models\Upload::whereIn('url', $value)->get();
        if($uploads) {
            $files = $uploads->toArray();
        }
    }
@endphp

<div
    x-data="UploaderComponent()"
    x-init="init($refs)"
    wire:ignore
>
    <div x-ref="container" class="container">
        <div x-ref="dropArea" class="dropArea" style="height: auto; overflow: auto">
            <span class="drop"> Drop files here or <a href="" x-ref="browse"><u>{{__('Browse')}}</u></a> </span>
            <input type="file" name="file" x-ref="fileUpload" >
        </div>
    </div>
</div>

<script>

    function UploaderComponent() {
        return {
            init(refs) {
                let preLoadFiles = @json($files);
                let uploadObject = new ej.inputs.Uploader({
                    dropArea: refs.dropArea,
                    files: preLoadFiles,
                    multiple: {{ $multiple ? 'true' : 'false' }},
                    enableRtl: {{ app()->getLocale() == 'ar' ? 'true' : 'false' }},
                    asyncSettings: {
                        saveUrl: '/api/upload/save',
                        removeUrl: '/api/upload/remove',
                    },{!! $mimes !!}{!! $minFileSize !!}{!! $maxFileSize !!}
                    rendering: function(args) {
                        if(args.isPreload) {

                            let file = preLoadFiles[args.index]

                            if(file) {
                                let input = document.createElement('input');
                                input.setAttribute('type', 'hidden');
                                input.setAttribute('name', '{{ $name }}');
                                input.setAttribute('value', file.url);
                                args.element.appendChild(input);
                            }
                        }

                    },
                    success: function(args) {
                        if(args.operation === 'upload') {

                            let headers = parseHeaders(args.response.headers);
                            let path = headers['status-description'];

                            if(path) {

                                let li = refs.dropArea.querySelector('[data-file-name="' + args.file.name + '"]');

                                let input = document.createElement('input');
                                input.setAttribute('type', 'hidden');
                                input.setAttribute('name', '{{ $name }}');
                                input.setAttribute('value', path);
                                input.setAttribute('id', args.file.id);
                                li.appendChild(input);
                            }
                        }
                    },
                    uploading: function(args) {
                        if(args.currentRequest) {
                            args.currentRequest.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
                        }
                    },
                    removing: function(args) {
                        if(args.currentRequest) {

                            if(args.filesData[0].id) {
                                let elem = document.getElementById(args.filesData[0].id);
                                if(elem) {
                                    args.currentRequest.setRequestHeader('FileUrl', elem.value);
                                }
                            }

                            args.currentRequest.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
                        }
                    }
                });

                uploadObject.appendTo(refs.fileUpload);

                (refs.container).style.visibility = 'visible';

                (refs.browse).onclick = function () {
                    document.getElementsByClassName('e-file-select-wrap')[0].querySelector('button').click();
                    return false;
                };
            }
        }
    }

</script>


@push('styles')
    <style>
        .container {
            visibility: hidden;
        }
        #loader {
            color: #008cff;
            font-family: 'Helvetica Neue','calibiri';
            font-size: 14px;
            height: 40px;
            left: 45%;
            position: absolute;
            top: 45%;
            width: 30%;
        }
        .e-file-select-wrap {
            display: none;
        }
        .dropArea .e-upload {
            border: 0;
            margin-top: 15px;
        }
        .dropArea {
            min-height: 18px;
            border: 1px dashed #c3c3cc;
            padding: 55px 20px 40px 20px;
            width: 400px;
        }
    </style>
@endpush
