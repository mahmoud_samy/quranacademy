{{--
-- Important note:
--
-- This template is based on an example from Tailwind UI, and is used here with permission from Tailwind Labs
-- for educational purposes only. Please do not use this template in your own projects without purchasing a
-- Tailwind UI license, or they’ll have to tighten up the licensing and you’ll ruin the fun for everyone.
--
-- Purchase here: https://tailwindui.com/
--}}

@props([
    'code' => 'USD',
    'symbol' => '$',
    'min' => null,
    'max' => null,
    'step' => ''
])

<div class="mt-1 flex rounded-md shadow-sm relative">
    <span class="inline-flex items-center px-3 py-1 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
        {{$symbol}}
    </span>

    <input {{ $attributes }} class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border border-gray-300">
    
    <div class="absolute inset-y-0 right-0 pr-3 mr-6 flex items-center pointer-events-none">
        <span class="text-gray-500 sm:text-sm sm:leading-5" id="price-currency">
            {{$code}}
        </span>
    </div>
</div>
