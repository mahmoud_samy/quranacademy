@props(['data' => [], 'value' => []])

<div
    x-data="{
        dataValue: @if($attributes->has('wire:model'))@entangle($attributes->wire('model'))@else{{ collect($value)->toJson(JSON_HEX_APOS) }}@endif,
        dataSource: {{ collect($data)->toJson(JSON_HEX_APOS) }}
    }"
    x-init="
        new ej.dropdowns.DropDownList({
            dataSource: dataSource,
            query: new ej.data.Query().select(['label', 'id']),
            value: dataValue,
            fields: { text: 'label', value: 'id' },
            placeholder: 'Select',
            allowFiltering: true,
            change: function(args) {
                dataValue = args.value
            },
            filtering: (e) => {
                // load overall data when search key empty.
                if(e.text == '') e.updateData(dataSource);
                else{ 
                var query = new ej.data.Query().select(['label', 'id']);
                query = (e.text !== '') ? query.where('label', 'contains', e.text, true) : query;
                e.updateData(dataSource, query);
                }
            }
        }, $refs.input);
    "
    class="form-input"
    wire:ignore
    autocomplete="off"
>
    <input autocomplete="off"
        {{ $attributes->whereDoesntStartWith('wire:model') }}
        x-ref="input"
    />
</div>
