<div
    x-data="addMoreComponent()"
    x-init="init()"
>
    <x-button.primary x-on:click="addMore">{{__('Add More')}}</x-button.primary>

    @if(isset($items))
        {{ $items }}
    @endif

    <template x-for="(item, index) in items" :key="item">
        <div>
            {{ $slot }}
            <x-button.red x-show="!hasItems && items.length > 1" x-on:click="remove(index)" class="mt-1">{{__('Remove')}}</x-button.red>
        </div>
    </template>
</div>

<script>

    function addMoreComponent() {
        return {
            items: [],
            hasItems: false,

            addMore() {
                let max = parseInt(Math.max( ...this.items ))

                if(isNaN(max)) {
                    max = 0;
                }

                this.items.push(max + 1);
            },

            remove(index) {
                this.items.splice(index, 1);
            },

            init() {
                if(!this.hasItems) {
                    this.addMore()
                }
            }
        }
    }

</script>
