{{--
-- Important note:
--
-- This template is based on an example from Tailwind UI, and is used here with permission from Tailwind Labs
-- for educational purposes only. Please do not use this template in your own projects without purchasing a
-- Tailwind UI license, or they’ll have to tighten up the licensing and you’ll ruin the fun for everyone.
--
-- Purchase here: //tailwindui.com/
--}}

<div
    x-data="{ color: @if($attributes->has('wire:model')) @entangle($attributes->wire('model')) @else '{{ $attributes->get('value') }}' @endif }"
    x-init="
        new ej.inputs.ColorPicker({
            value: color,
            showButtons: false,
            modeSwitcher: false,
            noColor: true,
            change: function(args) {
                color = args.currentValue.hex;
                ($refs.text).textContent = args.currentValue.hex ? args.currentValue.hex : 'No color';
            }
        }, $refs.button);
        ($refs.text).textContent = color;
    "
    wire:ignore
>
    <div class="flex rounded-md shadow-sm form-input border-cool-gray-300 block w-full">
        <div class="flex-1 sm:text-sm" x-ref="text"></div>
        <div class="rounded-full h-6 w-6 flex items-center justify-center" x-ref="button" ></div>
        @if($attributes->get('name'))
            <input type="hidden" name="{{ $attributes->get('name') }}" x-bind:value="color">
        @endif
    </div>
</div>
