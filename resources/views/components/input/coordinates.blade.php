@props(['imageSrc', 'imageName', 'name', 'id', 'label' => 'Image Coordinates', 'value' => null, 'options' => null])

@php
    // https://github.com/n-peugnet/image-map-creator
    $refName = str_replace(['[',']'], ['_',''], $name);
    $errName = str_replace(['[',']'], ['.',''], $name);
@endphp

<div
    class="mb-4 space-y-3"
    x-data="{
        open: false,
        start() {
            let imgCreator = new imageMapCreator('image-map-{{ $id }}', 800, 600);
            imgCreator.save = () => {

                @if($options)
                    var options = [];
                    $('#{{ $refName }} .coords-input select').each(function(){
                       options.push($(this).val()); // store in an array for reuse
                    });
                @endif

                var coords = document.getElementById('{{ $refName }}')
                coords.innerHTML = '';

                $($(imgCreator.map.toHtml()).children().get().reverse()).each(function (index) {
                    @if($options)
                    var div = document.createElement('div');
                    div.setAttribute('class', 'coords-input mb-2 p-4 rounded bg-gray-100');

                    var array = {{ $options }};

                    var select = document.createElement('select');

                    for (var i = 0; i < array.length; i++) {
                        var option = document.createElement('option');
                        option.value = array[i];
                        option.text = array[i];
                        option.selected = array[i] == options[index];
                        select.appendChild(option);
                    }

                    select.setAttribute('class', 'form-select block w-full pl-3 pr-10 py-2 text-base leading-6 border-gray-300 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5');
                    select.setAttribute('name', '{{ $name }}[items]['+ index +'][url]');
                    div.appendChild(select);

                    var input = document.createElement('input');
                    input.setAttribute('class', 'form-input rounded-md shadow-sm block mt-1 w-full');
                    input.setAttribute('type', 'text');
                    input.setAttribute('name', '{{ $name }}[items]['+ index +'][coords]');
                    input.setAttribute('value', $(this).attr('coords'));
                    div.appendChild(input);

                    coords.appendChild(div);
                    @else
                    var input = document.createElement('input');
                    input.setAttribute('class', 'coords-input form-input rounded-md shadow-sm block mt-1 w-full');
                    input.setAttribute('type', 'text');
                    input.setAttribute('name', '{{ $name }}[coords][]');
                    input.setAttribute('value', $(this).attr('coords'));
                    coords.appendChild(input);
                    @endif
                });

                $('#json_{{ $refName }}').val(imgCreator.exportMap());
                this.open = false
            }
            imgCreator.importMap = (json) => {
                if(json) {
                    let object = JSON.parse(json);
                    let objectMap = object.map;
                    imgCreator.map.setFromObject(objectMap);
                    imgCreator.reset();
                }
            }
            imgCreator.myHandeFile = imgCreator.handeFile
            imgCreator.handeFile = function(file) {
                imgCreator.myHandeFile(file)
                imgCreator.clearAreas()

                var fileInput = document.getElementById('file_input_{{ $refName }}')

                const dT = new DataTransfer();
                dT.items.add(file.file);
                fileInput.files = dT.files;
            }
            imgCreator.myClearAreas = imgCreator.clearAreas
            imgCreator.clearAreas = () => {
                imgCreator.myClearAreas();
                $('.coords-input').remove();
                $('#json_{{ $refName }}').val('');
            }
            imgCreator.myCreateArea = imgCreator.createArea
            imgCreator.createArea = (area) => {
                imgCreator.myCreateArea(area)
                imgCreator.save()
                this.open = true
            }
            imgCreator.myDeleteArea = imgCreator.deleteArea
            imgCreator.deleteArea = (area) => {
                imgCreator.myDeleteArea(area)
                imgCreator.save()
                this.open = true
            }
            @if($value)
            imgCreator.importMap($('#json_{{ $refName }}').val());
            @endif
            @if($imageSrc)
            imgCreator.img.data = imgCreator.p5.loadImage('{{ base64_encode_image($imageSrc) }}', img => {
                imgCreator.view.scale = 1;
                imgCreator.view.transX = 0;
                imgCreator.view.transY = 0;
                let xScale = imgCreator.width / img.width;
                let yScale = imgCreator.height / img.height;
                if (xScale < imgCreator.view.scale)
                    imgCreator.view.scale = xScale;
                if (yScale < imgCreator.view.scale)
                    imgCreator.view.scale = yScale;
                imgCreator.map.setSize(img.width, img.height);
            })
            @endif
        }
    }"
    x-init="start()"
>
    <x-button.primary x-on:click.prevent="open = true">{{__('Open Editor')}}</x-button.primary>

    <div class="relative" x-show="open" id="image-map-{{ $id }}"></div>

    <x-jet-input type="file" name="{{ $imageName }}" id="file_input_{{ $refName }}" class="hidden" />
    <x-jet-input type="hidden" name="{{ $name }}[json]" id="json_{{ $refName }}" class="hidden" :value='old($errName . ".json", $value["json"] ?? null)' />

    <x-jet-label for="{{ $refName }}" value="{{ $label }}" />
    <div id="{{ $refName }}">
        @if($options)
            @foreach(old($errName . ".items", $value["items"] ?? []) as $key => $item)
                <div class="p-4 mb-2 bg-gray-100 rounded coords-input">
                    <x-input.select name="{{ $name }}[items][{{ $key }}][url]">
                        @foreach(json_decode($options, true) as $value)
                        <option value="{{ $value }}" {{ $value == $item["url"] ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </x-input.select>
                    <x-jet-input class="block w-full mt-1" type="text" name="{{ $name }}[items][{{ $key }}][coords]" :value='$item["coords"]' />
                </div>
            @endforeach
        @else
            @foreach(old($errName . ".coords", $value["coords"] ?? []) as $coords)
                <x-jet-input class="block w-full mt-1 coords-input" type="text" name="{{ $name }}[coords][]" :value='$coords' />
            @endforeach
        @endif
    </div>
    <x-jet-input-error for="{{ $imageName }}" />
    <x-jet-input-error for="{{ $errName }}.coords" />
    <x-jet-input-error for="{{ $errName }}.json" />
</div>

@push('scripts')
    <script src="{{ asset('js/image-map-creator.bundle.js') }}" ></script>
    <script src="{{ asset('js/p5.js') }}"></script>
    <script src="{{ asset('js/p5.dom.js') }}"></script>
@endpush
