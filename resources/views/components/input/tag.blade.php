<div class="mt-4">
    <div>
        <input type="hidden" name="selected_tags" value="{{json_encode($selectedTags)}}">
        <div class="w-full">
            <div class="tags-input">
                @foreach ($selectedTags as $tag)
                    <div class="bg-blue-100 inline-flex items-center text-sm rounded mb-2 mr-1 overflow-hidden">
                        <span class="ml-2 mr-1 leading-relaxed truncate max-w-xs px-1">{{$tag['name']}}</span>
                        <button type="button" class="w-6 h-8 inline-block align-middle text-gray-500 bg-blue-200 focus:outline-none" wire:click="removeTag('{{$tag['id']}}')">
                            <svg class="w-6 h-6 fill-current mx-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" d="M15.78 14.36a1 1 0 0 1-1.42 1.42l-2.82-2.83-2.83 2.83a1 1 0 1 1-1.42-1.42l2.83-2.82L7.3 8.7a1 1 0 0 1 1.42-1.42l2.83 2.83 2.82-2.83a1 1 0 0 1 1.42 1.42l-2.83 2.83 2.83 2.82z"/>
                            </svg>
                        </button>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="relative mt-3 md:mt-0" x-data="{ isOpen: true }" x-on:click.away="isOpen = false">

            <input
                type="text"
                wire:model.debounce.500ms="newTag"
                wire:keydown.enter.prevent="addTag"
                @keydown.window="
                    if (event.keyCode === 191) {
                        event.preventDefault();
                        $refs.search.focus();
                    }
                "
                @focus="isOpen = true"
                @keydown.escape.window="isOpen = false"
                x-ref="search"
                class="appearance-none block w-full bg-white text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 mt-2"
                placeholder="{{ __('Enter some filters') }}"/>

            @if (strlen($newTag) >= 2)
                <div
                    class="z-50 absolute bg-gray-200 text-sm rounded w-64 mt-2"
                    x-show.transition.opacity="isOpen"
                >
                    @if (count($searchResults))
                        <ul>
                            @foreach ($searchResults as $result)
                                <li class="border-b border-gray-300">
                                    <button
                                        wire:click="selectTag('{{$result['id']}}')"
                                        x-on:click="isOpen = false"
                                        type="button"
                                        class="hover:bg-gray-400 w-full px-3 py-3 flex items-center transition ease-in-out duration-150"
                                        @if ($loop->last)
                                        @keydown.tab="isOpen = false"
                                        @endif
                                    >
                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                                        </svg>
                                        <span class="ml-4">{{ $result['name'] }}</span>
                                    </button>
                                </li>
                            @endforeach

                        </ul>
                    @else
                        <div class="px-3 py-3">{{ __('No results for') }} "{{ $newTag }}"</div>
                    @endif
                </div>
            @endif

        </div>
    </div>
</div>



{{--<div class="mt-4">--}}
{{--    <div>--}}
{{--        <input type="hidden" name="selected_tags" value="{{json_encode($selectedTags)}}">--}}
{{--        <div class="w-full tags">--}}
{{--            <div class="flex flex-auto flex-wrap p-1">--}}
{{--                @foreach ($selectedTags as $tag)--}}
{{--                <div class="flex justify-center items-center m-1 font-medium py-1 px-1 tag">--}}
{{--                    <span class="text">{{$tag['name']}}</span>--}}
{{--                    <button type="button" class="close" wire:click="removeTag('{{$tag['id']}}')">--}}
{{--                        <x-icon.close-with-circle class="close"/>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                @endforeach--}}
{{--                <input--}}
{{--                type="text"--}}
{{--                wire:model.debounce.500ms="newTag"--}}
{{--                wire:keydown.enter.prevent="addTag"--}}
{{--                @keydown.window="--}}
{{--                    if (event.keyCode === 191) {--}}
{{--                        event.preventDefault();--}}
{{--                        $refs.search.focus();--}}
{{--                    }--}}
{{--                "--}}
{{--                @focus="isOpen = true"--}}
{{--                @keydown.escape.window="isOpen = false"--}}
{{--                x-ref="search"--}}
{{--                class="appearance-none bg-white text-gray-700 border border-gray-200 rounded-full py-2 px-4 leading-tight focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500" --}}
{{--                placeholder="Enter some tags"/>--}}
{{--                <button wire:click="addTag" type="button" class="flex justify-center items-center m-1 py-1 px-1 btnAddTag">--}}
{{--                    + Add Tag--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="relative mt-3 md:mt-0" x-data="{ isOpen: true }" x-on:click.away="isOpen = false">--}}

{{--            <input--}}
{{--                type="text"--}}
{{--                wire:model.debounce.500ms="newTag"--}}
{{--                wire:keydown.enter.prevent="addTag"--}}
{{--                @keydown.window="--}}
{{--                    if (event.keyCode === 191) {--}}
{{--                        event.preventDefault();--}}
{{--                        $refs.search.focus();--}}
{{--                    }--}}
{{--                "--}}
{{--                @focus="isOpen = true"--}}
{{--                @keydown.escape.window="isOpen = false"--}}
{{--                x-ref="search"--}}
{{--                class="hidden appearance-none  w-full bg-white text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:ring-1 focus:ring-blue-500 focus:border-blue-500 mt-2" --}}
{{--                placeholder="Enter some tags"/>--}}
{{--        --}}
{{--                @if (strlen($newTag) >= 2)--}}
{{--                <div--}}
{{--                    class="z-50 absolute bg-gray-200 text-sm rounded w-64 mt-2"--}}
{{--                    x-show.transition.opacity="isOpen"--}}
{{--                >--}}
{{--                    @if (count($searchResults))--}}
{{--                        <ul>--}}
{{--                            @foreach ($searchResults as $result)--}}
{{--                                <li class="border-b border-gray-300">--}}
{{--                                    <button--}}
{{--                                        wire:click="selectTag('{{$result['id']}}')"--}}
{{--                                        x-on:click="isOpen = false"--}}
{{--                                        type="button"--}}
{{--                                        class="hover:bg-gray-400 w-full px-3 py-3 flex items-center transition ease-in-out duration-150"--}}
{{--                                        @if ($loop->last) --}}
{{--                                            @keydown.tab="isOpen = false"--}}
{{--                                        @endif--}}
{{--                                    >--}}
{{--                                        <svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">--}}
{{--                                            <path fill-rule="evenodd" d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />--}}
{{--                                        </svg>--}}
{{--                                        <span class="ml-4">{{ $result['name'] }}</span>--}}
{{--                                    </button>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--        --}}
{{--                        </ul>--}}
{{--                    @else--}}
{{--                        <div class="px-3 py-3">No results for "{{ $newTag }}"</div>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--            @endif--}}
{{--        --}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<style>--}}

{{--.tags --}}
{{--{--}}
{{--    border-radius: 6px;--}}
{{--    background-color: #f6f6f6;--}}
{{--}--}}

{{--.tag--}}
{{--{--}}
{{--    margin: 0 10px 0 0;--}}
{{--    padding: 6px 7px 8px 12px !important;--}}
{{--    border-radius: 18px;--}}
{{--    border: solid 1px #dddddd;--}}
{{--    background-color: #eeeeee;--}}
{{--    font-size: 14px;--}}
{{--    font-weight: 500;--}}
{{--}--}}

{{--.tag .text--}}
{{--{--}}
{{--    font-size: 14px;--}}
{{--    font-weight: 500;--}}
{{--    font-stretch: normal;--}}
{{--    font-style: normal;--}}
{{--    line-height: normal;--}}
{{--    letter-spacing: normal;--}}
{{--    color: #333333;--}}
{{--    position: relative;--}}
{{--    bottom: 1px;--}}
{{--}--}}

{{--.tag .close--}}
{{--{--}}
{{--    width: 14px;--}}
{{--    height: 14px;--}}
{{--    margin: 1px 0 2px 5px;--}}
{{--    object-fit: contain;--}}
{{--}--}}

{{--.tags input--}}
{{--{--}}
{{--    height: 35px;--}}
{{--    margin-top: 4px;--}}
{{--}--}}

{{--.btnAddTag--}}
{{--{--}}
{{--    padding: 2px 18px 6px 15px !important;--}}
{{--    border-radius: 18px;--}}
{{--    background-color: #e83d61 !important;--}}
{{--    color: #ffffff;--}}
{{--    height: 34px;--}}
{{--    font-size: 14px;--}}
{{--}--}}
{{--</style>--}}
