<x-modal {{ $attributes }}>
    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
        <div class="sm:flex sm:items-start">
            <div class="m-3 text-center w-full">
                <div class="mt-2">
                    <p class="text-sm text-gray-500">{!! $title !!}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="px-6 py-4 bg-gray-100 text-center">
        {{ $footer }}
    </div>
</x-modal>