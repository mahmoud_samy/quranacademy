@props(['active'])

@php
$classes = ($active ?? false)
            ? 'flex items-center px-2 py-2 text-base font-medium leading-6 transition duration-150 ease-in-out tab tabActive group focus:outline-none'
            : 'flex items-center px-2 py-2 text-base font-medium leading-6 transition duration-150 ease-in-out tab group focus:outline-none';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
