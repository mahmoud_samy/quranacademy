@props(['active'])

@php
$classes = ($active ?? false)
            ? 'flex items-center px-2 py-2 text-sm font-medium leading-5 transition duration-150 ease-in-out tab bg-gray-200 group focus:outline-none'
            : 'flex items-center px-2 py-2 text-sm leading-5 transition duration-150 ease-in-out tab group focus:outline-none hover:bg-gray-50 hover:shadow-md';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
