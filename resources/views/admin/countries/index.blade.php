<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Countries')}}
        </h2>
    </x-slot>

    <livewire:admin.countries.index />
</x-admin-layout>
