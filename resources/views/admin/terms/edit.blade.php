<x-admin-layout>

    <x-slot name="title">{{__('Edit Term And Condition')}}: {{$term->header}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.terms.update', $term->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.terms.field', [
            'term' => $term ?? null
        ])

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Term And Condition') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
