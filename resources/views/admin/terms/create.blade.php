<x-admin-layout>

    <x-slot name="title">{{__('Create Term And Condition')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.terms.store') }}" enctype="multipart/form-data">
        @csrf

        @include('admin.terms.field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Term And Condition') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
