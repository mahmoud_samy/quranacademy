<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Terms And Conditions')}}
        </h2>
    </x-slot>

    <livewire:admin.terms.index />
</x-admin-layout>
