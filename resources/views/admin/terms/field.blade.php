<div>
    <x-jet-label for="header" value="{{ __('Header') }}" />
    <x-input.textarea
        id="header"
        name="header"
        value='{!! old("header", $term->header ?? null) !!}'
        >
    </x-input.textarea>
    <x-jet-input-error for="header" class="mt-2" />
</div>

<div>
    <x-jet-label for="body" value="{{ __('Body') }}" />
    <x-input.textarea
        id="body"
        name="body"
        value='{!! old("body", $term->body ?? null) !!}'
        >
    </x-input.textarea>
    <x-jet-input-error for="body" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="lang" value="{{ __('Language') }}" />
    <x-input.select id="lang" name="lang">
        <option value="en" {{ $term != null && $term->lang == 'en' ? 'selected' : '' }}>{{__('English')}}</option>
        <option value="ar" {{ $term != null && $term->lang == 'ar' ? 'selected' : '' }}>{{__('Arabic')}}</option>
    </x-input.select>
    <x-jet-input-error for="lang" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_active" value="Active Status" />
    <div class="flex rounded-md">
        <input id="is_active"
            name="is_active"
            value="on"
            {{ old('is_active') == 'on' ? 'checked' : '' }}
            {{ ($term && $term->is_active == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_active" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_visible" value="Visible Status" />
    <div class="flex rounded-md">
        <input id="is_visible"
            name="is_visible"
            value="on"
            {{ old('is_visible') == 'on' ? 'checked' : '' }}
            {{ ($term && $term->is_visible == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_visible" class="mt-2" />
</div>
