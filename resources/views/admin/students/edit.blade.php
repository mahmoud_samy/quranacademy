<x-admin-layout>

    <x-slot name="title">{{__('Edit Student')}}: {{$student->name}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.students.update', $student->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.students.field', [
            'student' => $student ?? null,
            'countries' => $countries ?? null
        ])
        
        @include('admin.students.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Student') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
