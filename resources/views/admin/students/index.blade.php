<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Students')}}
        </h2>
    </x-slot>

    <livewire:admin.students.index />
</x-admin-layout>
