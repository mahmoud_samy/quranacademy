<div>
    <x-image-upload name="image" label="Image" :src="$student->profile_photo_url ?? null" :max-width="48" />
    <p class="text-sm text-gray-400 mt-1">{{ __('Maximum file size: 1MB') }}</p>
</div>

<div>
    <x-jet-label for="first_name" value="{{ __('First Name') }}" />
    <x-jet-input id="first_name" class="block w-full mt-1" type="text" name="first_name" :value='old("first_name", $student->first_name ?? null)' required autofocus autocomplete="first_name" />
    <x-jet-input-error for="first_name" class="mt-2" />
</div>

<div>
    <x-jet-label for="last_name" value="{{ __('Last Name') }}" />
    <x-jet-input id="last_name" class="block w-full mt-1" type="text" name="last_name" :value='old("last_name", $student->last_name ?? null)' required autofocus autocomplete="last_name" />
    <x-jet-input-error for="last_name" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="email" value="{{ __('Email') }}" />
    <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" :value='old("email", $student->email ?? null)' required />
    <x-jet-input-error for="email" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="gender" value="{{ __('Gender') }}" />
    <x-input.select id="gender" name="gender">
        <option value="0" {{ $student != null && $student->gender == 0 ? 'selected' : '' }}>{{__('Female')}}</option>
        <option value="1" {{ $student != null && $student->gender == 1 ? 'selected' : '' }}>{{__('Male')}}</option>
    </x-input.select>
    <x-jet-input-error for="gender" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="country_id" value="{{ __('country') }}" />
    <x-input.select id="country_id" name="country_id">
        @foreach ($countries as $country)
            <option value="{{$country->id}}" {{ $student && $student->country_id == $country->id ? 'selected' : '' }}>{{$country->locale_name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="country_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="date_of_birth" value="{{ __('Date Of Birth') }}" />
    <x-input.date id="date_of_birth" name="date_of_birth" :value='old("date_of_birth", $student->date_of_birth ?? null)'></x-input.date>
    <x-jet-input-error for="date_of_birth" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_active" value="Active Status" />
    <div class="flex rounded-md">
        <input id="is_active"
            name="is_active"
            value="on"
            {{ old('is_active') == 'on' ? 'checked' : '' }}
            {{ ($student && $student->is_active == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_active" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_blocked" value="Blocked Status" />
    <div class="flex rounded-md">
        <input id="is_blocked"
            name="is_blocked"
            value="on"
            {{ old('is_blocked') == 'on' ? 'checked' : '' }}
            {{ ($student && $student->is_blocked == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_blocked" class="mt-2" />
</div>