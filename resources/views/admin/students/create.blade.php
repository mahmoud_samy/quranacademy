<x-admin-layout>

    <x-slot name="title">{{__('Create Student')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.students.store') }}" enctype="multipart/form-data">
        @csrf

        @include('admin.students.field', [
            'student' => $student ?? null,
            'countries' => $countries ?? null
        ])
        
        @include('admin.students.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Student') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
