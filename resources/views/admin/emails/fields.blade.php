<div class="mt-8">
    <x-jet-label for="chanel_type" value="{{ __('Chanel Type') }}"/>
    <select id="chanel_type" name="chanel_type" style="width: 100%">
        <option value="all">All</option>
        <option value="email">Email</option>
        <option value="notification">Notification</option>
    </select>
    <x-jet-input-error for="users" class="mt-2"/>
</div>

<div class="mt-8">
    <x-jet-label for="name" value="{{ __('Users') }}"/>
    <select id="students-id" name="users[]" multiple style="width: 100%">
        <option value="all">All</option>
        @foreach($users as $user)
            <option value="{{$user->email}}">{{$user->email}}</option>
        @endforeach
    </select>
    <x-jet-input-error for="users" class="mt-2"/>
</div>

<div>
    <x-jet-label for="title" value="{{ __('Title') }}"/>
    <x-jet-input id="title" class="block w-full mt-1" type="text" name="title" required/>
    <x-jet-input-error for="title" class="mt-2"/>
</div>

<div class="mt-8">
    <x-jet-label for="body" value="{{ __('Body') }}"/>
    <textarea name="body" rows="7" style="width: 100%"></textarea>
    <x-jet-input-error for="body" class="mt-2"/>
</div>


@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endpush
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $("#students-id").select2({
            placeholder: "Select a Students",
            allowClear: true
        });
    </script>
@endpush
