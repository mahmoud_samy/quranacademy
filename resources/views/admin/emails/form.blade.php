<x-admin-layout>

    <x-slot name="title">{{__('Emails Notifications')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.settings.emails.store') }}" enctype="multipart/form-data">
        @csrf
        @include('admin.emails.fields', [
        'users' => $users ?? null
    ])

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Send Mail') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
