<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Sub Instructors')}}
        </h2>
    </x-slot>

    <livewire:admin.sub-instructors.index />
</x-admin-layout>
