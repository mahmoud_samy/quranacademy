<x-admin-layout>

    <x-slot name="title">{{__('Create Promocode')}}</x-slot>

    <livewire:admin.promocodes.form />

</x-admin-layout>
