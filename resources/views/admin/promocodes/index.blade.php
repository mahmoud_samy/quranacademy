<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Promocodes')}}
        </h2>
    </x-slot>

    <livewire:admin.promocodes.index />
</x-admin-layout>
