<x-admin-layout>

    <x-slot name="title">{{__('Edit Promocode')}}: {{$promocode->title}}</x-slot>

    <livewire:admin.promocodes.form :promocode="$promocode"/>

</x-admin-layout>
