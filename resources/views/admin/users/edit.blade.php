<x-admin-layout>

    <x-slot name="title">{{__('Edit Admin')}}: {{$user->name}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.users.update', $user->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.users.field', [
            'users' => $users ?? null,
        ])
        
        @include('admin.users.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update User') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
