<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Admins')}}
        </h2>
    </x-slot>

    <livewire:admin.users.index />
</x-admin-layout>
