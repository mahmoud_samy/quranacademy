<x-admin-layout>

    <x-slot name="title">{{__('Create Admin')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.users.store') }}" enctype="multipart/form-data">
        @csrf

        @include('admin.users.field', [
            'users' => $users ?? null,
        ])
        @include('admin.users.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create User') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
