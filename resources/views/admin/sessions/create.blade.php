<x-admin-layout>

    <x-slot name="title">{{__('Create Session')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.sessions.store') }}" enctype="multipart/form-data">
        @csrf


        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Session') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
