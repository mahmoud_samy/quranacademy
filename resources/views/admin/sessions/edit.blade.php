<x-admin-layout>

    <x-slot name="title">{{__('Edit Session')}}: #{{$session->id}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.sessions.update', $session->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        
        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Session') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
