<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Sessions')}}
        </h2>
    </x-slot>

    <livewire:admin.sessions.index />

    @push('styles')
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday-time@1.6.1/css/pikaday.css">
    @endpush
    
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/pikaday-time@1.6.1/pikaday.min.js"></script>
    @endpush
</x-admin-layout>
