<x-admin-layout>

    <x-slot name="title">{{__('Create Instructor Level')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.instructor-levels.store') }}" enctype="multipart/form-data">
        @csrf
        @include('admin.instractor_levels.fields', [
        'level' => $level ?? null
    ])

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Level') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
