<x-admin-layout>

    <x-slot name="title">{{__('Edit Level')}}: #{{$level->id}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.instructor-levels.update', $level->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.instractor_levels.fields', [
         'level' => $level ?? null
     ])



        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Level') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
