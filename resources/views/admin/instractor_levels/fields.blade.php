<div>
    <x-jet-label for="name" value="{{ __('Level Name') }}" />
    <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" :value='old("name", $level->name ?? null)' required autofocus autocomplete="name" />
    <x-jet-input-error for="name" class="mt-2" />
</div>
