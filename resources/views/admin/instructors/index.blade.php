<x-admin-layout>
    <x-slot name="title">
        <h2 class="text-xl">
            {{__('Instructors')}}
        </h2>
    </x-slot>

    <livewire:admin.instructors.index />
</x-admin-layout>
