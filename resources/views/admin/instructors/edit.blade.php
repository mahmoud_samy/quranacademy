<x-admin-layout>

    <x-slot name="title">{{__('Edit Instructor')}}: {{$instructor->name}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.instructors.update', $instructor->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.instructors.field', [
            'instructor' => $instructor ?? null,
            'countries' => $countries ?? null
        ])
        
        @include('admin.instructors.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Instructor') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
