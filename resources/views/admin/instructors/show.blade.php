<x-admin-layout>

    <x-slot name="title">{{__('Instructor')}}: #{{$instructor->id}}</x-slot>

    <div id="divProfile" class="container mx-auto px-4 grid grid-cols-4 gap-4 py-10 bg-gray-100">
        <div class="col-span-1">
            <div class="flex flex-col">
                <div class="bg-white pt-2 rounded-md shadow">
                    <div class="flex flex-col">
                        <div class="block">
                            <div class="flex justify-center relative">
                                <div class="relative block">
                                    <img 
                                        class="inline-block h-24 w-24 rounded-full" 
                                        src="{{$instructor->profile_photo_url}}" 
                                        alt="profile image"/>
                                </div>
                                <div class="absolute px-4 bottom-0">
                                    <span class="inline-block px-2 py-1 leading-none bg-red-500 text-white rounded-full font-semibold uppercase tracking-wide text-xs">{{__('Instructor')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <div class="flex flex-col justify-center text-center pt-2 pb-4">
                                <div>
                                    <h4 class="mt-1 text-gray-900 font-semibold text-lg">{{$instructor->name}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="block bg-gray-100">
                            <div>
                                <div class="flex flex-col justify-center text-center space-y-2 py-4 border-t border-gray-200">
                                    <div>
                                        <span class="font-semibold text-gray-700">
                                            {{__('Contact Info')}}
                                        </span>
                                    </div>
                                    <div>
                                        <a href="mailto:{{$instructor->email}}" class="block text-blue-700">{{$instructor->email}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-span-3">
            <div class="container flex flex-col space-y-6 mx-2">
                <div>
                    <div class="flex flex-col">
                        @if ($instructor->description) 
                            <h3 class="text-gray-900 text-lg font-bold">{{__('Bio')}}</h3>
                            <p class="text-gray-600">
                                {{$instructor->description}}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="flex flex-col space-y-2">
                    <div class="flex justify-between items-center">
                        <h3 class="text-gray-900 text-lg font-bold">{{__('Sessions')}}</h3>
                    </div>
                    <livewire:admin.instructors.sessions.index :instructor="$instructor" />
                </div>
            </div>
        </div>
    </div>
    
</x-admin-layout>
