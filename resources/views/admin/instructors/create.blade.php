<x-admin-layout>

    <x-slot name="title">{{__('Create Instructor')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.instructors.store') }}" enctype="multipart/form-data">
        @csrf

        @include('admin.instructors.field', [
            'instructor' => $instructor ?? null,
            'countries' => $countries ?? null
        ])
        
        @include('admin.instructors.password-field')

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Instructor') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
