@if ($errors->any())
    <h4>Errors</h4>
    @foreach ($errors->all() as $error)
        <div class="alert alert-dismissible bg-light-danger d-flex flex-column flex-sm-row w-100 p-5 mb-10">
            <div class="d-flex flex-column pe-0 pe-sm-10">
                <span>{{$error}}</span>
            </div>

        </div>
    @endforeach
@endif
<div>
    <x-image-upload name="image" label="Image" :src="$instructor->profile_photo_url ?? null" :max-width="48" />
    <p class="text-sm text-gray-400 mt-1">{{ __('Maximum file size: 1MB') }}</p>
</div>

<div>
    <x-video-upload name="video" label="Video" :src="$instructor->profile_video_url ?? null" :max-width="640" :max-height="320" />
    <p class="text-sm text-gray-400 mt-1">{{ __('Maximum file size: 50MB') }}</p>
</div>

<div>
    <x-jet-label for="first_name" value="{{ __('First Name') }}" />
    <x-jet-input id="first_name" class="block w-full mt-1" type="text" name="first_name" :value='old("first_name", $instructor->first_name ?? null)' required autofocus autocomplete="given-name" />
    <x-jet-input-error for="first_name" class="mt-2" />
</div>

<div>
    <x-jet-label for="last_name" value="{{ __('Last Name') }}" />
    <x-jet-input id="last_name" class="block w-full mt-1" type="text" name="last_name" :value='old("last_name", $instructor->last_name ?? null)' required  autocomplete="family-name" />
    <x-jet-input-error for="last_name" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="email" value="{{ __('Email') }}" />
    <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" :value='old("email", $instructor->email ?? null)' required />
    <x-jet-input-error for="email" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="level" value="{{ __('Level') }}" />
    <x-input.select id="level" name="instructor_level_id">
        @foreach($levels as $level)
        <option value="{{$level->id}}" {{ $instructor != null && $instructor->instructor_level_id == $level->id ? 'selected' : '' }}>{{$level->name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="gender" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="gender" value="{{ __('Gender') }}" />
    <x-input.select id="gender" name="gender">
        <option value="0" {{ $instructor != null && $instructor->gender == 0 ? 'selected' : '' }}>{{__('Female')}}</option>
        <option value="1" {{ $instructor != null && $instructor->gender == 1 ? 'selected' : '' }}>{{__('Male')}}</option>
    </x-input.select>
    <x-jet-input-error for="gender" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="country_id" value="{{ __('country') }}" />
    <x-input.select id="country_id" name="country_id">
        @foreach ($countries as $country)
            <option value="{{$country->id}}" {{ $instructor && $instructor->country_id == $country->id ? 'selected' : '' }}>{{$country->locale_name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="country_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="date_of_birth" value="{{ __('Date Of Birth') }}" />
    <x-input.date id="date_of_birth" name="date_of_birth" :value='old("date_of_birth", $instructor->date_of_birth ?? null)'></x-input.date>
    <x-jet-input-error for="date_of_birth" class="mt-2" />
</div>

<div>
    <x-jet-label for="years_of_experience" value="{{ __('Years Of Experience') }}" />
    <x-jet-input id="years_of_experience" class="block w-full mt-1" type="number" name="years_of_experience" :value='old("years_of_experience", $instructor->years_of_experience ?? 0)' required autocomplete="years_of_experience" />
    <x-jet-input-error for="years_of_experience" class="mt-2" />
</div>

<div>
    <x-jet-label for="description" value="{{ __('Description') }}" />
    <x-input.textarea
        id="description"
        name="description"
        value='{!! old("description", $instructor->description ?? null) !!}'
        >
    </x-input.textarea>
    <x-jet-input-error for="description" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="courses_types_id" value="{{ __('Courses Type') }}" />
    <x-input.select id="courses_types_id" name="courses_types_id">
        @foreach ($course_types as $course_type)
            <option value="{{$course_type->id}}" {{ $instructor && $instructor->courses_types_id == $course_type->id ? 'selected' : '' }}>{{$course_type->name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="courses_types_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="student_gender_types_id" value="{{ __('Student Gender Types') }}" />
    <x-input.select id="student_gender_types_id" name="student_gender_types_id">
        @foreach ($student_gender_types as $student_gender_type)
            <option value="{{$student_gender_type->id}}" {{ $instructor && $instructor->student_gender_types_id == $student_gender_type->id ? 'selected' : '' }}>{{$student_gender_type->name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="student_gender_types_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="student_age_types_id" value="{{ __('Student Age Types') }}" />
    <x-input.select id="student_age_types_id" name="student_age_types_id">
        @foreach ($student_age_types as $student_age_type)
            <option value="{{$student_age_type->id}}" {{ $instructor && $instructor->student_age_types_id == $student_age_type->id ? 'selected' : '' }}>{{$student_age_type->name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="student_age_types_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="currency_type_id" value="{{ __('Currency Type') }}" />
    <x-input.select id="currency_type_id" name="currency_type_id">
        @foreach ($currency_types as $currency_type)
            <option value="{{$currency_type->id}}" {{ $instructor && $instructor->currency_type_id == $currency_type->id ? 'selected' : '' }}>{{$currency_type->code}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="currency_type_id" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="public_rate_minutes_type_id" value="{{ __('Public Rate Minutes Types') }}" />
    <x-input.select id="public_rate_minutes_type_id" name="public_rate_minutes_type_id">
        @foreach ($rate_minutes_types as $rate_minutes_type)
            <option value="{{$rate_minutes_type->id}}" {{ $instructor && $instructor->public_rate_minutes_type_id == $rate_minutes_type->id ? 'selected' : '' }}>{{$rate_minutes_type->name}}</option>
        @endforeach
    </x-input.select>
    <x-jet-input-error for="public_rate_minutes_type_id" class="mt-2" />
</div>


<div class="bg-white overflow-hidden shadow rounded-md sm:rounded-lg">
    <ul role="list" class="divide-y divide-gray-200">
        @foreach ($rate_minutes_types as $rate_minutes_type)
        <li>
            <div class="block hover:bg-gray-50">
                <div class="px-4 py-4 sm:px-6">
                    <div class="flex items-center justify-between">
                        <div class="text-sm font-medium text-indigo-600 truncate">
                        {{$rate_minutes_type->name}}
                        </div>
                    </div>
                    <div class="mt-2 flex justify-between">
                        <div class="mr-6 flex items-center text-sm text-gray-500">
                            <x-jet-input id="rate_minutes_types[{{$rate_minutes_type->attr}}]" class="block w-full mt-1" type="text" name="rate_minutes_types[{{$rate_minutes_type->attr}}]" :value='old($rate_minutes_type->attr, $instructor[$rate_minutes_type->attr] ?? null)' />
                            <x-jet-input-error for="rate_minutes_types[{{$rate_minutes_type->attr}}]" class="mt-2" />
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>


<div>
    <x-jet-label for="is_active" value="Active Status" />
    <div class="flex rounded-md">
        <input id="is_active"
            name="is_active"
            value="on"
            {{ old('is_active') == 'on' ? 'checked' : '' }}
            {{ ($instructor && $instructor->is_active == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_active" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_blocked" value="Blocked Status" />
    <div class="flex rounded-md">
        <input id="is_blocked"
            name="is_blocked"
            value="on"
            {{ old('is_blocked') == 'on' ? 'checked' : '' }}
            {{ ($instructor && $instructor->is_blocked == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_blocked" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_Approval" value="Approved" />
    <div class="flex rounded-md">
        <input id="is_Approval"
            name="is_Approval"
            value="on"
            {{ old('is_Approval') == 'on' ? 'checked' : '' }}
            {{ ($instructor && $instructor->is_Approval == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_Approval" class="mt-2" />
</div>

<div>
    <x-jet-label for="is_profile_public" value="Public Profile" />
    <div class="flex rounded-md">
        <input id="is_profile_public"
            name="is_profile_public"
            value="on"
            {{ old('is_profile_public') == 'on' ? 'checked' : '' }}
            {{ ($instructor && $instructor->is_profile_public == true) ? 'checked' : '' }}
            type="checkbox"
            class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
        />
    </div>
    <x-jet-input-error for="is_profile_public" class="mt-2" />
</div>
