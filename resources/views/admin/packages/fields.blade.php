<div>
    <x-jet-label for="name" value="{{ __('Package Name') }}" />
    <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" :value='old("name", $package->name ?? null)' required autofocus autocomplete="first_name" />
    <x-jet-input-error for="name" class="mt-2" />
</div>

<div class="mt-4">
    <x-jet-label for="name" value="{{ __('Visibility') }}" />
    <x-input.select id="visibility" name="visibility">
        <option value="1" {{ $package != null && $package->visibility == 1 ? 'selected' : '' }}>{{__('Visible')}}</option>
        <option value="0" {{ $package != null && $package->visibility == 0 ? 'selected' : '' }}>{{__('Invisible')}}</option>
    </x-input.select>
    <x-jet-input-error for="visiility" class="mt-2" />
</div>
