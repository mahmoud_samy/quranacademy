<x-admin-layout>

    <x-slot name="title">{{__('Edit Package')}}: #{{$package->id}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.packages.update', $package->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @include('admin.packages.fields', [
         'package' => $package ?? null
     ])



        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Update Package') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
