<x-admin-layout>

    <x-slot name="title">{{__('Create Package')}}</x-slot>

    <form class="mt-4 space-y-4" method="POST" action="{{ route('admin.packages.store') }}" enctype="multipart/form-data">
        @csrf
        @include('admin.packages.fields', [
        'package' => $package ?? null
    ])

        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Create Package') }}
            </x-jet-button>
        </div>
    </form>

</x-admin-layout>
