<x-admin-layout>

    <x-slot name="title">{{__('Create Plan')}}</x-slot>

    <livewire:admin.plans.form></livewire:admin.plans.form>

</x-admin-layout>
