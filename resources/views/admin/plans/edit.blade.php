<x-admin-layout>

    <x-slot name="title">{{__('Edit Plan')}}: #{{$plan->id}}</x-slot>

    <livewire:admin.plans.form :plan="$plan"></livewire:admin.plans.form>

</x-admin-layout>
