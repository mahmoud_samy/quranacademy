<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="./images/logo/logo.svg" />

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <!-- custom styles -->
        <link href="./styles/style.css" rel="stylesheet" />
        <title>Quran Academy</title>
    </head>

    <body class="bg-offwhite">
        <header class="d-inline-block">
            <div class="logo ps-5-custom pt-5">
            <a href="./index.html"><img src="./images/logo/logo-lg.svg" width="180px" /></a>
            </div>
        </header>
        <main class="container mb-5 pb-5">
            <div class="row align-items-center">
            <div class="col-md-6 text-center order-md-1 order-2">
                <img src="./images/Quran&Mobile.png" class="image-responsive" />
            </div>
            <div class="col-md-6 order-md-2 order-1 text-start-responsive">
                <div class="title mb-5-custom">
                    <h1 class="text-green fs-xl fw-bold text-uppercase f-proxiBold">QURAN ACADEMY</h1>
                    <h2 class="text-dark fw-normal fs-lg f-millird">Learning quran as easy as one click</h2>
                    </div>
                <div>
                    <p class="text-dark mb-0">Download the app and start learning now!</p>
                    <a href="http://onelink.to/quranacademyapp" target="#">
                        <button href="#" class="btn-outline-green text-center">
                            <img src="./images/App-Store-logo1.png" class="appstore"/>
                        </button>
                    </a>
                    <a href="http://onelink.to/quranacademyapp" target="#">
                        <button href="#" class="btn-outline-green text-center">
                            <img src="./images/Google-Play-logo1.png" class="googleplay" />
                        </button>
                    </a>
                </div>
            </div>
            </div>
        </main>
        <footer class="fixed-bottom bg-dark p-3 f-millird">
            <div class="row">
            <div class="col-md-6 text-start  text-start-responsive">
                <small class="text-white"> © Quran Academy. All Rights Reserved</small>
            </div>
            <div class="col-md-6   text-end-responsive">
                <a href="./privacy-policy.html" class="text-white pe-2"><small>Privacy Policy</small></a>
                <a href="./privacy-policy.html" class="text-white"><small>Terms & Conditions</small></a>
            </div>
            </div>
            <div class="row">
            <div class="col f-millird  text-start-responsive">
                <p class="text-white mb-0">For support contact us via :
                <a class="text-white" href="mailto:info@quran-academy.co">info@quran-academy.co </a></p>
            </div>
            </div>
        </footer>
        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
    </body>
</html>