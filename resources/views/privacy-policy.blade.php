<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="./images/logo/logo.svg" />
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <!-- custom styles -->
    <link href="./styles/style.css" rel="stylesheet"/>
    <title>Quraan Academy</title>
  </head>
  <body class="bg-offwhite">
    <header class="d-inline-block">
        <div class="logo ps-5-custom  pt-3">
            <a href="./index.html"><img src="./images/logo/logo-lg.svg" width="150px"/></a>
        </div>
    </header>
      <main class="container mb-5 pb-5">
        <div>
            <h2 class="text-center w-100">Privacy Policy</h2>
            <p>Quran Academy app is a free mobile application. This page has been created to inform
               visitors regarding our policies with the collection and use of personal information.</p>
        </div>
        <div class="mt-2">
          <small>This Privacy Policy provides the following information:</small>
          <h4>What Information does Quran Academy collect?</h4>
          <p>If you choose to use Quran Academy App, you consent to the collection and use of information with this Policy. We collect various personal information from our users, such as name, gender, email, and phone number. This information and data are used for educational purposes, to improve Quran Academy application,
             and to analyze how the user uses the application.</p>
            <p>Quran Academy is committed to ensuring that your privacy is protected, and we will not use or share your information with anyone except as described in this Privacy Policy.</p>
        </div>
        <div class="mt-2">
            <h4>Sharing your personal information</h4>
            <p>We value your trust in providing us with your personal information, thus we strive to use methods to protect it. We do not share your personal information but may share generic demographic information not linked to any personal data.</p>
        </div>
        <div class="mt-2">
          <h4>Children policy</h4>
          <p>Children may use the App under the supervision of their parents or legal guardians, and the App does not collect information from anyone under 10 years old.</p>
          <p>If we discover that a child under 13 years old has provided us with personal information, we will immediately delete it from our application. If you are a parent or guardian and know that your child has provided us with personal information, please contact us to take the necessary actions.</p>
        </div>
        <div class="mt-2">
          <h4>Data</h4>
          <p>Student data can be accessed for review and correction through personal information. </p>
          <p>We confirm that we do not share any student data, and we confirm that we don’t keep any student profiles.</p>
        </div>
        <div class="mt-2">
          <h4>Cookies</h4>
          <p>Cookies are files containing a small amount of data that are commonly used as anonymous unique identifiers.
             These are sent to your browser from the websites you visit and are stored on your device's internal memory.</p>
          <p>In Quran Academy app, we use cookies to improve user experience. You can accept or decline cookies, but if you choose to decline "cookies" you may not be able to use some features of the Application.</p>
        </div>
        <div class="mt-2">
          <h4>Changes to this Privacy Policy</h4>
          <p>When we update our Privacy Policy, we will notify you of any changes by posting the new Privacy Policy on this page. Check this page frequently for any changes to stay informed about the protection of the personal information we collect.</p>
        </div>
        <div class="mt-2">
          <h4>Your acceptance of these terms</h4>
          <p>By using this app, you agree to our policies. Your continued use of the Application following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
        </div>
        <div class="mb-5">
          <h2>Contact us</h2>
          <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at Hello@black-squares.com
          </p>
        </div>
      </main>
      <footer class="fixed-bottom bg-dark p-3 f-millird">
        <div class="row">
          <div class="col-md-6 text-start  text-start-responsive">
            <small class="text-white"> © Quran Academy. All Rights Reserved</small>
          </div>
          <div class="col-md-6   text-end-responsive">
            <a href="#" class="text-white pe-2"><small>Privacy Policy</small></a>
            <a href="./terms.html" class="text-white"><small>Terms & Conditions</small></a>
          </div>
        </div>
        <div class="row">
          <div class="col f-millird  text-start-responsive">
            <p class="text-white mb-0">For support contact us via :
              <a class="text-white" href="mailto:info@quran-academy.co">info@quran-academy.co </a></p>
          </div>
        </div>
      </footer>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

  </body>
</html>