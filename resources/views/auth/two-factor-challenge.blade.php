<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <svg xmlns="http://www.w3.org/2000/svg" width="130.448" height="50.93" viewBox="0 0 130.448 50.93"><defs><style>.a{fill:#e5d995;}.b{fill:none;stroke:#3c7a3c;stroke-miterlimit:10;stroke-width:2.37px;}.c{fill:#3c7a3c;}.d{fill:#333;font-size:16px;font-family:BaskOldFace, Baskerville Old Face;}</style></defs><g transform="translate(-124.711 -54)"><g transform="translate(124.711 54)"><g transform="translate(23.231 20.965)"><path class="a" d="M806.56,524.031l-6.74-3.891a.379.379,0,0,0-.568.328v7.782a.379.379,0,0,0,.568.328l6.74-3.891a.379.379,0,0,0,0-.656" transform="translate(-799.252 -520.088)"/><path class="b" d="M806.56,524.031l-6.74-3.891a.379.379,0,0,0-.568.328v7.782a.379.379,0,0,0,.568.328l6.74-3.891A.379.379,0,0,0,806.56,524.031Z" transform="translate(-799.252 -520.088)"/></g><path class="c" d="M737.369,458.307l7.608-7.608-6.874-6.874V432.138H726.416l-7.151-7.151-7.155,7.151H700.749v11.218l-6.874,6.874,6.874,6.878v11.707H712.46l7.1,7.1,7.306-7.131h8.39l-2.5-2.5,2.5-2.5H724.835l-5.227,5.108-5.1-5.108h-8.759v-8.755l-4.81-4.806,4.81-4.806v-8.289h8.439l5.078-5.082,5.082,5.082h8.759v8.759l4.806,4.806-4.079,4.072Z" transform="translate(-693.875 -424.988)"/><g transform="translate(43.88 38.806)"><rect class="c" width="3.533" height="3.533" transform="translate(0 2.498) rotate(-45)"/><rect class="c" width="3.533" height="3.533" transform="translate(4.999 2.498) rotate(-45)"/></g></g><g transform="translate(195.158 64.132)"><text class="d" transform="translate(0.22 10)"><tspan x="0" y="0">Quran</tspan></text><text class="d" transform="translate(0 27.833)"><tspan x="0" y="0">Academy</tspan></text></g></g></svg>
        </x-slot>

        <div x-data="{ recovery: false }">
            <div class="mb-4 text-sm text-gray-600" x-show="! recovery">
                {{ __('Please confirm access to your account by entering the authentication code provided by your authenticator application.') }}
            </div>

            <div class="mb-4 text-sm text-gray-600" x-show="recovery">
                {{ __('Please confirm access to your account by entering one of your emergency recovery codes.') }}
            </div>

            <x-jet-validation-errors class="mb-4" />

            <form method="POST" action="{{ route('two-factor.login') }}">
                @csrf

                <div class="mt-4" x-show="! recovery">
                    <x-jet-label for="code" value="{{ __('Code') }}" />
                    <x-jet-input id="code" class="block mt-1 w-full" type="text" inputmode="numeric" name="code" autofocus x-ref="code" autocomplete="one-time-code" />
                </div>

                <div class="mt-4" x-show="recovery">
                    <x-jet-label for="recovery_code" value="{{ __('Recovery Code') }}" />
                    <x-jet-input id="recovery_code" class="block mt-1 w-full" type="text" name="recovery_code" x-ref="recovery_code" autocomplete="one-time-code" />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button type="button" class="text-sm text-gray-600 hover:text-gray-900 underline cursor-pointer"
                                    x-show="! recovery"
                                    x-on:click="
                                        recovery = true;
                                        $nextTick(() => { $refs.recovery_code.focus() })
                                    ">
                        {{ __('Use a recovery code') }}
                    </button>

                    <button type="button" class="text-sm text-gray-600 hover:text-gray-900 underline cursor-pointer"
                                    x-show="recovery"
                                    x-on:click="
                                        recovery = false;
                                        $nextTick(() => { $refs.code.focus() })
                                    ">
                        {{ __('Use an authentication code') }}
                    </button>

                    <x-jet-button class="ml-4">
                        {{ __('Log in') }}
                    </x-jet-button>
                </div>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
