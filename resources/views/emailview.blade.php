<!DOCTYPE html>
<html>
	<head>
		<title>Welcome Email</title>
	</head>
	<body>
		<h2>Hello, {{ $email }}	</h2>
		<h3>Welcome to {{config('app.name')}}</h3>
		<br/>
		<div>Your email verification code is "{{ $pincode }} " Please use it to complete your registration.</div>
		<br/>
		<div>Thank You :)</div>
	</body>
</html>