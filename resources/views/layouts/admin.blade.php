<x-app-layout>
    <div class="flex" x-data="{ sidebarOpen: false }" @keydown.window.escape="sidebarOpen = false">
        <!-- Off-canvas menu for mobile -->
        <div x-show="sidebarOpen" class="md:hidden sidebar" style="display: none;">
            <div class="fixed inset-0 z-40 flex">
                <div x-on:click="sidebarOpen = false" x-show="sidebarOpen" x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state." x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" class="fixed inset-0" style="display: none;">
                    <div class="absolute inset-0 bg-gray-600 opacity-75"></div>
                </div>
                <div x-show="sidebarOpen" x-description="Off-canvas menu, show/hide based on off-canvas menu state." x-transition:enter="transition ease-in-out duration-300 transform" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" class="relative flex flex-col flex-1 w-full max-w-xs bg-white" style="display: none;">
                    <div class="absolute top-0 right-0 p-1 -mr-14">
                        <button x-show="sidebarOpen" x-on:click="sidebarOpen = false" class="flex items-center justify-center w-12 h-12 rounded-full focus:outline-none focus:bg-gray-600" aria-label="Close sidebar" style="display: none;">
                            <svg class="w-6 h-6 text-white" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                            </svg>
                        </button>
                    </div>
                    <div class="flex-1 h-0 pt-5 overflow-y-auto">
                        <div class="flex items-center flex-shrink-0 px-4">
                            <img class="w-32 h-auto" src="{{ get_logo() }}" alt="">
                        </div>

                            <nav class="mt-5">
                                <div class="sbHeading">{{ __('Quick Links') }}</div>
                                <x-sidebar.responsive-nav-link href="{{route('admin.dashboard')}}" :active="request()->routeIs('admin.dashboard')">
                                    <x-icon.dashboard class="tabIcon"/>
                                    <span class="tabText">{{ __('Dashboard') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <div class="sbHeading">{{ __('Users') }}</div>
                                <x-sidebar.responsive-nav-link href="{{route('admin.users.index')}}" :active="request()->routeIs('admin.users.index')">
                                    <x-icon.user-circle class="tabIcon"/>
                                    <span class="tabText">{{ __('Admins') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <x-sidebar.responsive-nav-link href="{{route('admin.students.index')}}" :active="request()->routeIs('admin.students.index')">
                                    <x-icon.two-users class="tabIcon"/>
                                    <span class="tabText">{{ __('Students') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <x-sidebar.responsive-nav-link href="{{route('admin.instructors.index')}}" :active="request()->routeIs('admin.instructors.index')">
                                    <x-icon.users class="tabIcon"/>
                                    <span class="tabText">{{ __('Instructors') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <x-sidebar.responsive-nav-link href="{{route('admin.sub-instructors.index')}}" :active="request()->routeIs('admin.sub-instructors.index')">
                                    <x-icon.users class="tabIcon"/>
                                    <span class="tabText">{{ __('Sub Instructors') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <div class="sbHeading">{{ __('Main') }}</div>
                                <x-sidebar.responsive-nav-link href="{{route('admin.sessions.index')}}" :active="request()->routeIs('admin.sessions.index')">
                                    <x-icon.users class="tabIcon"/>
                                    <span class="tabText">{{ __('Sessions') }}</span>
                                </x-sidebar.responsive-nav-link>

                                 <x-sidebar.responsive-nav-link href="{{route('admin.packages.index')}}" :active="request()->routeIs('admin.packages.index')">
                                    <x-icon.users class="tabIcon"/>
                                    <span class="tabText">{{ __('Packages') }}</span>
                                </x-sidebar.responsive-nav-link>

                                 <x-sidebar.responsive-nav-link href="{{route('admin.plans.index')}}" :active="request()->routeIs('admin.plans.index')">
                                    <x-icon.users class="tabIcon"/>
                                    <span class="tabText">{{ __('Plans') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <div class="sbHeading">{{ __('System') }}</div>
                                <x-sidebar.responsive-nav-link href="{{route('admin.settings.emails.create')}}" :active="request()->routeIs('admin.settings.emails.create')">
                                    <x-icon.cash class="tabIcon"/>
                                    <span class="tabText">{{ __('Emails') }}</span>
                                </x-sidebar.responsive-nav-link>
                                <x-sidebar.responsive-nav-link href="{{route('admin.promocodes.index')}}" :active="request()->routeIs('admin.promocodes.index')">
                                    <x-icon.cash class="tabIcon"/>
                                    <span class="tabText">{{ __('Promocodes') }}</span>
                                </x-sidebar.responsive-nav-link>
                                <x-sidebar.responsive-nav-link href="{{route('admin.countries.index')}}" :active="request()->routeIs('admin.countries.index')">
                                    <x-icon.global class="tabIcon"/>
                                    <span class="tabText">{{ __('Countries') }}</span>
                                </x-sidebar.responsive-nav-link>

                                <x-sidebar.responsive-nav-link href="{{route('admin.terms.index')}}" :active="request()->routeIs('admin.terms.index')">
                                    <x-icon.clipboard-list class="tabIcon"/>
                                    <span class="tabText">{{ __('Terms And Conditions') }}</span>
                                </x-sidebar.responsive-nav-link>
                            </nav>

                    </div>
                </div>
                <div class="flex-shrink-0 w-14">
                    <!-- Force sidebar to shrink to fit close icon -->
                </div>
            </div>
        </div>

        <!-- Static sidebar for desktop -->
        <div class="hidden md:flex md:flex-shrink-0 sidebar">
            <div class="flex flex-col w-64 border-r border-gray-200">
                <div class="flex flex-col flex-1 h-0 pb-4 overflow-y-auto">
                    <!-- Sidebar component, swap this element with another sidebar if you like -->

                    <nav class="flex-1 space-y-1 bg-white">
                        <div class="sbHeading">{{ __('Quick Links') }}</div>
                        <x-sidebar.nav-link href="{{route('admin.dashboard')}}" :active="request()->routeIs('admin.dashboard')">
                            <x-icon.dashboard class="tabIcon"/>
                            <span class="tabText">{{ __('Dashboard') }}</span>
                        </x-sidebar.nav-link>

                        <div class="sbHeading">{{ __('Users') }}</div>
                        <x-sidebar.nav-link href="{{route('admin.users.index')}}" :active="request()->routeIs('admin.users.index')">
                            <x-icon.user-circle class="tabIcon"/>
                            <span class="tabText">{{ __('Admins') }}</span>
                        </x-sidebar.nav-link>

                        <x-sidebar.nav-link href="{{route('admin.students.index')}}" :active="request()->routeIs('admin.students.index')">
                            <x-icon.two-users class="tabIcon"/>
                            <span class="tabText">{{ __('Students') }}</span>
                        </x-sidebar.nav-link>

                        <x-sidebar.nav-link href="{{route('admin.instructors.index')}}" :active="request()->routeIs('admin.instructors.index')">
                            <x-icon.users class="tabIcon"/>
                            <span class="tabText">{{ __('Instructors') }}</span>
                        </x-sidebar.nav-link>

                        <x-sidebar.nav-link href="{{route('admin.instructor-levels.index')}}" :active="request()->routeIs('admin.instructor-levels.index')">
                            <x-icon.users class="tabIcon"/>
                            <span class="tabText">{{ __('Instructor Levels') }}</span>
                        </x-sidebar.nav-link>

{{--                        <x-sidebar.nav-link href="{{route('admin.sub-instructors.index')}}" :active="request()->routeIs('admin.sub-instructors.index')">--}}
{{--                            <x-icon.users class="tabIcon"/>--}}
{{--                            <span class="tabText">{{ __('Sub Instructors') }}</span>--}}
{{--                        </x-sidebar.nav-link>--}}

                        <div class="sbHeading">{{ __('Main') }}</div>
                        <x-sidebar.nav-link href="{{route('admin.sessions.index')}}" :active="request()->routeIs('admin.sessions.index')">
                            <x-icon.book class="tabIcon"/>
                            <span class="tabText">{{ __('Sessions') }}</span>
                        </x-sidebar.nav-link>


                        <x-sidebar.responsive-nav-link href="{{route('admin.packages.index')}}" :active="request()->routeIs('admin.packages.index')">
                            <x-icon.users class="tabIcon"/>
                            <span class="tabText">{{ __('Packages') }}</span>
                        </x-sidebar.responsive-nav-link>

                        <x-sidebar.responsive-nav-link href="{{route('admin.plans.index')}}" :active="request()->routeIs('admin.plans.index')">
                            <x-icon.users class="tabIcon"/>
                            <span class="tabText">{{ __('Plans') }}</span>
                        </x-sidebar.responsive-nav-link>


                        <div class="sbHeading">{{ __('System') }}</div>

                        <x-sidebar.nav-link href="{{route('admin.settings.emails.create')}}" :active="request()->routeIs('admin.settings.emails.create')">
                            <x-icon.cash class="tabIcon"/>
                            <span class="tabText">{{ __('Emails') }}</span>
                        </x-sidebar.nav-link>
                        <x-sidebar.nav-link href="{{route('admin.promocodes.index')}}" :active="request()->routeIs('admin.promocodes.index')">
                            <x-icon.cash class="tabIcon"/>
                            <span class="tabText">{{ __('Promocodes') }}</span>
                        </x-sidebar.nav-link>
                        <x-sidebar.nav-link href="{{route('admin.countries.index')}}" :active="request()->routeIs('admin.countries.index')">
                            <x-icon.global class="tabIcon"/>
                            <span class="tabText">{{ __('Countries') }}</span>
                        </x-sidebar.nav-link>

                        <x-sidebar.nav-link href="{{route('admin.terms.index')}}" :active="request()->routeIs('admin.terms.index')">
                            <x-icon.clipboard-list class="tabIcon"/>
                            <span class="tabText">{{ __('Terms And Conditions') }}</span>
                        </x-sidebar.nav-link>
                    </nav>
                </div>
            </div>
        </div>

        <div class="flex flex-col flex-1 w-0">
            <div class="pt-1 pl-1 md:hidden sm:pl-3 sm:pt-3">
                <button x-on:click.stop="sidebarOpen = true" class="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:bg-gray-200 transition ease-in-out duration-150" aria-label="Open sidebar">
                    <svg class="w-6 h-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
                    </svg>
                </button>
            </div>

            <header class="innerPageHeader">
                <div class="max-w-full px-5 mx-auto mt-4">
                    {{ $title ?? '' }}
                </div>
            </header>

            <main class="relative z-0 flex-1 min-h-screen focus:outline-none" tabindex="0" x-data="" x-init="$el.focus()">
                <div class="pb-6 mx-auto max-w-7xl sm:px-6 lg:px-7">
                    {{ $slot }}
                </div>
            </main>
        </div>
    </div>
</x-app-layout>
