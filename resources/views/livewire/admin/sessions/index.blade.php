<div>
    <div class="py-5 space-y-5">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="flex w-2/4 space-x-4">
                <x-button.link wire:click="toggleShowFilters">
                    @if ($showFilters) {{ __('Hide') }} @endif {{ __('Advanced Search...') }}
                </x-button.link>
            </div>

            <div class="flex items-center space-x-2">
                <x-input.group borderless paddingless for="perPage" label="{{__('Per Page')}}">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <x-dropdown label="{{ __('Bulk Actions') }}">
                    <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">
                        <x-icon.trash class="text-cool-gray-400"/> <span>{{ __('Delete') }}</span>
                    </x-dropdown.item>
                </x-dropdown>

                {{--<livewire:admin.import-users />--}}

                {{--<x-link.primary href="{{ route('admin.sessions.create') }}" class="bg-green-600 hover:bg-green-500 active:bg-green-700 border-green-600"><x-icon.plus/> {{ __('New') }}</x-link.primary>--}}
            </div>
        </div>

        <!-- Advanced Search -->
        @if ($showFilters)
            <div>
                <div class="relative flex px-4 pt-4 pb-12 rounded-md shadow-inner bg-white">
                    <div class="w-1/2 pr-2 space-y-4">
                        <x-input.group inline for="filter-course_type" label="Courses Type">
                            <x-input.select wire:model="filters.course_type" id="filter-course_type">
                                <option value="">{{__('All')}}</option>
                                @foreach ($course_types as $course_type)
                                    <option value="{{ $course_type->id}}">{{ $course_type->name ?? '-' }}</option>
                                @endforeach
                            </x-input.select>
                        </x-input.group>

                        <x-input.group inline for="filter-currency_type" label="Currency Type">
                            <x-input.select wire:model="filters.currency_type" id="filter-currency_type">
                                <option value="">{{__('All')}}</option>
                                @foreach ($currency_types as $currency_type)
                                    <option value="{{ $currency_type->id}}">{{ $currency_type->code ?? '-' }}</option>
                                @endforeach
                            </x-input.select>
                        </x-input.group>

                        <x-input.group inline for="filter-start-date-min" label="Minimum Start Date">
                            <x-input.date id="filter-start-date-min" name="filter-start-date-min" wire:model="filters.start-date-min" placeholder="MM/DD/YYYY"/>
                        </x-input.group>

                        <x-input.group inline for="filter-start-date-max" label="Maximum Start Date">
                            <x-input.date id="filter-start-date-max" name="filter-start-date-max" wire:model="filters.start-date-max" id="filter-start-date-max" placeholder="MM/DD/YYYY" />
                        </x-input.group>
                    </div>

                    <div class="w-1/2 pl-2 space-y-4">
                        <x-input.group inline for="filter-rate_minutes_type" label="Rate Minutes Type">
                            <x-input.select wire:model="filters.rate_minutes_type" id="filter-rate_minutes_type">
                                <option value="">{{__('All')}}</option>
                                @foreach ($rate_minutes_types as $rate_minutes_type)
                                    <option value="{{ $rate_minutes_type->id}}">{{ $rate_minutes_type->name ?? '-' }}</option>
                                @endforeach
                            </x-input.select>
                        </x-input.group>

                        <x-input.group inline for="filter-status" label="Status">
                            <x-input.select wire:model="filters.status" id="filter-status">
                                <option value="">{{__('All')}}</option>
                                <option value="Canceled">{{_('Canceled')}}</option>
                            @foreach (App\Models\session::STATUSES as $status_key => $status_label)
                                    <option value="{{$status_key}}">{{$status_label}}</option>
                                @endforeach
                            </x-input.select>
                        </x-input.group>

                        <x-input.group inline for="filter-end-date-min" label="Minimum End Date">
                            <x-input.date id="filter-end-date-min" name="filter-end-date-min" wire:model="filters.end-date-min" id="filter-end-date-min" placeholder="MM/DD/YYYY" />
                        </x-input.group>

                        <x-input.group inline for="filter-end-date-max" label="Maximum End Date">
                            <x-input.date id="filter-end-date-max" name="filter-end-date-max" wire:model="filters.end-date-max" id="filter-end-date-max" placeholder="MM/DD/YYYY" />
                        </x-input.group>

                        <x-input.group inline for="filter-booked" label="booked">
                            <x-input.select wire:model="filters.booked" id="filter-booked">
                                    <option value="">{{__('All')}}</option>
                                    <option value=1>Yes</option>
                                    <option value=2>No</option>
                            </x-input.select>
                        </x-input.group>

                        <x-button.link wire:click="resetFilters" class="absolute bottom-0 right-0 p-4">{{ __('Reset Filters') }}</x-button.link>
                    </div>
                </div>
            </div>
        @endif

        <!-- Booths Table -->
        <div class="flex-col space-y-4">

            <x-table>
                <x-slot name="head">
                    <x-table.heading class="w-8 pr-0">
                        <x-input.checkbox wire:model="selectPage" />
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('instructor_id')" :direction="$sorts['instructor_id'] ?? null">{{ __('Instructor') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('student_id')" :direction="$sorts['student_id'] ?? null">{{ __('Student') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('course_type_id')" :direction="$sorts['course_type_id'] ?? null">{{ __('Type') }}</x-table.heading>
                    <x-table.heading>{{ __('Date Time') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('rate_minutes_type_id')" :direction="$sorts['rate_minutes_type_id'] ?? null">{{ __('Minutes') }}</x-table.heading>
                    <x-table.heading >{{ __('Status') }}</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-cool-gray-200" wire:key="row-message">
                            <x-table.cell colspan="7">
                                @unless ($selectAll)
                                    <div>
                                        <span>{{ __('You have selected') }} <strong>{{ $rows->count() }}</strong> {{ __(' sessions, do you want to select all') }} <strong>{{ $rows->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">{{ __('Select All') }}</x-button.link>
                                    </div>
                                @else
                                    <span>{{ __('You are currently selecting all') }} <strong>{{ $rows->total() }}</strong>{{ __(' sessions') }} .</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($rows as $row)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $row->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $row->id }}" />
                            </x-table.cell>

                            <x-table.cell>
                                <div class="flex items-center">
                                    @if ($instructor = $row->instructor)
                                        <div class="flex-shrink-0 h-10 w-10">
                                            <img class="h-10 w-10 rounded-full" src="{{$instructor->profile_photo_url}}" alt="profile image for {{$instructor->name}}">
                                        </div>
                                        <div class="ml-4">
                                            <div class="text-sm font-medium text-gray-900">
                                            {{ $instructor->name }}
                                            </div>
                                            <div class="text-sm text-gray-500">
                                            {{ $instructor->email }}
                                            </div>
                                        </div>
                                    @else
                                        <span>-</span>
                                    @endif
                                </div>
                            </x-table.cell>

                            <x-table.cell>
                                <div class="flex items-center">
                                    @if ($student = $row->student)
                                        <div class="flex-shrink-0 h-10 w-10">
                                            <img class="h-10 w-10 rounded-full" src="{{$student->profile_photo_url}}" alt="profile image for {{$student->name}}">
                                        </div>
                                        <div class="ml-4">
                                            <div class="text-sm font-medium text-gray-900">
                                                {{ $student->name }}
                                            </div>
                                            <div class="text-sm text-gray-500">
                                                {{ $student->email }}
                                            </div>
                                        </div>
                                    @else
                                        <span>-</span>
                                    @endif
                                </div>
                            </x-table.cell>

                            <x-table.cell class="text-sm text-gray-500">
                                <span>{{$row->course_type->name ?? '-'}}</span>
                            </x-table.cell>
                            <x-table.cell class="text-sm text-gray-500">
                                <div>
                                    <div class="flex justify-between items-center">
                                        <span>{{__('From')}}</span>
                                        <span>
                                            {{ $row->start_at }}
                                        </span>
                                    </div>
                                    <div class="flex justify-between items-center">
                                        <span>{{__('To')}}</span>
                                        <span>
                                            {{ $row->end_at }}
                                        </span>
                                    </div>
                                </div>
                            </x-table.cell>
                            <x-table.cell class="text-sm text-gray-500">
                                <span>{{$row->rate_minutes_type->name ?? '-'}}</span>
                            </x-table.cell>

                            <x-table.cell class="text-sm text-gray-500">
                                <span>

                                    <?php
                                    $start_at = $row->start_at;
                                    $start_at = $row->end_at;
                                    if ($start_at && $start_at) {
                                   if($row->cancel_at){ echo 'Canceled';}
                                   elseif(($start_at  >  now())  &&  ( $start_at > now() )   ){echo 'FUTURE';}
                                   elseif(($start_at  <=  now())  &&  ( $start_at >= now() )   ){echo 'PROGRESS';}
                                   elseif(($start_at  <  now())  &&  ( $start_at < now() )   ){echo 'PAST';}
                                    }
                                    ?>

                                </span>
                            </x-table.cell>

                            <x-table.cell>
                                {{--<x-link.secondary href="{{route('admin.sessions.edit',$row->id)}}" >{{ __('Edit') }}</x-link.secondary>--}}
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="7">
                                <div class="flex items-center justify-center space-x-2">
                                    <x-icon.no-record class="w-8 h-8 text-cool-gray-400" />
                                    <span class="py-8 text-xl font-medium text-cool-gray-400">{{ __('No record found') }}</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $rows->links() }}
            </div>
        </div>
    </div>

    <!-- Delete Sessions Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">
                {{ __('Delete Session') }}
            </x-slot>

            <x-slot name="content">
                {{ __('Are you sure you want to continue? This action is irreversible') }}
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">{{ __('Cancel') }}</x-button.secondary>

                <x-button.red type="submit">{{ __('Delete') }}</x-button.red>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
