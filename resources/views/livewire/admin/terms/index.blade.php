<div>
    <div class="py-5 space-y-5">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="flex w-2/4 space-x-4">
                <div class="col-span-3 sm:col-span-2">
                    <div class="mt-1 flex rounded-md shadow-sm">
                      <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                        </svg>
                      </span>
                      <input type="text" wire:model.debounce.500ms="filters.search" name="search" id="search" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="Search Term...">
                    </div>
                </div>
            </div>

            <div class="flex items-center space-x-2">
                <x-input.group borderless paddingless for="perPage" label="{{__('Per Page')}}">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <x-dropdown label="{{ __('Bulk Actions') }}">
                    <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">
                        <x-icon.trash class="text-cool-gray-400"/> <span>{{ __('Delete') }}</span>
                    </x-dropdown.item>
                </x-dropdown>

                {{--<livewire:admin.import-users />--}}

                <x-link.primary href="{{ route('admin.terms.create') }}" class="bg-green-600 hover:bg-green-500 active:bg-green-700 border-green-600"><x-icon.plus/> {{ __('New') }}</x-link.primary>
            </div>
        </div>

        <!-- Booths Table -->
        <div class="flex-col space-y-4">

            <x-table>
                <x-slot name="head">
                    <x-table.heading class="w-8 pr-0">
                        <x-input.checkbox wire:model="selectPage" />
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('header')" :direction="$sorts['header'] ?? null">{{ __('Header') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('lang')" :direction="$sorts['lang'] ?? null">{{ __('Language') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('is_active')" :direction="$sorts['is_active'] ?? null">{{ __('Active Status') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('is_visible')" :direction="$sorts['is_visible'] ?? null">{{ __('Visible Status') }}</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-cool-gray-200" wire:key="row-message">
                            <x-table.cell colspan="7">
                                @unless ($selectAll)
                                    <div>
                                        <span>{{ __('You have selected') }} <strong>{{ $rows->count() }}</strong> {{ __(' terms, do you want to select all') }} <strong>{{ $rows->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">{{ __('Select All') }}</x-button.link>
                                    </div>
                                @else
                                    <span>{{ __('You are currently selecting all') }} <strong>{{ $rows->total() }}</strong>{{ __(' terms') }} .</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($rows as $row)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $row->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $row->id }}" />
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->header}}</span>
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->lang}}</span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-{{$row->active_status_color}}-100 text-{{$row->active_status_color}}-800">
                                    {{$row->active_status_label}}
                                </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-{{$row->active_visible_color}}-100 text-{{$row->active_visible_color}}-800">
                                    {{$row->active_visible_label}}
                                </span>
                            </x-table.cell>

                            <x-table.cell>
                                <x-link.secondary href="{{route('admin.terms.edit',$row->id)}}" >{{ __('Edit') }}</x-link.secondary>
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="7">
                                <div class="flex items-center justify-center space-x-2">
                                    <x-icon.no-record class="w-8 h-8 text-cool-gray-400" />
                                    <span class="py-8 text-xl font-medium text-cool-gray-400">{{ __('No record found') }}</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $rows->links() }}
            </div>
        </div>
    </div>

    <!-- Delete terms Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">
                {{ __('Delete Term') }}
            </x-slot>

            <x-slot name="content">
                {{ __('Are you sure you want to continue? This action is irreversible') }}
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">{{ __('Cancel') }}</x-button.secondary>

                <x-button.red type="submit">{{ __('Delete') }}</x-button.red>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
