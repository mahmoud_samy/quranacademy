<div>
    <form wire:submit.prevent="submit" class="mt-4 space-y-4" >


    <div class="mt-4">
            <x-jet-label for="package_id" value="{{ __('Package') }}"/>
            <x-input.select id="package_id" name="package_id" wire:model.defer="package_id">
                @foreach($packages as $package)
                    <option
                        value="{{$package->id}}" {{ $package_id == $package->id ? 'selected' : '' }}>{{$package->name}}</option>
                @endforeach
            </x-input.select>
            <x-jet-input-error for="package_id" class="mt-2"/>
        </div>
        <div class="mt-4">
            <x-jet-label for="name" value="{{ __('Visibility') }}"/>
            <x-input.select id="visibility" name="visibility" wire:model.defer="visibility">
                <option
                    value="1" {{ $visibility == 1 ? 'selected' : '' }}>{{__('Visible')}}</option>
                <option
                    value="0" {{ $visibility == 0 ? 'selected' : '' }}>{{__('Invisible')}}</option>
            </x-input.select>
            <x-jet-input-error for="visiility" class="mt-2"/>
        </div>

        <div class="mt-4">
            <x-jet-label for="instructor_level_id" value="{{ __('Instructor Level') }}"/>
            <x-input.select id="instructor_level_id" name="instructor_level_id" wire:model.defer="instructor_level_id">
                @foreach($levels as $level)
                    <option
                        value="{{$level->id}}" {{ $instructor_level_id == $level->id ? 'selected' : '' }}>{{$level->name}}</option>
                @endforeach
            </x-input.select>
            <x-jet-input-error for="instructor_level_id" class="mt-2"/>
        </div>


        <div>
            <x-jet-label for="name" value="{{ __('PlanName') }}"/>
            <x-jet-input id="name" class="block w-full mt-1" type="text" name="name"
                         wire:model="name"      required autofocus autocomplete="name"/>
            <x-jet-input-error for="name" class="mt-2" />
        </div>
        <div>
            <x-jet-label for="title" value="{{ __('Title') }}"/>
            <x-jet-input id="title" class="block w-full mt-1" type="text" name="title"
                         wire:model.defer="title"     required autocomplete="title"/>
            <x-jet-input-error for="name" class="mt-2" />
        </div>
        <div>
            <x-jet-label for="expire_date" value="{{ __('Expire After (Days)') }}"/>
            <x-jet-input id="expire_date" class="block w-full mt-1" type="number" name="expire_date"
                      wire:model.defer="expire_date"/>
            <x-jet-input-error for="expire_date" class="mt-2"/>
        </div>
        <div>
            <x-jet-label for="hours_limitation" value="{{ __('Hours Limitation') }}"/>
            <x-jet-input id="hours_limitation" class="block w-full mt-1" type="number" name="hours_limitation"
                         :value='old("hours_limitation", $hours_limitation ?? null)' required
                         autocomplete="hours_limitation" wire:model.defer="hours_limitation"/>
            <x-jet-input-error for="hours_limitation" class="mt-2"/>
        </div>
        <div>
            <x-jet-label for="price" value="{{ __('Price') }}"/>
            <x-jet-input id="price" class="block w-full mt-1" wire:model="price" type="text" name="price"
                         :value='old("price", $price ?? null)' required autocomplete="price"/>
            <x-jet-input-error for="price" class="mt-2"/>
        </div>
        <div>
            <x-jet-label for="discount" value="{{ __('Discount %') }}"/>
            <x-jet-input id="discount" class="block w-full mt-1" wire:model="discount" type="number"
                         name="discount" autocomplete="discount"/>
            <x-jet-input-error for="discount" class="mt-2"/>
        </div>
        <div>
            <x-jet-label for="net-price" value="{{ __('Net Price') }}"/>
            <x-jet-input id="net-price" class="block w-full mt-1" wire:model="net_price"
                         style="background:  #ccc" type="number" name="net_price"
                         :value='old("net_price", $net_price ?? null)' readonly autocomplete="net_price"/>
            <x-jet-input-error for="net-price" class="mt-2"/>
        </div>
        <div class="flex items-center justify-end mt-4">
            <x-jet-button class="ml-4 btnColorBlueberry">
                {{ __('Save') }}
            </x-jet-button>
        </div>
    </form>
</div>
