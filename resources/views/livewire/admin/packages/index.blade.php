<div>
    <div class="py-5 space-y-5">
        <div class="flex justify-between">
            <div class="flex w-2/4 space-x-4"></div>

            <div class="flex items-center space-x-2">
                <x-link.primary href="{{ route('admin.packages.create') }}" class="bg-green-600 hover:bg-green-500 active:bg-green-700 border-green-600"><x-icon.plus/>
                       {{ __('New') }}
                </x-link.primary>
            </div>
        </div>
        <div class="flex-col space-y-4">

            <x-table>
                <x-slot name="head">
                    <x-table.heading sortable multi-column wire:click="sortBy('name')" :direction="$sorts['name'] ?? null">{{ __('Name') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('visibility')" :direction="$sorts['visibility'] ?? null">{{ __('Visibility') }}</x-table.heading>
                    <x-table.heading sortable multi-column >{{ __('Action') }}</x-table.heading>
                </x-slot>

                <x-slot name="body">


                    @forelse ($rows as $row)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $row->id }}">
                            <x-table.cell><span>{{$row->name}}</span></x-table.cell>

                            <x-table.cell><span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full {{$row->visibility ? "bg-green-500" :"bg-red-500" }} text-white">
                                    {{$row->visibility ? 'Visible' : 'Invisible'}}
                                </span>
                            </x-table.cell>

                                <x-table.cell>
                                <x-link.secondary href="{{route('admin.packages.edit',$row->id)}}" >{{ __('Edit') }}</x-link.secondary>
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="7">
                                <div class="flex items-center justify-center space-x-2">
                                    <x-icon.no-record class="w-8 h-8 text-cool-gray-400" />
                                    <span class="py-8 text-xl font-medium text-cool-gray-400">{{ __('No record found') }}</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $rows->links() }}
            </div>
        </div>
    </div>
</div>
