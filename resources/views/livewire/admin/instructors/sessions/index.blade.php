<div class="flex flex-col space-y-4">
    <div class="w-full flex justify-end">
        <x-button.link wire:click="toggleShowFilters">
            @if ($showFilters) {{ __('Hide') }} @endif {{ __('Advanced Search...') }} 
        </x-button.link>
    </div>
    @if ($showFilters)
        <div>
            <div class="relative flex px-4 pt-4 pb-12 rounded-md shadow-inner bg-white">
                <div class="w-1/2 pr-2 space-y-4">

                    <x-input.group inline for="filter-course_type" label="Courses Type">
                        <x-input.select wire:model="filters.course_type" id="filter-course_type">
                            <option value="">{{__('All')}}</option>
                            @foreach ($course_types as $course_type)
                                <option value="{{ $course_type->id}}">{{ $course_type->name ?? '-' }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group inline for="filter-currency_type" label="Currency Type">
                        <x-input.select wire:model="filters.currency_type" id="filter-currency_type">
                            <option value="">{{__('All')}}</option>
                            @foreach ($currency_types as $currency_type)
                                <option value="{{ $currency_type->id}}">{{ $currency_type->code ?? '-' }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group inline for="filter-start-date-min" label="Minimum Start Date">
                        <x-input.date id="filter-start-date-min" name="filter-start-date-min" wire:model="filters.start-date-min" placeholder="MM/DD/YYYY"/>
                    </x-input.group>

                    <x-input.group inline for="filter-start-date-max" label="Maximum Start Date">
                        <x-input.date id="filter-start-date-max" name="filter-start-date-max" wire:model="filters.start-date-max" id="filter-start-date-max" placeholder="MM/DD/YYYY" />
                    </x-input.group>
                </div>

                <div class="w-1/2 pl-2 space-y-4">

                    <x-input.group inline for="filter-rate_minutes_type" label="Rate Minutes Type">
                        <x-input.select wire:model="filters.rate_minutes_type" id="filter-rate_minutes_type">
                            <option value="">{{__('All')}}</option>
                            @foreach ($rate_minutes_types as $rate_minutes_type)
                                <option value="{{ $rate_minutes_type->id}}">{{ $rate_minutes_type->name ?? '-' }}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group inline for="filter-status" label="Status">
                        <x-input.select wire:model="filters.status" id="filter-status">
                            <option value="">{{__('All')}}</option>
                            @foreach (App\Models\session::STATUSES as $status_key => $status_label)
                                <option value="{{$status_key}}">{{$status_label}}</option>
                            @endforeach
                        </x-input.select>
                    </x-input.group>

                    <x-input.group inline for="filter-end-date-min" label="Minimum End Date">
                        <x-input.date id="filter-end-date-min" name="filter-end-date-min" wire:model="filters.end-date-min" id="filter-end-date-min" placeholder="MM/DD/YYYY" />
                    </x-input.group>

                    <x-input.group inline for="filter-end-date-max" label="Maximum End Date">
                        <x-input.date id="filter-end-date-max" name="filter-end-date-max" wire:model="filters.end-date-max" id="filter-end-date-max" placeholder="MM/DD/YYYY" />
                    </x-input.group>
                    <x-button.link wire:click="resetFilters" class="absolute bottom-0 right-0 p-4">{{ __('Reset Filters') }}</x-button.link>
                </div>
            </div>
        </div>
    @endif
    @if ($rows->count())
        @foreach ($rows as $session)
        <div class="flex flex-col space-y-2 px-4 py-2 bg-white rounded-lg shadow-md ">
            <div class="flex justify-between items-center">
                <div class="flex space-x-2 items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-red-700" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    <span class="text-sm text-gray-700 font-medium">
                        {{$session->start_at->format('M d Y - H:i')}} | {{$session->end_at->format('M d Y - H:i')}}
                    </span>
                </div>
                <div>
                    <span class="text-xs font-normal">
                        {{__('Session duration')}}: <span class="font-semibold uppercase">{{$session->rate_minutes_type->name ?? '-'}}</span>
                    </span>
                </div>
            </div>
            <div class=" py-2">
                <div class="flex items-center">
                    @if ($session->student)
                        <div class="flex-shrink-0 h-10 w-10">
                            <img class="h-10 w-10 rounded-full" src="{{$session->student->profile_photo_url}}" alt="profile image for {{$session->student->name}}">
                        </div>
                        <div class="ml-4">
                            <div class="text-sm font-medium text-gray-900">
                                {{ $session->student->name }}
                            </div>
                            <div class="text-sm text-gray-500">
                                {{ $session->student->email }}
                            </div>
                        </div>
                    @else
                        <div class="w-1/3">
                            <div class="animate-pulse flex space-x-4">
                            <div class="rounded-full bg-blue-400 h-12 w-12"></div>
                            <div class="flex-1 space-y-4 py-1">
                                <div class="h-4 bg-blue-400 rounded w-3/4"></div>
                                <div class="space-y-2">
                                <div class="h-4 bg-blue-400 rounded"></div>
                                <div class="h-4 bg-blue-400 rounded w-5/6"></div>
                                </div>
                            </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="flex justify-between items-center">
                <div class="flex items-center space-x-1 px-4 py-1 leading-none bg-gray-100 text-gray-500 rounded-full font-semibold uppercase tracking-wide text-xs">
                    <div class="h-3 w-3 rounded-full bg-yellow-400"></div>
                    <span>{{$session->course_type->name ?? '-'}}</span>
                </div>
            </div>
        </div>
        @endforeach
        <div>
            {{ $rows->links() }}
        </div>
    @else
        <div class="flex items-center justify-center space-x-4 space-y-2 px-10 py-6 bg-white rounded-lg shadow-md">
            {{--<button
                wire:click="toggleMatchMakingModal"
                class="flex items-center space-x-1 px-4 py-3 leading-none bg-blue-100 text-gray-800 rounded-lg font-medium tracking-wide">
                <span class="uppercase">{{__('start adding sessions')}}</span>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-blue-700" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </button>--}}

            <div class="flex items-center space-x-1 px-4 py-3 leading-none bg-blue-100 text-gray-800 rounded-lg font-medium tracking-wide">
                <span class="uppercase">{{__('No Sessions')}}</span>
            </div>
        </div>
    @endif
</div>