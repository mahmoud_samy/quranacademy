<form class="mt-4 space-y-4" wire:submit.prevent="save">
    <div>
        <x-jet-label for="code" value="{{ __('Code') }}" />
        <div class="flex items-center space-x-4">
            <x-jet-input id="code" class="w-1/6 mt-1" type="text" name="code" wire:model="code" required autocomplete="code" />
            @if (!$promocode)
                <x-jet-button type="button" wire:click="GenerateCode">
                    {{ __('Generate Code') }}
                </x-jet-button>
            @endif
        </div>
        <x-jet-input-error for="code" class="mt-2" />
    </div>

    <div>
        <x-jet-label for="title" value="{{ __('Title') }}" />
        <x-jet-input id="title" class="block w-full mt-1" type="text" name="title" wire:model.defer="title" required autofocus/>
        <x-jet-input-error for="title" class="mt-2" />
    </div>

    <div>
        <x-jet-label for="description" value="{{ __('description') }}" />
        <x-input.textarea
            id="description"
            name="description"
            wire:model.defer="description"
            >
        </x-input.textarea>
        <x-jet-input-error for="description" class="mt-2" />
    </div>

    <div class="mt-4">
        <x-jet-label for="type" value="{{ __('Type') }}" />
        <x-input.select id="type" name="type" wire:model="type">
            <option value="">{{__('Select Type')}}</option>
            @foreach (App\Models\Promocode::TYPES as $value => $label)
                <option value="{{$value}}" {{ $promocode != null && $promocode->type == $value ? 'selected' : '' }}>{{$label}}</option>
            @endforeach
        </x-input.select>
        <x-jet-input-error for="type" class="mt-2" />
    </div>

    <div class="mt-4">
        <x-jet-label for="reward" value="{{ __('Reward') }}" />
        @if ($type == App\Models\Promocode::TYPE_PERCENTAGE)
            <x-input.money id="reward" type="number" name="reward" wire:model.defer="reward" code="{{null}}" symbol="%" min="0" max="100"></x-input.money>
        @else
            <x-input.money id="reward" type="number" name="reward" wire:model.defer="reward" step="any"></x-input.money>
        @endif
        <x-jet-input-error for="reward" class="mt-2" />
    </div>

    <div class="mt-4">
        <x-jet-label for="related_to" value="{{ __('Related To') }}" />
        <x-input.select id="related_to" name="related_to" wire:model="related_to">
            <option value="">{{__('Select Related To')}}</option>
            @foreach (App\Models\Promocode::RELATED_TO as $key => $value)
                <option value="{{$value['value']}}" {{ $promocode != null && $promocode->related_to == $value['value'] ? 'selected' : '' }}>{{$value['label']}}</option>
            @endforeach
        </x-input.select>
        <x-jet-input-error for="related_to" class="mt-2" />
    </div>

    <div>
        <x-jet-label for="expires_at" value="Expires At" />
        <x-input.date-time id="expires_at" name="expires_at" wire:model="expires_at" ></x-input.date-time>
        <x-jet-input-error for="expires_at" class="mt-2" />
    </div>

    <div>
        <x-jet-label for="is_disposable" value="Disposable Status" />
        <div class="flex rounded-md">
            <input 
                id="is_disposable"
                name="is_disposable"
                value="on"
                wire:model.defer="is_disposable"
                type="checkbox"
                class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
            />
        </div>
        <x-jet-input-error for="is_disposable" class="mt-2" />
    </div>

    <div class="flex items-center justify-end mt-4">
        <x-jet-button type="submit" class="ml-4 btnColorBlueberry">
            @if ($promocode)
                {{ __('Create Promocode') }}
            @else
                {{ __('Update Promocode') }}
            @endif
        </x-jet-button>
    </div>
</form>