<div>
    <div class="py-5 space-y-5">
        <!-- Top Bar -->
        <div class="flex justify-between">
            <div class="flex w-2/4 space-x-4">
                <div class="col-span-3 sm:col-span-2">
                    <div class="mt-1 flex rounded-md shadow-sm">
                      <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                        </svg>
                      </span>
                      <input type="text" wire:model.debounce.500ms="filters.search" name="search" id="search" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="Search Promocode...">
                    </div>
                </div>
            </div>

            <div class="flex items-center space-x-2">
                <x-input.group borderless paddingless for="perPage" label="{{__('Per Page')}}">
                    <x-input.select wire:model="perPage" id="perPage">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </x-input.select>
                </x-input.group>

                <x-dropdown label="{{ __('Bulk Actions') }}">
                    <x-dropdown.item type="button" wire:click="$toggle('showDeleteModal')" class="flex items-center space-x-2">
                        <x-icon.trash class="text-cool-gray-400"/> <span>{{ __('Delete') }}</span>
                    </x-dropdown.item>
                </x-dropdown>

                {{--<livewire:admin.import-users />--}}

                <x-link.primary href="{{ route('admin.promocodes.create') }}" class="bg-green-600 hover:bg-green-500 active:bg-green-700 border-green-600"><x-icon.plus/> {{ __('New') }}</x-link.primary>
            </div>
        </div>

        <!-- Booths Table -->
        <div class="flex-col space-y-4">

            <x-table>
                <x-slot name="head">
                    <x-table.heading class="w-8 pr-0">
                        <x-input.checkbox wire:model="selectPage" />
                    </x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('title')" :direction="$sorts['title'] ?? null">{{ __('Title') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('code')" :direction="$sorts['code'] ?? null">{{ __('Code') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('type')" :direction="$sorts['type'] ?? null">{{ __('Type') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('related_to')" :direction="$sorts['related_to'] ?? null">{{ __('Related To') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('reward')" :direction="$sorts['reward'] ?? null">{{ __('Reward') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('is_disposable')" :direction="$sorts['is_disposable'] ?? null">{{ __('Disposable') }}</x-table.heading>
                    <x-table.heading sortable multi-column wire:click="sortBy('expires_at')" :direction="$sorts['expires_at'] ?? null">{{ __('Expires At') }}</x-table.heading>
                    <x-table.heading />
                </x-slot>

                <x-slot name="body">
                    @if ($selectPage)
                        <x-table.row class="bg-cool-gray-200" wire:key="row-message">
                            <x-table.cell colspan="9">
                                @unless ($selectAll)
                                    <div>
                                        <span>{{ __('You have selected') }} <strong>{{ $rows->count() }}</strong> {{ __(' promocodes, do you want to select all') }} <strong>{{ $rows->total() }}</strong>?</span>
                                        <x-button.link wire:click="selectAll" class="ml-1 text-blue-600">{{ __('Select All') }}</x-button.link>
                                    </div>
                                @else
                                    <span>{{ __('You are currently selecting all') }} <strong>{{ $rows->total() }}</strong>{{ __(' promocodes') }} .</span>
                                @endif
                            </x-table.cell>
                        </x-table.row>
                    @endif

                    @forelse ($rows as $row)
                        <x-table.row wire:loading.class.delay="opacity-50" wire:key="row-{{ $row->id }}">
                            <x-table.cell class="pr-0">
                                <x-input.checkbox wire:model="selected" value="{{ $row->id }}" />
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->title}}</span>
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->code}}</span>
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->type_label}}</span>
                            </x-table.cell>

                            <x-table.cell>
                                @if ($row->related_to)
                                    <span>{{$row->related_to}}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </x-table.cell>

                            <x-table.cell>
                                <div class="flex space-x-1 items-center">
                                    <span>{{$row->reward}}</span>
                                    @if ($row->type == App\Models\Promocode::TYPE_PERCENTAGE)
                                        <span class="font-bold">%</span>
                                    @else
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8c-1.657 0-3 .895-3 2s1.343 2 3 2 3 .895 3 2-1.343 2-3 2m0-8c1.11 0 2.08.402 2.599 1M12 8V7m0 1v8m0 0v1m0-1c-1.11 0-2.08-.402-2.599-1M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>
                                    @endif
                                </div>
                            </x-table.cell>

                            <x-table.cell>
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-{{$row->disposable_status_color}}-100 text-{{$row->disposable_status_color}}-800">
                                    {{$row->disposable_status_label}}
                                </span>
                            </x-table.cell>

                            <x-table.cell>
                                <span>{{$row->expires_at}}</span>
                            </x-table.cell>
                            
                            <x-table.cell>
                                <x-link.secondary href="{{route('admin.promocodes.edit',$row->id)}}" >{{ __('Edit') }}</x-link.secondary>
                            </x-table.cell>
                        </x-table.row>
                    @empty
                        <x-table.row>
                            <x-table.cell colspan="9">
                                <div class="flex items-center justify-center space-x-2">
                                    <x-icon.no-record class="w-8 h-8 text-cool-gray-400" />
                                    <span class="py-8 text-xl font-medium text-cool-gray-400">{{ __('No record found') }}</span>
                                </div>
                            </x-table.cell>
                        </x-table.row>
                    @endforelse
                </x-slot>
            </x-table>

            <div>
                {{ $rows->links() }}
            </div>
        </div>
    </div>

    <!-- Delete promocodes Modal -->
    <form wire:submit.prevent="deleteSelected">
        <x-modal.confirmation wire:model.defer="showDeleteModal">
            <x-slot name="title">
                {{ __('Delete Promocode') }}
            </x-slot>

            <x-slot name="content">
                {{ __('Are you sure you want to continue? This action is irreversible') }}
            </x-slot>

            <x-slot name="footer">
                <x-button.secondary wire:click="$set('showDeleteModal', false)">{{ __('Cancel') }}</x-button.secondary>

                <x-button.red type="submit">{{ __('Delete') }}</x-button.red>
            </x-slot>
        </x-modal.confirmation>
    </form>
</div>
