<?php

$pinCode = [
    'pinCodeIsNotMatch'  => 'الرمز السري غير مطابق.',
    'pinCodeIsNotActive' => 'الرمز السري غير نشط.',
    'pinCodeisValid'     => 'الرقم السري صحيح.',
];

return [

    'globalLogin' => [
        'emailNotExist' => 'البريد الإلكتروني الذي أدخلته لا يتطابق مع أي حساب.',
        'invalidlogin'  => 'يتعذر عليك تسجيل الدخول.',
        'successfully'  => 'تم تسجيل الدخول بنجاح.',
    ],
    'globalLoginSocialite' => [
        'invalidlogin'  => 'يتعذر عليك تسجيل الدخول.',
        'emailNotExist' => 'البريد الإلكتروني الذي أدخلته لا يتطابق مع أي حساب.',
        'invalidlogin'  => 'يتعذر عليك تسجيل الدخول.',
        'successfully'  => 'تم تسجيل الدخول بنجاح.',
    ],
    'globalRefreshToken' => [
        'invalid'      => 'رمز التحديث غير صالح.',
        'successfully' => 'تم تسجيل الدخول بنجاح.',
    ],
    'globalCreateInstructor' => [
        'AuthNotExist'       => 'المصادقة غير موجودة.',
        'IsNotSubInstructor' => '.',
        'emailNotExist'      => 'البريد الإلكتروني لا يتطابق مع أي حساب.',
        'Successfully'       => 'تم التسجيل بنجاح.',
    ],
    'globalSendPinCode' => [
        'successfully'  => 'تم إرسال رمز التعريف الشخصي بنجاح.',
        'emailNotExist' => 'البريد الإلكتروني لا يتطابق مع أي حساب.',
        'emailisExist'  => 'هذا البريد الالكتروني موجود بالفعل.',
    ],

    'globalverifyPinCode' => $pinCode,

    'globalPasswordresets' => $pinCode + [
        'Successfully'  => 'إعادة تعيين كلمة المرور بنجاح.',
        'emailNotExist' => 'البريد الإلكتروني لا يتطابق مع أي حساب.',
    ],
    'globalRegisterInstructor' => [
        'emailNotExist'  => 'البريد الإلكتروني لا يتطابق مع أي حساب.',
        'emailisExist'   => 'هذا البريد الالكتروني موجود بالفعل.',
        'InvalidRequest' => 'الرمز غير مفعل.',
        'thisNameIsUse'  => 'هذا الاسم مستخدم',
        'Successfully'   => 'تم التسجيل بنجاح.',
    ],
    'globalRegister' => $pinCode + [
        'emailNotExist'  => 'البريد الإلكتروني لا يتطابق مع أي حساب.',
        'emailisExist'   => 'هذا البريد الالكتروني موجود بالفعل.',
        'InvalidRequest' => 'الرمز غير مفعل.',
        'thisNameIsUse'  => 'هذا الاسم مستخدم.',
        'Successfully'   => 'تم التسجيل بنجاح.',
    ],

];
