<?php

$Auth = [
    'unAuth'                   => 'غير موثق.',
    'NotExists'                => 'لا يوجد.',
    'rateMinutesTypeNotExists' => '.',
    'IsNotExists'              => 'غير موجود.',
    'NotYours'                 => 'ليس لك.',
    'lessThanTime'             => 'أقل من الوقت.',
    'NotInFuture'              => 'ليس في المستقبل.',
    'NotAvailable'             => 'غير متوفر.',
    'haveSessionAtThisTime'    => 'لديك جلسة بالفعل في هذا الوقت ، يرجى اختيار التاريخ المتاح.',
];

return [

    'sessionsCreate' => $Auth + [
        'Successfully' => 'تم إنشاء الجلسات بنجاح.',
    ],

    'sessionsEdit' => $Auth + [
        'Successfully'              => 'تم تعديل الجلسات بنجاح.',
        'sendToStudentToApprove'    => 'إرسال إخطار إلى الطالب للموافقة على هذا التعديل.',
        'sendToInstructorToApprove' => 'إرسال إخطار إلى المدرب للموافقة على هذا التعديل.',
    ],

    'sessionsCancel' => $Auth + [
        'Successfully' => 'جلسات الإلغاء بنجاح.',
        'lessThanTime' => 'عذرا لا يمكنك إلغاء هذه الجلسة، فقد بدأت بالفعل.',
    ],

    'sessionsDelete' => $Auth + [
        'Successfully' => 'تم حذف الجلسات بنجاح.',
    ],

    'sessionsBook' => $Auth + [
        'NotStudent'   => 'أنت لست طالب.',
        'Successfully' => 'تم حجز الجلسات بنجاح.',
    ],

];
