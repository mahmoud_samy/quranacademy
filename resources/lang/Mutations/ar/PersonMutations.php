<?php

return [

    'PersonChangePassword' => [
        'unAuth'              => 'غير موثق.',
        'oldPasswordNotMatch' => 'كلمة المرور القديمة غير متطابقة.',
        'newPasswordMatch'    => 'كلمة المرور الجديدة متطابقة مع كلمة المرور القديمة.',
        'oldPasswordrequired' => 'كلمة المرور القديمة مطلوبة.',
        'Successfully'        => 'تم تغيير الرقم السري بنجاح.',
    ],

    'PersonEdit' => [
        'oldPasswordNotMatch' => 'كلمة المرور القديمة غير متطابقة.',
        'newPasswordMatch'    => 'كلمة المرور الجديدة متطابقة مع كلمة المرور القديمة.',
        'successfully'        => 'تم التعديل بنجاح.',
    ],

];
