<?php
$Auth = [
    'unAuth'                   => 'Unauthenticated.',
    'NotExists'                => 'NotExists.',
    'rateMinutesTypeNotExists' => 'rateMinutesTypeNotExists.',
    'IsNotExists'              => 'IsNotExists.',
    'NotYours'                 => 'NotYours.',
    'lessThanTime'             => 'lessThanTime.',
    'NotInFuture'              => 'NotInFuture.',
    'NotAvailable'             => 'NotAvailable.',
    'haveSessionAtThisTime'    => 'You already have a session at this time, please choose available date.',
];

$messages = $Auth + [
    'successfully' => 'Successfully.',
    'usedSuccessfully' => 'Used The Promocode Successfully.',
    'notExists' => 'Promocode Not Exists.',
    'expired' => 'Promocode Is Expired.',
    'alreadyUsed' => 'Promocode Is Already Used.',
];

return [
    'getPromocode' => $messages,
    'verifyPromocode' => $messages,
];
