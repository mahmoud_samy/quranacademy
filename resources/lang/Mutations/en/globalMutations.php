<?php

$pinCode = [
    'pinCodeIsNotMatch'  => 'Pin Code Is Not Match.',
    'pinCodeIsNotActive' => 'Pin Code Is Not Active.',
    'pinCodeisValid'     => 'Pin Code Is Valid.',
];

return [

    'globalLogin' => [
        'emailNotExist' => 'The Email Address That You Have Entered Does Not Match Any Account.',
        'invalidlogin'  => 'Invalid Login.',
        'successfully'  => 'Login Successfully.',
    ],
    'globalLoginSocialite' => [
        'invalidlogin'  => 'Invalid Login.',
        'emailNotExist' => 'The Email Address That You Have Entered Does Not Match Any Account.',
        'invalidlogin'  => 'Invalid Login.',
        'successfully'  => 'Login Successfully.',
    ],
    'globalRefreshToken' => [
        'invalid'      => 'The Refresh Token Is Un Valid.',
        'successfully' => 'Login Successfully.',
    ],
    'globalCreateInstructor' => [
        'AuthNotExist'       => 'Auth doesn\'t Exist.',
        'IsNotSubInstructor' => 'Auth isn\'t SubInstructor.',
        'emailNotExist'      => 'The Email Address Not Match Any Account.',
        'Successfully'       => 'Register Successfully.',
    ],
    'globalSendPinCode' => [
        'successfully'  => 'Pin Code Sent Successfully.',
        'emailNotExist' => 'The Email Address Not Match Any Account.',
        'emailisExist'  => 'This Email Is Already Exists.',
    ],

    'globalverifyPinCode' => $pinCode,

    'globalPasswordresets' => $pinCode + [
        'Successfully'  => 'Password Resets Successfully.',
        'emailNotExist' => 'The Email Address Not Match Any Account.',
    ],
    'globalRegisterInstructor' => [
        'emailNotExist'  => 'The Email Address Not Match Any Account.',
        'emailisExist'   => 'This Email Is Already Exists.',
        'InvalidRequest' => 'Token Is Not Active.',
        'thisNameIsUse'  => 'This Name Is Used',
        'Successfully'   => 'Register Successfully.',
    ],
    'globalRegister' => $pinCode + [
        'emailNotExist'  => 'The Email Address Not Match Any Account.',
        'emailisExist'   => 'This Email Is Already Exists.',
        'InvalidRequest' => 'Token Is Not Active.',
        'thisNameIsUse'  => 'This Name Is Used.',
        'Successfully'   => 'Register Successfully.',
    ],

];
