<?php

$Auth = [
    'unAuth'                   => 'Unauthenticated.',
    'NotExists'                => 'NotExists.',
    'rateMinutesTypeNotExists' => 'rateMinutesTypeNotExists.',
    'IsNotExists'              => 'IsNotExists.',
    'NotYours'                 => 'NotYours.',
    'lessThanTime'             => 'lessThanTime.',
    'NotInFuture'              => 'NotInFuture.',
    'NotAvailable'             => 'NotAvailable.',
    'haveSessionAtThisTime'    => 'You already have a session at this time, please choose available date.',
];

return [

    'sessionsCreate' => $Auth + [
        'Successfully' => 'sessions Create Successfully.',
    ],

    'sessionsEdit' => $Auth + [
        'Successfully'              => 'sessions Edit Successfully.',
        'sendToStudentToApprove'    => 'send Notification To Student To Approv this edit.',
        'sendToInstructorToApprove' => 'send Notification To Instructor To Approv this edit.',
    ],

    'sessionsCancel' => $Auth + [
        'Successfully' => 'sessions Cancel Successfully.',
        'lessThanTime' => 'Sorry you can’t cancel, This session has already been started.',
    ],

    'sessionsDelete' => $Auth + [
        'Successfully' => 'sessions Delete Successfully.',
    ],

    'sessionsBook' => $Auth + [
        'NotStudent'   => 'your are Not Student.',
        'Successfully' => 'sessions Book Successfully.',
    ],

];
