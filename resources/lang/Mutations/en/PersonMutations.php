<?php

return [

    'PersonChangePassword' => [
        'unAuth'              => 'Unauthenticated.',
        'oldPasswordNotMatch' => 'Old Password Not Matched.',
        'newPasswordMatch'    => 'New Password Matched With Old Password.',
        'oldPasswordrequired' => 'old Password required.',
        'Successfully'        => 'Password Changed Successfully.',
    ],

    'PersonEdit' => [
        'oldPasswordNotMatch' => 'Old Password Not Matched.',
        'newPasswordMatch'    => 'New Password Matched With Old Password.',
        'successfully'        => 'Edit Changed Successfully.',
    ],

];
