<?php
$Auth = [
    'unAuth'                   => 'Unauthenticated.',
    'NotExists'                => 'NotExists.',
    'rateMinutesTypeNotExists' => 'rateMinutesTypeNotExists.',
    'IsNotExists'              => 'IsNotExists.',
    'NotYours'                 => 'NotYours.',
    'lessThanTime'             => 'lessThanTime.',
    'NotInFuture'              => 'NotInFuture.',
    'NotAvailable'             => 'NotAvailable.',
    'haveSessionAtThisTime'    => 'You already have a session at this time, please choose available date.',
];

$messages = $Auth + [
    'successfully' => 'Create payment intent successfully.',
];

return [
    'createPaymentIntent' => $messages,
];
