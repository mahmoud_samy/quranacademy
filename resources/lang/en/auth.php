<?php

return [

    /*
	|--------------------------------------------------------------------------
	| Authentication Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used during authentication for various
	| messages that we need to display to the user. You are free to modify
	| these language lines according to your application's requirements.
	|
	*/

    'failed'                          => 'These Credentials Do Not Match Our Records.',
    'throttle'                        => 'Too Many Login Attempts. Please Try Again In :Seconds Seconds.',
    'unAuth'                          => 'Unauthenticated',
    'login' => [
        'emailNotExist'                => 'The Email Address That You Have Entered Does Not Match Any Account.',
        'phoneNotExist'                => 'The Phone That You Have Entered Does Not Match Any Account.',
        'nameNotExist'                => 'The User Name That You Have Entered Does Not Match Any Account.',
        'invalidlogin'                => 'Invalid Login.',
        'invalidEmailCredentials'    => 'The Email Or Password Is Incorrect.',
        'invalidCredentials'        => 'The Username Or Password Is Incorrect.',
        'InvalidRequest'            => 'Token Is Not Active.',
        'successfully'                => 'Login Successfully.'
    ],
    'refreshToken' => [
        'invalid'                    => 'The Refresh Token Is Un Valid.',
        'successfully'                => 'Login Successfully.',
    ],
    'Logout' => [
        'successfully'                => 'Logout Successfully.',
    ],
    'updateProfile' => [
        'successfully'                => 'Update Profile Successfully.',
    ],
    'updatePassword' => [
        'confirmPassNotMatch'        => 'Confirm Password Not Match With New Password .',
        'oldNewMatch'                => 'Old Password Match With New Password.',
        'oldPasswordIsNotIsset'        => 'Old Password Is Not Exists.',
        'oldNotCorrect'                => 'Old Password Is Not Correct.',
        'successfully'                => 'Update Password Successfully.',
    ],
    'updateEmailOrPhone' => [
        'youCantUseYourEmail'        => 'you Cant Use Your Email .',
        'youCantThisEmail'            => 'you Cant Use This Email .',
        'youCantUseYourPhone'        => 'you Cant Use Your Phone .',
        'youCantThisPhone'            => 'you Cant Use This Phone .',
        'successfullyPhone'            => 'Update Phone Successfully.',
        'successfullyEmail'            => 'Update Email Successfully.',
    ],
    'pinCode' => [
        'pinCodeIsNotMatch'            => 'Pin Code Is Not Match.',
        'pinCodeIsNotActive'        => 'Pin Code Is Not Active.',
        'pinCodeisValid'            => 'Pin Code Is Valid.',
        'successfully'                => 'Pin Code Sent Successfully.',
        'emailNotExist'                => 'The Email Address Not Match Any Account.',
        'emailisExist'                => 'This Email Is Already Exists.',
    ],
    'passwordresets' => [
        'Successfully'  => 'Password Resets Successfully.',
        'emailNotExist' => 'The Email Address Not Match Any Account.',
    ],
    'register' => [
        'emailNotExist'                => 'The Email Address Not Match Any Account.',
        'emailisExist'                => 'This Email Is Already Exists.',
        'InvalidRequest'            => 'Token Is Not Active.',
        'thisNameIsUse'                => 'This Name Is Used',
        'Successfully'                => 'Register Successfully.'
    ],

];
