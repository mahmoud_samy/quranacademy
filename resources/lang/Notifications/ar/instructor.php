<?php

use App\Notifications\Instructor\createdButNotcompleted;
use App\Notifications\Instructor\sessionFiveMinutesBeforeRemaining;
use App\Notifications\Instructor\sessionOneHourBeforeRemaining;
use App\Notifications\Instructor\sessionsNewBook;
use App\Notifications\Instructor\sessionReplaced;
use App\Notifications\Instructor\sessionCancel;
use App\Notifications\Instructor\createNewMessage;
use App\Notifications\Instructor\sessionsAcceptEdit;
use App\Notifications\Instructor\interacting;

return [
    createdButNotcompleted::class => 'مرحبًا بك في أكاديمية القرآن ، سيتم مراجعة ملفك الشخصي في غضون 24-48 ساعة.',
    sessionFiveMinutesBeforeRemaining::class => 'تذكير ، لديك 5 دقائق متبقية لجلستك القادمة.',
    sessionOneHourBeforeRemaining::class => 'تذكير ، لديك ساعة واحدة متبقية لجلستك القادمة.',
    sessionsNewBook::class => 'تهانينا ، لقد تم إجراء حجز ، اضغط هنا للتحقق من ذلك.',
    sessionReplaced::class => 'تم إعادة جدولة جلستك مع <b>:student_name</b> لتكون في الوقت <b>:time</b>.',
    sessionCancel::class => 'تم إلغاء جلستك مع <b>:student_name</b> بناء على طلبه.',
    createNewMessage::class => 'لديك سؤال من <b>:student_name</b>.',
    sessionsAcceptEdit::class => 'تم إعادة جدولة جلستك مع <b>:student_name</b> لتكون في الوقت <b>:time</b>.',
    interacting::class => 'ابدأ في إنشاء الجلسات الآن.',
];
