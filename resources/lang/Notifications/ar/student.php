<?php

use App\Notifications\Student\create;
use App\Notifications\Student\sessionFiveMinutesBeforeRemaining;
use App\Notifications\Student\sessionOneHourBeforeRemaining;
use App\Notifications\Student\createNewMessage;
use App\Notifications\Student\sessionCancel;
use App\Notifications\Student\sessionsEditRequest;

return [
    create::class => 'أهلاً بكم في أكاديمية القرآن ، ابدأ بحجز جلسات القرآن الآن.',
    sessionFiveMinutesBeforeRemaining::class => 'تذكير ، لديك 5 دقائق متبقية لجلستك القادمة.',
    sessionOneHourBeforeRemaining::class => 'تذكير ، لديك ساعة واحدة متبقية لجلستك القادمة.',
    createNewMessage::class => 'لقد تلقيت رداً من <b>:Instructor_name</b>.',
    sessionCancel::class => 'تم إلغاء جلستك مع <b>:Instructor_name</b> .',
    sessionsEditRequest::class => 'المدرب <b>:Instructor_name</b> طلب منه إعادة جدولة الجلسة ، انقر هنا للرد.',
];
