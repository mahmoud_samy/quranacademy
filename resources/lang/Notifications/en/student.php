<?php

use App\Notifications\Student\create;
use App\Notifications\Student\sessionFiveMinutesBeforeRemaining;
use App\Notifications\Student\sessionOneHourBeforeRemaining;
use App\Notifications\Student\createNewMessage;
use App\Notifications\Student\sessionCancel;
use App\Notifications\Student\sessionsEditRequest;

return [
    create::class => 'Welcome to Quran Academy, Start reserving Quran Sessions now.',
    sessionFiveMinutesBeforeRemaining::class => 'Reminder, you have 5 mins remaining for your upcoming session.',
    sessionOneHourBeforeRemaining::class => 'Reminder, you have 1 hour remaining for your upcoming session.',
    createNewMessage::class => 'You received a reply from <b>:Instructor_name</b>.',
    sessionCancel::class => 'Your session with <b>:Instructor_name</b> has been cancelled.',
    sessionsEditRequest::class => 'Instructor <b>:Instructor_name</b> Asked to reschedule the session, click here to reply.',
];
