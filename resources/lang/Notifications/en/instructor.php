<?php

use App\Notifications\Instructor\createdButNotcompleted;
use App\Notifications\Instructor\sessionFiveMinutesBeforeRemaining;
use App\Notifications\Instructor\sessionOneHourBeforeRemaining;
use App\Notifications\Instructor\sessionsNewBook;
use App\Notifications\Instructor\sessionReplaced;
use App\Notifications\Instructor\sessionCancel;
use App\Notifications\Instructor\createNewMessage;
use App\Notifications\Instructor\sessionsAcceptEdit;
use App\Notifications\Instructor\interacting;

return [
    createdButNotcompleted::class => 'Welcome to Quran Academy, your profile will be reviewed within 24-48 hours.',
    sessionFiveMinutesBeforeRemaining::class => 'Reminder, you have 5 mins remaining for your upcoming session.',
    sessionOneHourBeforeRemaining::class => 'Reminder, you have 1 hour remaining for your upcoming session.',
    sessionsNewBook::class => 'Congratulations, a reservation has been made, click here to check it out.',
    sessionReplaced::class => 'Your Session with <b>:student_name</b> is rescheduled to be on the <b>:time</b>.',
    sessionCancel::class => 'Your session with <b>:student_name</b> is cancelled up on his request.',
    createNewMessage::class => 'You got new question from <b>:student_name</b>.',
    sessionsAcceptEdit::class => 'Your Session with <b>:student_name</b> is rescheduled to be on the <b>:time</b>.',
    interacting::class => 'Start creating sessions now.',
];
