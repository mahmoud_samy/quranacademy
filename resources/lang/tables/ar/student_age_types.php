<?php

return [

    'Adults Only'       => 'للكبار فقط',
    'Children Only'     => 'للأطفال فقط',
    'Adults & Children' => 'الكبار والأطفال',

];
