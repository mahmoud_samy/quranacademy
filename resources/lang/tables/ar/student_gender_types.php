<?php

return [

    'Female Only'   => 'الإناث فقط',
    'Male Only'     => 'الذكور فقط',
    'Male & Female' => 'ذكر وأنثى',

];
