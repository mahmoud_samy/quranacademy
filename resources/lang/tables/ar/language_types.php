<?php

return [
    'english' => 'الإنجليزية',
    'arabic'  => 'العربية',
    'french'  => 'الفرنسية',
    'spanish'  => 'الأسبانية',
    'deutsch'  => 'الألمانية'
];
