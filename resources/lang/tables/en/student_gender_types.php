<?php

return [

    'Female Only'   => 'Female Only',
    'Male Only'     => 'Male Only',
    'Male & Female' => 'Male and Female',

];
