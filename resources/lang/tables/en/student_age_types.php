<?php

return [

    'Adults Only'       => 'Adults Only',
    'Children Only'     => 'Children Only',
    'Adults & Children' => 'Adults and Children',

];
