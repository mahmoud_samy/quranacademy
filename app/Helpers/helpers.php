<?php

use App\Models\User;
use App\Models\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

if (!function_exists('get_logo')) {
    function get_logo()
    {
        return asset('images/logo/logo.svg');
    }
}

if (!function_exists('timezone_info')) {
    function timezone_info($key)
    {
        $info = [];
        $eventTimezone = domain_config('timezone') ?? '';
        if (isset($eventTimezone[0])) {

            $time = new \DateTime('now', new \DateTimeZone($eventTimezone[0]['name']));
            $offset = (int)$time->format('P') * 3600;

            $info = [
                "name" => $eventTimezone[0]['name'],
                "offset" => $offset
            ];
        } else {
            $info = [
                "name" => "Asia/Dubai",
                "offset" => "14400"
            ];
        }

        $info = $info[$key] ?? null;
        return $info;
    }
}

if (!function_exists('convert_datetime_to_utc')) {
    function convert_datetime_to_utc($datetime, $offset = 14400)
    {
        $utcDatetime = date("Y-m-d H:i:s", strtotime($datetime) - $offset);
        return $utcDatetime;
    }
}
