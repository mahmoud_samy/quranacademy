<?php

namespace App\Console\Commands;

use App\Models\instructor;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class ChangeSessionOldPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quran-academy:sessions:change-old-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change old price for instructor sessions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $instructors = instructor::has('sessions')->get();
        foreach ($instructors as $instructor) {
            $this->updateInstructorSessionsPrice($instructor);
        }

        return Command::SUCCESS;
    }

    public function updateInstructorSessionsPrice(instructor $instructor)
    {
        $instructor->sessions()
            ->whereNull('student_id')
            ->where('start_at', '>', now())
            ->whereHas('rate_minutes_type', function (Builder $query) {
                $query->where('attr', 'rate_15_min');
            })
            ->update([
                'rate_salary' => $instructor->rate_15_min
            ]);

        $instructor->sessions()
            ->whereNull('student_id')
            ->where('start_at', '>', now())
            ->whereHas('rate_minutes_type', function (Builder $query) {
                $query->where('attr', 'rate_30_min');
            })
            ->update([
                'rate_salary' => $instructor->rate_30_min
            ]);

        $instructor->sessions()
            ->whereNull('student_id')
            ->where('start_at', '>', now())
            ->whereHas('rate_minutes_type', function (Builder $query) {
                $query->where('attr', 'rate_60_min');
            })
            ->update([
                'rate_salary' => $instructor->rate_60_min
            ]);
    }
}
