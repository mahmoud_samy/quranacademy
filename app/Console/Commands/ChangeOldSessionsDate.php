<?php

namespace App\Console\Commands;

use App\Models\rateMinutesType;
use App\Models\session;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeOldSessionsDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quran-academy:sessions:add-start-end-datetime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add start_at and end_at to the sessions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $old_sessions = session::with('rate_minutes_type')->whereNull('start_at')->whereNull('end_at')->get();

        foreach ($old_sessions as $old_session) {

            $old_session->start_at = Carbon::parse($old_session->date->toDateString() . ' ' . $old_session->time->toTimeString());

            if ($session_rate_minute_type = $old_session->rate_minutes_type) {
                $old_session->end_at = Carbon::parse($old_session->date->toDateString() . ' ' . $old_session->time->toTimeString())->addMinutes($session_rate_minute_type->RateByMinutes);
            }

            $old_session->update();
        }

        return Command::SUCCESS;
    }
}
