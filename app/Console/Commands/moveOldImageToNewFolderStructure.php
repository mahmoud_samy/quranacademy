<?php

namespace App\Console\Commands;

use App\Models\student;
use App\Models\instructor;
use Illuminate\Support\Str;
use App\Models\SubInstructor;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class moveOldImageToNewFolderStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quran-academy:image:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move images from old folder structure to the new one.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->moveStudentImages();

        $this->moveInstructorImages();

        $this->moveSubInstructorImages();

        return Command::SUCCESS;
    }

    public function moveStudentImages()
    {
        $students = student::with('profile_image.rel_type')->whereNotNull('profile_image_id')->whereNull('profile_photo_path')->get();

        foreach ($students as $student) {
            $this->moveImage($student);
        }
    }

    public function moveInstructorImages()
    {
        $instructors = instructor::with('profile_image.rel_type')->whereNotNull('profile_image_id')->whereNull('profile_photo_path')->get();

        foreach ($instructors as $instructor) {
            $this->moveImage($instructor);
        }
    }

    public function moveSubInstructorImages()
    {
        $subInstructors = SubInstructor::with('profile_image.rel_type')->whereNotNull('profile_image_id')->whereNull('profile_photo_path')->get();

        foreach ($subInstructors as $subInstructor) {
            $this->moveImage($subInstructor);
        }
    }

    public function moveImage(Model $user)
    {
        if ($user->profile_image) {
            if (Storage::disk('images')->exists($user->profile_image->id)) {
                $old_image_path = 'public/images/' . $user->profile_image->id;
                $new_image_name = Str::random(10) . '.' . ($user->profile_image->rel_type->extension ?? 'jpg');
                $new_image_path = 'public/profile-photos/' . $new_image_name;

                Storage::move($old_image_path, $new_image_path);

                $user->forceFill([
                    'profile_photo_path' => 'profile-photos/' . $new_image_name,
                    'profile_image_id' => null
                ])->save();
            }
        }
    }
}
