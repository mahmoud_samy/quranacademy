<?php

namespace App\Console\Commands;

use App\Models\instructor;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class moveOldVideoToNewFolderStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quran-academy:video:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move videos from old folder structure to the new one.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->moveInstructorVideos();
        return Command::SUCCESS;
    }

    public function moveInstructorVideos()
    {
        $instructors = instructor::with('profile_vidoe')
            ->whereNotNull('profile_vidoe_id')
            ->whereNull('profile_video_path')->get();

        foreach ($instructors as $instructor) {
            $this->moveVideo($instructor);
        }
    }

    public function moveVideo(Model $user)
    {
        if ($user->profile_vidoe) {
            if (Storage::disk('Files')->exists($user->profile_vidoe->id)) {

                $old_video_path = 'public/Files/' . $user->profile_vidoe->id;
                $new_video_name = $user->profile_vidoe->name;
                $new_video_path = "public/profile-videos/$user->id/" . $new_video_name;

                Storage::move($old_video_path, $new_video_path);

                $user->forceFill([
                    'profile_video_path' => "profile-videos/$user->id/" . $new_video_name,
                    'profile_vidoe_id' => null
                ])->save();
            }
        }
    }
}
