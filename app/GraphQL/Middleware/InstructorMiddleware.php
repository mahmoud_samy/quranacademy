<?php

namespace App\GraphQL\Middleware;

use Illuminate\Support\Facades\Auth;

class InstructorMiddleware extends AuthMiddleware
{

    public function MiddlewareAuthInstructor()
    {
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        if (!Auth::user()->tokenCan('instructor')) return $this->updateResult('auth.NotInstructor', 502, false);
    }
}
