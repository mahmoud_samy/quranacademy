<?php

namespace App\GraphQL\Middleware;

use App\Models\student;
use App\Models\Pincode;
use App\Models\instructor;
use App\Models\SubInstructor;
use Illuminate\Support\Facades\Auth;
use harby\services\Services\Result;

class AuthMiddleware extends Result
{

    public function MiddlewareAuth()
    {
        if (!Auth::check()) return $this->updateResult('unAuth', 502, false);
    }

    public function emailisExist(string $email)
    {
        if (
            SubInstructor::byEmail($email)->exists() ||
            instructor::byEmail($email)->exists() ||
            student::byEmail($email)->exists()
        ) return $this->updateResult('emailisExist', 503);
    }

    public function emailNotExist(string $email)
    {
        if (!(SubInstructor::byEmail($email)->exists() ||
            instructor::byEmail($email)->exists() ||
            student::byEmail($email)->exists())) return $this->updateResult('emailNotExist', 404);
    }

    public function verifyPinCode(array $args): Result
    {

        $pincode = Pincode::firstOrCreate(['email' => $args['email']])->isValid($args['pin_code']);

        if (!$pincode->pinCodeIsMatch) $this->updateResult('pinCodeIsNotMatch', 502, $pincode->pinCodeisValid);
        if (!$pincode->pinCodeIsActive) $this->updateResult('pinCodeIsNotActive', 502, $pincode->pinCodeisValid);
        if ($pincode->pinCodeisValid) $this->updateResult('pinCodeisValid', 200, $pincode->pinCodeisValid);

        return $this;
    }
}
