<?php

namespace App\GraphQL\Middleware;

use Carbon\Carbon;
use App\Models\student;
use App\Models\session;
use App\Models\instructor;
use App\Models\variabledb;
use App\Models\rateMinutesType;
use Illuminate\Support\Facades\Auth;

class sessionsMiddleware extends PersonTypesMiddleware
{

    public ?session $session = null;

    public ?rateMinutesType $rate_minutes_type = null;

    public function RateByMinutes()
    {
        return $this->rate_minutes_type->RateByMinutes;
    }

    public function SetSession(?session $session): ?session
    {
        return $this->session = $session;
    }

    public function SetRateMinutesType(?rateMinutesType $rate_minutes_type): ?rateMinutesType
    {
        return $this->rate_minutes_type = $rate_minutes_type;
    }

    public function IfExistsSessions()
    {
        if (!$this::$check) return $this->makeResult([]);
        return $this->session ?? $this->updateResult('NotExists', 502, false);
    }

    public function IfExistsRateByMinutes()
    {
        if (!$this::$check) return $this->makeResult([]);
        return $this->rate_minutes_type ?? $this->updateResult('rateMinutesTypeNotExists', 502, false);
    }

    public function MiddlewareIfExistsSessions(Int $id)
    {
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        if (is_null($this->SetSession(session::find($id)))) return $this->updateResult('IsNotExists', 502, false);
        $this->SetRateMinutesType($this->session->rate_minutes_type);
    }

    public function MiddlewareAuthHasSessions(Int $id)
    {
        $this->MiddlewareIfExistsSessions($id);
        if (!$this::$check) return $this->makeResult([]);
        if (Auth::user()->tokenCan('instructor') && $this->session->instructor_id !== Auth::id()) return $this->updateResult('NotYours', 502, false);
        if (Auth::user()->tokenCan('student') && $this->session->student_id    !== Auth::id()) return $this->updateResult('NotYours', 502, false);
    }

    public function MiddlewareSessionsIfLessTime()
    {
        if (!$this::$check) return $this->makeResult([]);
        if ($this->session->ifStudentNotExists) return $this->makeResult([]);
        if (
            now()
            ->addHours(variabledb::val('TIME_CAN_SESSIONS_EDIT_WITHIN_BY_HOURS'))
            ->greaterThan($this->session->DateTime)
        ) return $this->updateResult('lessThanTime', 502, false);
    }

    public function MiddlewareIfDateTimeInFuture(Carbon $date, Carbon $time)
    {
        $this->IfExistsRateByMinutes();
        if (!$this::$check) return $this->makeResult([]);
        if ($this->session->ifStudentNotExists) return $this->makeResult([]);
        $Carbon = Carbon::createFromFormat('Y-m-d H:i:s', $date->format('Y-m-d') . ' ' . $time->format('H:i:s'))
            ->subHours(variabledb::val('TIME_CAN_SESSIONS_EDIT_WITHIN_BY_HOURS'))
            ->subMinutes($this->RateByMinutes());
        if ($Carbon < now()) return $this->updateResult('NotInFuture', 502, false);
    }

    public function getSessionsQuery($user)
    {
        $Query = $user->sessionsToUnits();
        if ($this->session) $Query = $Query->where('sessions.id', '!=', $this->session->id);
        return $Query;
    }

    public function MiddlewareIfUserHaveDateTimeAvailableExists($user, Carbon $date, Carbon $time, Int $repeat = 0, String $lop = 'addWeeks')
    {
        $this->IfExistsRateByMinutes();
        if ($user && !in_array(get_class($user), [instructor::class, student::class])) return $this->updateResult('haveSessionAtThisTime', 502, false);
        if (!$this::$check) return $this->makeResult([]);
        if ($this->getSessionsQuery($user)->DateTimeAvailableExists($this->rate_minutes_type, $date, $time, $repeat, $lop)) return $this->updateResult('haveSessionAtThisTime', 502, false);
    }

    public function beforeEdit(Int $id, Carbon $date, Carbon $time)
    {
        $this->MiddlewareAuthHasSessions($id);
        $this->MiddlewareSessionsIfLessTime();
        $this->MiddlewareIfDateTimeInFuture($date, $time);
        $this->MiddlewareIfUserHaveDateTimeAvailableExists(Auth::user(), $date, $time);
    }

    public function beforeDelete(Int $id)
    {
        $this->MiddlewareAuthHasSessions($id);
        $this->MiddlewareSessionsIfLessTime();
    }

    public function beforeCancel(Int $id)
    {
        $this->MiddlewareAuthHasSessions($id);
        $this->MiddlewareSessionsIfLessTime();
    }

    public function getSessions(array $ids)
    {
        return session::canFind()->findMany($ids)->filter(fn ($session) => !$session->has_conflict_in_book)->filter();
    }

    public function beforeBook(array $ids)
    {
        $this->MiddlewareAuthStudent();
        if (!$this::$check) return [];
        return $this->getSessions($ids);
    }
}
