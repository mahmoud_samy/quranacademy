<?php

namespace App\GraphQL\Middleware;

use Illuminate\Support\Facades\Auth;

class PersonTypesMiddleware extends AuthMiddleware
{

    public function MiddlewareAuthStudent()
    {
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        if (!Auth::user()->tokenCan('student')) return $this->updateResult('NotStudent', 502, false);
    }

    public function MiddlewareAuthInstructor()
    {
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        if (!Auth::user()->tokenCan('instructor')) return $this->updateResult('NotInstructor', 502, false);
    }
}
