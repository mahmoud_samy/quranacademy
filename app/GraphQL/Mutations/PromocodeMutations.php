<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Middleware\sessionsMiddleware;
use App\Models\Promocode;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PromocodeMutations extends sessionsMiddleware
{
    public function verifyPromocode($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);

        $promocode = Promocode::where('code', $args['code'])->first();

        if (!$promocode) {
            return $this->updateResult('notExists', 404)->makeResult();
        }

        if ($promocode->expires_at && $promocode->expires_at <= now()->toDateTimeString()) {
            return $this->updateResult('expired', 410)->makeResult();
        }

        $promocode_user = auth()->user()->promocodes()->where('promocode_id', $promocode->id)->first();

        if ($promocode_user && $promocode->is_disposable) {
            return $this->updateResult('alreadyUsed', 400)->makeResult();
        }

        return $this->updateResult('successfully', 200, true)
            ->makeResult([
                'promocode' => $promocode
            ]);
    }

    public function getPromocode($root, array $args, $context, ResolveInfo $resolveInfo)
    {

        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);

        $promocode = Promocode::where('code', $args['code'])->first();

        if (!$promocode) {
            return $this->updateResult('notExists', 404)->makeResult();
        }

        if ($promocode->expires_at && $promocode->expires_at <= now()->toDateTimeString()) {
            return $this->updateResult('expired', 410)->makeResult();
        }

        $promocode_user = auth()->user()->promocodes()->where('promocode_id', $promocode->id)->first();
        if ($promocode_user && $promocode->is_disposable) {
            return $this->updateResult('alreadyUsed', 400)->makeResult();
        }

        auth()->user()->promocodes()->attach([
            'promocode_id' => $promocode->id
        ]);

        return $this->updateResult('usedSuccessfully', 200, true)
            ->makeResult([
                'promocode' => $promocode
            ]);
    }
}
