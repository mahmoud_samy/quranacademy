<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Middleware\sessionsMiddleware;
use App\Models\session;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessageMutations extends sessionsMiddleware
{

    public function MessageCreate($root, array $args, $context, ResolveInfo $resolveInfo)
    {

        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareAuthHasSessions($args['session_id']);

        if (!$this::$check) return $this->makeResult([]);

        if (Auth::user()->tokenCan('instructor')) $args['instructor_id'] = Auth::user()->id;

        if (Auth::user()->tokenCan('student')) {
            $args['student_id'] = Auth::user()->id;


            $session = session::find($args['session_id']);
            if ($session) {
                $instructor = $session->instructor;
                if ($instructor) {
                    $details = [
                        'email' => $instructor->email,
                        'type' => 'student_send_message',
                        'title' => 'Mail from quran academy Student Send Message',
                        'body' => 'This is for testing email using smtp'
                    ];
                    dispatch(new \App\Jobs\SendEventsEmailJob($details));
                }
            }
        }

        return $this->updateResult('successfully', 200, true)->makeResult([
            'message' => $this->session->session_messages()->create($args),
            'session' => $this->session,
        ]);
    }
}
