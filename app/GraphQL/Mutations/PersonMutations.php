<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Middleware\PersonTypesMiddleware;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PersonMutations extends PersonTypesMiddleware
{

    public function PersonEdit($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);
        Auth::user()->updateProfile($args);
        if (array_key_exists('AccessTokenInputs', $args)) Auth::user()->token()->edit($args['AccessTokenInputs']);
        return $this->updateResult('successfully', 200, true)->makeResult([
            'Student'       => Auth::user()->tokenCan('student') ? Auth::user()->refresh() : null,
            'Instructor'    => Auth::user()->tokenCan('instructor') ? Auth::user()->refresh() : null,
            'SubInstructor' => Auth::user()->tokenCan('SubInstructor') ? Auth::user()->refresh() : null,
        ]);
    }

    public function PersonMarkNotificationsAsRead($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);
        Auth::user()->unreadNotifications->markAsRead();
        return $this->updateResult('PersonMutations.UserMarkNotificationsAsRead.Successfully', 200, true)->makeResult([]);
    }

    public function PersonChangePassword($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);
        if (!Auth::user()->needPassword && !array_key_exists('old', $args)) return $this->updateResult('oldPasswordrequired', 500, false)->makeResult([]);
        if (!Auth::user()->needPassword && !Hash::check($args['old'], Auth::user()->password)) return $this->updateResult('oldPasswordNotMatch', 501, false)->makeResult([]);
        if (!Auth::user()->needPassword &&   Hash::check($args['new'], Auth::user()->password)) return $this->updateResult('newPasswordMatch', 502, false)->makeResult([]);
        Auth::user()->HashAndEditPassword($args['new']);
        return $this->updateResult('Successfully', 200, true)->makeResult([]);
    }

    public function PersonTransfer($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthInstructor();

        $Transfer = Auth::user()->createNewTransfer($args);
        if ($Transfer['check']) return $this->updateResult('Successfully', 200, true)->makeResult([
            'StatusTransfer' => $Transfer,
            'Instructor'     => Auth::user(),
            'Transfer'       => $Transfer['model'],
        ]);
        else return $this->updateResult('fail', 404, false)->makeResult(['StatusTransfer' => $Transfer, 'Instructor' => Auth::user()]);
    }
}
