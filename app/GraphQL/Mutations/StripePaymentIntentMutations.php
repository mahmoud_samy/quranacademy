<?php

namespace App\GraphQL\Mutations;

use GraphQL\Type\Definition\ResolveInfo;
use App\GraphQL\Middleware\PersonTypesMiddleware;
use Illuminate\Support\Facades\Log;

class StripePaymentIntentMutations extends PersonTypesMiddleware
{
    public function createPaymentIntent($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);


        $stripe = new \Stripe\StripeClient(config('services.stripe.secret'));

        $response = $stripe->paymentIntents->create([
            'amount' => $args['amount'],
            'currency' => $args['currency'],
        ]);

        return $this->updateResult('successfully', 200, true)
            ->makeResult([
                'stripePaymentIntent' => [
                    'id' => $response->id,
                    'client_secret' => $response->client_secret,
                ]
            ]);
    }
}
