<?php

namespace App\GraphQL\Mutations;

use App\Jobs\SendEventsEmailJob;
use App\Models\session;
use App\Models\rateMinutesType;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use App\GraphQL\Middleware\sessionsMiddleware;
use App\Notifications\Instructor\sessionsNewBook;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class sessionsMutations extends sessionsMiddleware
{

    public function sessionsCreate($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthInstructor();

        $repeat = 0;
        $type = 'addWeeks';
        if (array_key_exists('repeat', $args) && $args['repeat'] && $args['repeat']['repeat'] && $args['repeat']['repeat'] > 0) ['repeat' => $repeat, 'type' => $type] = $args['repeat'];

        $rateMinutesType = rateMinutesType::find($args['rate_minutes_type_id']);

        $this->SetRateMinutesType($rateMinutesType);
        $this->MiddlewareIfUserHaveDateTimeAvailableExists(Auth::user(), $args['date']->clone(), $args['time']->clone(), $repeat, $type);

        if (!$this::$check) return $this->makeResult([]);

        $sessions = [];

        for ($i = 0; $i <= $repeat; $i++) {

            $date = $args['date']->clone()->$type($i)->format('Y-m-d');
            $time = $args['time']->format('H:i:s');

            $start_at = Carbon::parse($date . ' ' . $time);
            $end_at = Carbon::parse($date . ' ' . $time)->addMinutes($rateMinutesType->RateByMinutes);

            $sessions[] = session::create([
                'date'                 => $date,
                'instructor_id'        => Auth::user()->id,
                'rate_minutes_type_id' => $rateMinutesType->id,
                'time'                 => $time,
                'course_type_id'       => $args['course_type_id'],
                'rate_salary'          => Auth::user()[$rateMinutesType->attr] ?? 0,
                'currency_type_id'     => Auth::user()->currency_type_id,
                'start_at'             => $start_at,
                'end_at'               => $end_at,
            ]);
        }

        return $this->updateResult('Successfully', 200, true)->makeResult(['sessions' => $sessions]);
    }

    public function sessionsEdit($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);
        if ($args['date'] ?? false) {

            $this->beforeEdit($args['id'], $args['date']->clone(), $args['time']->clone());

            if (!$this::$check) return $this->makeResult([]);

            unset($args['id'], $args['directive']);

            if ($this->session->if_student_not_exists) {
                $this->session->editAndSave($args);
                $this->updateResult('Successfully', 200, true);
            } else {
                $this->MiddlewareIfUserHaveDateTimeAvailableExists($this->session->student, $args['date']->clone(), $args['time']->clone());
                if (!$this::$check) return $this->makeResult([]);
                $this->session->makeEdit($args);
                if (Auth::user()->tokenCan('student')) $this->updateResult('sendToInstructorToApprove', 200, true);
                else $this->updateResult('sendToStudentToApprove', 200, true);
            };
            return $this->makeResult([
                'session' => $this->session->refresh()
            ]);
        }

        if ($args['Materials']) {

            $this->MiddlewareAuthInstructor();
            $this->MiddlewareAuthHasSessions($args['id']);
            if (!$this::$check) return $this->makeResult([]);

            if ($args['Materials']['add'] ?? false) foreach ($args['Materials']['add'] as $file) $this->session->addMaterial($file);
            if ($args['Materials']['delete'] ?? false) foreach ($args['Materials']['delete'] as $id) $this->session->deleteMaterial($id);
            if ($args['Materials']['replace'] ?? false) foreach ($args['Materials']['replace'] as $replace) $this->session->replaceMaterial($replace['id'], $replace['file']);
        }

        return $this->updateResult('Successfully', 200, true)->makeResult([
            'session' => $this->session->refresh()
        ]);
    }

    public function sessionsCancel($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->beforeCancel($args['id']);

        if (!$this::$check) return $this->makeResult([]);

        return $this->updateResult('Successfully', 200, true)->makeResult([
            'session' => $this->session->Cancel($args)->refresh()
        ]);
    }

    public function sessionsSeeMessages($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);
        $this->MiddlewareAuthHasSessions($args['id']);

        if (!$this::$check) return $this->makeResult([]);

        return $this->updateResult('Successfully', 200, true)->makeResult([
            'session' => $this->session->BeSeen()
        ]);
    }

    public function sessionsDelete($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuth();

        if (!$this::$check) return $this->makeResult([]);

        Auth::user()->sessionsDelete($args['id']);

        return $this->updateResult('Successfully', 200, true)->makeResult([]);
    }

    public function sessionsBook($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $sessions = $this->beforeBook(array_map(fn ($item) => $item['id'], $args['sessions']));

        if (!$this::$check) return $this->makeResult([]);

        if ($sessions->isNotEmpty()) {
            $rate_minutes_ids = $sessions->pluck('rate_minutes_type_id');
            $sum_total_time = 0;
            foreach ($rate_minutes_ids as $id) {
                $rateMinutesType = rateMinutesType::find($id);
                $sum_total_time = $sum_total_time + $rateMinutesType->value;
            }
            $sessions->book($args['sessions']);
            $ids     = $sessions->pluck('id')->unique()->values()->toArray();
            $session = $sessions->first()->refresh();
            $session->instructor->notifyNow(new sessionsNewBook($session->student, $ids));
            if ($subscription = $session->student->PlanSubscription) {
                $subscription->plan_hours_remaining = ($subscription->plan_hours_remaining - ($sum_total_time / 60)) ;
                $subscription->save();
            }

            $details = [
                'email' => $session->student->email,
                'type' => 'enroll_in_session_instructor',
                'title' => 'Mail from quran academy student enroll in session ',
                'body' => 'This is for testing email using smtp'
            ];

            dispatch(new SendEventsEmailJob($details));
        };

        return $this->updateResult('Successfully', 200, true)->makeResult(['sessions' => $sessions]);
    }

    public function sessionsUpload($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthInstructor();

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareAuthHasSessions($args['id']);

        if (!$this::$check) return $this->makeResult([]);

        return $this->updateResult('Successfully', 200, true)->makeResult([
            'session' => $this->session->addMaterial($args['file'])
        ]);
    }

    public function sessionsChange($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthStudent();

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareAuthHasSessions($args['id_old']);

        $oldSession = $this->session;

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareIfExistsSessions($args['id_new']);

        $newSession = $this->session;

        if (
            $oldSession['rate_minutes_type_id'] !== $this->session['rate_minutes_type_id'] ||
            $oldSession['course_type_id'] !== $this->session['course_type_id'] ||
            $oldSession['rate_salary'] !== $this->session['rate_salary'] ||
            $oldSession['currency_type_id'] !== $this->session['currency_type_id']
        ) return $this->updateResult('youCantTakeThisSession', 200, true)->makeResult();

        return $this->updateResult('Successfully', 200, true)->makeResult([
            'session' => $this->session->replace($oldSession)
        ]);
    }

    public function sessionsAcceptEdit($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthStudent();

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareAuthHasSessions($args['id']);

        if (!$this::$check) return $this->makeResult([]);

        $newSession = $this->session;

        if (!$this->session->hasEditRequest) return $this->updateResult('thisSessionHasNotEditRequest', 200, true)->makeResult(['session' => $this->session]);

        return $this->updateResult('Successfully', 200, true)->makeResult(['session' => $this->session->AcceptEdit()]);
    }

    public function sessionsRating($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->MiddlewareAuthStudent();

        if (!$this::$check) return $this->makeResult([]);

        $this->MiddlewareAuthHasSessions($args['id']);

        if (!$this::$check) return $this->makeResult([]);

        return $this->updateResult('Successfully', 200, true)->makeResult(['session' => $this->session->rat($args['rating'], $args['reviews'] ?? null)]);
    }
}
