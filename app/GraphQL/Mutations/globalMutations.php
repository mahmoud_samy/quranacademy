<?php

namespace App\GraphQL\Mutations;

use App\Models\File;
use App\Models\image;
use App\Models\Pincode;
use App\Models\student;
use App\Models\instructor;
use App\Models\variabledb;
use Illuminate\Http\Request;
use App\Models\SubInstructor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use GraphQL\Type\Definition\ResolveInfo;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;
use App\GraphQL\Middleware\AuthMiddleware;
use Illuminate\Support\Facades\Log;

class globalMutations extends AuthMiddleware
{

    public function globalLogin($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        unset($args['directive']);

        $this->emailNotExist($args['email']);
        if (!$this::$check) return $this->makeResult();

        if (student::byEmail($args['email'])->exists()) {
            $makeResult['Student'] = $user = student::byEmail($args['email'])->first();
            if (is_null($user)) return $this->updateResult('invalidlogin', 502)->makeResult();
            if (!Hash::check($args['password'],  $user->password)) return $this->updateResult('invalidlogin', 502)->makeResult();
        }

        if (instructor::byEmail($args['email'])->exists()) {
            $makeResult['Instructor'] = $user = instructor::byEmail($args['email'])->first();
            if (is_null($user)) return $this->updateResult('invalidlogin', 502)->makeResult();
            if (!Hash::check($args['password'],  $user->password)) return $this->updateResult('invalidlogin', 502)->makeResult();
        }

        if (SubInstructor::byEmail($args['email'])->exists()) {
            $makeResult['SubInstructor'] = $user = SubInstructor::byEmail($args['email'])->first();
            if (is_null($user)) return $this->updateResult('invalidlogin', 502)->makeResult();
            if (!Hash::check($args['password'],  $user->password)) return $this->updateResult('invalidlogin', 502)->makeResult();
        }

        $makeResult['BearerTokenResponse'] = $user->MakeBearerToken();
        return $this->updateResult('successfully', 200, true)->makeResult($makeResult);
    }

    public function globalRequestCreateOauthToken($oauth_clients, $args)
    {
        return json_decode(app()->handle(Request::create('oauth/token', 'POST', [
            'client_id'     => $oauth_clients->id,
            'client_secret' => $oauth_clients->secret,
            'grant_type'    =>        'refresh_token',
            'refresh_token' => $args['refresh_token'],
        ], [], [], array_merge($_SERVER, ['HTTP_Accept' => 'application/json'])))->getContent(), true);
    }

    public function globalRefreshToken($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $tokenSubInstructor = $this->globalRequestCreateOauthToken(DB::table('oauth_clients')->where('password_client', 1)->where('provider', class_basename(SubInstructor::class))->first(), $args);
        $tokeninstructor    = $this->globalRequestCreateOauthToken(DB::table('oauth_clients')->where('password_client', 1)->where('provider', class_basename(instructor::class))->first(), $args);
        $tokenstudent       = $this->globalRequestCreateOauthToken(DB::table('oauth_clients')->where('password_client', 1)->where('provider', class_basename(student::class))->first(), $args);

        $token = $tokenSubInstructor + $tokeninstructor + $tokenstudent;

        if (!array_key_exists('access_token', $token)) return $this->updateResult('invalid', 502)->makeResult();

        $makeResult['BearerTokenResponse'] = $token;

        request()->headers->set('Authorization', $token['token_type'] . ' ' . $token['access_token']);

        foreach ([SubInstructor::class, instructor::class, student::class] as $model) if (
            Auth::guard(class_basename($model))->check() &&
            Auth::guard(class_basename($model))->user()->tokenCan(class_basename($model))
        ) $makeResult[ucfirst(class_basename($model))] = Auth::guard(class_basename($model))->user();

        return $this->updateResult('successfully', 200, true)->makeResult($makeResult);
    }

    public function globalSendPinCode($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        switch ($args['sendPinCodeFor']) {
            case "Register":
                $args['type'] = 'emailview';
                $args['Subject'] = 'Verify Mail';
                $this->emailisExist($args['email']);
                break;
            case "passwordresets":
                $args['type'] = 'passwordresetsview';
                $args['Subject'] = 'Reset Password Mail';
                $this->emailNotExist($args['email']);
                break;
        }

        if (!$this::$check) return $this->makeResult();

        Pincode::firstOrCreate(['email' => $args['email']])->getNewToken($args['type'], $args['Subject']);

        return $this->updateResult('successfully', 200, true)->makeResult([]);
    }

    public function globalverifyPinCode($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->verifyPinCode($args);

        return $this->makeResult();
    }

    public function globalRegister($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $Token = null;

        if (array_key_exists('verifyPinCodeInput', $args)) {

            $this->emailisExist($args['verifyPinCodeInput']['email']);
            if (!$this::$check) return $this->makeResult();

            $this->verifyPinCode($args['verifyPinCodeInput']);
            if (!$this::$check) return $this->makeResult();
        } else if (array_key_exists('SocialiteInput', $args)) {

            try {
                $Token = Socialite::driver($args['SocialiteInput']['provider'])->userFromToken($args['SocialiteInput']['token']);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return $this->updateResult('InvalidRequest', 404, false)->makeResult();
            }

            if (is_null($Token->email)) return $this->updateResult('InvalidRequest', 404, false)->makeResult();

            $this->emailisExist($Token->email);
            if (!$this::$check) return $this->makeResult();
        } else return $this->makeResult($makeResult);

        if (array_key_exists('subInstructorsInput', $args)) {

            if (array_key_exists('verifyPinCodeInput', $args)) {
                if (array_key_exists('password', $args['subInstructorsInput'])) $args['subInstructorsInput']['password'] = Hash::make($args['subInstructorsInput']['password']);
                else return $this->updateResult('passwordnotexists', 500, false)->makeResult($makeResult);
                $args['subInstructorsInput']['email'] = $args['verifyPinCodeInput']['email'];
            }

            if (array_key_exists('SocialiteInput', $args) && $Token) $args['subInstructorsInput']['email'] = $Token->email;

            $makeResult['SubInstructor'] = $user = SubInstructor::create($args['subInstructorsInput']);

            if (array_key_exists('profile_image', $args['subInstructorsInput'])) {
                $user->updateProfilePhoto($args['subInstructorsInput']['profile_image']);
                $user->save();
            }
        }

        if (array_key_exists('studentsInput', $args)) {

            if (array_key_exists('verifyPinCodeInput', $args)) {
                if (array_key_exists('password', $args['studentsInput'])) $args['studentsInput']['password'] = Hash::make($args['studentsInput']['password']);
                else return $this->updateResult('passwordnotexists', 500, false)->makeResult($makeResult);
                $args['studentsInput']['email'] = $args['verifyPinCodeInput']['email'];
            }

            if (array_key_exists('SocialiteInput', $args) && $Token) $args['studentsInput']['email'] = $Token->email;

            $makeResult['Student'] = $user = student::create($args['studentsInput']);

            if (array_key_exists('profile_image', $args['studentsInput'])) {
                $user->updateProfilePhoto($args['studentsInput']['profile_image']);
                $user->save();
            }
        }

        $makeResult['BearerTokenResponse'] = $user->MakeBearerToken();

        $details = [
            'email' => $user->email,
            'type' => 'welcome_registration',
            'title' => 'Mail from quran academy',
            'body' => 'This is for testing email using smtp'
        ];



        dispatch(new \App\Jobs\SendEventsEmailJob($details));


        return $this->updateResult('Successfully', 200, true)->makeResult($makeResult);
    }

    public function globalRegisterInstructor($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        if (array_key_exists('subInstructorsInput', $args)) {

            $user = SubInstructor::byEmail($args['subInstructorsInput']['email'])->first();

            if (
                !is_null($user) &&
                Hash::check($args['subInstructorsInput']['password'],  $user->password)
            ) $args['InstructorsInput']['email'] = $args['subInstructorsInput']['email'];

            else return $this->updateResult('emailNotExist', 404)->makeResult();
        } else if (array_key_exists('SocialiteInput', $args)) {

            try {
                $Token = Socialite::driver($args['SocialiteInput']['provider'])->userFromToken($args['SocialiteInput']['token']);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                return $this->updateResult('InvalidRequest', 404, false)->makeResult();
            }

            if (is_null($Token->email)) return $this->updateResult('InvalidRequest', 404, false)->makeResult();

            if (!SubInstructor::byEmail($Token->email)->exists()) return $this->updateResult('emailNotExist', 502)->makeResult();

            else $args['subInstructorsInput']['email'] = $Token->email;
        } else return $this->updateResult('emailisExist', 404)->makeResult();

        if (array_key_exists('InstructorsInput', $args)) {

            if (array_key_exists('password', $args['InstructorsInput'])) $args['InstructorsInput']['password'] = Hash::make($args['InstructorsInput']['password']);

            $args['InstructorsInput']['email'] = $args['subInstructorsInput']['email'];
            $args['InstructorsInput']['is_Approval'] = variabledb::val('IF_ADMIN_NOT_NEED_APPROVE_INSTRUCTORST');
            $args['InstructorsInput']['is_profile_public'] = 1;
            //$args['InstructorsInput']['profile_image_id'] = image::createFromFile($args['InstructorsInput']['profile_image'])->id;
            //$args['InstructorsInput']['profile_vidoe_id'] = File::createFromFile($args['InstructorsInput']['profile_vidoe'])->id;

            $instructor = instructor::create($args['InstructorsInput']);

            $instructor->updateProfilePhoto($args['InstructorsInput']['profile_image']);
            $instructor->updateProfileVideo($args['InstructorsInput']['profile_vidoe']);

            $makeResult['Instructor'] = $instructor;
            $makeResult['BearerTokenResponse'] = $makeResult['Instructor']->MakeBearerToken();

            $makeResult['Instructor']
                ->update_Spoken_Language_User($args['InstructorsInput']['Spoken_Language_User']);

            if (array_key_exists('letters', $args['InstructorsInput'])) $makeResult['Instructor']->add_letters($args['InstructorsInput']['letters']);
            if (array_key_exists('certificates', $args['InstructorsInput'])) $makeResult['Instructor']->add_certificates($args['InstructorsInput']['certificates']);
            if (array_key_exists('recommendations', $args['InstructorsInput'])) $makeResult['Instructor']->add_recommendations($args['InstructorsInput']['recommendations']);

            SubInstructor::byEmail($makeResult['Instructor']->email)->delete();
            return $this->updateResult('Successfully', 200, true)->makeResult($makeResult);
        }

        return $this->makeResult();
    }

    public function globalCreateInstructor($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $makeResult = [
            'BearerTokenResponse' => null,
            'Student'             => null,
            'Instructor'          => null,
            'SubInstructor'       => null,
        ];

        if (!Auth::check()) return $this->updateResult('AuthNotExist', 403)->makeResult($makeResult);
        if (!Auth::user()->tokenCan('SubInstructor')) return $this->updateResult('IsNotSubInstructor', 404)->makeResult($makeResult);

        $instructorArray = collect(Auth::user()->toArray())->merge($args)->all();

        $validator = Validator::make($instructorArray, [
            'first_name'              => 'required',
            'last_name'               => 'required',
            'email'                   => 'required',
            'Locale'                  => 'required',
            'years_of_experience'     => 'required',
            'description'             => 'required',
            'courses_types_id'        => 'required',
            'student_gender_types_id' => 'required',
            'student_age_types_id'    => 'required',
            'currency_type_id'        => 'required',
        ]);

        if (
            !array_key_exists('profile_image_id', $instructorArray) ||
            is_null($instructorArray['profile_image_id']) ||
            (is_null($instructorArray['rate_15_min']) &&
                is_null($instructorArray['rate_30_min']) &&
                is_null($instructorArray['rate_60_min'])) ||
            $validator->fails()
        ) return $this->updateResult('dataNotValid', 404)->makeResult($makeResult);

        Auth::user()->delete();

        //$instructorArray['profile_vidoe_id'] = File::createFromFile($instructorArray['profile_vidoe'])->id;
        $instructorArray['is_Approval'] = variabledb::val('IF_ADMIN_NOT_NEED_APPROVE_INSTRUCTORST');
        $instructorArray['is_profile_public'] = 1;

        unset($instructorArray['id']);

        $instructor = instructor::create($instructorArray);

        if (array_key_exists('profile_image', $instructorArray)) {
            $instructor->updateProfilePhoto($instructorArray['profile_image']);
        }

        if (array_key_exists('profile_vidoe', $instructorArray)) {
            $instructor->updateProfileVideo($instructorArray['profile_vidoe']);
        }

        $makeResult['Instructor'] = $instructor;
        $makeResult['BearerTokenResponse'] = $makeResult['Instructor']->MakeBearerToken();

        $makeResult['Instructor']->refresh();

        if (array_key_exists('certificates', $instructorArray)) $makeResult['Instructor']->add_certificates($instructorArray['certificates']);
        if (array_key_exists('Spoken_Language_User', $instructorArray)) $makeResult['Instructor']->update_Spoken_Language_User($instructorArray['Spoken_Language_User']);

        return $this->updateResult('Successfully', 200, true)->makeResult($makeResult);
    }

    public function globalPasswordresets($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $this->emailNotExist($args['verifyPinCodeInput']['email']);
        if (!$this::$check) return $this->makeResult();

        $this->verifyPinCode($args['verifyPinCodeInput']);
        if (!$this::$check) return $this->makeResult();

        if (student::byEmail($args['verifyPinCodeInput']['email'])->exists()) $makeResult['Student'] = $user = student::byEmail($args['verifyPinCodeInput']['email'])->first();
        if (instructor::byEmail($args['verifyPinCodeInput']['email'])->exists()) $makeResult['Instructor'] = $user = instructor::byEmail($args['verifyPinCodeInput']['email'])->first();
        if (SubInstructor::byEmail($args['verifyPinCodeInput']['email'])->exists()) $makeResult['SubInstructor'] = $user = SubInstructor::byEmail($args['verifyPinCodeInput']['email'])->first();

        $user->HashAndEditPassword($args['password']);
        $makeResult['BearerTokenResponse'] = $user->MakeBearerToken();
        return $this->updateResult('Successfully', 200, true)->makeResult($makeResult);
    }

    public function globalLoginSocialite($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        $makeResult = [];

        try {
            $Token = Socialite::driver($args['provider'])->userFromToken($args['token']);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $this->updateResult('InvalidRequest', 404, false)->makeResult($makeResult);
        }

        if (is_null($Token->email)) return $this->updateResult('InvalidRequest', 404, false)->makeResult($makeResult);

        $this->emailNotExist($Token->email);
        if (!$this::$check) return $this->makeResult($makeResult);

        if (student::byEmail($Token->email)->exists()) $makeResult['Student'] = $user = student::byEmail($Token->email)->first();
        if (instructor::byEmail($Token->email)->exists()) $makeResult['Instructor'] = $user = instructor::byEmail($Token->email)->first();
        if (SubInstructor::byEmail($Token->email)->exists()) $makeResult['SubInstructor'] = $user = SubInstructor::byEmail($Token->email)->first();

        $makeResult['BearerTokenResponse'] = $user->MakeBearerToken();
        return $this->updateResult('successfully', 200, true)->makeResult($makeResult);
    }

    public function globalLogout($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        if (Auth::check()) {
            Auth::user()->token()->revoke();
            Auth::user()->token()->delete();
        };

        return $this->updateResult('auth.Logout.successfully', 200, true)->makeResult();
    }

    public function globalcontactUs($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);

        Mail::send('contactUs', [
            'email' => $args['email'],
            'name'  => $args['name'],
            'msg'   => $args['message'],
        ], function ($message) {
            $message->to('mahmuod.sami.99@gmail.com');
        });

        return $this->updateResult('Successfully', 200, true)->makeResult([]);
    }
}
