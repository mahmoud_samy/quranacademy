<?php

namespace App\GraphQL\Mutations;

use App\GraphQL\Middleware\AuthMiddleware;
use App\GraphQL\Middleware\sessionsMiddleware;
use App\Models\Plan;
use App\Models\Subscription;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PlanMutations extends sessionsMiddleware
{
    public function subscribePlan($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        $user = Auth::user();

        $subscription  = Subscription::where('user_id',$user->id)->where('active',1)->first();

        if($subscription){
            return $this->updateResult('youAlreadySubscribedPlan', 501)->makeResult();
        }

        $plan = Plan::find($args['plan_id']);
        if (!$plan) {
            return $this->updateResult('notExists', 404)->makeResult();
        }

        $subscription = new Subscription();
        $subscription->plan_id = $plan->id;
        $subscription->user_id = $user->id ;
        $subscription->plan_hours_limitation = $plan->hours_limitation ;
        $subscription->plan_hours_remaining = $plan->hours_limitation ;
        $subscription->plan_price = $plan->price ;
        $subscription->plan_discount = $plan->discount ;
        $subscription->plan_net_price = $plan->net_price ;
        $subscription->expire_at = Carbon::now()->addDay($plan->expire_date);
        $subscription->save();

        return $this->updateResult('subscribedSuccessfully', 200, true)->makeResult([]);
    }

    public function cancelSubscribePlan($root, array $args, $context, ResolveInfo $resolveInfo)
    {
        $this->setResolveInfo($resolveInfo);
        $this->MiddlewareAuth();
        if (!$this::$check) return $this->makeResult([]);
        $user = Auth::user();

        $subscription = Subscription::where('user_id', $user->id)->where('active', 1)->first();
        if (!$subscription) {
            return $this->updateResult('notExists', 404)->makeResult();
        }
        $subscription->active = 0;
        $subscription->save();

        return $this->updateResult('subscriptionCanceledSuccessfully', 200, true)->makeResult([]);
    }

}
