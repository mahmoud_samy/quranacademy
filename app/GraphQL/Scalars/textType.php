<?php

namespace App\GraphQL\Scalars;

use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class textType extends ScalarType
{

    public $min = null;

    public $max = 65534;

    public $name = 'Text';

    public function __construct()
    {

        $this->description = 'text like normal string but limit from null to ' . $this->max . ' characters';
    }

    public function serialize($value)
    {
        return mb_substr(htmlspecialchars($value, ENT_NOQUOTES, 'utf-8', false), 0, $this->max);
    }

    public function parseValue($value)
    {
        if (strlen($value) > $this->max) {
            throw new Error($this->name . ' can not greater than : ' . $this->max);
        }
        return htmlspecialchars($value, ENT_NOQUOTES, 'utf-8', false);
    }

    public function parseLiteral($valueNode, array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) return $valueNode->value;
        // Intentionally without message, as all information already in wrapped Exception
        throw new Exception();
    }
}
