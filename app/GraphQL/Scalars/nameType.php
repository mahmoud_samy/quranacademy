<?php

namespace App\GraphQL\Scalars;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class nameType extends ScalarType
{

    public $min = 3;

    public $max = 191;

    public $name = 'Name';

    public function __construct()
    {

        $this->description = 'name like normal string but limit from ' . $this->min . ' to ' . $this->max . ' characters and cannot be null';
    }

    public function serialize($value)
    {
        return mb_substr(htmlspecialchars($value, ENT_NOQUOTES, 'utf-8', false), 0, $this->max);
    }

    public function parseValue($value)
    {
        if (is_null($value)) throw new Error($this->name . ' can not be null');
        if (strlen($value) < $this->min) throw new Error($this->name . ' can not lower than : '   . $this->min);
        if (strlen($value) > $this->max) throw new Error($this->name . ' can not greater than : ' . $this->max);
        return htmlspecialchars($value, ENT_NOQUOTES, 'utf-8', false);
    }

    public function parseLiteral($valueNode, array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) return $valueNode->value;
        // Intentionally without message, as all information already in wrapped Exception
        throw new Exception();
    }
}
