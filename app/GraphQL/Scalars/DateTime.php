<?php

namespace App\GraphQL\Scalars;

use Carbon\Carbon;

class DateTime extends DateScalar
{

    public $name = 'DateTime';

    public string $pattern = Carbon::DEFAULT_TO_STRING_FORMAT;

    protected function format(Carbon $carbon): string
    {
        return $carbon->toDateTimeString();
    }
}
