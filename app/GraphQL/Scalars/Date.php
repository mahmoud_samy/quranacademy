<?php

namespace App\GraphQL\Scalars;

use Carbon\Carbon;

class Date extends DateScalar
{

    public $name = 'Date';

    public string $pattern = 'Y-m-d';

    protected function format(Carbon $carbon): string
    {
        return $carbon->toDateString();
    }
}
