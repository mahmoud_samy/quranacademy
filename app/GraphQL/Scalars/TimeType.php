<?php

namespace App\GraphQL\Scalars;

use Carbon\Carbon;

use Exception;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;

class TimeType extends DateScalar
{

    public $name = 'Time';

    public string $pattern = 'H:i:s';

    protected function format(Carbon $carbon): string
    {
        return $carbon->toTimeString();
    }
}
