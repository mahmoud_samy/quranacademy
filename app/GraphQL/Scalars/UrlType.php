<?php

namespace App\GraphQL\Scalars;

use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use Illuminate\Support\Facades\Hash;

use App\Services\Functions;

class UrlType extends ScalarType
{

    public $name = 'Url';

    public function serialize($value)
    {
        return $value;
    }

    public function parseValue($value)
    {
        if (empty($value)) return '';
        $value = filter_var($value, FILTER_SANITIZE_URL);
        if (!preg_match(Functions::URLREGEX, $value)) throw new Error("Cannot represent following value as Url: " . Utils::printSafeJson($value));
        return $value;
    }

    public function parseLiteral($valueNode, array $variables = null)
    {
        if (!$valueNode instanceof StringValueNode) {
            throw new Error('Query error: Can only parse strings got: ' . $valueNode->kind, [$valueNode]);
        }
        return $valueNode->value;
    }
}
