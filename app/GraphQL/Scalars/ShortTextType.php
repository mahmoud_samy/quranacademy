<?php

namespace App\GraphQL\Scalars;

use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class ShortTextType extends textType
{

    public $max = 191;

    public $name = 'ShortText';
}
