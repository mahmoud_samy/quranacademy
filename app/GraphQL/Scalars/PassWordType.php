<?php

namespace App\GraphQL\Scalars;


use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;
use Illuminate\Support\Facades\Hash;

class PassWordType extends ScalarType
{

    public $name = 'Password';

    public function serialize($value)
    {
        return $value;
    }

    public function parseValue($value)
    {
        if (!preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?).{8,}$/", $value)) {
            throw new Error("Cannot represent following value as PassWord: " . Utils::printSafeJson($value));
        }
        return $value;
    }

    public function parseLiteral($valueNode, array $variables = null)
    {
        if (!$valueNode instanceof StringValueNode) {
            throw new Error('Query error: Can only parse strings got: ' . $valueNode->kind, [$valueNode]);
        }
        return $valueNode->value;
    }
}
