<?php

namespace App\GraphQL\Scalars;

use Carbon\Carbon;

class CreditCardExpirationDate extends DateScalar
{

    public $name = 'CreditCardExpirationDate';

    public string $pattern = 'm/Y';

    protected function format(Carbon $carbon): string
    {
        return $carbon->format($this->pattern);
    }
}
