<?php

namespace App\GraphQL\Directives\Bilders;

use Nuwave\Lighthouse\Schema\Directives\BaseDirective;

use Nuwave\Lighthouse\Support\Contracts\ArgBuilderDirective;
use Nuwave\Lighthouse\Support\Contracts\DefinedDirective;

class SearchInFieldLikeDirective extends BaseDirective implements ArgBuilderDirective, DefinedDirective
{

    public static function definition()
    {
        return 'SearchInFieldLike';
    }

    public function handleBuilder($builder, $value)
    {
        return $builder->where(
            $this->directiveArgValue('name', $this->nodeName()),
            'like',
            "%${value}%"
        );
    }
}
