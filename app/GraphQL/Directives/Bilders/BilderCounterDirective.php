<?php

namespace App\GraphQL\Directives\Bilders;

use Nuwave\Lighthouse\Schema\Directives\BaseDirective;

use Nuwave\Lighthouse\Support\Contracts\ArgBuilderDirective;
use Nuwave\Lighthouse\Support\Contracts\DefinedDirective;

class BilderCounterDirective extends BaseDirective implements ArgBuilderDirective, DefinedDirective
{

    public static function definition()
    {
        return 'BilderCounter';
    }

    public function handleBuilder($builder, $value)
    {
        return $builder->take($value);
    }
}
