<?php

namespace App\GraphQL\Directives;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\Directive;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ReturnFilterSanitizeStringDirective implements Directive, FieldMiddleware
{

    public function name(): string
    {
        return 'ReturnFilterSanitizeString';
    }

    public function handleField(FieldValue $fieldValue, Closure $next): FieldValue
    {
        $Resolver = $fieldValue->getResolver();
        return $next($fieldValue->setResolver(function ($root, array $args, GraphQLContext $context, ResolveInfo $info) use ($Resolver): string {
            return htmlspecialchars($Resolver($root, $args, $context, $info), ENT_NOQUOTES, 'utf-8', false);
        }));
    }
}
