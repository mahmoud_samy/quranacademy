<?php

namespace App\GraphQL\Directives;

use Nuwave\Lighthouse\Schema\Directives\BaseDirective;

use Nuwave\Lighthouse\Support\Contracts\ArgTransformerDirective;
use Nuwave\Lighthouse\Support\Contracts\DefinedDirective;
use GraphQL\Language\AST\FieldDefinitionNode;
use GraphQL\Language\AST\InputObjectTypeDefinitionNode;
use GraphQL\Language\AST\InputValueDefinitionNode;
use GraphQL\Language\AST\ObjectTypeDefinitionNode;
use Nuwave\Lighthouse\Schema\AST\DocumentAST;
use Nuwave\Lighthouse\Support\Contracts\ArgManipulator;

use App\Models\User;

class ConvertIdUserToUserModelDirective extends BaseDirective implements ArgTransformerDirective, DefinedDirective
{

    public static function definition(): string
    {
        return 'ConvertIdUserToUserModel';
    }

    /**
     *
     * @param  string  $argumentValue
     * @return User
     */
    public function transform($argumentValue)
    {
        return User::find($argumentValue);
    }
}
