<?php

namespace App\GraphQL\Queries;

use App\Models\instructor;
use App\Models\instructorView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;

class userQuery
{

    public function meStudents($root, array $args)
    {
        return Auth::check() && Auth::user()->tokenCan('student') ? Auth::user() : $this->AuthenticationException(['student']);
    }

    public function meInstructors($root, array $args)
    {
        return Auth::check() && Auth::user()->tokenCan('instructor') ? Auth::user() : $this->AuthenticationException(['instructor']);
    }

    public function meSubInstructors($root, array $args)
    {
        return Auth::check() && Auth::user()->tokenCan('SubInstructor') ? Auth::user() : $this->AuthenticationException(['SubInstructor']);
    }

    public function InstructorGetTransfers($root, array $args)
    {
        return Auth::check() && Auth::user()->tokenCan('instructor') ? Auth::user()->transfers() : $this->AuthenticationException(['instructor']);
    }

    public function AuthenticationException(array $guards)
    {
        throw new AuthenticationException('Unauthenticated.', $guards);
    }

    public function InstructorFind($root, array $args)
    {
        $inst = instructor::canFind()->find($args['id']);
        if (Auth::check() && Auth::user()->tokenCan('student') && $inst) instructorView::firstOrCreate([
            'student_id'    => Auth::id(),
            'instructor_id' => $args['id'],
        ]);
        return $inst;
    }
}
