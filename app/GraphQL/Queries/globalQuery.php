<?php

namespace App\GraphQL\Queries;

use App\Models\session;
use App\Models\session_message;
use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use harby\services\Eloquent\Request;
use Illuminate\Auth\AuthenticationException;
use App\Services\transferwise;
use harby\services\Services\Result;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class globalQuery extends Result
{


    public function EventLog($root, array $args, $context, $resolveInfo)
    {

        return Event::getEventLog();
    }

    public function sessionGet($root, array $args)
    {

        if (!Auth::check()) throw new AuthenticationException();

        return Auth::user()->sessions()->find($args['id']);
    }

    public function sessionGetToken($root, array $args)
    {

        if (!Auth::check()) throw new AuthenticationException();
        $user = Auth::user();
        $session = session::find($args['id']);
        if (!$session) {
            return null;
        }
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $apiKeySid = env('TWILIO_API_KEY_SID');
        $apiKeySecret = env('TWILIO_API_KEY_SECRET');

        $identity = [
            'user_id' => $user->id,
            'session_id' => $session->id
        ];

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            json_encode($identity)
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom("Room number #$session->id");
        $token->addGrant($grant);
        return $this->updateResult('successfully', 200, true)->makeResult(['token'=>$token]);
    }

    public function globalCheckIfEmailExists($root, array $args, $context, $resolveInfo)
    {
        return (student::byEmail($args['email'])->exists() ||
            instructor::byEmail($args['email'])->exists() ||
            SubInstructor::byEmail($args['email'])->exists()) ? true : false;
    }

    public function MessageGet($root, array $args)
    {

        if (!Auth::check()) throw new AuthenticationException();
        if (is_null($message = session_message::find($args['id']))) return null;
        if (Auth::user()->tokenCan('student') && $message->session->student_id    == Auth::id()) return $message->BeSeen();
        if (Auth::user()->tokenCan('instructor') && $message->session->instructor_id == Auth::id()) return $message->BeSeen();
        return null;
    }

    public function globalGetTransfarCountries($root, array $args, $context, $resolveInfo)
    {
        return transferwise::getCountries();
    }
}
