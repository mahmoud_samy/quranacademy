<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EventsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->data['type'] == 'welcome_registration') {
            return $this->view('api.emails_templates.welcome_registration');
        } elseif ($this->data['type'] == 'enroll_in_session_student') {
            return $this->view('api.emails_templates.enroll_in_session_student');
        } elseif ($this->data['type'] == 'enroll_in_session_instructor') {
            return $this->view('api.emails_templates.enroll_in_session_instructor');
        } elseif ($this->data['type'] == 'student_send_message') {
            return $this->view('api.emails_templates.student_send_message');
        }
    }
}
