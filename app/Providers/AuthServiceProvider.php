<?php

namespace App\Providers;

use App\Models\OauthAccessToken;
use App\Models\variabledb;
use App\Models\Pincode;
use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;

use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerPolicies();

        Passport::useTokenModel(OauthAccessToken::class);

        Passport::tokensCan([
            config('auth.guards.' . class_basename(student::class) . '.provider') => class_basename(student::class),
            config('auth.guards.' . class_basename(instructor::class) . '.provider') => class_basename(instructor::class),
            config('auth.guards.' . class_basename(SubInstructor::class) . '.provider') => class_basename(SubInstructor::class),
        ]);

        Passport::routes();

        if (Schema::hasTable('variabledbs')) {

            Passport::tokensExpireIn(now()->addDays(variabledb::val('TIME_EXPIRE_ACCESS_TOKEN')));

            Passport::refreshTokensExpireIn(now()->addDays(variabledb::val('TIME_EXPIRE_REFRESH_TOKEN')));

            Pincode::tokensExpireIn(now()->addMinutes(variabledb::val('EXPIRE_PIN_CODE_TOKEN_BY_MINUTES')));

            Pincode::setLengthPinCodeToken(variabledb::val('LENGTH_PIN_CODE_TOKEN'));
        }
    }
}
