<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class translateServiceProvider extends ServiceProvider {
    public function boot( ) {
        $this -> loadTranslationsFrom( resource_path( 'lang/tables'        ) , 'tables'        );
        $this -> loadTranslationsFrom( resource_path( 'lang/Mutations'     ) , 'Mutations'     );
        $this -> loadTranslationsFrom( resource_path( 'lang/Notifications' ) , 'Notifications' );
    }
}
