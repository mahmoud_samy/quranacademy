<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use Livewire\Component;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::connection()->enableQueryLog();
        Schema::defaultStringLength(191);
        Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            foreach (array_wrap($attributes) as $attribute) $this->where($attribute, 'LIKE', "%{$searchTerm}%");
            return $this;
        });
        Builder::macro('createOrUpdate', function ($formatted_array) {
            $row = $this->find($formatted_array['id']);
            if ($row === null) $this->create($formatted_array);
            else $row->update($formatted_array);
            return $this->find($formatted_array['id']);
        });
        Builder::macro('createOrUpdateWithoutGlobalScopes', function ($formatted_array) {
            $row = $this->getModel()->WithoutGlobalScopes()->find($formatted_array['id']);
            if ($row === null) $this->create($formatted_array);
            else $row->update($formatted_array);
            return $this->find($formatted_array['id']);
        });
        Builder::macro('createOrUpdateArrayWithoutGlobalScopes', function ($formatted_array) {
            $newModels = [];
            collect($formatted_array)->map(function ($formatted) use ($newModels) {
                $newModels[] = $this->createOrUpdateWithoutGlobalScopes($formatted);
            });
            return collect($newModels);
        });
        Builder::macro('tabelStatus', function () {
            return DB::select("show table status like '" . $this->getModel()->getTable() . "'")[0];
        });
        Builder::macro('NewId', function () {
            return $this->tabelStatus()->Auto_increment;
        });

        Component::macro('macroNotify', function ($message) {
            $this->dispatchBrowserEvent('notify', $message);
        });

        Builder::macro('whereMacroSearch', function ($field, $string) {
            return $string ? $this->where($field, 'like', '%' . $string . '%') : $this;
        });
        Builder::macro('orWhereMacroSearch', function ($field, $string) {
            return $string ? $this->orWhere($field, 'like', '%' . $string . '%') : $this;
        });
    }
}
