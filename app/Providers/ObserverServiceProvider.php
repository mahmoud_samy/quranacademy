<?php

namespace App\Providers;

use App\Models\instructor;
use App\Models\student;
use App\Models\SubInstructor;
use App\Models\User;
use App\Observers\InstructorObserver;
use App\Observers\StudentObserver;
use App\Observers\SubInstructorObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        instructor::observe(InstructorObserver::class);
        SubInstructor::observe(SubInstructorObserver::class);
        student::observe(StudentObserver::class);
    }
}
