<?php

namespace App\Services;

use Laravel\Passport\Passport;
use League\OAuth2\Server\CryptKey;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Bridge\ScopeRepository;
use Laravel\Passport\Bridge\ClientRepository;
use League\OAuth2\Server\Grant\AbstractGrant;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\RefreshTokenRepository;

class BaseAuthClass extends AbstractGrant
{

    public function respondToAccessTokenRequest($request, $responseType, $accessTokenTTL)
    {
    }

    public function __construct()
    {

        $this->ScopeRepository    = new ScopeRepository();

        $this->setScopeRepository(app()->make(ScopeRepository::class));

        $this->setAccessTokenRepository(app()->make(AccessTokenRepository::class));

        $this->setRefreshTokenRepository(app()->make(RefreshTokenRepository::class));

        $this->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        $this->setPrivateKey(new CryptKey('file://' . Passport::keyPath('oauth-private.key'), null, false));

        $this->setEncryptionKey(app('encrypter')->getKey());
    }

    public function getIdentifier()
    {
        return 'password';
    }

    public function makeBearerToken($user, $type)
    {

        $this->clients_id = DB::table('oauth_clients')
            ->where('password_client', 1)
            ->where('provider', $type)
            ->get()
            ->first()
            ->id;

        $this->Client = app()->make(ClientRepository::class)->getClientEntity($this->clients_id);

        $finalizedScopes = $this->ScopeRepository->finalizeScopes(
            $this->validateScopes([$type]),
            $this->getIdentifier(),
            $this->Client,
            $user->id
        );

        $token = $this->issueAccessToken(Passport::tokensExpireIn(), $this->Client, $user->id, $finalizedScopes);

        $refreshToken = $this->issueRefreshToken($token);

        $expireDateTime = $token->getExpiryDateTime()->getTimestamp() - \time();

        $refreshTokenPayload = json_encode([
            'client_id'            => $token->getClient()->getIdentifier(),
            'refresh_token_id'    => $refreshToken->getIdentifier(),
            'access_token_id'    => $token->getIdentifier(),
            'scopes'            => $token->getScopes(),
            'user_id'            => $token->getUserIdentifier(),
            'expire_time'        => $refreshToken->getExpiryDateTime()->getTimestamp(),
        ]);

        return [
            'token_type'    => 'Bearer',
            'expires_in'    => $expireDateTime,
            'access_token'    => (string) $token,
            'refresh_token'    => $this->encrypt($refreshTokenPayload)
        ];
    }
}
