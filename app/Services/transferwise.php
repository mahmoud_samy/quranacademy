<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\PendingRequest;

class transferwise
{

    const API_URL  = 'https://api.sandbox.transferwise.tech/';
    const CURRENCY = 'USD';

    /* ** start http      functions ** */

    public static function HttpWithHeaders(): PendingRequest
    {
        return Http
            ::withOptions(['base_uri'     => transferwise::API_URL])
            ->withHeaders(['Content-Type' => 'application/json'])
            ->withToken('505611f8-809c-49f2-8608-a029fbca3c33');
    }

    public static function get(string $url, array $data = []): Collection
    {
        return static::HttpWithHeaders()->get($url, $data)->collect();
    }

    public static function post(string $url, array $data = [], array $Headers = [], string $method = 'collect')
    {
        return static::HttpWithHeaders()->withHeaders($Headers)->post($url, $data)->$method();
    }

    /* ** end   http      functions ** */

    /* ** start serves    functions ** */
    public static function result(string $message, string $code, bool $check = false, string $arguments_name = '', string $arguments_value = '', array $data = []): array
    {
        return [
            'message'         => $message,
            'title'           => $code,
            'check'           => $check,
            'arguments_name'  => $arguments_name,
            'arguments_value' => $arguments_value,
            'data'            => $data,
        ];
    }
    public static function resultError(array $error): array
    {
        return static::result($error['message'], $error['code'], false, $error['arguments'][0], $error['arguments'][1] ?? '');
    }
    public static function sign(string $col): string
    {
        $key = openssl_pkey_get_private(file_get_contents(storage_path(env('TRANSFERWISE_KEY'))));
        return openssl_sign($col, $Xsignature, $key, OPENSSL_ALGO_SHA256) ? base64_encode($Xsignature) : '';
    }
    /* ** end   serves    functions ** */

    /* ** start get       functions ** */

    public static function getCountries(): Collection
    {
        return collect(static::get('v1/countries')['values']);
    }

    public static function getProfiles(string $type = 'business'): array
    {
        return static::get('v1/profiles')->where('type', $type)->first();
    }

    public static function getBalance(Int $profiles_id, string $Currency = transferwise::CURRENCY): array
    {
        return collect(static::get('v1/borderless-accounts', ['profileId' => $profiles_id])->first()['balances'])->where('currency', $Currency)->first();
    }

    public static function getTransfers(Int $offset = 0, Int $limit = 100): array
    {
        return static::get('v1/transfers', [
            'offset'  => $offset,
            'limit'   => $limi,
            'profile' => static::getProfiles()['id'],
        ]);
    }

    public static function getTransfersById(Int $transfers_id): Collection
    {
        return static::get('v1/transfers/' . $transfers_id);
    }

    public static function estimatedDeliveryDate(Int $transfers_id): string
    {
        return static::get('v1/delivery-estimates/' . $transfers_id)['estimatedDeliveryDate'];
    }

    /* ** end   get       functions ** */

    /* ** start CreateNew functions ** */

    public static function CreateNewQuotes(Int $profile_id, Int $sourceAmount, string $sourceCurrency = transferwise::CURRENCY, string $targetCurrency = transferwise::CURRENCY): Collection
    {
        return static::post('v2/quotes', [
            'profile'        => $profile_id,
            'sourceAmount'   => $sourceAmount,
            'sourceCurrency' => $sourceCurrency,
            'targetCurrency' => $targetCurrency,
        ])->except(['paymentOptions']);
    }

    public static function CreateNewAccounts(Int $profile_id, string $accountHolderName, array $details = [], string $type = 'aba', string $Currency = transferwise::CURRENCY): Collection
    {
        return static::post('v1/accounts', [
            'profile'           => $profile_id,
            'accountHolderName' => $accountHolderName,
            'type'              => $type,
            'currency'          => $Currency,
            'details'           => $details,
        ]);
    }

    public static function CreateNewAccountsWithUSD(Int $profile_id, string $accountHolderName, string $abartn, string $accountNumber, string $accountType, string $postCode, string $address, string $city, string $country): Collection
    {
        return static::CreateNewAccounts($profile_id, $accountHolderName, [
            'legalType'     => 'PRIVATE',
            'abartn'        => $abartn,
            'accountNumber' => $accountNumber,
            'accountType'   => $accountType,
            'address'       => [
                'postCode'  => $postCode,
                'firstLine' => $address,
                'city'      => $city,
                'country'   => $country,
            ]
        ]);
    }

    public static function CreateNewTransfers(Int $accounts_id, string $quotes_id): Collection
    {
        return static::post('v1/transfers', [
            'targetAccount'         => $accounts_id,
            'quoteUuid'             => $quotes_id,
            'customerTransactionId' => \Str::uuid()
        ]);
    }

    public static function CreateNewFund(Int $profiles_id, Int $transfers_id): Collection
    {
        $x2faApproval = static::post('v3/profiles/' . $profiles_id . '/transfers/' . $transfers_id . '/payments', ['type' => 'BALANCE'], [], 'headers')['x-2fa-approval'][0];
        return          static::post('v3/profiles/' . $profiles_id . '/transfers/' . $transfers_id . '/payments', ['type' => 'BALANCE'], [
            'x-2fa-approval' =>              $x2faApproval,
            'X-Signature'    => static::sign($x2faApproval),
        ]);
    }

    /* ** end   CreateNew functions ** */

    /* ** start last      functions ** */

    public static function NewTransfersUSD(Int $Amount, string $accountHolderName, string $abartn, string $accountNumber, string $accountType, string $postCode, string $address, string $city, string $country): array
    {
        $array = [];
        $array['profiles'] = static::getProfiles();
        if (static::getBalance($array['profiles']['id'])['amount']['value'] < $Amount) return static::result('we don\'t have Balance to fund this request', 'err_Balance');
        $array['accounts'] = static::CreateNewAccountsWithUSD(
            $array['profiles']['id'],
            $accountHolderName,
            $abartn,
            $accountNumber,
            $accountType,
            $postCode,
            $address,
            $city,
            $country
        )->toArray();
        if ($array['accounts']['errors'] ?? false) return static::resultError($array['accounts']['errors'][0]);
        $array['quotes'] = static::CreateNewQuotes($array['profiles']['id'], $Amount)->toArray();
        $array['transfers'] = static::Createnewtransfers($array['accounts']['id'], $array['quotes']['id'])->toArray();
        $array['fund'] = static::CreateNewFund($array['profiles']['id'], $array['transfers']['id'])->toArray();
        $array['estimatedDeliveryDate'] = static::estimatedDeliveryDate($array['transfers']['id']);
        return static::result('', '', true, '', '', $array);
    }

    /* ** end   last      functions ** */
}
