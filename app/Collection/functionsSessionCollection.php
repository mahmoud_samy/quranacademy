<?php

namespace App\Collection;

use App\Models\session;

class functionsSessionCollection extends functionsCollection
{

    public function CanBook(): functionsSessionCollection
    {
        return $this->filter(fn (session $model) => !$model->HasConflictInBook);
    }

    public function book(array $arrayOfNotes = []): functionsSessionCollection
    {
        foreach ($arrayOfNotes as $arrayOfNote) $array[$arrayOfNote['id']] = $arrayOfNote['note'] ?? null;
        return $this->each(fn (session $model) => $model->book($array[$model->id]));
    }

    public function oneHourBeforeNotify(): functionsSessionCollection
    {
        return $this->each(fn (session $model) => $model->oneHourBeforeNotify());
    }

    public function fiveMinutesBeforeNotify(): functionsSessionCollection
    {
        return $this->each(fn (session $model) => $model->fiveMinutesBeforeNotify());
    }
}
