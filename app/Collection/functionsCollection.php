<?php

namespace App\Collection;

use Illuminate\Database\Eloquent\Collection;

class functionsCollection extends Collection
{

    public function delete()
    {
        return $this->each(fn ($model) => $model->delete());
    }

    public function beActive()
    {
        return $this->each(fn ($model) => $model->beActive());
    }

    public function disActive()
    {
        return $this->each(fn ($model) => $model->disActive());
    }

    public function BeSeen()
    {
        return $this->each(fn ($model) => $model->BeSeen());
    }
}
