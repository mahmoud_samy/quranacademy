<?php

namespace App\Collection;

use Illuminate\Notifications\DatabaseNotificationCollection as NotificationCollectio;

class DatabaseNotificationCollection extends NotificationCollectio
{

    public function delete()
    {
        $this->each(function ($model) {
            return $model->delete();
        });
    }
}
