<?php

namespace App\Observers;

use App\Models\SubInstructor;

class SubInstructorObserver
{
    /**
     * Handle the SubInstructor "created" event.
     *
     * @param  \App\Models\SubInstructor  $subInstructor
     * @return void
     */
    public function created(SubInstructor $subInstructor)
    {
        //
    }

    /**
     * Handle the SubInstructor "updated" event.
     *
     * @param  \App\Models\SubInstructor  $subInstructor
     * @return void
     */
    public function updated(SubInstructor $subInstructor)
    {
        //
    }

    /**
     * Handle the SubInstructor "deleting" event.
     *
     * @param  \App\Models\SubInstructor $subInstructor
     * @return void
     */
    public function deleting(SubInstructor $subInstructor)
    {
        $subInstructor->deleteProfilePhoto();
    }

    /**
     * Handle the SubInstructor "deleted" event.
     *
     * @param  \App\Models\SubInstructor  $subInstructor
     * @return void
     */
    public function deleted(SubInstructor $subInstructor)
    {
        //
    }

    /**
     * Handle the SubInstructor "restored" event.
     *
     * @param  \App\Models\SubInstructor  $subInstructor
     * @return void
     */
    public function restored(SubInstructor $subInstructor)
    {
        //
    }

    /**
     * Handle the SubInstructor "force deleted" event.
     *
     * @param  \App\Models\SubInstructor  $subInstructor
     * @return void
     */
    public function forceDeleted(SubInstructor $subInstructor)
    {
        //
    }
}
