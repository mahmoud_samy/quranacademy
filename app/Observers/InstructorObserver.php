<?php

namespace App\Observers;

use App\Models\instructor;
use Illuminate\Database\Eloquent\Builder;
use App\Notifications\Instructor\createdButNotcompleted;
use App\Notifications\SubInstructor\createdButNotcompleted as SubInstructorCreatedButNotcompleted;

class InstructorObserver
{
    /**
     * Handle the instructor "created" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function created(instructor $instructor)
    {
        switch (get_class($instructor)) {
            case instructor::class:
                $instructor->notifyNow(new createdButNotcompleted($instructor));
                break;
            case SubInstructor::class:
                $instructor->notifyNow(new SubInstructorCreatedButNotcompleted($instructor));
                break;
        }
    }

    /**
     * Handle the instructor "updated" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function updated(instructor $instructor)
    {
        //
    }

    /**
     * Handle the instructor "updating" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function updating(instructor $instructor)
    {
        if ($instructor->isDirty('rate_15_min')) {
            $instructor->sessions()
                ->whereNull('student_id')
                ->whereHas('rate_minutes_type', function (Builder $query) {
                    $query->where('attr', 'rate_15_min');
                })
                ->update([
                    'rate_salary' => $instructor->rate_15_min
                ]);
        }

        if ($instructor->isDirty('rate_30_min')) {
            $instructor->sessions()
                ->whereNull('student_id')
                ->whereHas('rate_minutes_type', function (Builder $query) {
                    $query->where('attr', 'rate_30_min');
                })
                ->update([
                    'rate_salary' => $instructor->rate_30_min
                ]);
        }

        if ($instructor->isDirty('rate_60_min')) {
            $instructor->sessions()
                ->whereNull('student_id')
                ->whereHas('rate_minutes_type', function (Builder $query) {
                    $query->where('attr', 'rate_60_min');
                })
                ->update([
                    'rate_salary' => $instructor->rate_60_min
                ]);
        }
    }

    /**
     * Handle the instructor "deleted" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function deleted(instructor $instructor)
    {
        //
    }

    /**
     * Handle the instructor "deleting" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function deleting(instructor $instructor)
    {
        $instructor->deleteProfilePhoto();
        $instructor->deleteProfileVideo();
    }

    /**
     * Handle the instructor "restored" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function restored(instructor $instructor)
    {
        //
    }

    /**
     * Handle the instructor "force deleted" event.
     *
     * @param  \App\Models\instructor  $instructor
     * @return void
     */
    public function forceDeleted(instructor $instructor)
    {
        //
    }
}
