<?php

namespace App\Observers;

use App\Models\image;
use App\Models\student;
use Illuminate\Support\Facades\Storage;

class StudentObserver
{
    /**
     * Handle the student "created" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function created(student $student)
    {
        //
    }

    /**
     * Handle the student "updated" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function updated(student $student)
    {
        //
    }

    /**
     * Handle the student "deleted" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function deleted(student $student)
    {
        //
    }

    /**
     * Handle the student "deleting" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function deleting(student $student)
    {
        $student->deleteProfilePhoto();
    }

    /**
     * Handle the student "restored" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function restored(student $student)
    {
        //
    }

    /**
     * Handle the student "force deleted" event.
     *
     * @param  \App\Models\student  $student
     * @return void
     */
    public function forceDeleted(student $student)
    {
        //
    }
}
