<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'array',
        'is_active' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'flag',
        'locale_name'
    ];

    /**
     * @return String
     */
    public function getLocaleNameAttribute()
    {
        return $this->name[app()->getLocale()] ?? '-';
    }

    /**
     * @return String
     */
    public function getFlagAttribute()
    {
        return asset('images/vendor/flag-icon-css/flags/png/1x1/' . strtolower($this->iso_code) . '.png');
    }

    /**
     * @param $query
     * @return
     */
    public function scopeFilterByActive($query)
    {
        return $query->where('is_active', 1);
    }
}
