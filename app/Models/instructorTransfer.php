<?php

namespace App\Models;

use App\Services\transferwise;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class instructorTransfer extends Model
{

    use HasFactory;

    protected $fillable = [
        'quotes',
        'accounts',
        'transfers',
        'fund',
        'amount',
        'status',
        'estimatedDeliveryDate',
        'is_active',
    ];

    protected $casts = [
        'quotes'                => 'array',
        'accounts'              => 'array',
        'transfers'             => 'array',
        'fund'                  => 'array',
        'created_at'            => 'datetime:Y-m-d H:i:s',
        'updated_at'            => 'datetime:Y-m-d H:i:s',
        'estimatedDeliveryDate' => 'datetime:Y-m-d H:i:s',
    ];

    public function getstepAttribute()
    {
        if (env('APP_ENV') === 'testing') {
            transferwise::get('v1/simulation/transfers/' . $this->transfers['id'] . '/processing');
            transferwise::get('v1/simulation/transfers/' . $this->transfers['id'] . '/funds_converted');
        };
        return str_replace('_', ' ', transferwise::getTransfersById($this->transfers['id'])['status']);
    }

    public function getaccountHolderNameAttribute()
    {
        return $this->accounts['accountHolderName'] ?? null;
    }

    public function getabartnAttribute()
    {
        return $this->accounts['details']['abartn'] ?? null;
    }

    public function getaccountNumberAttribute()
    {
        return $this->accounts['details']['accountNumber'] ?? null;
    }

    public function getaccountTypeAttribute()
    {
        return $this->accounts['details']['accountType'] ?? null;
    }

    public function getpostCodeAttribute()
    {
        return $this->accounts['details']['address']['postCode'] ?? null;
    }

    public function getaddressAttribute()
    {
        return $this->accounts['details']['address']['firstLine'] ?? null;
    }

    public function getcityAttribute()
    {
        return $this->accounts['details']['address']['city'] ?? null;
    }

    public function getcountryAttribute()
    {
        return $this->accounts['details']['address']['country'] ?? null;
    }
}
