<?php

namespace App\Models;

use App\Services\Functions;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class image extends Model
{

    use HasFactory;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'url',
    ];

    public static function fileName()
    {
        return 'images';
    }

    public static function Storage()
    {
        return Storage::disk(static::fileName());
    }

    protected static function boot()
    {

        parent::boot();

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }

    protected $fillable = [
        'uplouder_type',
        'uplouder_id',
        'image_type_id',
        'width',
        'height',
        'size',
    ];

    public function geturlAttribute()
    {
        if ($this->link !== null) return url($this->link . '/' . Functions::hashThing(['id' => $this->id]));
        else return route('image_Real_path', [
            'image_id'   => Functions::hashThing(['id' => $this->id])
        ]);
    }

    public function rel_type()
    {
        return $this->belongsTo(imageType::class, 'image_type_id');
    }

    public function getTypeAttribute()
    {
        return $this->rel_type->mime_type;
    }

    public function getrealPathAttribute()
    {
        return static::Storage()->path($this->id);
    }

    public function getowner($Instructor)
    {
        $this->uplouder_type = get_class($Instructor);
        $this->uplouder_id = $Instructor->id;
        $this->save();
        return $this;
    }

    public static function createFromFile(UploadedFile $file)
    {
        //$file->storePublicly('public/images');

        $id = self::getNextId();

        static::Storage()->putFileAs('', $file, $id);

        [0 => $width, 1 => $height, 'mime' => $mime] = getimagesize(static::Storage()->path($id));

        $model = static::query()->create([
            'uplouder_type' => instructor::class,
            'uplouder_id'   => 0,
            'image_type_id' => imageType::idByMimeType($mime),
            'width'         => $width,
            'height'        => $height,
            'size'          => $file->getSize(),
        ]);

        return $model;
    }

    public static function getNextId()
    {
        return static::max('id') + 1;
    }
}
