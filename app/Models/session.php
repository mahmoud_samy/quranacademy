<?php

namespace App\Models;

use App\Traits\Scopes\sessionScope;
use App\Traits\Methods\sessionMethods;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Relations\sessionRelations;
use App\Traits\Attributes\sessionAttribute;
use App\Collection\functionsSessionCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class session extends Model
{

    use
        sessionRelations,
        sessionScope,
        HasFactory,
        sessionAttribute,
        sessionMethods;

    protected $table = 'sessions';

    protected $fillable = [
        'instructor_id',
        'rate_minutes_type_id',
        'date',
        'time',
        'course_type_id',
        'rate_salary',
        'currency_type_id',
        'start_at',
        'end_at',
    ];

    protected $casts = [
        'options'    => 'array',
        'time'       => 'datetime:H:i:s',
        'date'       => 'datetime:Y-m-d',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'DateTime'   => 'datetime:Y-m-d H:i:s',
        'cancel_at'  => 'datetime:Y-m-d H:i:s',
        'start_at'  => 'datetime',
        'end_at'  => 'datetime',
    ];

    public const BREAK_TIME_BETWEEN_SESSIONS = 5;

    public const STATUS_FUTURE = 1;
    public const STATUS_PROGRESS = 2;
    public const STATUS_PAST = 3;

    public const STATUSES = [
        self::STATUS_FUTURE => 'Future',
        self::STATUS_PROGRESS => 'Progress',
        self::STATUS_PAST => 'Past',
    ];

    protected static function boot(): void
    {

        parent::boot();

        self::creating(function ($model) {
            $model->options = [];
        });
    }

    public function newCollection(array $models = []): functionsSessionCollection
    {
        return new functionsSessionCollection($models);
    }
}
