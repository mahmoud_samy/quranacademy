<?php

namespace App\Models;

use Laravel\Passport\Token;
use Illuminate\Database\Eloquent\Builder;

use App\Collection\functionsCollection;

class OauthAccessToken extends Token
{

    protected $fillable = [
        'id',
        'user_id',
        'client_id',
        'name',
        'scopes',
        'revoked',
        'expires_at',
        'created_at',
        'updated_at',
        'firebaseId',
    ];

    protected static function boot()
    {

        parent::boot();

        static::addGlobalScope('Ordered', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }

    public function newCollection(array $models = [])
    {
        return new functionsCollection($models);
    }

    public function edit(array $array)
    {
        $this->update($array);
        return $this->refresh();
    }

    public function user()
    {

        $model = config('auth.providers.' . $this->scopes[0] . '.model');

        return $this->belongsTo($model, 'user_id', (new $model)->getKeyName());
    }
}
