<?php

namespace App\Models;

use App\Scopes\ActiveInstructorScope;
use App\Traits\HasPromocode;
use App\Traits\HasProfileVideo;
use Illuminate\Support\Facades\DB;
use App\Traits\Models\Authenticatable;
use App\Traits\Scopes\instructorScope;
use App\Traits\Methods\InstructorMethods;
use App\Traits\Relations\InstructorRelations;

class instructor extends Authenticatable
{

    use InstructorRelations;
    use InstructorMethods;
    use instructorScope;
    use HasProfileVideo;
    use HasPromocode;

    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'gender',
        'Locale',
        'date_of_birth',
        'password',
        'years_of_experience',
        'description',
        'is_profile_public',
        'rate_15_min',
        'rate_30_min',
        'rate_60_min',
        'courses_types_id',
        'student_gender_types_id',
        'student_age_types_id',
        'currency_type_id',
        'profile_image_id',
        'profile_vidoe_id',
        'is_Approval',
        'country_id',
        'public_rate_minutes_type_id',
    ];

    protected $appends = [
        'profile_video_url'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new ActiveInstructorScope());
    }

    public function getaverageRatingsAttribute(): int
    {
        return round(static::getRating()->find($this->id)->ratings_average);
    }

    public function gettotalStudentsCounterAttribute(): int
    {
        return (int)$this->students()->count();
    }

    public function gettotalProfileViewsCounterAttribute(): int
    {
        return (int)$this->instructorView()->count();
    }

    public function gettotalEarningCounterAttribute(): int
    {
        return (int)$this
            ->sessions()
            ->canEarning()
            ->select(DB::raw(' SUM( `rate_salary` ) as total_earnings'))
            ->first()
            ->total_earnings;
    }

    public function getpublicRateMinutesAttribute(): int
    {
        return ($this)[$this->public_rate_minutes_type->attr ?? 0] ??
            $this->rate_15_min ??
            $this->rate_30_min ??
            $this->rate_60_min ??
            0;
    }

    public function gettotalSessionsAskedUnSeenAttribute(): int
    {
        return $this
            ->sessions()
            ->addSelect(
                DB::raw('( select count( * )' .
                    ' from ' . with(new session_message)->getTable() .
                    ' where ' . with(new session_message)->getTable() . '.session_id = ' . with(new session)->getTable() . '.id' .
                    ' and ' . with(new session_message)->getTable() . '.seen_at is null ' .
                    ' and ' . with(new session_message)->getTable() . '.student_id is not null ' .
                    ' and ' . with(new session)->getTable() . '.id is not null ' .
                    ') as asking')
            )
            ->having('asking', '>', 0)
            ->get()
            ->count();
    }

    public function getfireBaseCloudMessageingAttribute(): array
    {
        return $this->FireBaseCloudMessageingAttributesBase() + [
                "total_sessions_asked_un_seen" => $this->total_sessions_asked_un_seen,
                "total_students_counter" => $this->total_students_counter,
                "total_profile_views_counter" => $this->total_profile_views_counter,
                "total_earning_counter" => $this->total_earning_counter,
            ];
    }

    public function getcurrentBalanceAttribute(): int
    {
        return $this->totalEarningCounter - (int)$this
                ->transfers()
                ->select(DB::raw(' SUM( `amount` ) as total_amount'))
                ->first()
                ->total_amount;
    }
}
