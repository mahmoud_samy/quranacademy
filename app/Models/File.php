<?php

namespace App\Models;

use App\Services\Functions;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class File extends Model
{
    protected $fillable = [
        'instructor_id',
        'type',
        'size',
        'name',
        'extension',
        'to_id',
        'to_type',
        'mime_type',
    ];

    public static function fileName()
    {
        return 'Files';
    }

    public static function Storage()
    {
        return Storage::disk(static::fileName());
    }

    protected static function boot()
    {

        parent::boot();

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }

    public function geturlAttribute()
    {
        switch ($this->type) {
            case 'video':
                return route('video_Real_path', ['video_id' => Functions::hashThing(['id' => $this->id])]);
                break;
            default:
                $path = Storage::disk(self::fileName())->url("Files/" . $this->id);
                return asset($path);
                break;
        }
    }

    public function getrealPathAttribute()
    {
        return static::Storage()->path($this->id);
    }

    public function getowner(instructor $Instructor, $type = 'video')
    {
        $this->type          = $type;
        $this->instructor_id = $Instructor->id;
        $this->save();
        return $this;
    }

    public static function createFromFile(UploadedFile $file)
    {

        //$file->storePublicly('public/Files');

        $id = self::getNextId();
        static::Storage()->putFileAs('', $file, $id);

        $model = static::query()->create([
            'type'      => 'type',
            'size'      => $file->getSize(),
            'extension' => $file->getExtension(),
            'mime_type' => $file->getClientMimeType(),
            'name'      => $file->getClientOriginalName(),
        ]);

        return $model;
    }

    public function replace(UploadedFile $file)
    {

        //$file->storePublicly('public/Files');

        $id = self::getNextId();
        static::Storage()->putFileAs('', $file, $id);

        $this->update([
            'size'      => $file->getSize(),
            'extension' => $file->getExtension(),
            'mime_type' => $file->getClientMimeType(),
            'name'      => $file->getClientOriginalName(),
        ]);

        return $this->refresh();
    }

    public static function getNextId()
    {
        return static::max('id') + 1;
    }
}
