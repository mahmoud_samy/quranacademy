<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstructorLevel extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($level) {
            $instructors = instructor::where('instructor_level_id', $level->id)->get();
            foreach ($instructors as $instructor) {
                $instructor->instructor_level_id = null;
                $instructor->save();
            }

            $plans = Plan::where('instructor_level_id', $level->id)->get();
            foreach ($plans as $plan) {
                $plan->instructor_level_id = null;
                $plan->save();
            }
        });

    }


    public function instructors()
    {
        return $this->hasMany(instructor::class,'instructor_level_id','id');
    }

}
