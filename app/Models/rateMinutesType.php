<?php

namespace App\Models;

use App\Traits\Models\Type;

class rateMinutesType extends Type
{

    public function getRateByMinutesAttribute()
    {
        return (int) $this->context;
    }
}
