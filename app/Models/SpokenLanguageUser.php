<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpokenLanguageUser extends Model
{

    protected $fillable = [
        'language_types_id',
    ];

    public function languageType()
    {
        return $this->belongsTo(languageType::class);
    }
}
