<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class privacyPolicy extends Model
{

    public function getPublic($root, $args)
    {
        return static::where('is_active', 1)
            ->where('is_visible', 1)
            ->where('lang', app()->getLocale())
            ->get();
    }
}
