<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Promocode extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'code',
        'type',
        'related_to',
        'description',
        'reward',
        'extra_info',
        'is_disposable',
        'expires_at',
    ];

    protected $casts = [
        'extra_info' => 'array',
        'is_disposable' => 'boolean'
    ];

    protected $appends = [
        'type_label',
        'disposable_status_label',
        'disposable_status_color',
    ];

    // related model
    public const RELATED_TO_FIRST_SESSEION = 1;
    public const RELATED_TO_MULTIPLE_SESSEION = 2;

    // related models
    public const RELATED_TO = [
        self::RELATED_TO_FIRST_SESSEION => [
            'label' => 'First Session',
            'value' => 'FIRST_SESSION',
        ],
        self::RELATED_TO_MULTIPLE_SESSEION => [
            'label' => 'Multiple Sessions',
            'value' => 'MULTIPLE_SESSION',
        ],
    ];

    // type
    public const TYPE_AMOUNT = 1;
    public const TYPE_PERCENTAGE = 2;

    // types
    public const TYPES = [
        self::TYPE_AMOUNT => 'Amount',
        self::TYPE_PERCENTAGE => 'Percentage',
    ];

    /**
     * type_label function
     *
     * @return string
     */
    public function getTypeLabelAttribute()
    {
        return self::TYPES[$this->type] ?? '-';
    }

    /**
     * disposable_status_label function
     *
     * @return string
     */
    public function getDisposableStatusLabelAttribute()
    {
        return [
            1 => 'One use',
            0 => 'Multible use',
        ][$this->is_disposable] ?? 'One use';
    }

    /**
     * disposable_status_color function
     *
     * @return string
     */
    public function getDisposableStatusColorAttribute()
    {
        return [
            1 => 'green',
            0 => 'red',
        ][$this->is_disposable] ?? 'green';
    }

    /**
     * Get all of the students that have this promocode.
     */
    public function students()
    {
        return $this->morphedByMany(student::class, 'relatable', 'promocode_user');
    }

    /**
     * Get all of the instructors that have this promocode.
     */
    public function instructors()
    {
        return $this->morphedByMany(instructor::class, 'relatable', 'promocode_user');
    }
}
