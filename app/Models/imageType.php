<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class imageType extends Model
{

    protected $fillable = [
        'mime_type',
        'extension',
        'name',
        'is_active',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {

        parent::boot();

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }

    public function scopeidByMimeType($query, $value)
    {
        return $query->where('mime_type', $value)->first()->id;
    }

    public function scopeidByDataFormat($query, $value)
    {
        return $query->where('name', $value)->first()->id;
    }
}
