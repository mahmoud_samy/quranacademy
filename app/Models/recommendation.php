<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class recommendation extends Model
{

    protected $fillable = [
        'instructor_id',
        'file_id',
    ];

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
