<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rating extends Model {

    use HasFactory;

    protected $fillable = [
        'student_id'    ,
        'instructor_id' ,
        'rating'        ,
        'reviews'       ,
    ];

    public function instructor( ) {
        return $this -> belongsTo( instructor :: class ) ;
    }

    public function student( ) {
        return $this -> belongsTo( student    :: class ) ;
    }

    public function session( ) {
        return $this -> belongsTo( session    :: class ) ;
    }

}
