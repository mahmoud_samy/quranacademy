<?php

namespace App\Models;

use App\Traits\Models\Authenticatable;
use App\Notifications\Student\create;
use App\Traits\HasPromocode;

class student extends Authenticatable
{
    use HasPromocode;

    public const CHILDREN_AGE = 12;

    protected static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $model->notifyNow(new create($model));
        });
    }

    public function getSessionInThisWeekQuery()
    {
        $date = now()->modify('saturday this week')->subWeek();
        return $this
            ->sessions()
            ->whereNull('cancel_at')
            ->whereNull('delete_for_students')
            ->BetweenDates($date, $date->clone()->addWeek()->subDay());
    }

    public function getSessionInThisWeekAttribute()
    {
        return $this->getSessionInThisWeekQuery()->get();
    }

    public function getCountThisWeekStudyHoursAttribute()
    {
        return $this->SessionInThisWeek->where('DateTime', '<', now())->map(function ($session) {
                return $session->rate_minutes_type->RateByMinutes;
            })->sum() / 60;
    }

    public function getCountThisWeekSessionScheduleAttribute()
    {
        return (int)$this->SessionInThisWeek->where('DateTime', '>', now())->count();
    }

    public function getCountThisWeekSessionAttribute()
    {
        return (int)$this->SessionInThisWeek->count();
    }

    public function getfireBaseCloudMessageingAttribute(): array
    {
        return $this->FireBaseCloudMessageingAttributesBase() + [
            "count_this_week_study_hours"      => $this->CountThisWeekStudyHours,
            "count_this_week_session_schedule" => $this->CountThisWeekSessionSchedule,
            "count_this_week_session"          => $this->CountThisWeekSession,
        ];
    }

    public function PlanSubscription()
    {
        return $this->hasOne(Subscription::class, 'user_id')
            ->where('expire_at', '>', now())
            ->where('active',1);
    }

}
