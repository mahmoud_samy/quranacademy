<?php

namespace App\Models;

use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{

    protected $primaryKey = 'email';

    public $incrementing = false;

    protected $guarded = [];

    public static $tokensExpireIn = 0;

    public static $LengthPinCodeToken = 0;

    protected $fillable = [
        'email',
        'token',
        'created_at',
        'updated_at',
    ];

    public static function tokensExpireIn($time)
    {
        static::$tokensExpireIn = $time;
    }

    public static function setLengthPinCodeToken($length)
    {
        static::$LengthPinCodeToken = $length;
    }

    public function getNewToken($type = 'emailview', $Subject = 'Quran Academy Verify Mail')
    {
        $pincode = rand(pow(10, static::$LengthPinCodeToken - 1), pow(10, static::$LengthPinCodeToken) - 1);

        Mail::send($type, [
            'email'        => $this->email,
            'pincode'    => $pincode
        ], function ($message) use ($Subject) {
            $message->to($this->email)->subject($Subject);
        });

        $this->token = app('encrypter')->encrypt($pincode);
        $this->save();

        return $this;
    }

    public function IsActive()
    {

        return ($this->updated_at->addMinutes(variabledb::val('EXPIRE_PIN_CODE_TOKEN_BY_MINUTES')) > now()) ? true : false;
    }

    public function IsMatch(int $pinCode)
    {

        return ($pinCode === app('encrypter')->decrypt($this->token)) ? true : false;
    }

    public function isValid(int $pinCode)
    {

        $this->pinCodeIsMatch = $this->IsMatch($pinCode);

        $this->pinCodeIsActive = $this->IsActive();

        $this->pinCodeisValid = $this->pinCodeIsMatch && $this->pinCodeIsActive;

        return $this;
    }
}
