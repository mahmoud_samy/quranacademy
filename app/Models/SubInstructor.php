<?php

namespace App\Models;

use App\Traits\Models\Authenticatable;

class SubInstructor extends instructor
{

    protected $appends = ['languages'];

    protected $hidden = [];

    public function updateProfile(array $args)
    {

        $this->updateBaseData($args);

        if (array_key_exists('Spoken_Language_User', $args) && !is_null($args['Spoken_Language_User'])) $this->update_Spoken_Language_User($args['Spoken_Language_User']);
        if (array_key_exists('rate_15_min', $args) && !is_null($args['rate_15_min'])) $this->rate_15_min                 = $args['rate_15_min'];
        if (array_key_exists('rate_30_min', $args) && !is_null($args['rate_30_min'])) $this->rate_30_min                 = $args['rate_30_min'];
        if (array_key_exists('rate_60_min', $args) && !is_null($args['rate_60_min'])) $this->rate_60_min                 = $args['rate_60_min'];
        if (array_key_exists('currency_type_id', $args) && !is_null($args['currency_type_id'])) $this->currency_type_id            = $args['currency_type_id'];
        if (array_key_exists('student_gender_types_id', $args) && !is_null($args['student_gender_types_id'])) $this->student_gender_types_id     = $args['student_gender_types_id'];
        if (array_key_exists('student_age_types_id', $args) && !is_null($args['student_age_types_id'])) $this->student_age_types_id        = $args['student_age_types_id'];
        if (array_key_exists('years_of_experience', $args) && !is_null($args['years_of_experience'])) $this->years_of_experience         = $args['years_of_experience'];
        if (array_key_exists('description', $args) && !is_null($args['description'])) $this->description                 = $args['description'];
        if (array_key_exists('country_id', $args) && !is_null($args['country_id'])) $this->country_id                  = $args['country_id'];
        if (array_key_exists('courses_types_id', $args) && !is_null($args['courses_types_id'])) $this->courses_types_id            = $args['courses_types_id'];
        if (array_key_exists('public_rate_minutes_type_id', $args) && !is_null($args['public_rate_minutes_type_id'])) $this->public_rate_minutes_type_id = $args['public_rate_minutes_type_id'];
        $this->save();

        return $this;
    }

    public function getfireBaseCloudMessageingAttribute(): array
    {
        return $this->FireBaseCloudMessageingAttributesBase();
    }
}
