<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class instructorView extends Model
{

    protected $fillable = [
        'instructor_id',
        'student_id'
    ];
}
