<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class term extends Model
{
    protected $fillable = [
        'header',
        'body',
        'lang',
        'is_active',
        'is_visible',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'active_status_label',
        'active_status_color',
        'active_visible_label',
        'active_visible_color',
    ];

    // active status
    public const STATUS_ACTIVE = 1;
    public const STATUS_INACTIVE = 0;

    // visible status
    public const STATUS_VISIBLE = 1;
    public const STATUS_HIDDEN = 0;

    /**
     * active_status_label function
     *
     * @return string
     */
    public function getActiveStatusLabelAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ][$this->is_active] ?? 'Active';
    }

    /**
     * active_status_color function
     *
     * @return string
     */
    public function getActiveStatusColorAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'green',
            self::STATUS_INACTIVE => 'red',
        ][$this->is_active] ?? 'green';
    }

    /**
     * active_visible_label function
     *
     * @return string
     */
    public function getActiveVisibleLabelAttribute()
    {
        return [
            self::STATUS_VISIBLE => 'Visible',
            self::STATUS_HIDDEN => 'Hidden',
        ][$this->is_visible] ?? 'Visible';
    }

    /**
     * active_visible_color function
     *
     * @return string
     */
    public function getActiveVisibleColorAttribute()
    {
        return [
            self::STATUS_VISIBLE => 'green',
            self::STATUS_HIDDEN => 'red',
        ][$this->is_visible] ?? 'green';
    }

    public function getPublic($root, $args)
    {
        return static::where('is_active', 1)->where('is_visible', 1)->where('lang', app()->getLocale())->get();
    }
}
