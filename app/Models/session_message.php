<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Notifications\Instructor\createNewMessage as createNewMessageForInstructor;
use App\Notifications\Student\createNewMessage    as createNewMessageForStudent;
use Illuminate\Support\Facades\Log;

class session_message extends Model
{

    use HasFactory;

    protected $fillable = [
        'session_id',
        'instructor_id',
        'student_id',
        'message',
    ];

    protected static function boot(): void
    {

        parent::boot();

        self::created(function ($model) {
            $model->session->touch();
            if ($model->student) $model->session->instructor->notify(new createNewMessageForInstructor($model->session, ['message_id' => $model->id]));
            if ($model->instructor) $model->session->student->notify(new createNewMessageForStudent($model->session, ['message_id' => $model->id]));
        });
    }

    public function instructor()
    {
        return $this->belongsTo(instructor::class);
    }

    public function BeSeen()
    {
        $this->seen_at = now();
        $this->save();
        return $this->refresh();
    }

    public function session()
    {
        return $this->belongsTo(session::class);
    }

    public function student()
    {
        return $this->belongsTo(student::class);
    }

    public function scopestudentUnSeen(Builder $query)
    {
        return $query->whereNull('seen_at')->whereNotNull('student_id');
    }

    public function scopeinstructorUnSeen(Builder $query)
    {
        return $query->whereNull('seen_at')->whereNotNull('instructor_id');
    }
}
