<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class variabledb extends Model
{

    protected $fillable = [
        'forAdmin',
        'note',
        'key',
        'value'
    ];

    public function scopeval($query, $key)
    {
        $record = $query->withoutGlobalScope('Admin')->where('key', $key)->first();
        if ($record) return $record->value;
        else return null;
    }

    public function scopeset($query, $key, $val)
    {
        $record = $query->withoutGlobalScope('Admin')->where('key', $key)->first();
        if ($record) {
            $record->value = $val;
            $record->save();
            return $record->refresh();
        } else return null;
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('Admin', function ($builder) {
            $builder->where('forAdmin', 0);
        });

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }
}
