<?php

namespace App\Models;

use Illuminate\Notifications\DatabaseNotification;

use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Collection\DatabaseNotificationCollection;

use App\Notifications\Instructor\createdButNotcompleted            as createdButNotcompletedForInstructor;
use App\Notifications\Instructor\sessionFiveMinutesBeforeRemaining as sessionFiveMinutesBeforeRemainingForInstructor;
use App\Notifications\Instructor\sessionOneHourBeforeRemaining     as sessionOneHourBeforeRemainingForInstructor;
use App\Notifications\Instructor\sessionsNewBook                   as sessionsNewBookForInstructor;
use App\Notifications\Instructor\sessionReplaced                   as sessionReplacedForInstructor;
use App\Notifications\Instructor\sessionCancel                     as sessionCancelForInstructor;
use App\Notifications\Instructor\createNewMessage                  as createNewMessageForInstructor;
use App\Notifications\Instructor\sessionsAcceptEdit                as sessionsAcceptEditForInstructor;
use App\Notifications\Instructor\interacting                       as interactingForInstructor;

use App\Notifications\Student\create                               as createForStudent;
use App\Notifications\Student\sessionFiveMinutesBeforeRemaining    as sessionFiveMinutesBeforeRemainingForStudent;
use App\Notifications\Student\sessionOneHourBeforeRemaining        as sessionOneHourBeforeRemainingForStudent;
use App\Notifications\Student\createNewMessage                     as createNewMessageForStudent;
use App\Notifications\Student\sessionCancel                        as sessionCancelForStudent;
use App\Notifications\Student\sessionsEditRequest                  as sessionsEditRequestForStudent;

use App\Notifications\SubInstructor\createdButNotcompleted         as createdButNotcompletedForSubInstructor;

class notification extends DatabaseNotification
{

    protected $fillable = [
        'id',
        'type',
        'notifiable_id',
        'notifiable_type',
        'related_with_id',
        'related_with_type',
        'data',
    ];

    protected static function boot()
    {

        parent::boot();

        static::addGlobalScope('Ordered', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }

    protected $casts = [
        'data' => 'array',
    ];

    public function notifiable()
    {
        return $this->morphTo();
    }

    public function related_with()
    {
        return $this->morphTo();
    }

    public function newCollection(array $models = []): DatabaseNotificationCollection
    {
        return new DatabaseNotificationCollection($models);
    }

    /**
     * 
       to get Builder for if notification Exist
     * 
     * @var Builder $query 
     * @var mixed   $related_with 
     * @var string  $type 
     * 
     * @return Builder
     */
    public function scopefor(Builder $query, $related_with, string $type = ''): Builder
    {
        return $query
            ->where('related_with_id', $related_with->id)
            ->where('related_with_type', get_class($related_with))
            ->where('type', $type);
    }

    static public function getDetails(string $name): array
    {
        switch ($name) {
            case createdButNotcompletedForInstructor::class:
                return ['kind_number' => 0, 'click_action' => ''];
            case sessionFiveMinutesBeforeRemainingForInstructor::class:
                return ['kind_number' => 1, 'click_action' => 'Instructor::SessionsAction'];
            case sessionOneHourBeforeRemainingForInstructor::class:
                return ['kind_number' => 2, 'click_action' => 'Instructor::SessionsAction'];
            case sessionsNewBookForInstructor::class:
                return ['kind_number' => 3, 'click_action' => 'Instructor::NotificationsAction'];
            case sessionReplacedForInstructor::class:
                return ['kind_number' => 4, 'click_action' => 'Instructor::NotificationsAction'];
            case sessionCancelForInstructor::class:
                return ['kind_number' => 5, 'click_action' => 'Instructor::NotificationsAction'];
            case createNewMessageForInstructor::class:
                return ['kind_number' => 6, 'click_action' => 'Instructor::SessionQuestionsAction'];
            case sessionsAcceptEditForInstructor::class:
                return ['kind_number' => 7, 'click_action' => 'Instructor::NotificationsAction'];
            case interactingForInstructor::class:
                return ['kind_number' => 8, 'click_action' => 'Instructor::HomeAction'];
            case createForStudent::class:
                return ['kind_number' => 9, 'click_action' => ''];
            case sessionFiveMinutesBeforeRemainingForStudent::class:
                return ['kind_number' => 10, 'click_action' => 'Student::SessionsAction'];
            case sessionOneHourBeforeRemainingForStudent::class:
                return ['kind_number' => 11, 'click_action' => 'Student::SessionsAction'];
            case createNewMessageForStudent::class:
                return ['kind_number' => 12, 'click_action' => 'Student::NotificationsAction'];
            case sessionCancelForStudent::class:
                return ['kind_number' => 13, 'click_action' => 'Student::NotificationsAction'];
            case sessionsEditRequestForStudent::class:
                return ['kind_number' => 14, 'click_action' => 'Student::NotificationsAction'];
            case createdButNotcompletedForSubInstructor::class:
                return ['kind_number' => 15, 'click_action' => ''];
        };
    }

    public function getDetailsAttribute(): array
    {
        return static::getDetails($this->type);
    }

    public function getmessageAttribute(): string
    {
        return $this->type::getMessageDatabaseTranslate($this);
    }

    public function getkindAttribute(): string
    {
        return class_basename($this->type);
    }

    public function getkindNumberAttribute(): int
    {
        return $this->Details['kind_number'];
    }

    public function getclickActionAttribute(): string
    {
        return $this->Details['click_action'];
    }

    public function getsessionAttribute(): ?session
    {
        return ($this->related_with_type === session::class) ? $this->related_with : null;
    }

    public function getinstructorAttribute(): ?instructor
    {
        return ($this->related_with_type === instructor::class) ? $this->related_with : null;
    }

    public function getstudentAttribute(): ?student
    {
        return ($this->related_with_type === student::class) ? $this->related_with : null;
    }
}
