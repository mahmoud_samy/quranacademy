<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    public function Active_plan()
    {
        return static::query()
            ->where('plans.visibility', 1);
    }

    public function instructor_level()
    {
        return $this->belongsTo(InstructorLevel::class, 'instructor_level_id', 'id');
    }

    public function instructors()
    {
        if (!$this->instructor_level) {
            return $this->belongsTo(InstructorLevel::class, 'instructor_level_id', 'id');
        }
        return $this->instructor_level->instructors();
    }
}
