<?php

namespace App\Traits;

use App\Models\Promocode;

trait HasPromocode
{
    /**
     * Get all of the promocodes for the modal.
     */
    public function promocodes()
    {
        return $this->morphToMany(Promocode::class, 'relatable', 'promocode_user')->withTimestamps();
    }
}
