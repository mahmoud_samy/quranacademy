<?php

namespace App\Traits\Attributes;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait sessionAttribute
{

    public function getstudentCountUnSeenAttribute(): ?int
    {
        return $this->messagesStudentUnSeen()->count();
    }

    public function getinstructorCountUnSeenAttribute(): ?int
    {
        return $this->messagesInstructorUnSeen()->count();
    }

    public function getcanStartAttribute(): bool
    {
        return $this->DateTime->subMinute(5) <= now() && $this->DateTime->addMinutes($this->rate_minutes_type->RateByMinutes) >= now();
    }

    public function getcountUnSeenMessagesAttribute(): ?int
    {
        if (!Auth::check()) return null;
        if (Auth::user()->tokenCan('student')) return $this->messagesInstructorUnSeen()->count();
        if (Auth::user()->tokenCan('instructor')) return $this->messagesStudentUnSeen()->count();
        return null;
    }

    public function getDateTimeAttribute(): Carbon
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date->format('Y-m-d') . ' ' . $this->time->format('H:i:s'));
    }

    public function gettoTimeAttribute(): Carbon
    {
        return $this->time->addMinutes($this->rate_minutes_type->RateByMinutes);
    }

    public function getHasConflictInBookAttribute(): ?bool
    {
        if ($this->DateTime < now()) return true;
        if ($this->ifStudentExists) return true;
        if (!Auth::check()) return null;
        if (!Auth::user()->tokenCan('student')) return true;
        return Auth::user()->sessionsToUnits()->DateTimeAvailableExists($this->rate_minutes_type, $this->date, $this->time);
    }

    public function getifStudentNotExistsAttribute(): bool
    {
        return !$this->ifStudentExists;
    }

    public function getifStudentExistsAttribute(): bool
    {
        return $this->student()->exists();
    }

    public function getNotificationDateTimeAttribute(): string
    {
        return $this->DateTime->isoFormat('Do of MMMM YYYY h:mm A ');
    }

    public function gethasEditRequestAttribute(): bool
    {
        return array_key_exists('course_type_id',  $this->options) || array_key_exists('time',  $this->options) || array_key_exists('date',  $this->options);
    }
}
