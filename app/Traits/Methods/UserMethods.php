<?php

namespace App\Traits\Methods;

use App\Models\image;
use App\Services\BaseAuthClass;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

trait UserMethods
{

    public function setLocale()
    {

        $this->Locale = app()->getLocale();
        $this->save();
    }

    public function EditPassword($password)
    {

        $this->password = $password;
        $this->save();
        return $this->refresh();
    }

    public function HashAndEditPassword($password)
    {

        return $this->EditPassword(Hash::make($password));
    }

    public function MakeBearerToken()
    {

        return (new BaseAuthClass())->makeBearerToken($this, class_basename(static::class));
    }

    public static function Storage()
    {
        return Storage::disk('users');
    }

    public function updateBaseData(array $args)
    {
        if (array_key_exists('profile_image', $args) && !is_null($args['profile_image'])) {
            $this->updateProfilePhoto($args['profile_image']);
        }

        if (array_key_exists('first_name', $args) && !is_null($args['first_name'])) $this->first_name    = $args['first_name'];
        if (array_key_exists('last_name', $args) && !is_null($args['last_name'])) $this->last_name     = $args['last_name'];
        if (array_key_exists('gender', $args) && !is_null($args['gender'])) $this->gender        = $args['gender'];
        if (array_key_exists('date_of_birth', $args) && !is_null($args['date_of_birth'])) $this->date_of_birth = $args['date_of_birth'];
        if (array_key_exists('country_id', $args) && !is_null($args['country_id'])) $this->country_id    = $args['country_id'];
        $this->save();
        return $this->refresh();
    }

    public function updateProfile(array $args)
    {

        $this->updateBaseData($args);

        return $this;
    }

    public function sessionsDelete(int $id)
    {

        $this->sessions()->where('id', $id)->update(['delete_for_students' => now()]);

        return $this;
    }

    public function FireBaseCloudMessageingAttributesBase(): array
    {
        return [
            "id"                          => $this->id,
            "count_notifications_un_read" => $this->count_notifications_un_read,
        ];
    }

    public function touching()
    {
        $this->touch();
        return $this;
    }
}
