<?php

namespace App\Traits\Methods;

use Carbon\Carbon;
use App\Models\File;
use App\Models\letter;
use App\Models\variabledb;
use App\Models\instructor;
use App\Models\certificate;
use App\Services\transferwise;
use App\Models\recommendation;
use App\Notifications\Instructor\interacting;

trait InstructorMethods
{

    public function profile_vidoe_owner(): instructor
    {
        $this->profile_vidoe->getowner($this);
        return $this->refresh();
    }

    public function updatevidoe($file): instructor
    {
        //$this->profile_vidoe_id = ($File = File::createFromFile($file)->id);
        $this->updateProfileVideo($file);
        $this->save();
        $this->profile_vidoe_owner();
        return $this->refresh();
    }

    public function add_letters($files): instructor
    {
        foreach ($files as $file) {
            $file = File::createFromFile($file)->getowner($this, letter::class);
            $this->letters()->create(['file_id' => $file->id]);
        }
        return $this->refresh();
    }

    public function add_certificates($files): instructor
    {
        foreach ($files as $file) {
            $file = File::createFromFile($file)->getowner($this, certificate::class);
            $this->certificates()->create(['file_id' => $file->id]);
        }
        return $this->refresh();
    }

    public function add_recommendations($files): instructor
    {
        foreach ($files as $file) {
            $file = File::createFromFile($file)->getowner($this, recommendation::class);
            $this->recommendations()->create(['file_id' => $file->id]);
        }
        return $this->refresh();
    }

    public function delete_letters($ids): instructor
    {
        $this->letters()->whereIn('id', $ids)->delete();
        return $this->refresh();
    }

    public function delete_certificates($ids): instructor
    {
        $this->certificates()->whereIn('id', $ids)->delete();
        return $this->refresh();
    }

    public function delete_recommendations($ids): instructor
    {
        $this->recommendations()->whereIn('id', $ids)->delete();
        return $this->refresh();
    }

    public function update_Spoken_Language_User($array): instructor
    {
        if ($this->SpokenLanguageUser->isNotEmpty()) $this->SpokenLanguageUser->map(function ($rec) use ($array) {
            if (!in_array($rec->language_types_id, $array)) $rec->delete();
        });
        foreach ($array as $id) {
            if (!$this->SpokenLanguageUser()->where('language_types_id', $id)->exists()) $this->SpokenLanguageUser()->create(['language_types_id' => $id]);
        }
        return $this->refresh();
    }

    public function updateProfile(array $args)
    {

        $this->updateBaseData($args);

        if (array_key_exists('years_of_experience', $args) && !is_null($args['years_of_experience'])) $this->years_of_experience         = $args['years_of_experience'];
        if (array_key_exists('description', $args) && !is_null($args['description'])) $this->description                 = $args['description'];
        if (array_key_exists('is_profile_public', $args) && !is_null($args['is_profile_public'])) $this->is_profile_public           = $args['is_profile_public'];
        if (array_key_exists('rate_15_min', $args) && !is_null($args['rate_15_min'])) $this->rate_15_min                 = $args['rate_15_min'];
        if (array_key_exists('rate_30_min', $args) && !is_null($args['rate_30_min'])) $this->rate_30_min                 = $args['rate_30_min'];
        if (array_key_exists('rate_60_min', $args) && !is_null($args['rate_60_min'])) $this->rate_60_min                 = $args['rate_60_min'];
        if (array_key_exists('currency_type_id', $args) && !is_null($args['currency_type_id'])) $this->currency_type_id            = $args['currency_type_id'];
        if (array_key_exists('student_gender_types_id', $args) && !is_null($args['student_gender_types_id'])) $this->student_gender_types_id     = $args['student_gender_types_id'];
        if (array_key_exists('student_age_types_id', $args) && !is_null($args['student_age_types_id'])) $this->student_age_types_id        = $args['student_age_types_id'];
        if (array_key_exists('courses_types_id', $args) && !is_null($args['courses_types_id'])) $this->courses_types_id            = $args['courses_types_id'];
        if (array_key_exists('country_id', $args) && !is_null($args['country_id'])) $this->country_id                  = $args['country_id'];
        if (array_key_exists('public_rate_minutes_type_id', $args) && !is_null($args['public_rate_minutes_type_id'])) $this->public_rate_minutes_type_id = $args['public_rate_minutes_type_id'];
        $this->is_Approval = variabledb::val('IF_ADMIN_NOT_NEED_APPROVE_INSTRUCTORST');
        $this->save();
        if (array_key_exists('profile_vidoe', $args) && !is_null($args['profile_vidoe'])) $this->updateProfileVideo($args['profile_vidoe']);
        //if (array_key_exists('profile_vidoe', $args) && !is_null($args['profile_vidoe'])) $this->updatevidoe($args['profile_vidoe']);
        if (array_key_exists('Spoken_Language_User', $args) && !is_null($args['Spoken_Language_User'])) $this->update_Spoken_Language_User($args['Spoken_Language_User']);
        if (array_key_exists('letters', $args) && !is_null($args['letters'])) $this->add_letters($args['letters']);
        if (array_key_exists('certificates', $args) && !is_null($args['certificates'])) $this->add_certificates($args['certificates']);
        if (array_key_exists('recommendations', $args) && !is_null($args['recommendations'])) $this->add_recommendations($args['recommendations']);
        if (array_key_exists('delete_letters_ids', $args) && !is_null($args['delete_letters_ids'])) $this->delete_letters($args['delete_letters_ids']);
        if (array_key_exists('delete_recommendations_ids', $args) && !is_null($args['delete_recommendations_ids'])) $this->delete_recommendations($args['delete_recommendations_ids']);
        if (array_key_exists('delete_certificates_ids', $args) && !is_null($args['delete_certificates_ids'])) $this->delete_certificates($args['delete_certificates_ids']);

        return $this;
    }

    public function sessionsDelete(int $id): instructor
    {

        $this->sessions()->where('id', $id)->update(['delete_for_instructors' => now()]);

        return $this;
    }

    public function interacting(): instructor
    {
        $this->touch();
        $this->notifyNow(new interacting($this));;
        return $this;
    }

    public static function schedule(): array
    {

        return ['interacting' => static::where('updated_at', '<=', now()->subWeek())->get()->each(fn (instructor $instructor) => $instructor->interacting())];
    }

    public function createNewTransfer(array $Transfer): array
    {

        if ($Transfer['amount'] > $this->currentBalance) return [
            'message'         => 'enter amount less than your current Balance.',
            'title'           => 'NOT_VALID_AMOUNT',
            'check'           => false,
            'arguments_name'  => 'amount',
            'arguments_value' => $Transfer['amount'],
            'data'            => [],
        ];

        $transferwise = transferwise::NewTransfersUSD(
            $Transfer['amount'],
            $Transfer['accountHolderName'],
            $Transfer['abartn'],
            $Transfer['accountNumber'],
            $Transfer['accountType'],
            $Transfer['postCode'],
            $Transfer['address'],
            $Transfer['city'],
            $Transfer['country'],
        );

        if ($transferwise['check']) $transferwise['model'] = $this->transfers()->create([
            'amount'                => $Transfer['amount'],
            'status'                => $transferwise['data']['transfers']['status'],
            'estimatedDeliveryDate' => Carbon::parse($transferwise['data']['estimatedDeliveryDate']),
        ] + $transferwise['data']);

        return $transferwise;
    }
}
