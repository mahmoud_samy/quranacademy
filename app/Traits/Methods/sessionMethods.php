<?php

namespace App\Traits\Methods;

use App\Notifications\Student\sessionCancel                        as SessionCancelForStudent;
use App\Notifications\Student\sessionOneHourBeforeRemaining        as sessionOneHourBeforeRemainingForstudent;
use App\Notifications\Student\sessionFiveMinutesBeforeRemaining    as sessionfiveMinutesBeforeRemainingForstudent;
use App\Notifications\Student\sessionsEditRequest;

use App\Notifications\Instructor\sessionCancel                     as SessionCancelForInstructor;
use App\Notifications\Instructor\sessionReplaced                   as sessionReplacedForinstructor;
use App\Notifications\Instructor\sessionOneHourBeforeRemaining     as sessionOneHourBeforeRemainingForinstructor;
use App\Notifications\Instructor\sessionFiveMinutesBeforeRemaining as sessionfiveMinutesBeforeRemainingForinstructor;
use App\Notifications\Instructor\sessionsAcceptEdit;

use Carbon\Carbon;
use App\Models\File;
use App\Models\session;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

trait sessionMethods
{

    public function AcceptEdit(): session
    {
        $this->update($this->options);
        $this->options = [];
        $this->save();
        $this->instructor->notify(new sessionsAcceptEdit($this));
        return $this;
    }

    public function editAndSave($array): session
    {
        $date = $this->date;
        $time = $this->time;

        if (array_key_exists('course_type_id', $array)) {
            $this->course_type_id = $array['course_type_id'];
        }

        if (array_key_exists('date', $array) && $array['date'] instanceof Carbon) {
            $date = $array['date']->format('Y-m-d');
        }

        if (array_key_exists('time', $array) && $array['time'] instanceof Carbon) {
            $time = $array['time']->format('H:i:s');
        }

        if (array_key_exists('date', $array) && $array['date'] instanceof Carbon || array_key_exists('time', $array) && $array['time'] instanceof Carbon) {
            $this->date = $date;
            $this->time = $time;

            $this->start_at = Carbon::parse($date . ' ' . $time);
            $this->end_at = Carbon::parse($date . ' ' . $time)->addMinutes($this->rate_minutes_type->RateByMinutes);
        }

        $this->save();
        return $this;
    }

    public function makeEdit($array): session
    {
        $this->editAndSave($array);
        if (!$this->if_student_not_exists) $this->student->notify(new sessionsEditRequest($this));
        return $this;
    }

    public function Cancel(): session
    {
        $this->cancel_at = now();
        $this->save();

        if (Auth::check() && Auth::user()->tokenCan('instructor') && !$this->if_student_not_exists) $this->student->notify(new SessionCancelForStudent($this));
        if (Auth::check() && Auth::user()->tokenCan('student')) {
            $this->instructor->notify(new SessionCancelForInstructor($this));
            static::create($this->toArray() + ['student_id' => null]);
        };

        return $this;
    }

    public function BeSeen(): session
    {
        if (Auth::user()->tokenCan('student')) $this->messagesInstructorUnSeen()->update(['seen_at' => now()]);
        if (Auth::user()->tokenCan('instructor')) $this->messagesStudentUnSeen()->update(['seen_at' => now()]);
        return $this;
    }

    public function book(string $note = null): session
    {
        $this->student_id = Auth::id();
        $this->note       = $note;
        $this->save();
        return $this->refresh();
    }

    public function rat(int $rating, string $reviews = null): session
    {
        $this->rating()->create([
            'student_id'    => $this->student_id,
            'instructor_id' => $this->instructor_id,
            'rating'        => $rating >= 5 ? 5 : $rating,
            'reviews'       => $reviews,
        ]);
        return $this->refresh();
    }

    public function Cancelbook(): session
    {
        $this->student_id = null;
        $this->note       = null;
        $this->save();
        return $this->refresh();
    }

    public function replace(session $oldSession): session
    {

        $this->student_id = $oldSession->student_id;
        $this->note       = $oldSession->note;
        $this->save();

        $this->instructor->notifyNow(new sessionReplacedForinstructor($this, ['old_Session_id' => $oldSession->id]));

        $oldSession->Cancelbook();

        return $this->refresh();
    }

    public function addMaterial(UploadedFile $file): session
    {

        $this->materials()->save(File::createFromFile($file))->getowner($this->instructor, 'materials');

        return $this->refresh();
    }

    public function deleteMaterial(Int $id): session
    {

        $this->materials()->find($id)->delete();

        return $this->refresh();
    }

    public function replaceMaterial(Int $id, UploadedFile $file): session
    {

        $find = $this->materials()->find($id);
        if ($find) $find->replace($file);

        return $this->refresh();
    }

    public function oneHourBeforeNotify(): session
    {
        $this->one_hour_before_notify = 1;
        $this->save();
        $this->student->notifyNow(new sessionOneHourBeforeRemainingForstudent($this));
        $this->instructor->notifyNow(new sessionOneHourBeforeRemainingForinstructor($this));
        return $this->refresh();
    }

    public function fiveMinutesBeforeNotify(): session
    {
        $this->one_hour_before_notify = 1;
        $this->five_Minutes_before_notify = 1;
        $this->save();
        $this->student->notifyNow(new sessionfiveMinutesBeforeRemainingForstudent($this));
        $this->instructor->notifyNow(new sessionfiveMinutesBeforeRemainingForinstructor($this));
        return $this->refresh();
    }

    static public function schedule(array $models = []): array
    {
        return [
            'one hour before'     => static::oneHourBefore()->get()->oneHourBeforeNotify(),
            'five Minutes before' => static::fiveMinutesBefore()->get()->fiveMinutesBeforeNotify(),
        ];
    }
}
