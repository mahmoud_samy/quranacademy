<?php

namespace App\Traits\Boots;

trait globalBootTypeFunction
{

    protected static function boot()
    {

        parent::boot();

        //FIXME: check the effect for comment addGlobalScope all
        //static::addGlobalScope('all', function ($builder) {
        //    $builder->addSelect('*');
        //});

        static::addGlobalScope('is_active', function ($builder) {
            $builder->where(with(new static)->getTable() . '.is_active', 1);
        });
    }
}
