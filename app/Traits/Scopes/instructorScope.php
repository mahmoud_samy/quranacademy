<?php

namespace App\Traits\Scopes;

use App\Models\rating;
use App\Models\student;
use App\Models\session;
use App\Models\courseType;
use App\Models\studentAgeType;
use App\Models\studentGenderType;
use Illuminate\Support\Facades\DB;
use App\Models\SpokenLanguageUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

trait instructorScope
{

    public function scopeSpokenLanguageUserSearchIn(Builder $query, array $ids)
    {
        $_TableName = with(new SpokenLanguageUser)->getTable();
        return $query->Join($_TableName, function ($join) use ($_TableName, $ids) {
            $join->on($_TableName . '.spokenable_id', '=', with(new static)->getTable() . '.id');
            $join->where($_TableName . '.spokenable_type', '=', static::class);
            $join->whereIn($_TableName . '.language_types_id', $ids);
        });
    }

    public function scopenameSearchIn(Builder $query, string $name)
    {
        $name = collect(preg_replace('/[^A-Za-z0-9\-]/', '', $name))->unique()->filter()->values()->implode("|");
        if ($name) $query->where(function ($query) use ($name) {
            $query->Where('first_name', 'REGEXP', $name);
            $query->orWhere('last_name', 'REGEXP', $name);
        });
        return $query;
    }

    public function scopeInSession(Builder $query, array $args = [])
    {
        $_sessionName = with(new session)->getTable();
        (new session)->scopecanFind($query);
        return $query->join($_sessionName, function ($join) use ($_sessionName, $args) {
            $join
                ->on($_sessionName . '.instructor_id', with(new static)->getTable() . '.id')
                ->whereBetween($_sessionName . '.rate_salary', [
                    $args['price_from'] ?? 0,
                    $args['price_to'] ?? 9E10
                ])
                ->whereBetween($_sessionName . '.date', [
                    ($args['date_from'] ??                           now())->format('Y-m-d'),
                    ($args['date_to'] ?? ($args['date_from'] ?? now())->addMonths(1))->format('Y-m-d')
                ]);
            if (array_key_exists('courses_types_id', $args)) $join->whereIn($_sessionName . '.course_type_id', [$args['courses_types_id'], courseType::getId('both')]);
            if (array_key_exists('rate_minutes_type_id', $args)) $join->where($_sessionName . '.rate_minutes_type_id',   $args['rate_minutes_type_id']);
        });
    }

    public function scopeToStudent(Builder $query, student $student)
    {

        $student_gender_types = [studentGenderType::getId('Male & Female')];
        $student_age_types    = [studentAgeType::getId('Adults & Children')];

        if (!is_null($student->gender)) switch ($student->gender) {
            case true:
                $student_gender_types[] = studentGenderType::getId('Male Only');
                break;
            case false:
                $student_gender_types[] = studentGenderType::getId('Female Only');
                break;
        }

        if (!is_null($student->date_of_birth)) {
            if ($student->date_of_birth->diffInYears() > student::CHILDREN_AGE) $student_age_types[] = studentAgeType::getId('Adults Only');
            else $student_age_types[] = studentAgeType::getId('Children Only');
        }

        return $query
            ->whereIn(with(new static)->getTable() . '.student_gender_types_id', $student_gender_types)
            ->whereIn(with(new static)->getTable() . '.student_age_types_id', $student_age_types);
    }

    public function InstructorSearch($root, $args)
    {

        $args = array_filter($args, static fn ($var) => $var !== null);

        $_thisName = with(new static)->getTable();

        $query = static::query()
            ->select($_thisName . '.*')
            ->groupBy($_thisName . '.id')
            ->orderBy($_thisName . '.id', 'ASC');

        if (Auth::check() && Auth::user()->tokenCan('student')) $query->ToStudent(Auth::user());

        if (array_key_exists('gender', $args)) $query->where('gender', intval($args['gender']));
        if (array_key_exists('country_id', $args)) $query->where('country_id', intval($args['country_id']));
        if (array_key_exists('name', $args)) $query->nameSearchIn(($args['name']));
        if (array_key_exists('Spoken_Language_User', $args)) $query->SpokenLanguageUserSearchIn(($args['Spoken_Language_User']));
        if (array_key_exists('average_ratings', $args)) $query->getRatingHaving(intval($args['average_ratings']));

        return $query->InSession($args)->canFind();
    }

    public function InstructorTopRated()
    {
        return \App\Models\instructor::withSum('rating','rating')->
        orderBy('rating_sum_rating', 'desc');
    }
    public function scopegetRatingHaving(Builder $query, int $rat)
    {
        $query->getRating()->having('ratings_average', '>=', $rat);
    }

    public function scopecanFind(Builder $query)
    {
        $_TableName    = with(new static)->getTable();
        return $query
            ->where($_TableName . '.is_Approval', 1)
            ->where($_TableName . '.is_profile_public', 1)
            ->where($_TableName . '.is_active', 1)
            ->where($_TableName . '.is_blocked', 0)
            ->whereNull($_TableName . '.deleted_at');
    }
    public function Active_instructor()
    {
        return static::query()
            ->where(  'instructors.is_Approval', 1)
            ->where(  'instructors.is_profile_public', 1)
            ->where(  'instructors.is_active', 1)
            ->where(  'instructors.is_blocked', 0)
            ->whereNull(  'instructors.deleted_at');
    }

    public function scopegetRating(Builder $query)
    {
        $tableName  = with(new static)->getTable();
        $ratingName = with(new rating)->getTable();
        $query
            ->Join($ratingName, $ratingName . '.instructor_id', '=', $tableName . '.id')
            ->addSelect(DB::raw('AVG( `' . $ratingName . '`.`rating` ) as ratings_average'));
    }
}
