<?php

namespace App\Traits\Scopes;

use Carbon\Carbon;
use App\Models\rateMinutesType;
use App\Models\session;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

trait sessionScope
{

    public string $timePattern = 'H:i:s';

    public string $datePattern = 'Y-m-d';

    public function scopeDate(Builder $query, Carbon $date, String $Operator = '=')
    {
        return $query->where(with(new static)->getTable() . '.date', $Operator, $date->format($this->datePattern));
    }

    public function scopeBetweenDates(Builder $query, Carbon $date, Carbon $ToDate)
    {
        return $query->whereBetween(with(new static)->getTable() . '.date', [$date->format($this->datePattern), $ToDate->format($this->datePattern)]);
    }

    public function scopeTime(Builder $query, Carbon $time, String $Operator = '=')
    {
        return $query->where(with(new static)->getTable() . '.time', $Operator, $time->format($this->timePattern));
    }

    public function scopeBetweenTimes(Builder $query, Carbon $time, Carbon $ToTime)
    {
        $than = now()->startOfDay();
        if ($time <= $than) return $query
            ->whereBetween(with(new static)->getTable() . '.time', [$time->format($this->timePattern), '23:59:59'])
            ->orWhereBetween(with(new static)->getTable() . '.time', ['00:00:00', $ToTime->format($this->timePattern)]);
        return $query->whereBetween(with(new static)->getTable() . '.time', [$time->format($this->timePattern), $ToTime->format($this->timePattern)]);
    }

    public function scopeDateTimeAvailable(Builder $query, rateMinutesType $rateMinutesTypeQuery, Carbon $date, Carbon $time, Int $repeat = 0, String $lop = 'addWeeks')
    {

        $query->whereNull('cancel_at')->BetweenDates($date, $date->$lop($repeat));

        $query->where(function ($query) use ($time, $rateMinutesTypeQuery) {
            rateMinutesType::addSelect('*')->each(function ($rateMinutesType, $numbet) use ($time, $rateMinutesTypeQuery, $query) {
                $query->orWhere(function ($subQuery) use ($time, $rateMinutesTypeQuery, $rateMinutesType) {
                    return $subQuery
                        ->BetweenTimes(
                            $time->clone()->subMinutes($rateMinutesType->RateByMinutes),
                            $time->clone()->addMinutes($rateMinutesTypeQuery->RateByMinutes + session::BREAK_TIME_BETWEEN_SESSIONS)
                        )
                        ->where('rate_minutes_type_id', $rateMinutesType->id);
                });
            });
        });

        return $query;
    }

    public function scopeDateTimeAvailableExists(Builder $query, rateMinutesType $rateMinutesType, Carbon $date, Carbon $time, Int $repeat = 0, String $lop = 'addWeeks')
    {
        return $query->DateTimeAvailable($rateMinutesType, $date, $time, $repeat, $lop)->exists();
    }

    public function scopecanFind(Builder $query)
    {
        $_thisName    = with(new static)->getTable();
        return $query
            ->where($_thisName . '.is_active', 1)
            ->whereNull($_thisName . '.cancel_at')
            ->whereNull($_thisName . '.delete_for_instructors')
            ->whereNull($_thisName . '.student_id');
    }

    public function scopecanEarning(Builder $query)
    {
        return $query
            ->whereNotNull(($_thisName = with(new static)->getTable()) . '.student_id')
            ->where($_thisName . '.created_at', '<=', now()->subDay())
            ->where($_thisName . '.date', '<=', now()->subDay())
            ->where($_thisName . '.is_active', 1)
            ->whereNull($_thisName . '.cancel_at');
    }

    public function scopeoneHourBefore(Builder $query)
    {
        $_thisName    = with(new static)->getTable();
        return $query
            ->Date(now())
            ->where($_thisName . '.is_active', 1)
            ->whereNull($_thisName . '.cancel_at')
            ->whereNotNull($_thisName . '.student_id')
            ->where($_thisName . '.one_hour_before_notify', 0)
            ->BetweenTimes(now()->addHour()->subMinutes(10), now()->addHour());
    }

    public function scopefiveMinutesBefore(Builder $query)
    {
        $_thisName    = with(new static)->getTable();
        return $query
            ->Date(now())
            ->BetweenTimes(now(), now()->addMinutes(5))
            ->where($_thisName . '.is_active', 1)
            ->whereNull($_thisName . '.cancel_at')
            ->whereNotNull($_thisName . '.student_id')
            ->where($_thisName . '.one_hour_before_notify', 1)
            ->where($_thisName . '.five_Minutes_before_notify', 0);
    }

    public function scopegetUnits(Builder $query, Carbon $afterDate, bool $by_time = false, bool $Can_Book = false, bool $is_confirmed = false, array $filters = []): array
    {
        if ($is_confirmed) $query->whereNotNull('student_id');
        foreach ($filters as $where => $val) $query->where($where, $val);
        if ($by_time) $query = $query->BetweenTimes($afterDate, Carbon::createFromFormat($this->timePattern, '23:59:59'));
        $sessions = $query->Date($afterDate, '>=')->Date($afterDate, '<=')->get()->sortBy('DateTime');
        if ($Can_Book) $sessions = $sessions->CanBook();
        return [
            'sessions' => $sessions,
            'status'   => [
                'Date'                => $afterDate,
                'counter'             => $sessions->count(),
                'counterConfirmed'    => $sessions->whereNotNull('student_id')->count(),
                'counterNotConfirmed' => $sessions->whereNull('student_id')->count(),
            ],
        ];
    }

    public function scopeFilterByStatus(Builder $query, $status = null)
    {
        if ($status) {
            switch ($status) {
                case 'Canceled':
                    $query->whereNotNull('cancel_at');
                    break;
                    case session::STATUS_FUTURE:
                    $query->where('start_at', '>', now())->where('end_at', '>', now());
                    break;
                case session::STATUS_PROGRESS:
                    $query->where('start_at', '<=', now())->where('end_at', '>=', now());
                    break;
                case session::STATUS_PAST:
                    $query->where('start_at', '<', now())->where('end_at', '<', now());
                    break;
            }
        }
    }
    public function scopeFilterByBooked(Builder $query, $booked = null)
    {
        if ($booked) {
            switch ($booked) {
                case 1:
                    $query->whereNotNull('student_id');
                    break;
                case 2:
                    $query->whereNull('student_id');
                    break;
            }
        }
    }
}
