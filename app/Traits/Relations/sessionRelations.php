<?php

namespace App\Traits\Relations;

use App\Models\currencyType;
use App\Models\courseType;
use App\Models\instructor;
use App\Models\rateMinutesType;
use App\Models\student;
use App\Models\session_message;
use App\Models\File;
use App\Models\rating;

trait sessionRelations
{

    public function currency_type()
    {
        return $this->belongsTo(currencyType::class);
    }

    public function course_type()
    {
        return $this->belongsTo(courseType::class);
    }

    public function instructor()
    {
        return $this->belongsTo(instructor::class);
    }

    public function student()
    {
        return $this->belongsTo(student::class);
    }

    public function rate_minutes_type()
    {
        return $this->belongsTo(rateMinutesType::class);
    }

    public function materials()
    {
        return $this->morphMany(File::class, 'to');
    }

    public function session_messages()
    {
        return $this->hasMany(session_message::class)->orderBy('created_at', 'desc');
    }

    public function messages()
    {
        return $this->session_messages();
    }

    public function messagesStudentUnSeen()
    {
        return $this->messages()->studentUnSeen();
    }

    public function messagesInstructorUnSeen()
    {
        return $this->messages()->instructorUnSeen();
    }

    public function rating()
    {
        return $this->hasOne(rating::class);
    }
}
