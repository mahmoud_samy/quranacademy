<?php

namespace App\Traits\Relations;

use App\Models\currencyType;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\File;
use App\Models\SpokenLanguageUser;
use App\Models\languageType;
use App\Models\letter;
use App\Models\certificate;
use App\Models\recommendation;
use App\Models\session;
use App\Models\student;
use App\Models\instructorView;
use App\Models\rateMinutesType;
use App\Models\rating;
use App\Models\instructorTransfer;

trait InstructorRelations
{

    public function currency_type()
    {
        return $this->belongsTo(currencyType::class);
    }

    public function courses_type()
    {
        return $this->belongsTo(courseType::class, 'courses_types_id');
    }

    public function student_gender_types()
    {
        return $this->belongsTo(studentGenderType::class);
    }

    public function student_age_types()
    {
        return $this->belongsTo(studentAgeType::class);
    }

    public function profile_vidoe()
    {
        return $this->belongsTo(File::class, 'profile_vidoe_id');
    }

    public function public_rate_minutes_type()
    {
        return $this->belongsTo(rateMinutesType::class, 'public_rate_minutes_type_id');
    }

    public function SpokenLanguageUser()
    {
        return $this->morphMany(SpokenLanguageUser::class, 'spokenable');
    }

    public function getlanguagesAttribute()
    {
        return languageType::whereIn('id', $this->SpokenLanguageUser->pluck('language_types_id')->all())->get();
    }

    public function letters()
    {
        return $this->hasMany(letter::class);
    }

    public function certificates()
    {
        return $this->hasMany(certificate::class);
    }

    public function recommendations()
    {
        return $this->hasMany(recommendation::class);
    }

    public function students()
    {
        return $this->belongsToMany(student::class, session::class)->distinct(with(new student)->getTable() . '.id');
    }

    public function instructorView()
    {
        return $this->hasMany(instructorView::class);
    }

    public function rating()
    {
        return $this->hasMany(rating::class);
    }

    public function transfers()
    {
        return $this->hasMany(instructorTransfer::class)->orderBy('created_at', 'desc');
    }
}
