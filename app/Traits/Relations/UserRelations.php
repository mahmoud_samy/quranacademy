<?php

namespace App\Traits\Relations;

use App\Models\image;
use App\Models\pincode;
use App\Models\session;
use App\Models\Country;
use App\Models\notification;
use App\Models\session_message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Passport;

trait UserRelations
{

    public function tokens()
    {
        return $this->hasMany(Passport::tokenModel(), 'user_id')->whereJsonContains('scopes', class_basename(get_class($this)))->orderBy('created_at', 'desc');
    }

    public function profile_image()
    {
        return $this->belongsTo(image::class, 'profile_image_id');
    }

    public function pincode()
    {
        return $this->hasOne(pincode::class);
    }

    public function Notifications()
    {
        return $this->morphMany(notification::class, 'notifiable')->orderBy('updated_at', 'desc');
    }

    public function sessions()
    {
        return $this->hasMany(session::class);
    }

    public function sessionsToUnits()
    {
        return $this->sessions()->whereNull('cancel_at');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function sessionsByUnits($root, $args)
    {

        $afterDate  = array_key_exists('afterDate', $args) ? $args['afterDate'] : now();
        $hasInUnits = array_key_exists('hasInUnits', $args) ? $args['hasInUnits'] : 'addDays';

        if (!array_key_exists('firstUnits', $args)) $args['firstUnits'] = 7;

        for ($i = 0; $i < $args['firstUnits']; $i++) $sessionsArray[$i] = $root->sessionsToUnits()->getUnits(
            $afterDate->clone()->$hasInUnits($i),
            $i === 0,
            Auth::id() !== $root->id,
            array_key_exists('is_confirmed', $args) &&                                 $args['is_confirmed'] ? true : false,
            array_key_exists('filter', $args) && is_array($args['filter']) ? $args['filter']        : []
        );

        return $sessionsArray;
    }

    public function sessionsGet($root, $args)
    {

        $sessions = $root->sessions()
            ->Select('sessions.*', DB::raw('CONCAT( `date` , " " , `time` ) as datetime'), 'rate_minutes_types.value', DB::raw(' DATE_ADD(CONCAT( `date` , " " , `time` ), INTERVAL rate_minutes_types.value MINUTE) as enddate'))
            ->join('rate_minutes_types', 'sessions.rate_minutes_type_id', '=', 'rate_minutes_types.id')
            ->whereNotNull('student_id');

        if (array_key_exists('orderBy', $args)) switch ($args['orderBy']) {
                //"select this to sort to future"
            case 'ASC';
                $sessions
                    ->havingRaw('DATE_ADD(datetime, INTERVAL rate_minutes_types.value MINUTE) > ?', [now()])
                    ->orderBy('datetime', 'ASC')
                    ->where('sessions.is_active', 1)
                    ->whereNull('cancel_at')
                    ->whereNull('delete_for_instructors');
                break;
                //"select this to sort to past"  
            case 'DESC';
                $sessions
                    ->havingRaw('DATE_ADD(datetime, INTERVAL rate_minutes_types.value MINUTE) <= ?', [now()])
                    ->orderBy('datetime', 'DESC');
                break;
        }

        return $sessions;
    }

    public function sessionsAsked()
    {
        $table_name_message = with(new session_message)->getTable();
        $table_name_session = with(new session)->getTable();
        return $this
            ->sessions()
            ->select($table_name_session . '.*')
            ->addSelect(
                DB::raw('( select count( * )' .
                    ' from '  . $table_name_message .
                    ' where ' . $table_name_message . '.session_id = ' . $table_name_session . '.id' .
                    ' and '   . $table_name_session . '.id is not null ' .
                    ') as asking')
            )
            ->having('asking', '>', 0)
            ->orderBy($table_name_session . '.updated_at', 'DESC');
    }

    public function canEarning()
    {
        return $this
            ->sessions()
            ->canEarning();
    }
}
