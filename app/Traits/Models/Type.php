<?php

namespace App\Traits\Models;

use App\Traits\Scopes\RandemScope;
use App\Traits\Scopes\getIdInTypeScope;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Boots\globalBootTypeFunction;
use App\Traits\Attributes\transNameAttribute;

class Type extends Model
{
    use transNameAttribute;
    use globalBootTypeFunction;
    use getIdInTypeScope;
    use RandemScope;
}
