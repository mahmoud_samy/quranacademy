<?php

namespace App\Traits\Models;

use App\Traits\Scopes\UserScopes;
use App\Traits\Methods\UserMethods;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User;
use Laravel\Jetstream\HasProfilePhoto;
use App\Traits\Relations\UserRelations;
use Illuminate\Notifications\Notifiable;
use App\Traits\Attributes\UserAttributes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Authenticatable extends User
{
    use
        Notifiable,
        HasApiTokens,
        HasProfilePhoto,
        UserAttributes,
        UserMethods,
        UserScopes,
        HasFactory;

    use UserRelations {
        UserRelations::notifications insteadof Notifiable;
        UserRelations::tokens        insteadof HasApiTokens;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile_images_id',
        'first_name',
        'last_name',
        'email',
        'Locale',
        'gender',
        'date_of_birth',
        'password',
        'country_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'gender'            => 'boolean',
        'is_active'         => 'boolean',
        'is_blocked'        => 'boolean',
        'is_Approval'       => 'boolean',
        'is_profile_public' => 'boolean',
    ];

    protected $dates = [
        'email_verified_at',
        'date_of_birth',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'gender_label',
        'active_status_label',
        'active_status_color',
    ];

    public const STATUS_ACTIVE = 1;
    public const STATUS_INACTIVE = 0;

    public const GENDER_FEMALE = 0;
    public const GENDER_MALE = 1;

    /**
     * active_status_label function
     *
     * @return string
     */
    public function getActiveStatusLabelAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ][$this->is_active] ?? 'Active';
    }

    /**
     * active_status_color function
     *
     * @return string
     */
    public function getActiveStatusColorAttribute()
    {
        return [
            self::STATUS_ACTIVE => 'green',
            self::STATUS_INACTIVE => 'red',
        ][$this->is_active] ?? 'green';
    }

    /**
     * gender_label function
     *
     * @return string
     */
    public function getGenderLabelAttribute()
    {
        return [
            self::GENDER_MALE => 'Male',
            self::GENDER_FEMALE => 'Female',
        ][$this->gender] ?? '-';
    }
}
