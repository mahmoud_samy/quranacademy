<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasProfileVideo
{
    /**
     * Update the instructor's profile video.
     *
     * @param  \Illuminate\Http\UploadedFile  $video
     * @return void
     */
    public function updateProfileVideo(UploadedFile $video)
    {
        tap($this->profile_video_path, function ($previous) use ($video) {
            $this->forceFill([
                'profile_video_path' => $video->storeAs(
                    "profile-videos/$this->id",
                    $video->getClientOriginalName(),
                    ['disk' => $this->profileVideoDisk()]
                )
            ])->save();

            if ($previous) {
                Storage::disk($this->profileVideoDisk())->delete($previous);
            }
        });
    }

    /**
     * Delete the instructor's profile video.
     *
     * @return void
     */
    public function deleteProfileVideo()
    {
        Storage::disk($this->profileVideoDisk())->delete($this->profile_video_path);

        $this->forceFill([
            'profile_video_path' => null,
        ])->save();
    }

    /**
     * Get the URL to the instructor's profile video.
     *
     * @return string
     */
    public function getProfileVideoUrlAttribute()
    {
        return $this->profile_video_path
            ? Storage::disk($this->profileVideoDisk())->url($this->profile_video_path)
            : null;
    }

    /**
     * Get the name to the instructor's profile video.
     *
     * @return string
     */
    public function getProfileVideoNameAttribute()
    {
        return $this->profile_video_path
            ? basename($this->profile_video_url)
            : null;
    }

    /**
     * Get the disk that profile videos should be stored on.
     *
     * @return string
     */
    protected function profileVideoDisk()
    {
        return isset($_ENV['VAPOR_ARTIFACT_NAME']) ? 's3' : 'public';
    }
}
