<?php

namespace App\Notifications;

use App\Notifications\Classes\NotificationType;
use Illuminate\Broadcasting\PrivateChannel;

use App\Models\Notification;

class EditSessionsRequestForInstructor extends NotificationType {

    static public function getDataArrayMessageToTranslate( Notification $Notification ) {
        return [ ] ;
    }

}
