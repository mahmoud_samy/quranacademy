<?php

namespace App\Notifications\Classes;

use harby\services\Services\FireBaseCloudMessageingGun;

use App\Models\Notification;

class FireBaseChannel
{

    public function send($notifiable, Notification $notification)
    {
        $notifiable->refresh();
        return FireBaseCloudMessageingGun::fire(
            $notifiable->firebaseIds,
            $notification->fireBaseCloudMessageingNotification(),
            [
                "user"         => $notifiable->fireBaseCloudMessageing,
                "Notification" => $notification->fireBaseCloudMessageingData(),
                "other_data"   => $notification->other_data,
            ]
        );
    }
}
