<?php

namespace App\Notifications\Classes;

use Auth;

use App\Notifications\Classes\dataBaseChannal;
use App\Notifications\Classes\FireBaseChannel;

use App\Models\notification;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

abstract class NotificationType extends notification
{

    use SerializesModels;

    public $related_with;

    public notification $model;

    public array $data       = [];

    public $other_data       = '';

    public Int $viaType      = self::VIABOTH;

    const VIADATABASEONLY    = 1;

    const VIAPUSHONLY        = 2;

    const VIABOTH            = 3;

    static public string $getTypeMessageToTranslate = 'all';

    public function __construct($related_with, array $data = [])
    {
        $this->related_with = $related_with;
        $this->data         = $data;
    }

    public function via($notifiable): array
    {
        switch ($this->viaType) {
            case self::VIADATABASEONLY:
                return [dataBaseChannal::class];
            case self::VIAPUSHONLY:
                return [FireBaseChannel::class];
            case self::VIABOTH:
                return [dataBaseChannal::class, FireBaseChannel::class];
            default:
                return [dataBaseChannal::class, FireBaseChannel::class];
        }
    }

    public function broadcastType()
    {
        return class_basename(static::class);
    }

    public function fireBaseCloudMessageingNotification(): array
    {
        return [
            'title'        => env('APP_NAME'),
            'body'         => static::getMessageDatabaseTranslate($this),
            'click_action' => static::getDetails(static::class)['click_action'],
            "sound"        => "default"
        ];
    }

    public function fireBaseCloudMessageingData(): array
    {
        return [
            'kind'        => $this->broadcastType(),
            'kind_number' => static::getDetails(static::class)['kind_number'],
        ];
    }

    abstract static protected function getDataArrayMessageToTranslate(Notification $notification);

    public function toDatabase($notifiable)
    {
        $notifiable->Notifications()->for($this->related_with, static::class)->delete();
        return [
            'id'                => $this->id,
            'type'              => static::class,
            'notifiable_id'     => $notifiable->id,
            'notifiable_type'   => get_class($notifiable),
            'related_with_id'   => $this->related_with->id,
            'related_with_type' => get_class($this->related_with),
            'data'              => $this->data,
        ];
    }

    public static function getMessageNotifications(array $data, String $lang): string
    {
        return __('Notifications::' . static::$getTypeMessageToTranslate . '.' . static::class, $data, $lang);
    }

    public static function getMessageDatabaseTranslate(Notification $Notification)
    {
        return static::getMessageNotifications(static::getDataArrayMessageToTranslate($Notification), app()->getLocale());
    }
}
