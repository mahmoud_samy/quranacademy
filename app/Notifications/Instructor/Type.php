<?php

namespace App\Notifications\Instructor;

use App\Notifications\Classes\NotificationType;

use App\Models\Notification;

abstract class Type extends NotificationType {

    static public string $getTypeMessageToTranslate = 'instructor' ;

    static public function getDataArrayMessageToTranslate( Notification $Notification ) {
        return [ ] ;
    }

}