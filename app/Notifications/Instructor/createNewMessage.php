<?php

namespace App\Notifications\Instructor;

use App\Models\Notification;
use App\Models\session;
use Illuminate\Support\Facades\Log;

class createNewMessage extends Type
{

    public Int $viaType = self::VIAPUSHONLY;

    public function __construct(session $related_with, array $data = [])
    {
        $this->related_with = $related_with;
        $this->data         = $data;
        $this->other_data   = $data + [
            'session_id'       => $this->related_with->id,
            'sender_name'      => $this->related_with->student->name,
            'sender_image_url' => $this->related_with->student->profile_image ? $this->related_with->student->name : null,
        ];
    }

    static public function getDataArrayMessageToTranslate(Notification $Notification)
    {
        return [
            'student_name' => $Notification->related_with->student->name,
        ];
    }
}
