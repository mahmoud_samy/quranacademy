<?php

namespace App\Notifications\Student;

use App\Notifications\Classes\NotificationType;

use App\Models\Notification;

abstract class Type extends NotificationType {

    static public string $getTypeMessageToTranslate = 'student' ;

    static public function getDataArrayMessageToTranslate( Notification $Notification ) {
        return [ ] ;
    }

}