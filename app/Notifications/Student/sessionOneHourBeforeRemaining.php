<?php

namespace App\Notifications\Student;

use App\Models\session ;

class sessionOneHourBeforeRemaining extends Type {

    public Int $viaType= self::VIAPUSHONLY ;

    public function __construct( session $related_with , array $data = [ ] ) {
        $this -> related_with = $related_with ;
        $this -> data         = $data         ;
        $this -> other_data   = [ 'session_id' => $related_with -> id ] ;
    }

}