<?php

namespace App\Notifications\Student;

use App\Models\Notification;
use App\Models\session;

class createNewMessage extends Type
{

    public function __construct(session $related_with, array $data = [])
    {
        $this->related_with = $related_with;
        $this->data         = $data;
        $this->other_data   = $data + [
            'session_id'       => $this->related_with->id,
            'sender_name'      => $this->related_with->Instructor->name,
            'sender_image_url' => $this->related_with->Instructor->profile_image ? $this->related_with->Instructor->name : null,
        ];
    }

    static public function getDataArrayMessageToTranslate(Notification $Notification)
    {
        return [
            'Instructor_name' => $Notification->related_with->Instructor->name,
        ];
    }
}
