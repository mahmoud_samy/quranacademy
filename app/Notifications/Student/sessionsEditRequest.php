<?php

namespace App\Notifications\Student;

use App\Models\Notification;
use App\Models\session;

class sessionsEditRequest extends Type {

    public function __construct( session $related_with , array $data = [ ] ) {
        $this -> related_with = $related_with ;
        $this -> data         = $data         ;
        $this -> other_data   = [ 'session_id' => $related_with -> id ] ;
    }


    static public function getDataArrayMessageToTranslate( Notification $Notification ) {
        return [
            'Instructor_name' => $Notification -> related_with -> Instructor -> name
        ] ;
    }

}