<?php

namespace App\Notifications\SubInstructor;

use App\Notifications\Classes\NotificationType;
use Illuminate\Broadcasting\PrivateChannel;

use App\Models\Notification;

class createdButNotcompleted extends NotificationType {

    public Int $viaType = self::VIADATABASEONLY ;

    static public string $getTypeMessageToTranslate = 'SubInstructor' ;

    static public function getDataArrayMessageToTranslate( Notification $Notification ) {
        return [ ] ;
    }

}