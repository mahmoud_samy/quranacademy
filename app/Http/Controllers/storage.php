<?php

namespace App\Http\Controllers;

use App\Models\image;
use App\Services\Functions;
use App\Models\File as ModelsFile;
use Illuminate\Support\Facades\File;
use harby\services\Services\VideoStream;
use Illuminate\Support\Facades\Response;

class storage extends Controller
{

    public function getVideoFromModelFolder(string $crypticId)
    {

        $path = ModelsFile::Storage()->path(Functions::unhashThing($crypticId)['id']);

        if (!File::exists($path)) abort(404);

        else return VideoStream::playVideo($path);
    }

    public function getImageFromModelFolder(string $crypticId)
    {

        $path = image::Storage()->path(Functions::unhashThing($crypticId)['id']);

        if (!File::exists($path)) abort(404);

        else return Response::make(File::get($path), 200)->header("Content-Type", File::mimeType($path));
    }

    public function getFileFromModelFolder(string $crypticId)
    {

        $path = ModelsFile::Storage()->path(Functions::unhashThing($crypticId)['id']);

        if (!File::exists($path)) abort(404);

        else return Response::make(File::get($path), 200)->header("Content-Type", File::mimeType($path));
    }
}
