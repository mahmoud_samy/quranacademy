<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\SendEmailJob;
use App\Models\instructor;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Notifications\Instructor\createdButNotcompleted;
use Illuminate\Http\Request;

class EmailsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function create()
    {
        return view('admin.emails.form')->with([
            'users' => User::get(),
        ]);
    }


    public function store(Request $request)
    {
        if ($request->chanel_type != 'notification') {
            $details['title'] = $request->title;
            $details['body'] = $request->body;
            if ($request->users && $request->users[0] == 'all') {
                $users = User::get()->toArray();
                $details['emails'] = $users;
                dispatch(new SendEmailJob($details));
            } else {
                $users = array_values($request->users);
                $details['emails'] = $users;
                dispatch(new SendEmailJob($details));
            }
        }
        if ($request->chanel_type != 'email') {
            instructor::first()->notify(new createdButNotcompleted( instructor::first()));

            //send notificaitons here
        }
        return redirect()->back();
    }
}
