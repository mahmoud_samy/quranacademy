<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\student;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Student\StoreStudentRequest;
use App\Http\Requests\Admin\Student\UpdateStudentRequest;

class StudentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.students.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = cache()->rememberForever('countries', function () {
            return Country::all();
        });

        return view('admin.students.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Student\StoreStudentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request)
    {

        $student = new student();
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->gender = $request->gender;
        $student->date_of_birth = $request->date_of_birth;
        $student->is_active = $request->is_active ? true : false;
        $student->is_blocked = $request->is_blocked  ? true : false;
        $student->country_id = $request->country_id;
        $student->password = bcrypt($request->password);

        $student->save();

        if ($request->hasFile('image')) {
            $student->updateProfilePhoto($request->image);
        }

        return redirect()->route('admin.students.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  student $user
     * @return \Illuminate\Http\Response
     */
    public function edit(student $student)
    {
        $countries = cache()->rememberForever('countries', function () {
            return Country::all();
        });

        return view('admin.students.edit', compact('student', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Student\UpdateStudentRequest  $request
     * @param  student $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, student $student)
    {
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->gender = $request->gender;
        $student->date_of_birth = $request->date_of_birth;
        $student->is_active = $request->is_active ? true : false;
        $student->is_blocked = $request->is_blocked  ? true : false;
        $student->country_id = $request->country_id;

        if (!empty($request->password)) {
            $student->password = bcrypt($request->password);
        }

        $student->update();

        if ($request->hasFile('image')) {
            $student->updateProfilePhoto($request->image);
        }

        return redirect()->route('admin.students.index');
    }
}
