<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promocode;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Promocode\StorePromocodeRequest;
use App\Http\Requests\Admin\Promocode\UpdatePromocodeRequest;

class PromocodeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.promocodes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promocodes.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Promocode $promocode
     * @return \Illuminate\Http\Response
     */
    public function edit(Promocode $promocode)
    {
        return view('admin.promocodes.edit', compact('promocode'));
    }
}
