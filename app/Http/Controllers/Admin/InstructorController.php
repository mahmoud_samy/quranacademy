<?php

namespace App\Http\Controllers\Admin;

use App\Models\Country;
use App\Models\instructor;
use App\Models\courseType;
use App\Models\currencyType;
use App\Models\InstructorLevel;
use App\Models\studentAgeType;
use App\Models\rateMinutesType;
use App\Models\studentGenderType;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Instructor\StoreInstructorRequest;
use App\Http\Requests\Admin\Instructor\UpdateInstructorRequest;
use Illuminate\Http\Request;

class InstructorController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.instructors.index');
    }

    /**
     * Display a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(instructor $instructor)
    {
        return view('admin.instructors.show', compact('instructor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = cache()->rememberForever('countries', function () {
            return Country::all();
        });

        $course_types = cache()->rememberForever('course_types', function () {
            return courseType::all();
        });

        $student_gender_types = cache()->rememberForever('student_gender_types', function () {
            return studentGenderType::all();
        });

        $student_age_types = cache()->rememberForever('student_age_types', function () {
            return studentAgeType::all();
        });

        $currency_types = cache()->rememberForever('currency_types', function () {
            return currencyType::all();
        });

        $rate_minutes_types = cache()->rememberForever('rate_minutes_types', function () {
            return rateMinutesType::all();
        });
        $levels = InstructorLevel::all();

        return view('admin.instructors.create', compact('countries', 'course_types', 'student_gender_types', 'student_age_types', 'currency_types', 'rate_minutes_types','levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Instructor\StoreInstructorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInstructorRequest $request)
    {

        $instructor = new instructor();
        $instructor->first_name = $request->first_name;
        $instructor->last_name = $request->last_name;
        $instructor->email = $request->email;
        $instructor->gender = $request->gender;
        $instructor->date_of_birth = $request->date_of_birth;
        $instructor->country_id = $request->country_id;
        $instructor->years_of_experience = $request->years_of_experience;
        $instructor->description = $request->description;
        $instructor->courses_types_id = $request->courses_types_id;
        $instructor->student_gender_types_id = $request->student_gender_types_id;
        $instructor->student_age_types_id = $request->student_age_types_id;
        $instructor->currency_type_id = $request->currency_type_id;
        $instructor->public_rate_minutes_type_id = $request->public_rate_minutes_type_id;
        $instructor->is_active = $request->is_active ? true : false;
        $instructor->is_blocked = $request->is_blocked  ? true : false;
        $instructor->is_Approval = $request->is_Approval ? true : false;
        $instructor->is_profile_public = $request->is_profile_public ? true : false;
        $instructor->password = bcrypt($request->password);
        $instructor->instructor_level_id = $request->instructor_level_id;

        foreach ($request->rate_minutes_types as $key => $value) {
            $instructor[$key] = (int) $value;
        }

        $instructor->save();

        if ($request->hasFile('image')) {
            $instructor->updateProfilePhoto($request->image);
        }

        if ($request->hasFile('video')) {
            $instructor->updateProfileVideo($request->video);
        }

        return redirect()->route('admin.instructors.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function edit(instructor $instructor)
    {
        $countries = cache()->rememberForever('countries', function () {
            return Country::all();
        });

        $course_types = cache()->rememberForever('course_types', function () {
            return courseType::all();
        });

        $student_gender_types = cache()->rememberForever('student_gender_types', function () {
            return studentGenderType::all();
        });

        $student_age_types = cache()->rememberForever('student_age_types', function () {
            return studentAgeType::all();
        });

        $currency_types = cache()->rememberForever('currency_types', function () {
            return currencyType::all();
        });

        $rate_minutes_types = cache()->rememberForever('rate_minutes_types', function () {
            return rateMinutesType::all();
        });

        $levels = InstructorLevel::all();
        return view('admin.instructors.edit', compact('instructor', 'countries', 'course_types', 'student_gender_types', 'student_age_types', 'currency_types', 'rate_minutes_types','levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Instructor\UpdateInstructorRequest  $request
     * @param  instructor $instructor
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInstructorRequest $request, instructor $instructor)
    {
        $instructor->first_name = $request->first_name;
        $instructor->last_name = $request->last_name;
        $instructor->email = $request->email;
        $instructor->gender = $request->gender;
        $instructor->date_of_birth = $request->date_of_birth;
        $instructor->country_id = $request->country_id;
        $instructor->years_of_experience = $request->years_of_experience;
        $instructor->description = $request->description;
        $instructor->courses_types_id = $request->courses_types_id;
        $instructor->student_gender_types_id = $request->student_gender_types_id;
        $instructor->student_age_types_id = $request->student_age_types_id;
        $instructor->currency_type_id = $request->currency_type_id;
        $instructor->public_rate_minutes_type_id = $request->public_rate_minutes_type_id;
        $instructor->is_Approval = $request->is_Approval ? true : false;
        $instructor->is_profile_public = $request->is_profile_public ? true : false;
        $instructor->is_active = $request->is_active ? true : false;
        $instructor->is_blocked = $request->is_blocked  ? true : false;
        $instructor->instructor_level_id = $request->instructor_level_id;

        foreach ($request->rate_minutes_types as $key => $value) {
            $instructor[$key] = (int) $value;
        }

        if (!empty($request->password)) {
            $instructor->password = bcrypt($request->password);
        }

        $instructor->update();

        if ($request->hasFile('image')) {
            $instructor->updateProfilePhoto($request->image);
        }

        if ($request->hasFile('video')) {
            $instructor->updateProfileVideo($request->video);
        }

        return redirect()->route('admin.instructors.index');
    }
}
