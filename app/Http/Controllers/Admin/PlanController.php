<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Plan\StorePlanRequest;
use App\Http\Requests\Admin\Plan\UpdatePlanRequest;
use App\Models\Package;
use App\Models\Plan;
use App\Http\Controllers\Controller;

class PlanController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view('admin.plans.index');
    }

    public function create()
    {
        return view('admin.plans.create');
    }

    public function edit(Plan $plan)
    {
        $packages = Package::get();
        return view('admin.plans.edit', compact('plan','packages'));
    }
}
