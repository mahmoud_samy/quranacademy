<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Package\StorePackageRequest;
use App\Http\Requests\Admin\Package\UpdatePackageRequest;
use App\Models\Package;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PackageController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view('admin.packages.index');
    }

    public function create()
    {
        return view('admin.packages.create');
    }

    public function store(StorePackageRequest $request)
    {
        $package = new Package();
        $package->name = $request->name;
        $package->visibility = $request->visibility;
        $package->save();
        return redirect()->route('admin.packages.index');
    }

    public function edit(Package $package)
    {
        return view('admin.packages.edit', compact('package'));
    }

    public function update(UpdatePackageRequest $request, Package $package)
    {
        $package->name = $request->name;
        $package->visibility = $request->visibility;
        $package->update();
        return redirect()->route('admin.packages.index');
    }
}
