<?php

namespace App\Http\Controllers\Admin;

use App\Models\term;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Term\StoreTermRequest;
use App\Http\Requests\Admin\Term\UpdateTermRequest;

class TermController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.terms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.terms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Term\StoreTermRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTermRequest $request)
    {

        $term = new term;
        $term->header = $request->header;
        $term->body = $request->body;
        $term->lang = $request->lang;
        $term->is_active = $request->is_active ? true : false;
        $term->is_visible = $request->is_visible ? true : false;
        $term->save();

        return redirect()->route('admin.terms.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  term $term
     * @return \Illuminate\Http\Response
     */
    public function edit(term $term)
    {
        return view('admin.terms.edit', compact('term'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\Term\UpdateTermRequest  $request
     * @param  term $term
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTermRequest $request, term $term)
    {
        $term->header = $request->header;
        $term->body = $request->body;
        $term->lang = $request->lang;
        $term->is_active = $request->is_active ? true : false;
        $term->is_visible = $request->is_visible ? true : false;
        $term->update();

        return redirect()->route('admin.terms.index');
    }
}
