<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\InstructorLevel\StoreInstructorLevelRequest;
use App\Http\Requests\Admin\InstructorLevel\UpdateInstructorLevelRequest;
use App\Http\Requests\Admin\Package\StorePackageRequest;
use App\Http\Requests\Admin\Package\UpdatePackageRequest;
use App\Models\InstructorLevel;
use App\Models\Package;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InstructorLevelController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        return view('admin.instractor_levels.index');
    }

    public function create()
    {
        return view('admin.instractor_levels.create');
    }

    public function store(StoreInstructorLevelRequest $request)
    {
        $instructor_level = new InstructorLevel();
        $instructor_level->name = $request->name;
        $instructor_level->save();
        return redirect()->route('admin.instructor-levels.index');
    }

    public function edit(InstructorLevel $instructorLevel)
    {
        return view('admin.instractor_levels.edit')->with([
            'level' => $instructorLevel,
        ]);
    }

    public function update(UpdateInstructorLevelRequest $request, InstructorLevel $instructorLevel)
    {
        $instructorLevel->name = $request->name;
        $instructorLevel->update();
        return redirect()->route('admin.instructor-levels.index');
    }

    public function destroy(InstructorLevel $instructorLevel)
    {
        $instructorLevel->delete();
        return redirect()->route('admin.instructor-levels.index');
    }
}
