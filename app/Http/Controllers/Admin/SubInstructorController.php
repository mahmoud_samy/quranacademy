<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class SubInstructorController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sub-instructors.index');
    }
}
