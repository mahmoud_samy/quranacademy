<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local = ($request->hasHeader('localization')) ? $request->header('localization') : 'en';
        app()->setLocale($local);
        if (Auth::check() && Auth::user()->Locale != $local) Auth::user()->setLocale();
        return $next($request);
    }
}
