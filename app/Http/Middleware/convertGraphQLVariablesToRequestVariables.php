<?php

namespace App\Http\Middleware;

use Closure;

class convertGraphQLVariablesToRequestVariables {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request , Closure $next , $vars = [ ] ) {
        if ( $request -> operations ) $vars = json_decode( $request -> operations , true ) [ 'variables' ] ;
        if ( $request -> variables  ) $vars = $request -> variables ;
        if ( is_array( $vars ) && count( $vars ) ){
            $request -> request -> add( $vars );
            if ( $request -> variables === null  ) $request -> request -> add( [ 'variables' => $vars ] );
        };
        return $next( $request );
    }
}
