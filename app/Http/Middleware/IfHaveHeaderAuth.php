<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IfHaveHeaderAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) return $next($request);
        if (Auth::guard('instructor')->check() && Auth::guard('instructor')->user()->tokenCan('instructor')) Auth::login(Auth::guard('instructor')->user()->touching());
        if (Auth::guard('student')->check() && Auth::guard('student')->user()->tokenCan('student')) Auth::login(Auth::guard('student')->user()->touching());
        if (Auth::guard('SubInstructor')->check() && Auth::guard('SubInstructor')->user()->tokenCan('SubInstructor')) Auth::login(Auth::guard('SubInstructor')->user()->touching());
        return $next($request);
    }
}
