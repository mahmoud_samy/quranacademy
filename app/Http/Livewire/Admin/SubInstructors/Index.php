<?php

namespace App\Http\Livewire\Admin\SubInstructors;

use App\Http\Livewire\Admin\MainComponent;
use App\Models\SubInstructor;

class Index extends MainComponent
{

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
        'email' => null,
    ];

    public function getRowsQuery()
    {
        return SubInstructor::query()
            ->with('country')
            ->when($this->filters['search'], fn ($query, $search) => $query->whereMacroSearch('first_name', strtolower($search))->orWhereMacroSearch('last_name', strtolower($search)))
            ->when($this->filters['email'], fn ($query, $email) => $query->whereMacroSearch('email', strtolower($email)));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' instructors');
    }

    public function render()
    {
        return view('livewire.admin.sub-instructors.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
