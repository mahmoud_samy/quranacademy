<?php

namespace App\Http\Livewire\Admin\Countries;

use App\Models\Country;
use App\Http\Livewire\Admin\MainComponent;

class Index extends MainComponent
{

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
    ];

    public function getRowsQuery()
    {
        return Country::query()
            ->when($this->filters['search'], fn ($query, $search) => $query->whereMacroSearch('name', $search));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' promocodes');
    }

    public function render()
    {
        return view('livewire.admin.countries.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
