<?php

namespace App\Http\Livewire\Admin\Students;

use App\Http\Livewire\Admin\MainComponent;
use App\Models\student;

class Index extends MainComponent
{

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
        'email' => null,
    ];

    public function getRowsQuery()
    {
        return student::query()
            ->with('country')
            ->when($this->filters['search'], fn ($query, $search) => $query->whereMacroSearch('first_name', strtolower($search))->orWhereMacroSearch('last_name', strtolower($search)))
            ->when($this->filters['email'], fn ($query, $email) => $query->whereMacroSearch('email', strtolower($email)));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' students');
    }

    public function render()
    {
        return view('livewire.admin.students.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
