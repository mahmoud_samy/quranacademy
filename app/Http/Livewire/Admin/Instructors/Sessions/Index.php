<?php

namespace App\Http\Livewire\Admin\Instructors\Sessions;

use App\Http\Livewire\Admin\MainComponent;
use App\Models\courseType;
use App\Models\currencyType;
use App\Models\instructor;
use App\Models\rateMinutesType;
use Carbon\Carbon;

class Index extends MainComponent
{
    public instructor $instructor;
    public $courses_type;

    public $currency_types;

    public $rate_minutes_types;

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'course_type' => null,
        'currency_type' => null,
        'rate_minutes_type' => null,
        'status' => null,
        'start-date-min' => null,
        'start-date-max' => null,
        'end-date-min' => null,
        'end-date-max' => null,
    ];

    public function mount()
    {
        $this->course_types = cache()->rememberForever('courses_type', function () {
            return courseType::all();
        });

        $this->currency_types = cache()->rememberForever('currency_types', function () {
            return currencyType::all();
        });

        $this->rate_minutes_types = cache()->rememberForever('rate_minutes_types', function () {
            return rateMinutesType::all();
        });
    }

    public function getRowsQuery()
    {
        return $this->instructor->sessions()
            ->with('instructor', 'student', 'currency_type', 'course_type', 'rate_minutes_type')
            ->when($this->filters['course_type'], fn ($query, $course_type) => $query->where('course_type_id', $course_type))
            ->when($this->filters['currency_type'], fn ($query, $currency_type) => $query->where('currency_type_id', $currency_type))
            ->when($this->filters['rate_minutes_type'], fn ($query, $rate_minutes_type) => $query->where('rate_minutes_type_id', $rate_minutes_type))
            ->when($this->filters['status'], fn ($query, $status) => $query->filterByStatus($status))
            ->when($this->filters['start-date-min'], fn ($query, $date) => $query->where('start_at', '>=', Carbon::parse($date)))
            ->when($this->filters['end-date-min'], fn ($query, $date) => $query->where('end_at', '>=', Carbon::parse($date)))
            ->when($this->filters['start-date-max'], fn ($query, $date) => $query->where('start_at', '<=', Carbon::parse($date)))
            ->when($this->filters['end-date-max'], fn ($query, $date) => $query->where('end_at', '<=', Carbon::parse($date)))
            ->orderBy('start_at');
    }

    public function render()
    {
        return view('livewire.admin.instructors.sessions.index', [
            'rows' => $this->rows
        ]);
    }
}
