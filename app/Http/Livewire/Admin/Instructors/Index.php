<?php

namespace App\Http\Livewire\Admin\Instructors;

use App\Models\instructor;
use App\Http\Livewire\Admin\MainComponent;
use App\Models\InstructorLevel;
use App\Scopes\ActiveInstructorScope;

class Index extends MainComponent
{

    public $levels = [];
    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
        'email' => null,
        'instructor_level_id' => null,
    ];

    public function mount()
    {
        $this->levels = InstructorLevel::all();
    }

    public function getRowsQuery()
    {
        return instructor::query()->withoutGlobalScope(ActiveInstructorScope::class)
            ->with('country')
            ->when($this->filters['search'], fn($query, $search) => $query->whereMacroSearch('first_name', strtolower($search))->orWhereMacroSearch('last_name', strtolower($search)))
            ->when($this->filters['email'], fn($query, $email) => $query->whereMacroSearch('email', strtolower($email)))
            ->when($this->filters['instructor_level_id'], fn($query, $instructor_level_id) => $query->whereMacroSearch('instructor_level_id', $instructor_level_id));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' instructors');
    }

    public function render()
    {
        return view('livewire.admin.instructors.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
