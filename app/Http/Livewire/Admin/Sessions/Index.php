<?php

namespace App\Http\Livewire\Admin\Sessions;

use App\Models\courseType;
use App\Models\currencyType;
use App\Models\rateMinutesType;
use App\Http\Livewire\Admin\MainComponent;
use App\Models\session;
use Carbon\Carbon;

class Index extends MainComponent
{
    public $courses_type;

    public $currency_types;

    public $rate_minutes_types;

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'course_type' => null,
        'currency_type' => null,
        'rate_minutes_type' => null,
        'status' => null,
        'start-date-min' => null,
        'start-date-max' => null,
        'end-date-min' => null,
        'end-date-max' => null,
        'booked' => null,
    ];

    public function mount()
    {
        $this->course_types = cache()->rememberForever('courses_type', function () {
            return courseType::all();
        });

        $this->currency_types = cache()->rememberForever('currency_types', function () {
            return currencyType::all();
        });

        $this->rate_minutes_types = cache()->rememberForever('rate_minutes_types', function () {
            return rateMinutesType::all();
        });
    }

    public function getRowsQuery()
    {
        return session::query()->orderBy('start_at','DESC')
        ->with('instructor', 'student', 'currency_type', 'course_type', 'rate_minutes_type')
            ->when($this->filters['course_type'], fn ($query, $course_type) => $query->where('course_type_id', $course_type))
            ->when($this->filters['currency_type'], fn ($query, $currency_type) => $query->where('currency_type_id', $currency_type))
            ->when($this->filters['rate_minutes_type'], fn ($query, $rate_minutes_type) => $query->where('rate_minutes_type_id', $rate_minutes_type))
            ->when($this->filters['status'], fn ($query, $status) => $query->filterByStatus($status))
            ->when($this->filters['booked'], fn ($query, $booked) => $query->filterByBooked($booked))
            ->when($this->filters['start-date-min'], fn ($query, $date) => $query->where('start_at', '>=', Carbon::parse($date)))
            ->when($this->filters['end-date-min'], fn ($query, $date) => $query->where('end_at', '>=', Carbon::parse($date)))
            ->when($this->filters['start-date-max'], fn ($query, $date) => $query->where('start_at', '<=', Carbon::parse($date)))
            ->when($this->filters['end-date-max'], fn ($query, $date) => $query->where('end_at', '<=', Carbon::parse($date)));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' sessions');
    }

    public function render()
    {
        return view('livewire.admin.sessions.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
