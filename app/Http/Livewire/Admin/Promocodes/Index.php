<?php

namespace App\Http\Livewire\Admin\Promocodes;

use App\Models\Promocode;
use Illuminate\Support\Str;
use App\Http\Livewire\Admin\MainComponent;

class Index extends MainComponent
{

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
    ];

    public function getRowsQuery()
    {
        return Promocode::query()
            ->when($this->filters['search'], fn ($query, $search) => $query->whereMacroSearch('code', Str::upper($search)));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' promocodes');
    }

    public function render()
    {
        return view('livewire.admin.promocodes.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
