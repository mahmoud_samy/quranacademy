<?php

namespace App\Http\Livewire\Admin\Promocodes;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\Promocode;
use Illuminate\Support\Str;

class Form extends Component
{
    public $promocode;

    public $code;
    public $title;
    public $description;
    public $related_to;
    public $type;
    public $reward;
    public $is_disposable;
    public $expires_at;

    public function mount()
    {
        if ($this->promocode) {
            $this->code = $this->promocode->code;
            $this->title = $this->promocode->title;
            $this->description = $this->promocode->description;
            $this->type = $this->promocode->type;
            $this->related_to = $this->promocode->related_to;
            $this->reward = $this->promocode->reward;
            $this->is_disposable = $this->promocode->is_disposable;
            $this->expires_at = $this->promocode->expires_at;
        }
    }

    public function GenerateCode()
    {
        $this->code = Str::upper(Str::random(6));
    }

    public function save()
    {
        if (!$this->promocode) {
            $this->createPromocode();
        } else {
            $this->updatePromocode();
        }

        return redirect()->route('admin.promocodes.index');
    }

    public function createPromocode()
    {
        $this->validateCreatePromocode();

        Promocode::create([
            'title' => $this->title,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'reward' => $this->reward,
            'related_to' => $this->related_to,
            'is_disposable' => $this->is_disposable ? true : false,
            'expires_at' => $this->expires_at ? Carbon::parse($this->expires_at) : null
        ]);
    }

    public function updatePromocode()
    {
        $this->validateUpdatePromocode();

        $this->promocode->update([
            'title' => $this->title,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'reward' => $this->reward,
            'related_to' => $this->related_to,
            'is_disposable' => $this->is_disposable ? true : false,
            'expires_at' => $this->expires_at ? Carbon::parse($this->expires_at) : null
        ]);
    }

    public function validateCreatePromocode()
    {
        $this->validate([
            'title' => 'required|string',
            'code' => 'required|string|unique:promocodes,code',
            'description' => 'string',
            'type' => 'required',
            'reward' => 'required',
            'related_to' => 'nullable',
            'is_disposable' => 'nullable',
            'expires_at' => 'nullable',
        ]);
    }

    public function validateUpdatePromocode()
    {
        $this->validate([
            'title' => 'required|string',
            'code' => 'required|string|unique:promocodes,code,' . $this->promocode->id,
            'description' => 'string',
            'type' => 'required',
            'reward' => 'required',
            'related_to' => 'nullable',
            'is_disposable' => 'nullable',
            'expires_at' => 'nullable',
        ]);
    }

    public function render()
    {
        return view('livewire.admin.promocodes.form');
    }
}
