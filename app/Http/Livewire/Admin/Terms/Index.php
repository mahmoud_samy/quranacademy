<?php

namespace App\Http\Livewire\Admin\Terms;

use App\Models\term;
use App\Http\Livewire\Admin\MainComponent;

class Index extends MainComponent
{

    protected $listeners = [
        'refreshImport' => '$refresh'
    ];

    public $filters = [
        'search' => null,
        'email' => null,
    ];

    public function getRowsQuery()
    {
        return term::query()
            ->when($this->filters['search'], fn ($query, $search) => $query->whereMacroSearch('header', strtolower($search)));
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' terms');
    }

    public function render()
    {
        return view('livewire.admin.terms.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
