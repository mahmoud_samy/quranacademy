<?php

namespace App\Http\Livewire\Admin\Packages;

use App\Models\courseType;
use App\Models\currencyType;
use App\Models\Package;
use App\Http\Livewire\Admin\MainComponent;
use Carbon\Carbon;

class Index extends MainComponent
{
    public function mount(){}

    public function getRowsQuery()
    {
        return Package::query();
    }

    //TODO: find a cleaner way to handle this
    public function deleteSelected()
    {
        $deleteCount = $this->selectedRowsQuery->count();

        $this->selectedRowsQuery->get()->each(function ($user) {
            $user->delete();
        });

        $this->showDeleteModal = false;

        $this->macroNotify('You\'ve deleted ' . $deleteCount . ' sessions');
    }

    public function render()
    {
        return view('livewire.admin.packages.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
