<?php

namespace App\Http\Livewire\Admin\InstructorLevels;

use App\Http\Livewire\Admin\MainComponent;
use App\Models\InstructorLevel;
use Livewire\Component;

class Index extends MainComponent
{
    public function mount()
    {
    }

    public function getRowsQuery()
    {
        return InstructorLevel::query();
    }

    public function render()
    {
        return view('livewire.admin.instructor-levels.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
