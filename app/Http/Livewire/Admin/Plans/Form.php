<?php

namespace App\Http\Livewire\Admin\Plans;

use App\Http\Requests\Admin\Plan\StorePlanRequest;
use App\Http\Requests\Admin\Plan\UpdatePlanRequest;
use App\Models\InstructorLevel;
use App\Models\Package;
use App\Models\Plan;
use Livewire\Component;

class Form extends Component
{
    public $package_id;
    public $visibility;
    public $name;
    public $title;
    public $expire_date;
    public $hours_limitation;
    public $price;
    public $discount;
    public $net_price;

    public $plan;
    public $instructor_level_id;
    public $levels = [];
    public $packages = [];


    public function mount()
    {
        $this->packages = Package::all();
        $this->levels = InstructorLevel::all();

        if ($this->plan) {
            $this->package_id = $this->plan->package_id;
            $this->visibility = $this->plan->visibility;
            $this->name = $this->plan->name;
            $this->title = $this->plan->title;
            $this->expire_date = $this->plan->expire_date;
            $this->hours_limitation = $this->plan->hours_limitation;
            $this->price = $this->plan->price;
            $this->discount = $this->plan->discount;
            $this->net_price = $this->plan->net_price;
            $this->instructor_level_id = $this->plan->instructor_level_id;
        } else {
            $this->package_id = $this->packages->first()->id;
            $this->visibility = 1;
        }

    }


    public function submit()
    {
        if ($this->plan) {
        $this->update();
        }else{
            $this->store();
        }
    }

    public function store()
    {
        $this->validate([
            'package_id' => ['required'],
            'name' => ['required', 'string', 'max:255','min:3'],
            'title' => ['required', 'string', 'max:255','min:3'],
            'hours_limitation' => ['required', 'integer'],
            'expire_date' => ['required', 'integer'],
            'price' => ['required'],
        ]);
        $plan = new Plan();
        $plan->package_id = $this->package_id;
        $plan->visibility = $this->visibility;
        $plan->title = $this->title;
        $plan->name = $this->name;
        $plan->hours_limitation = $this->hours_limitation;
        $plan->expire_date = $this->expire_date;
        $plan->price = $this->price;
        $plan->instructor_level_id = $this->instructor_level_id;
        $plan->discount = $this->discount ?? 0;
        $plan->net_price = $this->price * ((100 - $this->discount) / 100);

        $plan->save();
        return redirect()->route('admin.plans.index');
    }
    public function update()
    {

        $this->validate([
            'package_id' => ['required'],
            'name' => ['required', 'string', 'max:255','min:3'],
            'title' => ['required', 'string', 'max:255','min:3'],
            'hours_limitation' => ['required', 'integer'],
            'expire_date' => ['required', 'integer'],
            'price' => ['required'],
        ]);

        $this->plan->package_id = $this->package_id;
        $this->plan->visibility = $this->visibility;
        $this->plan->title = $this->title;
        $this->plan->name = $this->name;
        $this->plan->hours_limitation = $this->hours_limitation;
        $this->plan->expire_date = $this->expire_date;
        $this->plan->price = $this->price;
        $this->plan->instructor_level_id = $this->instructor_level_id;
        $this->plan->discount = $this->discount ?? 0;
        $this->plan->net_price = $this->price * ((100 - $this->discount) / 100);



        $this->plan->update();
        return redirect()->route('admin.plans.index');
    }

    public function updatingPrice($value)
    {
        if ($this->discount > 0) {
            $this->net_price = $value * ((100 - $this->discount) / 100);
        } else {
            $this->net_price = $value;
        }
    }
    public function updatingDiscount($value)
    {
        if ($value > 0) {
            $this->net_price = $this->price * ((100 - $value) / 100);
        } else {
            $this->net_price = $this->price;
        }
    }

    public function render()
    {
        return view('livewire.admin.plans.form');
    }
}
