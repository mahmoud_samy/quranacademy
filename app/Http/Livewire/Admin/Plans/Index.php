<?php

namespace App\Http\Livewire\Admin\Plans;


use App\Models\Package;
use App\Http\Livewire\Admin\MainComponent;
use App\Models\Plan;

class Index extends MainComponent
{
    public function mount(){}

    public $price = 10;
    public $discount;
    public $net_price;

    public function test()
    {
        dd('sdf');
    }
    public function getRowsQuery()
    {
        return Plan::query();
    }

    public function render()
    {
        return view('livewire.admin.plans.index', [
            'rows' => $this->rows
        ])->layout('layouts.admin');
    }
}
