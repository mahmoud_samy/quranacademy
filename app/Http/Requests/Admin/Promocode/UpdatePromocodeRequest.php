<?php

namespace App\Http\Requests\Admin\Promocode;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePromocodeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'reward' => ['required', 'string', 'max:255'],
            'extra_info' => ['required', 'string', 'max:255'],
            'expires_at' => ['required'],
        ];
    }
}
