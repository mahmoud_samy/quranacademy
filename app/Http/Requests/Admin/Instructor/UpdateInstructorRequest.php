<?php

namespace App\Http\Requests\Admin\Instructor;

use App\Models\rateMinutesType;
use Laravel\Fortify\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class UpdateInstructorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'country_id' => ['required', 'exists:countries,id'],
            'years_of_experience' => ['required', 'numeric'],
            'courses_types_id' => ['required', 'exists:course_types,id'],
            'student_gender_types_id' => ['required', 'exists:student_gender_types,id'],
            'student_age_types_id' => ['required', 'exists:student_age_types,id'],
            'currency_type_id' => ['required', 'exists:currency_types,id'],
            'public_rate_minutes_type_id' => ['required', 'exists:rate_minutes_types,id'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:instructors,email,' . $this->instructor->id],
            'password' => ['nullable', 'string', new Password, 'confirmed'],
            'image' => 'nullable|image|mimes:jpeg,png,jpg,bmp,gif,svg|max:1024',
            'video' => 'nullable|max:50000',
            'rate_minutes_types' => ['required', 'array'],
            'rate_minutes_types.*' => ['required', 'numeric'],
        ];
    }
}
