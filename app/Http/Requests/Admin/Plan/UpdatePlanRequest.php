<?php

namespace App\Http\Requests\Admin\Plan;

use Laravel\Fortify\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_id' => ['required'],
            'name' => ['required', 'string', 'max:255','min:3'],
            'title' => ['required', 'string', 'max:255','min:3'],
            'hours_limitation' => ['required', 'integer'],
            'price' => ['required'],
        ];
    }
}
