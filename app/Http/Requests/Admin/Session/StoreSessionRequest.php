<?php

namespace App\Http\Requests\Admin\Session;

use App\Actions\Fortify\PasswordValidationRules;
use Illuminate\Foundation\Http\FormRequest;

class StoreSessionRequest extends FormRequest
{
    use PasswordValidationRules;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'country_id' => ['required', 'exists:countries,id'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:students'],
            'password' => $this->passwordRules(),
            'image' => 'nullable|image|mimes:jpeg,png,jpg,bmp,gif,svg|max:1024'
        ];
    }
}
