<?php

namespace Tests\Traits;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\instructor;
use App\Models\student;
use App\Models\courseType;
use App\Models\rateMinutesType;
use App\Models\session;

use Illuminate\Http\UploadedFile ;

trait sessiontrait {

	public $sessionstype = '
		id
		date
		time
		rate_salary
		created_at
		updated_at
		DateTime
		cancel_at
		note
		rating{
			student{
				id
			}
			instructor{
				id
			}
			reviews
			rating
		}
		student{
			id
		}
		instructor{
			id
		}
		rate_minutes_type{
			id
		}
		currency_type{
			id
		}
		course_type{
			id
		}
		materials{
			id
			name
			size
		}
	';

	public function sessionsCreate ( ) {
		return "mutation( \$data : sessionsCreateInput ! ) { sessionsCreate ( data : \$data ) {
			$this->Status
			sessions{ $this->sessionstype }
		} }";
	}

	public function sessionsEdit ( ) {
		return "mutation( \$data : sessionsEditInput ! ) { sessionsEdit ( data : \$data  ) {
			$this->Status
			session{ $this->sessionstype }
		} }";
	}

	public function sessionsDelete ( ) {
		return "mutation( \$data : sessionsDeleteInput ! ) { sessionsDelete ( data : \$data ) {
			$this->Status
		} }";
	}

	public function sessionsCancel ( ) {
		return "mutation( \$data : sessionsCancelInput ! ) { sessionsCancel ( data : \$data ) {
			$this->Status
			session{ $this->sessionstype }
		} }";
	}

	public function sessionsAcceptEdit ( ) {
		return "mutation( \$data : sessionsAcceptEditInput ! ) { sessionsAcceptEdit ( data : \$data ) {
			$this->Status
			session{ $this->sessionstype }
		} }";
	}

	public function sessionsBook ( ) {
		return "mutation( \$data : sessionsBookInput ! ) { sessionsBook ( data : \$data ) {
			$this->Status
			sessions { $this->sessionstype }
		} }";
	}

	public function sessionsUpload ( ) {
		return "mutation( \$data : sessionsUploadInput ! ) { sessionsUpload ( data : \$data ) {
			$this->Status
			session { $this->sessionstype }
		} }";
	}

	public function sessionsChange ( ) {
		return "mutation( \$data : sessionsChangeInput ! ) { sessionsChange ( data : \$data ) {
			$this->Status
			session { $this->sessionstype }
		} }";
	}
	public function sessionsRating ( ) {
		return "mutation( \$data : sessionsRatingInput ! ) { sessionsRating ( data : \$data ) {
			$this->Status
			session { $this->sessionstype }
		} }";
	}

	public function meInstructors ( ) {
		return "query(
			\$is_confirmed: Boolean
			\$filter : sessionsByUnitsFilterInput
		){
			meInstructors{
				total_students_counter
				total_earning_counter
				total_profile_views_counter
				sessionsByUnits(
					is_confirmed : \$is_confirmed
					filter : \$filter
				){ 
					status{
						Date
						counter
						counterConfirmed
						counterNotConfirmed
					}
					sessions{
						$this->sessionstype
					}
				}
			}
		}";
	}

	public function meStudent ( ) {
		return "{
			meStudents{
				CountThisWeekStudyHours
				CountThisWeekSessionSchedule
				CountThisWeekSession
				sessionsByUnits{ 
					status{
						counter
						Date
						greater_than_three
					}
					sessions {
						$this->sessionstype
					}
				}
			}
		}";
	}

	public function sessionsCreateArray ( ) {
		return [
			'course_type_id'       => courseType      ::    RandemOne (   ) -> id                                   ,
			'rate_minutes_type_id' => rateMinutesType ::    RandemOne (   ) -> id                                   ,
			'date'                 => now            ( ) -> addDays   ( 1 ) -> format    ( 'Y-m-d' )                ,
			'time'                 => now            ( ) -> addHours  ( 1 ) -> addMinutes( 1 ) -> format( 'H:i:s' ) ,
		] ;
	}

	public function sessionsEditArray ( ) {
		return [
			'course_type_id' => courseType ::     RandemOne (   ) -> id                                   ,
			'date'           => now        ( ) -> addDays   ( 1 ) -> format     ( 'Y-m-d' )               ,
			'time'           => now        ( ) -> addHours  ( 4 ) -> addMinutes( 1 ) -> format( 'H:i:s' ) ,
		] ;
	}

}