<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Models\instructor;
use App\Models\student;
use App\Models\SubInstructor;
use App\Models\Pincode;
use App\Models\variabledb;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;
use App\Models\image;
use App\Models\languageType;
use App\Models\rateMinutesType;
use App\Models\rating;

use Illuminate\Http\UploadedFile;

class PersonTest extends TestCase
{

    public function PersonEdit()
    {
        return 'mutation( $data : PersonEditInput ! ) {
			PersonEdit (
				data : $data 
			) {
				Student{
                    id
					first_name
                    last_name
                    gender
					date_of_birth
					country{ id name }
				}
				Instructor{
                    id
					first_name
                    last_name
                    gender
                    date_of_birth
                    years_of_experience
                    description
                    is_profile_public
                    rate_15_min
                    rate_30_min
					rate_60_min
					public_rate_minutes_type_id
					public_rate_minutes
					country{ id name }
                    currency_type{
                        id
                    }
                    student_gender_types{
                        id
                    }
                    student_age_types{
                        id
                    }
                    Languages{
                        id
                    }
                    certificates{
                        id
                    }
				}
				SubInstructor{
                    id
					first_name
                    last_name
                    gender
                    date_of_birth
                    years_of_experience
                    description
                    is_profile_public
                    rate_15_min
                    rate_30_min
					rate_60_min
					public_rate_minutes_type_id
					country{ id name }
                    currency_type{
                        id
                    }
                    student_gender_types{
                        id
                    }
                    student_age_types{
                        id
                    }
                    Languages{
                        id
                    }
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
    }

    public function PersonChangePassword()
    {
        return 'mutation(
			$data : PersonChangePasswordInput !
		) {
			PersonChangePassword (
				data : $data 
			) {
				Status {
					code
					check
					title
					message
				}
			}
		}';
    }

    public function makevar($ifPass = true)
    {
        return [
            'first_name'                  => $this->faker()->unique()->name,
            'last_name'                   => $this->faker()->unique()->name,
            'gender'                      => $this->faker()->boolean,
            'date_of_birth'               => now()->subYears(5)->format('Y-m-d'),

            'years_of_experience'         => $this->faker()->unique()->randomNumber,
            'description'                 => $this->faker()->unique()->text,
            'is_profile_public'           => $this->faker()->boolean(),
            'rate_15_min'                 => $this->faker()->unique()->randomNumber,
            'rate_30_min'                 => $this->faker()->unique()->randomNumber,
            'rate_60_min'                 => $this->faker()->unique()->randomNumber,

            'profile_image'               => $this->makeFile('/app/public/logo'),
            'profile_vidoe'               => $this->makeFile('/app/public/logo'),

            'courses_types_id'            => courseType::RandemOne()->id,
            'student_gender_types_id'     => studentGenderType::RandemOne()->id,
            'student_age_types_id'        => studentAgeType::RandemOne()->id,
            'currency_type_id'            => currencyType::RandemOne()->id,
            'public_rate_minutes_type_id' => rateMinutesType::RandemOne()->id,
            'country_id'                  => 1,
            'Spoken_Language_User'        => [
                languageType::RandemOne()->id,
                languageType::RandemOne()->id,
                languageType::RandemOne()->id,
            ],
        ];
    }

    public function testStudent()
    {

        $student = student::factory()->create();

        $this->assertResponseStatusMessageAndLog(
            $req = $this
                ->withHeaders(['Authorization' => $this->login($student)])
                ->GraphQLFiles($this->PersonEdit(), ['data' => $data = $this->makevar()]),
            'PersonMutations.PersonEdit.successfully',
            true,
            200,
            [],
            'successfully'
        );

        $req->assertJson(['data' => ['PersonEdit' =>  ['Instructor' => null, 'SubInstructor' => null, 'Student' => [
            'id'            => $student->id,
            'first_name'    => $data['first_name'],
            'last_name'     => $data['last_name'],
            'gender'        => $data['gender'],
            'date_of_birth' => (string) $data['date_of_birth'],
        ]]]]);
    }

    public function testInstructor()
    {

        instructor::factory()->create()->add_letters([$this->makeFile('/app/public/logo')]);

        $instructor = instructor::factory()->create();

        $instructor->add_certificates([$this->makeFile('/app/public/logo'), $this->makeFile('/app/public/logo')]);

        $data['delete_certificates_ids'] = $instructor->certificates->pluck('id')->all();

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->PersonEdit(), ['data' => $data = $this->makevar()]),
            'PersonMutations.PersonEdit.successfully',
            true,
            200,
            [],
            'successfully'
        );

        $response->assertJson(['data' => ['PersonEdit' =>  ['Student' => null, 'SubInstructor' => null, 'Instructor' => [
            'id'                          =>            $instructor->id,
            'date_of_birth'               => (string) $data['date_of_birth'],
            'first_name'                  =>            $data['first_name'],
            'last_name'                   =>            $data['last_name'],
            'gender'                      =>            $data['gender'],
            'years_of_experience'         =>            $data['years_of_experience'],
            'description'                 =>            $data['description'],
            'is_profile_public'           =>            $data['is_profile_public'],
            'rate_15_min'                 =>            $data['rate_15_min'],
            'rate_30_min'                 =>            $data['rate_30_min'],
            'rate_60_min'                 =>            $data['rate_60_min'],
            'public_rate_minutes_type_id' =>            $data['public_rate_minutes_type_id'],
            'currency_type'               => ['id' =>  $data['currency_type_id']],
            'student_gender_types'        => ['id' =>  $data['student_gender_types_id']],
            'student_age_types'           => ['id' =>  $data['student_age_types_id']],
        ]]]]);

        $response->assertJsonCount(count(array_unique($data['Spoken_Language_User'])), 'data.PersonEdit.Instructor.Languages');

        $response->assertJsonCount(2, 'data.PersonEdit.Instructor.certificates');
    }

    public function testSubInstructor()
    {

        $SubInstructor = SubInstructor::factory()->create();

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($SubInstructor)])
                ->GraphQLFiles($this->PersonEdit(), ['data' => $data = $this->makevar()]),
            'PersonMutations.PersonEdit.successfully',
            true,
            200,
            [],
            'successfully'
        );

        $response->assertJson(['data' => ['PersonEdit' =>  ['Student' => null, 'Instructor' => null, 'SubInstructor' => [
            'id'                          =>            $SubInstructor->id,
            'date_of_birth'               => (string) $data['date_of_birth'],
            'first_name'                  =>            $data['first_name'],
            'last_name'                   =>            $data['last_name'],
            'gender'                      =>            $data['gender'],
            'years_of_experience'         =>            $data['years_of_experience'],
            'description'                 =>            $data['description'],
            'rate_15_min'                 =>            $data['rate_15_min'],
            'rate_30_min'                 =>            $data['rate_30_min'],
            'rate_60_min'                 =>            $data['rate_60_min'],
            'public_rate_minutes_type_id' =>            $data['public_rate_minutes_type_id'],
            'currency_type'               => ['id' =>  $data['currency_type_id']],
            'student_gender_types'        => ['id' =>  $data['student_gender_types_id']],
            'student_age_types'           => ['id' =>  $data['student_age_types_id']],
            'is_profile_public'           => null,
        ]]]]);

        $response->assertJsonCount(count(array_unique($data['Spoken_Language_User'])), 'data.PersonEdit.SubInstructor.Languages');
    }

    public function testChangePasswordEditunAuth()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1D',
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.unAuth',
            false,
            502,
            [],
            'successfully'
        );
    }

    public function testChangePasswordEditOldPasswordRequired()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders([
                    'Authorization' => $this->login(instructor::factory()->create()),
                ])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.oldPasswordrequired',
            false,
            500,
            [],
            'successfully'
        );
    }

    public function testChangePasswordEditOldPasswordNotMatch()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login(instructor::factory()->create())])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1W',
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.oldPasswordNotMatch',
            false,
            501,
            [],
            'oldPasswordNotMatch'
        );
    }

    public function testChangePasswordEditNewPasswordMatch()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login(instructor::factory()->create())])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1D',
                    'new' => 'password1D',
                ]]),
            'PersonMutations.PersonChangePassword.newPasswordMatch',
            false,
            502,
            [],
            'newPasswordMatch'
        );
    }

    public function testChangePasswordEditInstructorHavPasswordSuccessfully()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login(instructor::factory()->create())])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1D',
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.Successfully',
            true,
            200,
            [],
            'Successfully'
        );
    }

    public function testChangePasswordEditInstructorNotHavPasswordSuccessfully()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login(instructor::factory()->create())])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1D',
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.Successfully',
            true,
            200,
            [],
            'Successfully'
        );
    }

    public function testChangePasswordEditStudentNotHavPasswordSuccessfully()
    {

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login(student::factory()->create())])
                ->GraphQLFiles($this->PersonChangePassword(), ['data' => [
                    'old' => 'password1D',
                    'new' => 'password1W',
                ]]),
            'PersonMutations.PersonChangePassword.Successfully',
            true,
            200,
            [],
            'Successfully'
        );
    }

    public function testPersonUpdateAccessToken()
    {

        $student = student::factory()->create();

        $data = ['firebaseId' => $this->faker()->text(190)];

        $this
            ->GraphQLFiles($this->PersonEdit(), ['data' => ['AccessTokenInputs' => $data]])
            ->assertResponseStatusMessageAndLog('PersonMutations.PersonEdit.unAuth', false, 502, [], 'unAuth');

        $this
            ->withHeaders(['Authorization' => $this->login($student)])
            ->GraphQLFiles($this->PersonEdit(), ['data' => ['AccessTokenInputs' => $data]])
            ->assertResponseStatusMessageAndLog('PersonMutations.PersonEdit.successfully', true, 200, [], 'successfully');

        $this->assertStringContainsString(auth()->user()->token()->refresh()->firebaseId, $data['firebaseId']);
    }

    public function testPersonRating()
    {

        $instructor = instructor::factory()->create();

        $rating = rating::factory(5)->create(['instructor_id' => $instructor->id]);

        $ascasc = $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles('{ meInstructors { average_ratings } }')
            ->assertJson(['data' => ['meInstructors' =>  ['average_ratings' => round($rating->avg('rating'))]]]);
    }
}
