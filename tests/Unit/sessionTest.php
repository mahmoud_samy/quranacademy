<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\instructor;
use App\Models\student;
use App\Models\courseType;
use App\Models\rateMinutesType;
use App\Models\session;

use Illuminate\Http\UploadedFile;

use Tests\Traits\sessiontrait;

use App\Notifications\EditSessionsRequestForInstructor;
use App\Notifications\CancelSessionsForStudent;

use App\Notifications\Instructor\sessionReplaced;

use App\Notifications\Student\sessionsEditRequest;
use App\Notifications\Student\sessionCancel      as SessionCancelForStudent;

use App\Notifications\Instructor\sessionsNewBook as sessionsNewBookforInstructor;
use App\Notifications\Instructor\sessionCancel   as SessionCancelForInstructor;
use App\Notifications\Instructor\sessionsAcceptEdit;

class sessionTest extends TestCase
{

    use sessiontrait;

    public function testSessionsNotRepeatCreate()
    {

        $instructor = instructor::factory()->create();

        $this->assertResponseStatusMessageAndLog(
            $req = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsCreate(), ['data' => $data = $this->sessionsCreateArray()]),
            'sessionsMutations.sessionsCreate.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $req->assertJson(['data' => ['sessionsCreate' => ['sessions' => [0 => [
            'instructor'        => ['id'   => $instructor->id],
            'currency_type'     => ['id'   => $instructor->currency_type_id],
            'rateMinutesType' => ['id'   => $data['rate_minutes_type_id']],
            'course_type'       => ['id'   => $data['course_type_id']],
            'date'              =>             $data['date'],
            'time'              =>             $data['time'],
            'DateTime'          =>             $data['date'] . ' ' . $data['time'],
            'rate_salary'       => $instructor[rateMinutesType::find($data['rate_minutes_type_id'])->attr],
        ]],]]]);

        $req->assertJsonCount(1, 'data.sessionsCreate.sessions');
    }

    public function testSessionsOneCreate()
    {

        $instructor = instructor::factory()->create();
        $data = $this->sessionsCreateArray() + [
            'repeat' =>  null
        ];

        $this->assertResponseStatusMessageAndLog(
            $req = $this->withHeaders(['Authorization' => $this->login($instructor)])->GraphQLFiles($this->sessionsCreate(), ['data' => $data]),
            'sessionsMutations.sessionsCreate.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $req->assertJson(['data' => ['sessionsCreate' =>  ['sessions' => [[
            'instructor'        => ['id' => $instructor->id],
            'currency_type'     => ['id' => $instructor->currency_type_id],
            'rateMinutesType' => ['id' => $data['rate_minutes_type_id']],
            'course_type'       => ['id' => $data['course_type_id']],
            'date'              =>           $data['date'],
            'time'              =>           $data['time'],
            'rate_salary'       => $instructor[rateMinutesType::find($data['rate_minutes_type_id'])->attr],
        ]]]]]);

        $req->assertJsonCount(1, 'data.sessionsCreate.sessions');
    }

    public function testSessionsCreate()
    {

        $instructor = instructor::factory()->create();
        $data = $this->sessionsCreateArray() + ['repeat' =>  [
            'repeat' => $repeat = $this->faker()->numberBetween(1, 8),
            'type'   => 'weekly',
        ]];

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsCreate(), ['data' => $data]),
            'sessionsMutations.sessionsCreate.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $response->assertJsonCount($repeat + 1, 'data.sessionsCreate.sessions');

        for ($i = 0; $i <= $repeat; $i++) $response->assertJson(['data' => ['sessionsCreate' => ['sessions' => [$i => [
            'instructor'        => ['id' => $instructor->id],
            'currency_type'     => ['id' => $instructor->currency_type_id],
            'rateMinutesType' => ['id' => $data['rate_minutes_type_id']],
            'course_type'       => ['id' => $data['course_type_id']],
            'time'              =>           $data['time'],
            'date'              => now()->addDays(1)->addWeeks($i)->format('Y-m-d'),
            'rate_salary'       => $instructor[rateMinutesType::find($data['rate_minutes_type_id'])->attr],
        ]]]]]);
    }

    public function testSessionsEdit()
    {

        $instructor = instructor::factory()->create();

        $session    = session::factory()->create(['instructor_id' => $instructor->id]);

        $data = $this->sessionsEditArray() + [
            'id'   => $session->id,
            'time' => now()->addHours(1)->addMinutes(1)->format('H:i:s'),
        ];

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsEdit(), ['data' => $data]),
            'sessionsMutations.sessionsEdit.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $response->assertJson(['data' => ['sessionsEdit' =>  ['session' => [
            'instructor'  => ['id' => $instructor->id],
            'course_type' => ['id' => $data['course_type_id']],
            'time'        => $data['time'],
            'date'        => $data['date'],
        ]]]]);
    }

    public function testSessionsEditWithStudent()
    {

        $instructor = instructor::factory()->create();
        $student    = student::factory()->create();

        $session    = session::factory()->create([
            'instructor_id' => $instructor->id,
            'student_id'    => $student->id,
        ]);

        $data = $this->sessionsEditArray() + ['id' => $session->id];

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->sessionsEdit(), ['data' => $data])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsEdit.sendToStudentToApprove', true, 200, [], 'sendToStudentToApprove')
            ->assertJson(['data' => ['sessionsEdit' =>  ['session' => [
                'instructor'  => ['id' => $instructor->id],
                'course_type' => ['id' => $session->course_type->id],
                'date'        =>           $session->date->format('Y-m-d'),
                'time'        =>           $session->time->format('H:i:s'),
            ]]]])
            ->assertMessagesLog('Notifications', 'student', $student->Notifications()->get(), sessionsEditRequest::class, ['Instructor_name' => $instructor->name]);

        $session = $session->refresh();

        $this->assertTrue($session->options['course_type_id'] === $data['course_type_id']);
        $this->assertStringContainsString($session->options['date'],   $data['date']);
        $this->assertStringContainsString($session->options['time'],   $data['time']);

        $this
            ->passport($student, ['student'], 'student')
            ->GraphQLFiles($this->sessionsAcceptEdit(), ['data' => ['id' => $session->id]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsAcceptEdit.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['sessionsAcceptEdit' =>  ['session' => [
                'instructor'  => ['id' => $instructor->id],
                'course_type' => ['id' => $data['course_type_id']],
                'time'        => $data['time'],
                'date'        => $data['date'],
            ]]]])
            ->assertMessagesLog('Notifications', 'instructor', $instructor->Notifications()->get(), sessionsAcceptEdit::class, [
                'student_name' => $session->refresh()->student->name,
                'time'         => $session->refresh()->NotificationDateTime,
            ]);

        $this
            ->passport($student, ['student'], 'student')
            ->GraphQLFiles($this->sessionsAcceptEdit(), ['data' => ['id' => $session->id]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsAcceptEdit.thisSessionHasNotEditRequest', true, 200, [], 'thisSessionHasNotEditRequest');
    }

    public function testSessionsEditFromStudent()
    {

        $instructor = instructor::factory()->create();
        $student    = student::factory()->create();

        $session = session::factory()->create([
            'instructor_id' => $instructor->id,
            'student_id'    => $student->id,
        ]);

        $data = $this->sessionsEditArray() + ['id' => $session->id];

        $this
            ->withHeaders(['Authorization' => $this->login($student)])
            ->GraphQLFiles($this->sessionsEdit(), ['data' => $data])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsEdit.sendToInstructorToApprove', true, 200, [], 'sendToInstructorToApprove')
            ->assertJson(['data' => ['sessionsEdit' =>  ['session' => [
                'instructor'  => ['id' => $instructor->id],
                'course_type' => ['id' => $session->course_type->id],
                'date'        =>           $session->date->format('Y-m-d'),
                'time'        =>           $session->time->format('H:i:s'),
            ]]]])
            ->assertMessagesLog('Notifications', 'student', $student->Notifications()->get(), sessionsEditRequest::class, ['Instructor_name' => $instructor->name]);

        $session = $session->refresh();

        $this->assertTrue($session->options['course_type_id'] === $data['course_type_id']);
        $this->assertStringContainsString($session->options['date'],   $data['date']);
        $this->assertStringContainsString($session->options['time'],   $data['time']);
    }

    public function testSessionsCancel()
    {

        $instructor = instructor::factory()->create();

        $session = session::factory()->create([
            'instructor_id' => $instructor->id,
        ]);

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsCancel(), ['data' => ['id' => $session->id]]),
            'sessionsMutations.sessionsCancel.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $this->assertFalse(is_null($response->json('data.sessionsCancel.session.cancel_at')));
    }

    public function testSessionsCancelWithStudent()
    {

        $instructor = instructor::factory()->create();
        $student    = student::factory()->create();

        $session = session::factory()->create([
            'instructor_id' => $instructor->id,
            'student_id'    => $student->id,
        ]);

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($student)])
                ->GraphQLFiles($this->sessionsCancel(), ['data' => ['id' => $session->id]]),
            'sessionsMutations.sessionsCancel.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $this->assertFalse(is_null($response->json('data.sessionsCancel.session.cancel_at')));

        $this->assertMessagesLog('Notifications', 'instructor', $instructor->Notifications()->get(), SessionCancelForInstructor::class, [
            'student_name' => $student->name,
        ]);
    }

    public function testSessionsCancelFromStudent()
    {

        $instructor = instructor::factory()->create();
        $student    = student::factory()->create();

        $session = session::factory()->create([
            'instructor_id' => $instructor->id,
            'student_id'    => $student->id,
        ]);

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsCancel(), ['data' => ['id' => $session->id]]),
            'sessionsMutations.sessionsCancel.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $this->assertFalse(is_null($response->json('data.sessionsCancel.session.cancel_at')));

        $this->assertMessagesLog('Notifications', 'student', $student->Notifications()->get(), SessionCancelForStudent::class, [
            'Instructor_name' => $instructor->name,
        ]);
    }

    public function testSessionsDeleteByStudent()
    {

        $student    = student::factory()->create();

        $session = session::factory()->create([
            'student_id'    => $student->id,
        ]);

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($student)])
                ->GraphQLFiles($this->sessionsDelete(), ['data' => ['id' => $session->id]]),
            'sessionsMutations.sessionsDelete.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $session = $session->refresh();

        $this->assertFalse(is_null($session->delete_for_students));
    }

    public function testSessionsDeleteFromStudent()
    {

        $instructor = instructor::factory()->create();

        $session = session::factory()->create([
            'instructor_id' => $instructor->id,
        ]);

        $this->assertResponseStatusMessageAndLog(
            $response = $this
                ->withHeaders(['Authorization' => $this->login($instructor)])
                ->GraphQLFiles($this->sessionsDelete(), ['data' => ['id' => $session->id]]),
            'sessionsMutations.sessionsDelete.Successfully',
            true,
            200,
            [],
            'Successfully'
        );

        $session = $session->refresh();

        $this->assertFalse(is_null($session->delete_for_instructors));
    }

    public function testGetSessionsInstructor()
    {

        $instructor = instructor::factory()->create();

        $newSessionCounter = 0;
        $coust             = 0;

        $newSession = session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'created_at'    => now()->subDay(),
            'date'          => now()->subDay(),
        ])->map(function ($session) use (&$coust) {
            $coust = $coust + (int) $session->rate_salary;
            $session->student_id = student::factory()->create()->id;
            $session->save();
            return $session;
        });

        $newSessionCounter = $newSessionCounter + $newSession->count();

        session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'date' => now()->subDay()
        ]);

        $newSession0 = session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'date'          => now()->format('Y-m-d')
        ])->map(function ($session) {
            $session->student_id = student::factory()->create()->id;
            $session->save();
            return $session;
        });

        $newSessionCounter = $newSessionCounter + $newSession0->count();

        $newSession2 = session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'date' => now()->addDays(2)->format('Y-m-d')
        ])->map(function ($session) {
            $session->student_id = student::factory()->create()->id;
            $session->save();
            return $session;
        });

        $newSessionCounter = $newSessionCounter + $newSession2->count();

        $newSession4 = session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'date' => now()->addDays(4)->format('Y-m-d')
        ])->map(function ($session) {
            return $session;
        });

        $newSession6 = session::factory(8)->create([
            'instructor_id' => $instructor->id,
            'date' => now()->addDays(6)->format('Y-m-d')
        ])->map(function ($session, $num) {
            $session->student_id = student::factory()->create()->id;
            if ($num == 3) $session->Cancel();
            $session->save();
            return $session;
        });

        $newSessionCounter = $newSessionCounter + $newSession6->count();

        $response = $this->withHeaders(['Authorization' => $this->login($instructor)])->GraphQLFiles($this->meInstructors());

        $response->assertJson(['data' => ['meInstructors' =>  [
            'total_students_counter'      => $newSessionCounter,
            'total_earning_counter'       => $coust,
            'total_profile_views_counter' => 0,
        ]]]);

        $response->assertJsonCount($coust0 = 0, 'data.meInstructors.sessionsByUnits.0.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [0 => ['status' => [
            'counter'             => $coust0,
            'counterConfirmed'    => $coust0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession0->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.1.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [1 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession0->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount($coust2 = $newSession2->count(), 'data.meInstructors.sessionsByUnits.2.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [2 => ['status' => [
            'counter'             => $coust2,
            'counterConfirmed'    => $coust2,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession2->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.3.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [3 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession2->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount($coust4 = $newSession4->count(), 'data.meInstructors.sessionsByUnits.4.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [4 => ['status' => [
            'counter'             => $coust4,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => $coust4,
            'Date'                => $newSession4->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.5.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [5 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession4->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response->assertJsonCount($coust6 = $newSession6->whereNull('cancel_at')->count(), 'data.meInstructors.sessionsByUnits.6.sessions');

        $response->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [6 => ['status' => [
            'counter'             => $coust6,
            'counterConfirmed'    => $coust6,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession6->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response2 = $this->withHeaders([
            'Authorization' => $this->login($instructor),
        ])->GraphQLFiles($this->meInstructors(), ['is_confirmed' => true]);

        $response2->assertJson(['data' => ['meInstructors' =>  [
            'total_earning_counter'       => $coust,
            'total_students_counter'      => $newSessionCounter,
            'total_profile_views_counter' => 0,
        ]]]);

        $response2->assertJsonCount($coust0 = 0, 'data.meInstructors.sessionsByUnits.0.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [0 => ['status' => [
            'counter'             => $coust0,
            'counterConfirmed'    => $coust0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession0->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.1.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [1 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession0->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount($coust2 = $newSession2->count(), 'data.meInstructors.sessionsByUnits.2.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [2 => ['status' => [
            'counter'             => $coust2,
            'counterConfirmed'    => $coust2,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession2->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.3.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [3 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession2->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.4.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [4 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession4->first()->date->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount(0, 'data.meInstructors.sessionsByUnits.5.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [5 => ['status' => [
            'counter'             => 0,
            'counterConfirmed'    => 0,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession4->first()->date->addDay()->format('Y-m-d'),
        ]]]]]]);

        $response2->assertJsonCount($coust6 = $newSession6->whereNull('cancel_at')->count(), 'data.meInstructors.sessionsByUnits.6.sessions');

        $response2->assertJson(['data' => ['meInstructors' =>  ['sessionsByUnits' =>  [6 => ['status' => [
            'counter'             => $coust6,
            'counterConfirmed'    => $coust6,
            'counterNotConfirmed' => 0,
            'Date'                => $newSession6->first()->date->format('Y-m-d'),
        ]]]]]]);
    }

    public function testSessionsBookinstructor()
    {

        $session = session::factory(5)->create();

        $this->assertResponseStatusMessageAndLog(
            $this
                ->withHeaders(['Authorization' => $this->login(instructor::factory()->create())])
                ->GraphQLFiles($this->sessionsBook(), ['data' => [
                    'sessions' => $session->map(fn ($var) => ['id' => $var->id, 'note' => $this->faker->unique()->text(190)])->toArray(),
                    'Card'     => $this->card()
                ]]),
            'sessionsMutations.sessionsBook.NotStudent',
            false,
            502,
            [],
            'NotStudent'
        );
    }

    public function testSessionsBook()
    {

        $student  = student::factory()->create();
        $sessions = session::factory(5)->create();

        $response = $this
            ->withHeaders(['Authorization' => $this->login($student)])
            ->GraphQLFiles($this->sessionsBook(), ['data' => [
                'sessions' => $array = $sessions->map(fn ($var) => [
                    'id'   => $var->id,
                    'note' => $this->faker->boolean ? $this->faker->unique()->text(190) : null
                ]),
                'Card'     => $this->card()
            ]])
            ->assertJson(['data' => ['sessionsBook' =>  ['sessions' => $array->map(fn ($item) => [
                'id'      => $item['id'],
                'note'    => $item['note'],
                'student' => ['id' => $student->id]
            ])->toArray()]]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsBook.Successfully', true, 200, [], 'Successfully')
            ->assertMessagesLog('Notifications', 'instructor', $sessions->first()->instructor->Notifications()->get(), sessionsNewBookforInstructor::class, []);

        $sessions->map(function ($session, $num) use ($student, $array) {
            $this->assertStringContainsString($session->refresh()->note ?? '', $array[$num]['note'] ?? '');
            $this->assertTrue($session->refresh()->id         === $array[$num]['id']);
            $this->assertTrue($session->refresh()->student_id === $student->id);
        });
    }

    public function testSessionsChange()
    {

        $session = session::factory()->create();

        $session2 = session::factory()->create(['student_id' => student::factory()->create()->id] + $session->makeHidden(['instructor', 'id'])->toArray());

        $session->makeVisible(['instructor', 'id']);

        $response = $this
            ->passport($session2->student, ['student'], 'student')
            ->GraphQLFiles($this->sessionsChange(), ['data' => [
                'id_old' => $session2->id,
                'id_new' => $session->id
            ]])
            ->assertJson(['data' => ['sessionsChange' =>  ['session' => [
                'id'      => $session->refresh()->id,
                'student' => ['id' => $session->student->id]
            ]]]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsChange.Successfully', true, 200, [], 'Successfully')
            ->assertMessagesLog('Notifications', 'instructor', $session2->instructor->Notifications()->get(), sessionReplaced::class, [
                'student_name' => $session2->student->name,
                'time'         => $session->NotificationDateTime,
            ]);

        $this->assertNull($session2->refresh()->student_id);
    }

    public function testSessionsUpload()
    {

        $session = session::factory()->create();
        $add     = $this->makeFile('/app/public/0');
        $replace = $this->makeFile('/app/public/1');

        $response = $this
            ->passport($session->instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->sessionsEdit(), ['data' => [
                'id'        => $session->id,
                'Materials' => [
                    'add'    => [$add],
                    'delete' => [$session->materials[0]->id],
                    'replace' => [[
                        'id'   => $session->materials[1]->id,
                        'file' => $replace
                    ]],
                ],
            ]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsEdit.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['sessionsEdit' =>  ['session' => ['materials' => [[
                'size' => $replace->getSize(),
                'name' => $replace->getClientOriginalName(),
            ], [
                'size' => $add->getSize(),
                'name' => $add->getClientOriginalName(),
            ]]]]]]);

        $this->assertIsArray($response->json('data.sessionsEdit.session'));
        $this->assertFalse(empty($response->json('data.sessionsEdit.session.materials')));
        $this->assertCount(2,    $response->json('data.sessionsEdit.session.materials'));
    }

    public function testGetSessionsStudentCounters()
    {

        $student = student::factory()->create();

        $date = now()->modify('saturday this week')->subWeek();

        session::factory()->create([
            'student_id' => $student->id,
            'date'       => $date->clone()->subDay()
        ]);

        $session = session::factory(2)->create([
            'student_id' => $student->id,
            'date'       => $date->clone()->addDay()
        ]);

        session::factory(2)->create([
            'student_id' => $student->id,
            'date'       => $date->clone()->addDay(),
            'cancel_at'  => now()
        ]);

        $counters = $student->FireBaseCloudMessageing;

        $this->assertTrue($counters['count_this_week_study_hours'] === $session->map(function ($session) {
            return $session->rate_minutes_type->RateByMinutes;
        })->sum() / 60);

        $this->assertTrue($counters['count_this_week_session'] === $session->count());

        $this->assertTrue($counters['count_this_week_session'] === 2);

        $this->assertTrue($counters['count_this_week_session_schedule'] === $session->where('date', '>', now())->where('time', '>', now())->count());
    }

    public function testRatingSessions()
    {

        $student = student::factory()->create();

        $session = session::factory()->create([
            'student_id' => $student->id,
            'date'       => now()->subWeek()
        ]);

        $this
            ->passport($student, ['student'], 'student')
            ->GraphQLFiles($this->sessionsRating(), ['data' => $data = [
                'id'      => $session->id,
                'rating'  => $this->faker()->numberBetween(1, 5),
                'reviews' => $this->faker()->text(191),
            ]])
            ->assertResponseStatusMessageAndLog('sessionsMutations.sessionsRating.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['sessionsRating' =>  ['session' => ['rating' => [
                'student'    => ['id' => $student->id],
                'instructor' => ['id' => $session->instructor->id],
                'rating'  => $data['rating'],
                'reviews' => $data['reviews'],
            ]]]]]);
    }
}
