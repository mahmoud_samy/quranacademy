<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;

class pinCodeTest extends TestCase {

	public function sendPinconde ( ) {
		return 'mutation(
			$data : globalsendPinCodeInput !
		) {
			globalSendPinCode (
				data : $data 
			) {
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function globalverifyPinCode ( ) {
		return 'mutation(
			$data : globalverifyPinCodeInput !
		) {
			globalverifyPinCode (
				data : $data 
			) {
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function testSendPinCodeToRegister( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> sendPinconde( ) , [ 'data' => [
			'email'          => $this -> faker( ) -> unique( ) -> email ,
			'sendPinCodeFor' => 'Register'                              ,
		] ] ) , 'globalMutations.globalSendPinCode.successfully' , true , 200 , [ ] , 'successfully' );;

	}

	public function testSendPinCodeToRegisterButEmailIsUse( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> sendPinconde( ) , [ 'data' => [
			'email'          => student ::factory ( ) -> create ( ) -> email ,
			'sendPinCodeFor' => 'Register'                                         ,
		] ] ) , 'globalMutations.globalSendPinCode.emailisExist' , false , 503 , [ ] , 'emailisExist' );;

	}

	public function testSendPinCodeToPasswordresets( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> sendPinconde( ) , [ 'data' => [
			'email'          => student ::factory ( ) -> create ( ) -> email ,
			'sendPinCodeFor' => 'passwordresets'                                   ,
		] ] ) , 'globalMutations.globalSendPinCode.successfully' , true , 200 , [ ] , 'successfully' );;

	}

	public function testSendPinCodeToPasswordresetsButEmailIsNotUse( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> sendPinconde( ) , [ 'data' => [
			'email'          => $this -> faker( ) -> unique( ) -> email ,
			'sendPinCodeFor' => 'passwordresets'                        ,
		] ] ) , 'globalMutations.globalSendPinCode.emailNotExist' , false , 404 , [ ] , 'emailNotExist' );;

	}

	public function testNotMatchPinCode( ) {

		$email = $this -> faker( ) -> unique( ) -> safeEmail( );

		$Pincode = Pincode::firstOrCreate([ 'email' => $email ]) -> getNewToken( ) ;

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> globalverifyPinCode( ) , [ 'data' => [
			'email'		=> $email ,
			'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) + 1 ,
		] ] ) , 'globalMutations.globalverifyPinCode.pinCodeIsNotMatch' , false , 502 , [ ] , 'pinCodeIsNotMatch' );

	}

	public function testExpirePinCode( ) {

		$email = $this -> faker( ) -> unique( ) -> safeEmail( );

		$Pincode = Pincode::firstOrCreate([ 'email' => $email ]) -> getNewToken( ) ;

		$Pincode -> updated_at = now( ) -> addMinutes( - ( variabledb::val( 'EXPIRE_PIN_CODE_TOKEN_BY_MINUTES' ) + 1 ) ) ;
		$Pincode -> save( ) ;

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> globalverifyPinCode( ) , [ 'data' => [
			'email'		=> $email ,
			'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) + 1 ,
		] ] ) , 'globalMutations.globalverifyPinCode.pinCodeIsNotActive' , false , 502 , [ ] , 'pinCodeIsNotActive' );

	}

	public function testValidPinCode( ) {

		$email = $this -> faker( ) -> unique( ) -> safeEmail( );

		$Pincode = Pincode::firstOrCreate([ 'email' => $email ]) -> getNewToken( ) ;

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> globalverifyPinCode( ) , [ 'data' => [
			'email'		=> $email ,
			'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) ,
		] ] ) , 'globalMutations.globalverifyPinCode.pinCodeisValid' , true , 200 , [ ] , 'pinCodeIsNotActive' );

	}

}
