<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;
use App\Models\image;
use App\Models\languageType;

use Illuminate\Http\UploadedFile ;
use App\Notifications\Instructor\createdButNotcompleted ;

class registerInstructorTest extends TestCase {

	public function globalRegisterInstructor ( ) {
		return 'mutation(
			$data : globalRegisterInstructorInput !
		) {
			globalRegisterInstructor (
				data : $data 
			) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					email
					Languages{
						name
					}
					profile_vidoe{
						url
					}
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function makevar ( $ifPass = true ) {
		$array = [
			'first_name'              => $this -> faker( ) -> unique( ) -> name         ,
			'last_name'               => $this -> faker( ) -> unique( ) -> name         ,

			'years_of_experience'     => $this -> faker( ) -> unique( ) -> randomNumber ,
			'description'             => $this -> faker( ) -> unique( ) -> text         ,
			'is_profile_public'       => $this -> faker( ) -> boolean( )                ,
			'rate_15_min'             => $this -> faker( ) -> unique( ) -> randomNumber ,
			'rate_30_min'             => $this -> faker( ) -> unique( ) -> randomNumber ,
			'rate_60_min'             => $this -> faker( ) -> unique( ) -> randomNumber ,

			'profile_image'           => $this -> makeFile( '/app/public/logo' )        ,
			'profile_vidoe'           => $this -> makeFile( '/app/public/logo' )        ,

			'courses_types_id'        => courseType       ::RandemOne( ) -> id          ,
			'student_gender_types_id' => studentGenderType::RandemOne( ) -> id          ,
			'student_age_types_id'    => studentAgeType   ::RandemOne( ) -> id          ,
			'currency_type_id'        => currencyType     ::RandemOne( ) -> id          ,
			'Spoken_Language_User'    => [
				languageType::RandemOne( ) -> id , 
				languageType::RandemOne( ) -> id , 
				languageType::RandemOne( ) -> id , 
			] ,
		];
		if( $ifPass ) $array = array_merge( $array , [
			'gender'        => $this -> faker( ) -> boolean                 ,
			'date_of_birth' => now( ) -> subYears( 5 ) -> format( 'Y-m-d' ) ,
			'password'      => 'password1D'                                 ,
		] );
		return $array ;
	}

	public function testRegisterEmailemailNotExistInstructor( ) {

		$this -> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalRegisterInstructor( ) , [ 'data' => [
			'subInstructorsInput' => [
				'email'    => $this -> faker( ) -> unique( ) -> safeEmail( ) ,
				'password' => 'password1D' ,
			],
			'InstructorsInput' => $this -> makevar( ),
		] ] ) , 'globalMutations.globalRegisterInstructor.emailNotExist' , false , 404 , [ ] , 'emailNotExist' );

	}

	public function testRegisterEmailisExistStudent( ) {

		$this
			-> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalRegisterInstructor( ) , [ 'data' => [
				'subInstructorsInput' => [
					'email'    => $email = SubInstructor ::factory ( )   -> create ( ) -> email ,
					'password' => 'password1D' ,
				],
				'InstructorsInput' => $this -> makevar( ),
			] ] ) , 'globalMutations.globalRegisterInstructor.Successfully' , true , 200 , [ ] , 'Successfully' )
			-> assertJson([ 'data' => [ 'globalRegisterInstructor' =>  [
				'Instructor'    => [ 'email' => $email ] ,
				'Student'       => null                  ,
				'SubInstructor' => null                  ,
			] ] ])
			-> assertMessagesLog( 'Notifications' , 'instructor' , instructor :: where( 'email' , $email ) -> first( ) -> Notifications ( ) -> get ( ) , createdButNotcompleted :: class , [ ] ) 
			-> assertBearerTokenResponse( $req , 'globalRegisterInstructor' )
		;

	}

	public function testRegisterEmailisExistStudentWithSoc( ) {

		if( env( 'GOOGLE_ACCESS_TOKEN' ) ){
			SubInstructor :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );
			instructor    :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );
			student       :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );

			SubInstructor ::factory ( ) -> create ( [ 'email' => 'mahmoud.samy@black-squares.com' ] ) ;

			$this -> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalRegisterInstructor( ) , [ 'data' => [
				'SocialiteInput' => [
					'token'    => env( 'GOOGLE_ACCESS_TOKEN' ) ,
					'provider' => 'google' ,
				],
				'InstructorsInput' => $this -> makevar( false ),
			] ] ) , 'globalMutations.globalRegisterInstructor.Successfully' , true , 200 , [ ] , 'Successfully' );
	
			$this -> assertBearerTokenResponse( $req , 'globalRegisterInstructor' ) ;
	
			$req -> assertJson([ 'data' => [ 'globalRegisterInstructor' =>  [
				'Instructor'    => [ 'email' => 'mahmoud.samy@black-squares.com' ] ,
				'Student'       => null                                            ,
				'SubInstructor' => null                                            ,
			] ] ]) ;

		}

		$this -> assertTrue( true );

	}

}
