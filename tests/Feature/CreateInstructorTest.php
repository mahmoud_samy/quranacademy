<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;
use App\Models\rateMinutesType;
use App\Models\image;
use App\Models\languageType;

use Illuminate\Http\UploadedFile ;

class CreateInstructorTest extends TestCase {

	public function globalCreateInstructor ( ) {
		return 'mutation( $data : globalCreateInstructorInput ! ) {
			globalCreateInstructor ( data : $data ) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					id
					profile_vidoe{
						id
						url
					}
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function makevar ( $ifPass = true ) {
		return [
			'rate_15_min'                 => $this -> faker( ) -> unique( ) -> randomNumber ,
			'rate_30_min'                 => $this -> faker( ) -> unique( ) -> randomNumber ,
			'rate_60_min'                 => $this -> faker( ) -> unique( ) -> randomNumber ,
			'profile_image'               => $this -> makeFile( '/app/public/logo' )        ,
			'profile_vidoe'               => $this -> makeFile( '/app/public/logo' )        ,
			'courses_types_id'            => courseType       :: RandemOne( ) -> id          ,
			'public_rate_minutes_type_id' => rateMinutesType  :: RandemOne( ) -> id          ,
			'student_gender_types_id'     => studentGenderType:: RandemOne( ) -> id          ,
			'student_age_types_id'        => studentAgeType   :: RandemOne( ) -> id          ,
			'currency_type_id'            => currencyType     :: RandemOne( ) -> id          ,
			'Spoken_Language_User'        => [
				languageType :: RandemOne( ) -> id , 
				languageType :: RandemOne( ) -> id , 
				languageType :: RandemOne( ) -> id , 
			] ,
		];
	}

	public function testRegisterEmailNotExistInstructor( ) {

		$this
			-> GraphQLFiles( $this -> globalCreateInstructor( ) , [ 'data' => $this -> makevar( ) ] )
			-> assertResponseStatusMessageAndLog ( 'globalMutations.globalCreateInstructor.AuthNotExist' , false , 403 , [ ] , 'there is no email with this record')
		;

	}

	public function testRegisterEmailemailNotExistsgtru( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> withHeaders([
			'Authorization' => $this -> login( student ::factory ( ) -> create ( ) ),
		]) -> GraphQLFiles( $this -> globalCreateInstructor( ) , [
			'data' => $this -> makevar( ),
		] ) , 'globalMutations.globalCreateInstructor.IsNotSubInstructor' , false , 404 , [ ] , 'there is no email with this record');

	}

	public function testRegisterEmailisExistStudent( ) {

		$this -> assertResponseStatusMessageAndLog ( $req = $this -> withHeaders([
			'Authorization' => $this -> login( SubInstructor ::factory ( ) -> create ( ) ),
		]) -> GraphQLFiles( $this -> globalCreateInstructor( ) , [
			'data' => $this -> makevar( ),
		] ) , 'globalMutations.globalCreateInstructor.Successfully' , true , 200 , [ ] , 'there is no email with this record');

		$this -> assertBearerTokenResponse( $req , 'globalCreateInstructor' ) ;

	}

}
