<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\instructor;
use App\Models\session;
use App\Models\student;
use App\Services\transferwise;
use Illuminate\Support\Facades\Http;

class transferwiseTest extends TestCase
{

    public function PersonTransfer()
    {
        return 'mutation( $data : PersonTransferInput ! ) { PersonTransfer ( data : $data  ) {
			Instructor{
				id
				first_name
				last_name
				gender
				date_of_birth
				years_of_experience
				description
				is_profile_public
				rate_15_min
				rate_30_min
				rate_60_min
				public_rate_minutes_type_id
				public_rate_minutes
				country{ id name }
				currency_type{
					id
				}
				student_gender_types{
					id
				}
				student_age_types{
					id
				}
				Languages{
					id
				}
				certificates{
					id
				}
			}
			Status {
				code
				check
				title
				message
			}
			StatusTransfer {
				message
				title
				check
				arguments_name
				arguments_value
			}
			Transfer {
				id
				amount
				step
				accountHolderName
				abartn
				accountNumber
				accountType
				postCode
				address
				city
				country
				created_at
				updated_at
				estimatedDeliveryDate
			}
		} }';
    }

    public function InstructorGetTransfers()
    {
        return '{ InstructorGetTransfers{ data{
		id
		amount
		step
		accountHolderName
		abartn
		accountNumber
		accountType
		postCode
		address
		city
		country
		created_at
		updated_at
		estimatedDeliveryDate
	} } }';
    }

    public function makeData(instructor $instructor): array
    {
        $abartn = [
            '063109935',
            '063113057',
            '067091719',
            '063103915',
            '263190812',
            '071923909',
            '074908594',
            '086300041',
            '042101190',
            '042100230',
            '083002342',
            '072405455',
            '072401404',
            '072400052',
            '081019104',
            '053100737',
            '042000314',
            '044002161',
            '041002711',
            '041200050',
            '042207735',
            '042202196',
            '043018868',
            '064103833',
        ];
        return [
            'amount'            => (int) floor($instructor->total_earning_counter / 2),
            'accountHolderName' => $this->faker()->firstName . ' ' . $this->faker()->lastName,
            'abartn'            => $abartn[array_rand($abartn, 1)],
            'accountNumber'     => $this->faker()->bankAccountNumber,
            'accountType'       => $this->faker()->boolean ? 'CHECKING' : 'SAVINGS',
            'postCode'          => $this->faker()->postcode,
            'address'           => $this->faker()->address,
            'city'              => $this->faker()->city,
            'country'           => transferwise::getCountries()->where('name', $this->faker()->country)->first()['code'] ?? 'AL'
        ];
    }

    public function makeHttpFake(): void
    {
        Http::fake([
            transferwise::API_URL . 'v1/profiles'               => Http::response([
                [
                    'id'   => 16183076,
                    'type' => 'personal',
                    'details' => [
                        'firstName'       => 'mahmoud',
                        'lastName'        => 'harby',
                        'dateOfBirth'     => '1949-02-15',
                        'phoneNumber'     => '+442038087139',
                        'avatar'          => null,
                        'occupation'      => null,
                        'occupations'     => null,
                        'primaryAddress'  => 37182346,
                        'firstNameInKana' => null,
                        'lastNameInKana'  => null,
                    ]
                ], [
                    'id'      => 16183077,
                    'type'    => 'business',
                    'details' => [
                        'name' => 'bss',
                        'registrationNumber' => '07209813',
                        'acn' => null,
                        'abn' => null,
                        'arbn' => null,
                        'companyType' => 'LIMITED',
                        'companyRole' => 'OWNER',
                        'descriptionOfBusiness' => 'IT_SERVICES',
                        'primaryAddress' => 37182347,
                        'webpage' => null,
                        'businessCategory' => 'IT_SERVICES',
                        'businessSubCategory' => null,
                    ]
                ]
            ]),
            transferwise::API_URL . 'v1/countries'              => Http::response(['values' => [
                ['code' => 'AL', 'name' => 'Albania'], ['code' => 'DZ', 'name' => 'Algeria'],
                ['code' => 'AS', 'name' => 'American Samoa'], ['code' => 'AD', 'name' => 'Andorra'],
                ['code' => 'AO', 'name' => 'Angola'], ['code' => 'AI', 'name' => 'Anguilla'],
                ['code' => 'AQ', 'name' => 'Antarctica'], ['code' => 'AG', 'name' => 'Antigua and Barbuda'],
                ['code' => 'AR', 'name' => 'Argentina'], ['code' => 'AM', 'name' => 'Armenia'],
                ['code' => 'AW', 'name' => 'Aruba'], ['code' => 'AU', 'name' => 'Australia'],
                ['code' => 'AT', 'name' => 'Austria'], ['code' => 'AZ', 'name' => 'Azerbaijan'],
                ['code' => 'BS', 'name' => 'Bahamas'], ['code' => 'BH', 'name' => 'Bahrain'],
                ['code' => 'BD', 'name' => 'Bangladesh'], ['code' => 'BB', 'name' => 'Barbados'],
                ['code' => 'BY', 'name' => 'Belarus'], ['code' => 'BE', 'name' => 'Belgium'],
                ['code' => 'BZ', 'name' => 'Belize'], ['code' => 'BJ', 'name' => 'Benin'],
                ['code' => 'BM', 'name' => 'Bermuda'], ['code' => 'BT', 'name' => 'Bhutan'],
                ['code' => 'BO', 'name' => 'Bolivia'], ['code' => 'BQ', 'name' => 'Bonaire'],
                ['code' => 'BA', 'name' => 'Bosnia and Herzegovina'], ['code' => 'BW', 'name' => 'Botswana'],
                ['code' => 'BV', 'name' => 'Bouvet Island'], ['code' => 'BR', 'name' => 'Brazil'],
                ['code' => 'IO', 'name' => 'British Indian Ocean Territory'], ['code' => 'VG', 'name' => 'British Virgin Islands'],
                ['code' => 'BN', 'name' => 'Brunei Darussalam'], ['code' => 'BG', 'name' => 'Bulgaria'],
                ['code' => 'BF', 'name' => 'Burkina Faso'], ['code' => 'KH', 'name' => 'Cambodia'],
                ['code' => 'CM', 'name' => 'Cameroon'], ['code' => 'CA', 'name' => 'Canada'],
                ['code' => 'CV', 'name' => 'Cape Verde'], ['code' => 'KY', 'name' => 'Cayman Islands'],
                ['code' => 'CL', 'name' => 'Chile'], ['code' => 'CN', 'name' => 'China'],
                ['code' => 'CX', 'name' => 'Christmas Island'], ['code' => 'CC', 'name' => 'Cocos (Keeling) Islands'],
                ['code' => 'CO', 'name' => 'Colombia'], ['code' => 'KM', 'name' => 'Comoros'],
                ['code' => 'CK', 'name' => 'Cook Islands'], ['code' => 'CR', 'name' => 'Costa Rica'],
                ['code' => 'HR', 'name' => 'Croatia'], ['code' => 'CW', 'name' => 'Curaçao'],
                ['code' => 'CY', 'name' => 'Cyprus'], ['code' => 'CZ', 'name' => 'Czech Republic'],
                ['code' => 'CI', 'name' => 'Côte d\'Ivoire'], ['code' => 'DK', 'name' => 'Denmark'],
                ['code' => 'DJ', 'name' => 'Djibouti'], ['code' => 'DM', 'name' => 'Dominica'],
                ['code' => 'DO', 'name' => 'Dominican Republic'], ['code' => 'EC', 'name' => 'Ecuador'],
                ['code' => 'EG', 'name' => 'Egypt'], ['code' => 'SV', 'name' => 'El Salvador'],
                ['code' => 'GQ', 'name' => 'Equatorial Guinea'], ['code' => 'EE', 'name' => 'Estonia'],
                ['code' => 'ET', 'name' => 'Ethiopia'], ['code' => 'FK', 'name' => 'Falkland Islands'],
                ['code' => 'FO', 'name' => 'Faroe Islands'], ['code' => 'FJ', 'name' => 'Fiji'],
                ['code' => 'FI', 'name' => 'Finland'], ['code' => 'FR', 'name' => 'France'],
                ['code' => 'GF', 'name' => 'French Guiana'], ['code' => 'PF', 'name' => 'French Polynesia'],
                ['code' => 'TF', 'name' => 'French Southern Territories'], ['code' => 'GA', 'name' => 'Gabon'],
                ['code' => 'GM', 'name' => 'Gambia'], ['code' => 'GE', 'name' => 'Georgia'],
                ['code' => 'DE', 'name' => 'Germany'], ['code' => 'GH', 'name' => 'Ghana'],
                ['code' => 'GI', 'name' => 'Gibraltar'], ['code' => 'GR', 'name' => 'Greece'],
                ['code' => 'GL', 'name' => 'Greenland'], ['code' => 'GD', 'name' => 'Grenada'],
                ['code' => 'GP', 'name' => 'Guadeloupe'], ['code' => 'GT', 'name' => 'Guatemala'],
                ['code' => 'GG', 'name' => 'Guernsey'], ['code' => 'GN', 'name' => 'Guinea'],
                ['code' => 'GW', 'name' => 'Guinea-Bissau'], ['code' => 'GY', 'name' => 'Guyana'],
                ['code' => 'HT', 'name' => 'Haiti'], ['code' => 'HN', 'name' => 'Honduras'],
                ['code' => 'HK', 'name' => 'Hong Kong'], ['code' => 'HU', 'name' => 'Hungary'],
                ['code' => 'IS', 'name' => 'Iceland'], ['code' => 'IN', 'name' => 'India'],
                ['code' => 'ID', 'name' => 'Indonesia'], ['code' => 'IE', 'name' => 'Ireland'],
                ['code' => 'IM', 'name' => 'Isle of Man'], ['code' => 'IL', 'name' => 'Israel'],
                ['code' => 'IT', 'name' => 'Italy'], ['code' => 'JM', 'name' => 'Jamaica'],
                ['code' => 'JP', 'name' => 'Japan'], ['code' => 'JE', 'name' => 'Jersey'],
                ['code' => 'JO', 'name' => 'Jordan'], ['code' => 'KZ', 'name' => 'Kazakhstan'],
                ['code' => 'KE', 'name' => 'Kenya'], ['code' => 'KI', 'name' => 'Kiribati'],
                ['code' => 'XK', 'name' => 'Kosovo, Republic of'], ['code' => 'KW', 'name' => 'Kuwait'],
                ['code' => 'KG', 'name' => 'Kyrgyzstan'], ['code' => 'LA', 'name' => 'Laos'],
                ['code' => 'LV', 'name' => 'Latvia'], ['code' => 'LB', 'name' => 'Lebanon'],
                ['code' => 'LS', 'name' => 'Lesotho'], ['code' => 'LR', 'name' => 'Liberia'],
                ['code' => 'LI', 'name' => 'Liechtenstein'], ['code' => 'LT', 'name' => 'Lithuania'],
                ['code' => 'LU', 'name' => 'Luxembourg'], ['code' => 'MO', 'name' => 'Macao'],
                ['code' => 'MK', 'name' => 'Macedonia, Former Yugoslav Republic of'], ['code' => 'MG', 'name' => 'Madagascar'],
                ['code' => 'MW', 'name' => 'Malawi'], ['code' => 'MY', 'name' => 'Malaysia'],
                ['code' => 'MV', 'name' => 'Maldives'], ['code' => 'ML', 'name' => 'Mali'],
                ['code' => 'MT', 'name' => 'Malta'], ['code' => 'MH', 'name' => 'Marshall Islands'],
                ['code' => 'MQ', 'name' => 'Martinique'], ['code' => 'MR', 'name' => 'Mauritania'],
                ['code' => 'MU', 'name' => 'Mauritius'], ['code' => 'YT', 'name' => 'Mayotte'],
                ['code' => 'MX', 'name' => 'Mexico'], ['code' => 'FM', 'name' => 'Micronesia, Federated States of'],
                ['code' => 'MD', 'name' => 'Moldova'], ['code' => 'MC', 'name' => 'Monaco'],
                ['code' => 'MN', 'name' => 'Mongolia'], ['code' => 'ME', 'name' => 'Montenegro'],
                ['code' => 'MS', 'name' => 'Montserrat'], ['code' => 'MA', 'name' => 'Morocco'],
                ['code' => 'MZ', 'name' => 'Mozambique'], ['code' => 'MM', 'name' => 'Myanmar'],
                ['code' => 'NA', 'name' => 'Namibia'], ['code' => 'NR', 'name' => 'Nauru'],
                ['code' => 'NP', 'name' => 'Nepal'], ['code' => 'NL', 'name' => 'Netherlands'],
                ['code' => 'NC', 'name' => 'New Caledonia'], ['code' => 'NZ', 'name' => 'New Zealand'],
                ['code' => 'NI', 'name' => 'Nicaragua'], ['code' => 'NE', 'name' => 'Niger'],
                ['code' => 'NG', 'name' => 'Nigeria'], ['code' => 'NU', 'name' => 'Niue'],
                ['code' => 'NF', 'name' => 'Norfolk Island'], ['code' => 'NO', 'name' => 'Norway'],
                ['code' => 'OM', 'name' => 'Oman'], ['code' => 'PK', 'name' => 'Pakistan'],
                ['code' => 'PW', 'name' => 'Palau'], ['code' => 'PS', 'name' => 'Palestine'],
                ['code' => 'PA', 'name' => 'Panama'], ['code' => 'PG', 'name' => 'Papua New Guinea'],
                ['code' => 'PY', 'name' => 'Paraguay'], ['code' => 'PE', 'name' => 'Peru'],
                ['code' => 'PH', 'name' => 'Philippines'], ['code' => 'PN', 'name' => 'Pitcairn Islands'],
                ['code' => 'PL', 'name' => 'Poland'], ['code' => 'PT', 'name' => 'Portugal'],
                ['code' => 'PR', 'name' => 'Puerto Rico'], ['code' => 'QA', 'name' => 'Qatar'],
                ['code' => 'RO', 'name' => 'Romania'], ['code' => 'RU', 'name' => 'Russian Federation'],
                ['code' => 'RW', 'name' => 'Rwanda'], ['code' => 'RE', 'name' => 'Réunion'],
                ['code' => 'BL', 'name' => 'Saint Barthélemy'], ['code' => 'SH', 'name' => 'Saint Helena'],
                ['code' => 'KN', 'name' => 'Saint Kitts and Nevis'], ['code' => 'LC', 'name' => 'Saint Lucia'],
                ['code' => 'MF', 'name' => 'Saint Martin (French part)'], ['code' => 'PM', 'name' => 'Saint Pierre and Miquelon'],
                ['code' => 'VC', 'name' => 'Saint Vincent and the Grenadines'], ['code' => 'WS', 'name' => 'Samoa'],
                ['code' => 'SM', 'name' => 'San Marino'], ['code' => 'ST', 'name' => 'Sao Tome and Principe'],
                ['code' => 'SA', 'name' => 'Saudi Arabia'], ['code' => 'SN', 'name' => 'Senegal'],
                ['code' => 'RS', 'name' => 'Serbia'], ['code' => 'SC', 'name' => 'Seychelles'],
                ['code' => 'SL', 'name' => 'Sierra Leone'], ['code' => 'SG', 'name' => 'Singapore'],
                ['code' => 'SX', 'name' => 'Sint Maarten (Dutch part)'], ['code' => 'SK', 'name' => 'Slovakia'],
                ['code' => 'SI', 'name' => 'Slovenia'], ['code' => 'SB', 'name' => 'Solomon Islands'],
                ['code' => 'ZA', 'name' => 'South Africa'], ['code' => 'GS', 'name' => 'South Georgia and the South Sandwich Islands'],
                ['code' => 'KR', 'name' => 'South Korea'], ['code' => 'ES', 'name' => 'Spain'],
                ['code' => 'LK', 'name' => 'Sri Lanka'], ['code' => 'SR', 'name' => 'Suriname'],
                ['code' => 'SZ', 'name' => 'Swaziland'], ['code' => 'SE', 'name' => 'Sweden'],
                ['code' => 'CH', 'name' => 'Switzerland'], ['code' => 'TW', 'name' => 'Taiwan'],
                ['code' => 'TJ', 'name' => 'Tajikistan'], ['code' => 'TZ', 'name' => 'Tanzania'],
                ['code' => 'TH', 'name' => 'Thailand'], ['code' => 'TL', 'name' => 'Timor-Leste'],
                ['code' => 'TG', 'name' => 'Togo'], ['code' => 'TK', 'name' => 'Tokelau'],
                ['code' => 'TO', 'name' => 'Tonga'], ['code' => 'TT', 'name' => 'Trinidad and Tobago'],
                ['code' => 'TN', 'name' => 'Tunisia'], ['code' => 'TR', 'name' => 'Turkey'],
                ['code' => 'TM', 'name' => 'Turkmenistan'], ['code' => 'TC', 'name' => 'Turks and Caicos Islands'],
                ['code' => 'TV', 'name' => 'Tuvalu'], ['code' => 'UG', 'name' => 'Uganda'],
                ['code' => 'UA', 'name' => 'Ukraine'], ['code' => 'AE', 'name' => 'United Arab Emirates'],
                ['code' => 'GB', 'name' => 'United Kingdom'], ['code' => 'US', 'name' => 'United States'],
                ['code' => 'UY', 'name' => 'Uruguay'], ['code' => 'UZ', 'name' => 'Uzbekistan'],
                ['code' => 'VU', 'name' => 'Vanuatu'], ['code' => 'VA', 'name' => 'Vatican City'],
                ['code' => 'VN', 'name' => 'Vietnam'], ['code' => 'WF', 'name' => 'Wallis and Futuna'],
                ['code' => 'EH', 'name' => 'Western Sahara'], ['code' => 'ZM', 'name' => 'Zambia'],
                ['code' => 'ZW', 'name' => 'Zimbabwe'], ['code' => 'AX', 'name' => 'Åland Islands'],
            ]]),
            transferwise::API_URL . 'v1/borderless-accounts?*'  => Http::response([[
                'id'               => 26089,
                'profileId'        => 16183077,
                'recipientId'      => 147832011,
                'creationTime'     => '2021-05-11T16:08:26.490Z',
                'modificationTime' => '2021-05-11T16:08:26.490Z',
                'active'           => true,
                'eligible'         => true,
                'balances'         => [
                    [
                        'id'          => 70149,
                        'balanceType' => 'AVAILABLE',
                        'currency'    => 'AUD',
                        'bankDetails' => null,
                        'amount'      => [
                            'value'     => 4000000.0,
                            'currency'  => 'AUD'
                        ],
                        'reservedAmount' => [
                            'value'    => 0.0,
                            'currency' => 'AUD'
                        ]
                    ], [
                        'id'          => 70148,
                        'balanceType' => 'AVAILABLE',
                        'currency'    => 'USD',
                        'bankDetails' => null,
                        'amount'      => [
                            'value'     => 2994884.0,
                            'currency'  => 'USD',
                        ],
                        'reservedAmount' => [
                            'value'    => 0.0,
                            'currency' => 'USD'
                        ]
                    ], [
                        'id'          => 70147,
                        'balanceType' => 'AVAILABLE',
                        'currency'    => 'EUR',
                        'bankDetails' => null,
                        'amount' => [
                            'value' => 2000000.0,
                            'currency' => 'EUR',
                        ],
                        'reservedAmount' => [
                            'value' => 0.0,
                            'currency' => 'EUR',
                        ]
                    ], [
                        'id'             => 70080,
                        'balanceType'    => 'AVAILABLE',
                        'currency'       => 'GBP',
                        'amount'         => [
                            'value'        => 1000000.0,
                            'currency'     => 'GBP',
                            'bankDetails'    => null,
                        ],
                        'reservedAmount' => [
                            'value'        => 0.0,
                            'currency'     => 'GBP',
                        ]
                    ]
                ]
            ]]),
            transferwise::API_URL . 'v1/delivery-estimates/*'   => Http::response([
                'estimatedDeliveryDate' => now()->addDay()->format('Y-m-d\TH:i:s.000+0000')
            ]),
            transferwise::API_URL . 'v2/quotes'                 => Http::response([
                'sourceAmount'                  => 47.0,
                'guaranteedTargetAmountAllowed' => false,
                'targetAmountAllowed'           => true,
                'notices'                       => [],
                'rateTimestamp'                 => '2021-05-24T12:09:37Z',
                'clientId'                      => 'transferwise-personal-tokens',
                'id'                            => '64502a64-5634-435f-9e00-3acc9a94cab6',
                'type'                          => 'REGULAR',
                'status'                        => 'PENDING',
                'profile'                       => 16183077,
                'sourceCurrency'                => 'USD',
                'targetCurrency'                => 'USD',
                'rate'                          => 1,
                'createdTime'                   => '2021-05-24T12:09:37Z',
                'user'                          => 5712124,
                'rateType'                      => 'FIXED',
                'rateExpirationTime'            => '2021-06-23T12:09:37Z',
                'payOut'                        => 'BANK_TRANSFER',
                'guaranteedTargetAmount'        => false,
                'providedAmountType'            => 'SOURCE',
                'expirationTime'                => '2021-05-24T12:39:37Z',
                'payInCountry'                  => 'GB',
                'funding'                       => 'POST',
                'transferFlowConfig' => ['highAmount' => [
                    'showFeePercentage'       => false,
                    'trackAsHighAmountSender' => false,
                    'showEducationStep'       => false,
                    'offerPrefundingOption'   => false
                ]]
            ]),
            transferwise::API_URL . 'v3/profiles/*'             => Http::response([
                "type"                 => "BALANCE",
                "status"               => "COMPLETED",
                "errorCode"            => null,
                "errorMessage"         => null,
                "balanceTransactionId" => 1876122
            ], 200, ['x-2fa-approval' => 'test']),
            transferwise::API_URL . 'v1/simulation/transfers/*' => Http::response([
                'id'                    => 49696710,
                'user'                  => 5712124,
                'targetAccount'         => 147844351,
                'sourceAccount'         => null,
                'quote'                 => null,
                'quoteUuid'             => '93fe72b5-8660-4d18-921a-3dc77bac3f24',
                'status'                => 'funds_converted',
                'reference'             => '',
                'rate'                  => 1.0,
                'created'               => '2021-05-24 11:45:42',
                'business'              => 16183077,
                'transferRequest'       => null,
                'details'               => ['reference' => ''],
                'hasActiveIssues'       => false,
                'sourceCurrency'        => 'USD',
                'sourceValue'           => 67.95,
                'targetCurrency'        => 'USD',
                'targetValue'           => 67.95,
                'customerTransactionId' => '019f1da5-4c6b-4692-964e-9d55febf12e4',
            ]),
            transferwise::API_URL . 'v1/transfers*'             => function ($item) {
                $response = [
                    'id'                    => 49696710,
                    'user'                  => 5712124,
                    'targetAccount'         => 147844351,
                    'sourceAccount'         => null,
                    'quote'                 => null,
                    'quoteUuid'             => '93fe72b5-8660-4d18-921a-3dc77bac3f24',
                    'status'                => 'funds_converted',
                    'reference'             => '',
                    'rate'                  => 1.0,
                    'created'               => '2021-05-24 11:45:42',
                    'business'              => 16183077,
                    'transferRequest'       => null,
                    'details'               => ['reference' => ''],
                    'hasActiveIssues'       => false,
                    'sourceCurrency'        => 'USD',
                    'sourceValue'           => 67.95,
                    'targetCurrency'        => 'USD',
                    'targetValue'           => 67.95,
                    'customerTransactionId' => '019f1da5-4c6b-4692-964e-9d55febf12e4',
                ];
                if ($item->method() ===                         'POST') return $response;
                if ($item->url() === transferwise::API_URL . 'v1/transfers/' . $response['id']) return $response;
                dd(
                    $item->method(),
                    $item->data(),
                    $item->url(),
                );
            },
            transferwise::API_URL . 'v1/accounts'               => function ($item) {
                if (($item->data()['details']['address']['country'] ?? false) === 'test') return [
                    'timestamp' => '2021-05-24T12:17:15.300768826Z',
                    'errors' => [[
                        'code'      => 'NOT_VALID',
                        'message'   => 'Must be ISO 3166-1 alpha-2 country code.',
                        'path'      => 'address.country',
                        'arguments' => [
                            0 => 'country'
                        ]
                    ]]
                ];
                else return [
                    'id'                => 147844381,
                    'business'          => 16183077,
                    'user'              => 5712124,
                    'active'            => true,
                    'ownedByCustomer'   => false,
                    'country'           => 'US',
                    'profile'           => $item->data()['profile'],
                    'accountHolderName' => $item->data()['accountHolderName'],
                    'currency'          => $item->data()['currency'],
                    'type'              => $item->data()['type'],
                    'details'            => [
                        'address'                 => $item->data()['details']['address'],
                        'legalType'               => $item->data()['details']['legalType'],
                        'accountNumber'           => $item->data()['details']['accountNumber'],
                        'abartn'                  => $item->data()['details']['abartn'],
                        'accountType'             => $item->data()['details']['accountType'],
                        'email'                   => null,
                        'sortCode'                => null,
                        'bankgiroNumber'          => null,
                        'ifscCode'                => null,
                        'bsbCode'                 => null,
                        'institutionNumber'       => null,
                        'transitNumber'           => null,
                        'phoneNumber'             => null,
                        'bankCode'                => null,
                        'russiaRegion'            => null,
                        'routingNumber'           => null,
                        'branchCode'              => null,
                        'cpf'                     => null,
                        'cardNumber'              => null,
                        'idType'                  => null,
                        'idNumber'                => null,
                        'idCountryIso3'           => null,
                        'idValidFrom'             => null,
                        'idValidTo'               => null,
                        'clabe'                   => null,
                        'swiftCode'               => null,
                        'dateOfBirth'             => null,
                        'clearingNumber'          => null,
                        'bankName'                => null,
                        'branchName'              => null,
                        'businessNumber'          => null,
                        'province'                => null,
                        'city'                    => null,
                        'rut'                     => null,
                        'token'                   => null,
                        'cnpj'                    => null,
                        'payinReference'          => null,
                        'pspReference'            => null,
                        'orderId'                 => null,
                        'idDocumentType'          => null,
                        'idDocumentNumber'        => null,
                        'targetProfile'           => null,
                        'targetUserId'            => null,
                        'taxId'                   => null,
                        'job'                     => null,
                        'nationality'             => null,
                        'interacAccount'          => null,
                        'bban'                    => null,
                        'town'                    => null,
                        'postCode'                => null,
                        'language'                => null,
                        'billerCode'              => null,
                        'customerReferenceNumber' => null,
                        'IBAN'                    => null,
                        'iban'                    => null,
                        'BIC'                     => null,
                        'bic'                     => null,
                    ],
                ];
            },
        ]);
    }

    public function testPersonTransfer()
    {

        $instructor = instructor::factory()->create();

        $sessions = session::factory(5)->create([
            'instructor_id' => $instructor->id,
            'created_at'    => now()->subDay(),
            'date'          => now()->subDay(),
            'student_id'    => student::factory()->create(),
        ]);

        // if( ( int ) exec( 'ping -c 1 -q google.com >&/dev/null; echo $?' ) !== 0 ) 
        $this->makeHttpFake();

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->PersonTransfer(), ['data' => (['country' => 'test'] + $this->makeData($instructor))])
            ->assertResponseStatusMessageAndLog('PersonMutations.PersonTransfer.fail', false, 404, [], 'fail')
            ->assertJson(['data' => ['PersonTransfer' =>  ['StatusTransfer' => [
                'message'         => 'Must be ISO 3166-1 alpha-2 country code.',
                'title'           => 'NOT_VALID',
                'check'           => false,
                'arguments_name'  => 'country',
                'arguments_value' => '',
            ]]]]);

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->PersonTransfer(), ['data' => (['amount' => $amount = 999999] + $this->makeData($instructor))])
            ->assertResponseStatusMessageAndLog('PersonMutations.PersonTransfer.fail', false, 404, [], 'fail')
            ->assertJson(['data' => ['PersonTransfer' =>  ['StatusTransfer' => [
                'message'         => 'enter amount less than your current Balance.',
                'title'           => 'NOT_VALID_AMOUNT',
                'check'           => false,
                'arguments_name'  => 'amount',
                'arguments_value' => (string) $amount,
            ]]]]);

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->PersonTransfer(), ['data' => $makeData = $this->makeData($instructor)])
            ->assertResponseStatusMessageAndLog('PersonMutations.PersonTransfer.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['PersonTransfer' =>  ['Transfer' => $makeData + ['step' => 'funds converted'], 'StatusTransfer' => [
                'message'         => '',
                'title'           => '',
                'check'           => true,
                'arguments_name'  => '',
                'arguments_value' => '',
            ]]]]);

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles('{ meInstructors {
				id
				total_earning_counter
				current_balance
				sessionsCanEarning{ paginatorInfo{ total } }
			} }')
            ->assertJson(['data' => ['meInstructors' => [
                'total_earning_counter' => $sessions->sum('rate_salary'),
                'current_balance'       => $instructor->total_earning_counter - $makeData['amount'],
                'sessionsCanEarning'    => ['paginatorInfo' => ['total' => $sessions->count()]],
            ]]]);

        $this
            ->passport($instructor, ['instructor'], 'instructor')
            ->GraphQLFiles($this->InstructorGetTransfers())
            ->assertJson(['data' => ['InstructorGetTransfers' => ['data' => [0 => $makeData + ['step' => 'funds converted']]]]]);
    }
}
