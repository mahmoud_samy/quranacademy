<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\session;

use App\Notifications\Instructor\interacting;

use App\Notifications\Student\sessionOneHourBeforeRemaining        as sessionOneHourBeforeRemainingForstudent        ;
use App\Notifications\Instructor\sessionOneHourBeforeRemaining     as sessionOneHourBeforeRemainingForinstructor     ;
use App\Notifications\Student\sessionFiveMinutesBeforeRemaining    as sessionfiveMinutesBeforeRemainingForstudent    ;
use App\Notifications\Instructor\sessionFiveMinutesBeforeRemaining as sessionfiveMinutesBeforeRemainingForinstructor ;

use Notification ;

class notificationTest extends TestCase {

	public function testTimerd( ) {

		Notification::fake( [
			interacting                                    :: class ,
			sessionOneHourBeforeRemainingForstudent        :: class ,
			sessionOneHourBeforeRemainingForinstructor     :: class ,
			sessionfiveMinutesBeforeRemainingForstudent    :: class ,
			sessionfiveMinutesBeforeRemainingForinstructor :: class ,
		] ) ;

		$this -> database_startup( ) ;

		$instructor = instructor::factory ( ) -> create( [ 'updated_at' => now( ) -> subMonth( ) ] );

		session::factory ( ) -> create( [
			'date'       => now     (                )                         ,
			'time'       => now     (                ) -> addHours ( 2 )       ,
			'student_id' => student::factory ( ) -> create   (   ) -> id ,
		] );

		$oneHour = session::factory ( ) -> create( [
			'date'       => now     (                )                         ,
			'time'       => now     (                ) -> addHour ( )       ,
			'student_id' => student::factory ( ) -> create  ( ) -> id ,
		] );

		$fiveMinutes = session::factory ( ) -> create( [
			'one_hour_before_notify' => 1 ,
			'date'       => now     (                )                         ,
			'time'       => now     (                ) -> addMinutes ( 5 )       ,
			'student_id' => student::factory ( ) -> create     (   ) -> id ,
		] );

		$asdsad = $this -> get( route( 'schedule' ) ) -> json( '*' ) ;

		Notification::assertSentTo( [ $oneHour -> instructor     ] , sessionOneHourBeforeRemainingForinstructor     :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]                         ) &&
			( $notification -> other_data                                               === [ 'session_id' => $oneHour -> id              ]                         ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Instructor::SessionsAction'                                            ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( sessionOneHourBeforeRemainingForinstructor::class )     ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 2                                                                       ) &&
			true
		);

		Notification::assertSentTo( [ $oneHour -> student        ] , sessionOneHourBeforeRemainingForstudent        :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]                         ) &&
			( $notification -> other_data                                               === [ 'session_id' => $oneHour -> id              ]                         ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Student::SessionsAction'                                               ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( sessionOneHourBeforeRemainingForstudent::class    )     ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 11                                                                      ) &&
			true
		);

		Notification::assertSentTo( [ $fiveMinutes -> instructor ] , sessionfiveMinutesBeforeRemainingForinstructor :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]                         ) &&
			( $notification -> other_data                                               === [ 'session_id' => $fiveMinutes -> id          ]                         ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Instructor::SessionsAction'                                            ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( sessionfiveMinutesBeforeRemainingForinstructor::class ) ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 1                                                                       ) &&
			true
		);

		Notification::assertSentTo( [ $fiveMinutes -> student    ] , sessionfiveMinutesBeforeRemainingForstudent :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]                         ) &&
			( $notification -> other_data                                               === [ 'session_id' => $fiveMinutes -> id          ]                         ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Student::SessionsAction'                                               ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( sessionfiveMinutesBeforeRemainingForstudent::class    ) ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 10                                                                      ) &&
			true
		);

		Notification::assertSentTo( [ $instructor                ] , interacting                                 :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]                         ) &&
			( $notification -> other_data                                               === ''                                                                      ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Instructor::HomeAction'                                                ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( interacting::class                                    ) ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 8                                                                       ) &&
			true
		);

	}

}
