<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;

class loginTest extends TestCase {

	public function globalLogin ( ) {
		return 'mutation( $data : globalLoginInput ! ) {
			globalLogin ( data : $data ) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					id
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function globalLoginSocialite ( ) {
		return 'mutation( $data : globalloginSocialite ! ) {
			globalLoginSocialite ( data : $data ) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					id
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function globalLogout ( ) {
		return 'mutation{
			globalLogout{
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function testLoginEmailNotExist( ) {

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => $this -> faker( ) -> unique( ) -> email ,
			'password' => 'password1D'                            ,
		] ] );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'BearerTokenResponse' => null ,
			'Student'             => null ,
			'Instructor'          => null ,
			'SubInstructor'       => null ,
		] ] ]) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.emailNotExist' , false , 404 , [ ] , 'emailNotExist' );

	}

    public function testLoginInvalidCredentialsStudent( ) {

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => student ::factory ( ) -> create ( ) -> email ,
			'password' => 'password1Dc'                                      ,
		] ] );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'BearerTokenResponse' => null ,
			'Student'             => null ,
			'Instructor'          => null ,
			'SubInstructor'       => null ,
		] ] ]) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.invalidlogin' , false , 502 , [ ] , 'invalidlogin' );

	}

	public function testLoginStudent( ) {

		$student = student ::factory ( )   -> create ( ) ;

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => $student -> email ,
			'password' => 'password1D'      ,
		] ] );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'Student'       => [ 'id' => $student -> id ] ,
			'Instructor'    => null                       ,
			'SubInstructor' => null                       ,
		] ] ]) ;

		$this -> assertBearerTokenResponse( $req , 'globalLogin' ) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.successfully' , true , 200 , [ ] , 'successfully' );

	}

	public function testLoginStudentSocialite( ) {

		if( env( 'GOOGLE_ACCESS_TOKEN' ) ){

			SubInstructor :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );
			instructor    :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );
			student       :: where( 'email' , 'mahmoud.samy@black-squares.com' ) -> delete( );
	
			$student = student ::factory ( ) -> create ( [ 'email' => 'mahmoud.samy@black-squares.com' ] ) ;
	
			$req = $this -> GraphQLFiles( $this -> globalLoginSocialite( ) , [ 'data' => [
				'token'    => env( 'GOOGLE_ACCESS_TOKEN' ) ,
				'provider' => 'google' ,
			] ] );

			$this -> assertBearerTokenResponse( $req , 'globalLoginSocialite' ) ;

			$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLoginSocialite.successfully' , true , 200 , [ ] , 'successfully' );
	
			$req -> assertJson([ 'data' => [ 'globalLoginSocialite' =>  [
				'Student'       => [ 'id' => $student -> id ] ,
				'Instructor'    => null                       ,
				'SubInstructor' => null                       ,
			] ] ]) ;

		}
	
		$this -> assertTrue( true );
	
	}

	public function testLoginInvalidCredentialsInstructor( ) {

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => instructor ::factory ( ) -> create ( ) -> email ,
			'password' => 'password1Dc'                                         ,
		] ] );

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.invalidlogin' , false , 502 , [ ] , 'successfully' );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'BearerTokenResponse' => null ,
			'Student'             => null ,
			'Instructor'          => null ,
			'SubInstructor'       => null ,
		] ] ]) ;

	}

	public function testLoginInstructor( ) {

		$instructor = instructor ::factory ( )   -> create ( ) ;

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => $instructor -> email ,
			'password' => 'password1D'      ,
		] ] );

		$this -> assertBearerTokenResponse( $req , 'globalLogin' ) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.successfully' , true , 200 , [ ] , 'successfully' );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'Instructor'    => [ 'id' => $instructor -> id ] ,
			'Student'       => null                          ,
			'SubInstructor' => null                          ,
		] ] ]) ;

	}

	public function testLogoutInstructor( ) {

		$instructor = instructor ::factory ( )   -> create ( ) ;

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => $instructor -> email ,
			'password' => 'password1D'      ,
		] ] );

		$this -> assertBearerTokenResponse( $req , 'globalLogin' ) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.successfully' , true , 200 , [ ] , 'successfully' );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'Instructor'    => [ 'id' => $instructor -> id ] ,
			'Student'       => null                          ,
			'SubInstructor' => null                          ,
		] ] ]) ;

		$Token = $req -> json ( 'data.globalLogin.BearerTokenResponse' ) ;

		$req = $this
			-> withHeaders([ 'Authorization' => $Token [ 'token_type' ] . ' ' . $Token [ 'access_token' ] ])
			-> GraphQLFiles( $this -> globalLogout( ) )
		;

		$this -> assertEmpty( $instructor -> tokens( ) -> get( ) ) ;

	}

	public function testLoginInvalidCredentialsSubInstructor( ) {

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => SubInstructor ::factory ( ) -> create ( ) -> email ,
			'password' => 'password1Dc'                                         ,
		] ] );

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.invalidlogin' , false , 502 , [ ] , 'successfully' );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'BearerTokenResponse' => null ,
			'Student'             => null ,
			'Instructor'          => null ,
			'SubInstructor'       => null ,
		] ] ]) ;

	}

	public function testLoginSubInstructor( ) {

		$SubInstructor = SubInstructor ::factory ( )   -> create ( ) ;

		$req = $this -> GraphQLFiles( $this -> globalLogin( ) , [ 'data' => [
			'email'    => $SubInstructor -> email ,
			'password' => 'password1D'      ,
		] ] );

		$this -> assertBearerTokenResponse( $req , 'globalLogin' ) ;

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalLogin.successfully' , true , 200 , [ ] , 'successfully' );

		$req -> assertJson([ 'data' => [ 'globalLogin' =>  [
			'SubInstructor' => [ 'id' => $SubInstructor -> id ] ,
			'Student'       => null                             ,
			'Instructor'    => null                             ,
		] ] ]) ;

	}

}
