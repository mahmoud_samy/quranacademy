<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;

use Illuminate\Http\UploadedFile;

use App\Notifications\SubInstructor\createdButNotcompleted;
use App\Notifications\Student\create;

class registerTest extends TestCase
{

    public function globalRegister()
    {
        return 'mutation(
			$data : globalRegisterInput !
		) {
			globalRegister (
				data : $data 
			) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
					email
					profile_image{
						id 
						url
					}
				}
				Instructor{
					id
					email
					profile_image{
						id 
						url
					}
				}
				SubInstructor{
					id
					email
					profile_image{
						id 
						url
					}
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
    }

    public function testRegisterEmailisExistStudent()
    {

        $email = student::factory()->create()->email;

        $Pincode = Pincode::firstOrCreate(['email' => $email])->getNewToken();

        $this->assertResponseStatusMessageAndLog($req = $this->GraphQLFiles($this->globalRegister(), ['data' => [
            'verifyPinCodeInput' => [
                'email'    => $email,
                'pin_code' => app('encrypter')->decrypt($Pincode->token) + 1,
            ]
        ]]), 'globalMutations.emailisExist', false, 503, [], 'emailisExist');
    }

    public function testRegisterStudentNotMatchPinCode()
    {

        $email = $this->faker()->unique()->safeEmail();

        $Pincode = Pincode::firstOrCreate(['email' => $email])->getNewToken();

        $this->assertResponseStatusMessageAndLog($req = $this->GraphQLFiles($this->globalRegister(), ['data' => [
            'verifyPinCodeInput' => [
                'email'    => $email,
                'pin_code' => app('encrypter')->decrypt($Pincode->token) + 1,
            ]
        ]]), 'globalMutations.globalRegister.pinCodeIsNotMatch', false, 502, [], 'pinCodeIsNotMatch');
    }

    public function testRegisterSubInstructor()
    {

        $email = $this->faker()->unique()->safeEmail();

        $Pincode = Pincode::firstOrCreate(['email' => $email])->getNewToken();

        $this
            ->assertResponseStatusMessageAndLog($req = $this->GraphQLFiles($this->globalRegister(), ['data' => [
                'verifyPinCodeInput' => [
                    'email'    => $email,
                    'pin_code' => app('encrypter')->decrypt($Pincode->token),
                ],
                'subInstructorsInput' => [
                    'first_name'    => $this->faker()->unique()->name,
                    'last_name'     => $this->faker()->unique()->name,
                    'gender'        => $this->faker()->boolean,
                    'date_of_birth' => now()->subYears(5)->format('Y-m-d'),
                    'password'      => 'password1D',
                    'profile_image' => $this->makeFile('/app/public/logo'),
                ]
            ]]), 'globalMutations.globalRegister.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['globalRegister' =>  [
                'Student'       => null,
                'Instructor'    => null,
                'SubInstructor' => ['email' => $email],
            ]]])
            ->assertMessagesLog('Notifications', 'SubInstructor', SubInstructor::where('email', $email)->first()->Notifications()->get(), createdButNotcompleted::class, [])
            ->assertBearerTokenResponse($req, 'globalRegister');
    }

    public function testRegisterInstructor()
    {

        $email = $this->faker()->unique()->safeEmail();

        $Pincode = Pincode::firstOrCreate(['email' => $email])->getNewToken();

        $this
            ->assertResponseStatusMessageAndLog($req = $this->GraphQLFiles($this->globalRegister(), ['data' => [
                'verifyPinCodeInput' => [
                    'email'    => $email,
                    'pin_code' => app('encrypter')->decrypt($Pincode->token),
                ],
                'studentsInput' => [
                    'first_name'    => $this->faker()->unique()->name,
                    'last_name'     => $this->faker()->unique()->name,
                    'gender'        => $this->faker()->boolean,
                    'date_of_birth' => now()->subYears(5)->format('Y-m-d'),
                    'password'      => 'password1D',
                    'profile_image' => $this->makeFile('/app/public/logo'),
                ],
            ]]), 'globalMutations.globalRegister.Successfully', true, 200, [], 'Successfully')
            ->assertJson(['data' => ['globalRegister' =>  [
                'SubInstructor' => null,
                'Instructor'    => null,
                'Student'       => ['email' => $email],
            ]]])
            ->assertMessagesLog('Notifications', 'student', student::where('email', $email)->first()->Notifications()->get(), create::class, [])
            ->assertBearerTokenResponse($req, 'globalRegister');
    }

    public function testRegisterSubInstructorWithprovide()
    {

        if (env('GOOGLE_ACCESS_TOKEN')) {
            SubInstructor::where('email', 'mahmoud.samy@black-squares.com')->delete();
            instructor::where('email', 'mahmoud.samy@black-squares.com')->delete();
            student::where('email', 'mahmoud.samy@black-squares.com')->delete();

            $req = $this->GraphQLFiles($this->globalLoginSocialite(), ['data' => [
                'SocialiteInput' => [
                    'token'    => env('GOOGLE_ACCESS_TOKEN'),
                    'provider' => 'google',
                ],
                'subInstructorsInput' => [
                    'first_name'    => $this->faker()->unique()->name,
                    'last_name'     => $this->faker()->unique()->name,
                ],
            ]]);

            $req->assertJson(['data' => ['globalRegister' =>  [
                'Student'       => null,
                'Instructor'    => null,
                'SubInstructor' => ['email' => $email],
            ]]]);

            $this->assertBearerTokenResponse($req, 'globalRegister');
        }

        $this->assertTrue(true);
    }

    public function testRegisterInstructorWithprovide()
    {

        if (env('GOOGLE_ACCESS_TOKEN')) {
            SubInstructor::where('email', 'mahmoud.samy@black-squares.com')->delete();
            instructor::where('email', 'mahmoud.samy@black-squares.com')->delete();
            student::where('email', 'mahmoud.samy@black-squares.com')->delete();

            $req = $this->GraphQLFiles($this->globalLoginSocialite(), ['data' => [
                'SocialiteInput' => [
                    'token'    => env('GOOGLE_ACCESS_TOKEN'),
                    'provider' => 'google',
                ],
                'studentsInput' => [
                    'first_name'    => $this->faker()->unique()->name,
                    'last_name'     => $this->faker()->unique()->name,
                ],
            ]]);

            $req->assertJson(['data' => ['globalRegister' =>  [
                'SubInstructor' => null,
                'Instructor'    => null,
                'Student'       => ['email' => $email],
            ]]]);

            $this->assertBearerTokenResponse($req, 'globalRegister');
        }

        $this->assertTrue(true);
    }
}
