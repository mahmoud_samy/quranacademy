<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;

class refreshTokenTest extends TestCase {

	public function globalRefreshToken ( ) {
		return 'mutation(
			$data : globalRefreshTokenInput !
		) {
			globalRefreshToken (
				data : $data 
			) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					id
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function testRefreshTokenStudent( ) {

		$student = student ::factory( ) -> create ( ) ;

		$BearerTokenResponse = $student -> MakeBearerToken( ) ;

		$req = $this -> GraphQLFiles( $this -> globalRefreshToken( ) , [ 'data' => [
			'refresh_token' => $BearerTokenResponse [ 'refresh_token' ] ,
		] ] );

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalRefreshToken.successfully' , true , 200 , [ ] , 'successfully' );

		$this -> assertBearerTokenResponse( $req , 'globalRefreshToken' ) ;

		$req -> assertJson([ 'data' => [ 'globalRefreshToken' =>  [
			'Student'       => [ 'id' => $student -> id ] ,
			'Instructor'    => null                       ,
			'SubInstructor' => null                       ,
		] ] ]) ;

	}

	public function testRefreshTokenInstructor( ) {

		$instructor = instructor ::factory( ) -> create ( ) ;
		$BearerTokenResponse = $instructor -> MakeBearerToken( ) ;

		$req = $this -> GraphQLFiles( $this -> globalRefreshToken( ) , [ 'data' => [
			'refresh_token' => $BearerTokenResponse [ 'refresh_token' ] ,
		] ] );

		$this -> assertResponseStatusMessageAndLog ( $req , 'globalMutations.globalRefreshToken.successfully' , true , 200 , [ ] , 'successfully' );

		$this -> assertBearerTokenResponse( $req , 'globalRefreshToken' ) ;

		$req -> assertJson([ 'data' => [ 'globalRefreshToken' =>  [
			'Instructor'    => [ 'id' => $instructor -> id ] ,
			'Student'       => null                          ,
			'SubInstructor' => null                          ,
		] ] ]) ;

	}

}
