<?php

namespace Tests\Feature;

use Tests\TestCase;

use App\Models\instructor;
use App\Models\student;

use Illuminate\Http\UploadedFile ;

class instructorViewTest extends TestCase {

	public function instructorView ( ) {
		return 'query(
			$id : Int!
		) {
			InstructorFind (
				id : $id 
			) {
				id
				first_name
				total_profile_views_counter
			}
		}';
	}

    public function testStudent( ) {

        $student    = student    ::factory ( ) -> create ( ) ;
		$instructor = instructor ::factory ( ) -> create ( [
			'is_Approval'       => 1 ,
			'is_profile_public' => 1 ,
			'is_active'         => 1 ,
			'is_blocked'        => 0 ,
			'deleted_at'        => null
		] ) ;

		$req = $this
			-> GraphQLFiles( $this -> instructorView( ) , [ 'id' => $instructor -> id ] )
			-> assertJson([ 'data' => [ 'InstructorFind' => [
				'id'                          => $instructor -> id         ,
				'first_name'                  => $instructor -> first_name ,
				'total_profile_views_counter' => 0                         ,
			] ] ] )
		;

		$this -> log( 'on get instructor without auth student total profile views counter case' , $req -> json( 'data.InstructorFind.total_profile_views_counter' ) , 'query' , 'primary' , true , 200 );

		$req = $this
			-> withHeaders([ 'Authorization' => $this -> login( $student ) ])
			-> GraphQLFiles( $this -> instructorView( ) , [ 'id' => $instructor -> id ] )
			-> assertJson([ 'data' => [ 'InstructorFind' => [
				'id'                          => $instructor -> id         ,
				'first_name'                  => $instructor -> first_name ,
				'total_profile_views_counter' => 1                         ,
			] ] ] )
		;

		$this -> log( 'on get instructor with auth student total profile views counter case' , $req -> json( 'data.InstructorFind.total_profile_views_counter' ) , 'query' , 'primary' , true , 200 );

    }

}