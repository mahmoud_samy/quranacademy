<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\User;

use App\Models\SubInstructor;
use App\Models\instructor;
use App\Models\student;
use App\Models\Pincode;
use App\Models\variabledb;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;

use Illuminate\Http\UploadedFile ;


class PasswordresetsTest extends TestCase {

	public function globalPasswordresets ( ) {
		return 'mutation(
			$data : globalPasswordresetsInput !
		) {
			globalPasswordresets (
				data : $data 
			) {
				BearerTokenResponse{
					access_token
					refresh_token
					expires_in
					token_type
				}
				Student{
					id
				}
				Instructor{
					id
				}
				SubInstructor{
					id
				}
				Status {
					code
					check
					title
					message
				}
			}
		}';
	}

	public function testRegisterEmailNotExistStudent( ) {

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> globalPasswordresets( ) , [ 'data' => [
			'verifyPinCodeInput' => [
				'email'    => $this -> faker( ) -> unique( ) -> email ,
				'pin_code' => 'ghhjgjg' ,
			],
			'password' => 'password1D' ,
		] ] ) , 'globalMutations.globalPasswordresets.emailNotExist' , false , 404 , [ ] , 'emailNotExist' );

	}

	public function testNotMatchPinCode( ) {

		$email = student ::factory ( ) -> create  ( ) -> email  ;

		$Pincode = Pincode::firstOrCreate([ 'email' => $email ]) -> getNewToken( ) ;

		$this -> assertResponseStatusMessageAndLog ( $this -> GraphQLFiles( $this -> globalPasswordresets( ) , [ 'data' => [
			'verifyPinCodeInput' => [
				'email'    => $email     ,
				'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) + 1 ,
			],
			'password' => 'password1D' ,
		] ] ) , 'globalMutations.globalPasswordresets.pinCodeIsNotMatch' , false , 502 , [ ] , 'pinCodeIsNotMatch' );

	}

	public function testPasswordResetsStudent( ) {

		$student = student ::factory ( )                   -> create      ( ) ;
		$email   = $student                                       -> email           ;
		$Pincode = Pincode ::firstOrCreate([ 'email' => $email ]) -> getNewToken( )  ;

		$this -> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalPasswordresets( ) , [ 'data' => [
			'verifyPinCodeInput' => [
				'email'    => $email     ,
				'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) ,
			],
			'password' => 'password1D' ,
		] ] ) , 'globalMutations.globalPasswordresets.Successfully' , true , 200 , [ ] , 'Successfully' );

		$req -> assertJson([ 'data' => [ 'globalPasswordresets' =>  [
			'Student'       => [ 'id' => $student -> id ] ,
			'Instructor'    => null                       ,
			'SubInstructor' => null                       ,
		] ] ]) ;

		$this -> assertBearerTokenResponse( $req , 'globalPasswordresets' ) ;

	}

	public function testPasswordResetsSubInstructor( ) {

		$SubInstructor = SubInstructor ::factory ( )              -> create      ( ) ;
		$email         = $SubInstructor                                -> email           ;
		$Pincode       = Pincode::firstOrCreate([ 'email' => $email ]) -> getNewToken( )  ;

		$this -> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalPasswordresets( ) , [ 'data' => [
			'verifyPinCodeInput' => [
				'email'    => $email     ,
				'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) ,
			],
			'password' => 'password1D' ,
		] ] ) , 'globalMutations.globalPasswordresets.Successfully' , true , 200 , [ ] , 'Successfully' );

		$req -> assertJson([ 'data' => [ 'globalPasswordresets' =>  [
			'Student'       => null                             ,
			'SubInstructor' => [ 'id' => $SubInstructor -> id ] ,
			'Instructor'    => null                             ,
		] ] ]) ;

		$this -> assertBearerTokenResponse( $req , 'globalPasswordresets' ) ;

	}

	public function testPasswordResetsinstructor( ) {

		$instructor = instructor ::factory ( )                -> create      ( ) ;
		$email      = $instructor                                    -> email           ;
		$Pincode    = Pincode ::firstOrCreate([ 'email' => $email ]) -> getNewToken( )  ;

		$this -> assertResponseStatusMessageAndLog ( $req = $this -> GraphQLFiles( $this -> globalPasswordresets( ) , [ 'data' => [
			'verifyPinCodeInput' => [
				'email'    => $email     ,
				'pin_code'	=> app( 'encrypter' ) -> decrypt( $Pincode -> token ) ,
			],
			'password' => 'password1D' ,
		] ] ) , 'globalMutations.globalPasswordresets.Successfully' , true , 200 , [ ] , 'Successfully' );

		$req -> assertJson([ 'data' => [ 'globalPasswordresets' =>  [
			'Student'       => null                          ,
			'SubInstructor' => null                          ,
			'Instructor'    => [ 'id' => $instructor -> id ] ,
		] ] ]) ;

		$this -> assertBearerTokenResponse( $req , 'globalPasswordresets' ) ;

	}

}