<?php

namespace Tests\Feature;

use Tests\TestCase ;

use App\Models\session_message;
use App\Models\instructor;
use App\Models\student;

use App\Notifications\CreateNewMessage;

use App\Notifications\Instructor\createNewMessage as createNewMessageForInstructor ;
use App\Notifications\Student\createNewMessage    as createNewMessageForStudent    ;

use Notification ;

class askMessageTest extends TestCase {

	public $session = 'session{
		id
		count_un_seen_messages
		messages{ data{
			message
			seen_at
		} }
	}';

	public $message = 'message{
		id
		message
		seen_at
	}';

	public function MessageCreate ( ) {
		return 'mutation( $data : MessageCreateInput ! ) { MessageCreate ( data : $data ) {
			' . $this -> Status   . '
			' . $this -> session  . '
			' . $this -> message . '
		} }';
	}

	public function MessageGet ( ) {
		return 'query( $id : Int! ) {
			MessageGet ( id : $id ) { 
				id
				message
				seen_at
			}
		}';
	}

	public function SessionsSeeMessages ( ) {
		return 'mutation( $data : sessionsSeeMessagesInput ! ) { sessionsSeeMessages ( data : $data ) {
			' . $this -> Status   . '
			' . $this -> session  . '
		} }';
	}

	public function messageVars ( session_message $message ) : array {
		return [ 'data' => [
			'session_id' => $message -> session_id ,
			'message'    => $message -> message      ,
		] ];
	}

	public function testMessageCreateWithoutAuth( ) {

		$this 
			-> GraphQLFiles ( $this -> MessageCreate( ) , $this -> messageVars ( session_message::factory ( ) -> make ( ) ) )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.unAuth' , false , 502 , [ ] , 'unAuth' )
		;

	}

	public function testMessageCreateForeOthers( ) {

		$this 
			-> passport( student::factory ( ) -> create ( ) , [ 'student' ] , 'student' )
			-> GraphQLFiles ( $this -> MessageCreate( ) , $this -> messageVars ( session_message::factory ( ) -> make ( ) ) )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.NotYours' , false , 502 , [ ] , 'NotYours' )
		;

	}

	public function testMessageCreateFromStudent( ) {

		$message = session_message::factory ( ) -> make ( ) ;

		session_message::factory ( 5 ) -> make ( ) -> map( fn( $msg ) => $message -> session -> session_messages( ) -> create( $msg -> toArray( ) ) ) ;

		$array = [ 'student_name' => $message -> session -> student -> name ];

		Notification::fake( createNewMessageForInstructor :: class ) ;

		$this
			-> passport( $message -> session -> student , [ 'student' ] , 'student' )
			-> GraphQLFiles ( $this -> MessageCreate( ) , $var = $this -> messageVars ( $message ) )
			-> assertJson   ( [ 'data'     => [ 'MessageCreate' => [
				'session' => [ 'count_un_seen_messages' => $message -> session  -> refresh( ) -> instructorCountUnSeen ] ,
				'message' => [ 'message' => $var[ 'data' ] [ 'message' ] ]
			] ] ] )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.successfully' , true , 200 , [ ] , 'successfully' )
		;

		Notification::assertSentTo( [ $message -> session -> instructor ] , createNewMessageForInstructor :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]          ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Instructor::SessionQuestionsAction'                     ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( createNewMessageForInstructor :: class ) ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 6                                                        ) &&
			true
		);


		$this -> assertTrue( $message -> session -> instructor -> refresh( ) -> fireBaseCloudMessageing[ 'total_sessions_asked_un_seen' ] === 1         );
		$this -> assertTrue( count( $message -> session -> messages -> whereNull( 'instructor_id' ) ) === $message -> session -> studentCountUnSeen     );
		$this -> assertTrue( count( $message -> session -> messages -> whereNull( 'student_id'    ) ) === $message -> session -> instructorCountUnSeen  );

		$this 
			-> passport                          ( $message -> session -> student , [ 'student' ] , 'student'                                     )
			-> GraphQLFiles                      ( $this -> SessionsSeeMessages( ) , [ 'data' => [ 'id' => $message -> session -> id ] ]          )
			-> assertJson                        ( [ 'data' => [ 'sessionsSeeMessages' => [ 'session' => [  'count_un_seen_messages' => 0 ] ] ] ] )
			-> assertResponseStatusMessageAndLog ( 'sessionsMutations.sessionsSeeMessages.Successfully' , true , 200 , [ ] , 'successfully'       )
		;

		$message -> session  -> refresh( );

		$this -> assertTrue( $message -> session -> instructorCountUnSeen === 0 );

		$this
			-> passport( $message -> session -> student , [ 'student' ] , 'student' )
			-> GraphQLFiles ( $this -> MessageCreate( ) , $var = $this -> messageVars ( $message ) )
			-> assertJson   ( [ 'data' => [ 'MessageCreate' => [
				'session' => [  'count_un_seen_messages' => $message -> session  -> refresh( ) -> instructorCountUnSeen ],
				'message' => [ 'message' => $var[ 'data' ] [ 'message' ] ]
			] ] ] )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.successfully' , true , 200 , [ ] , 'successfully' )
		;

		Notification::assertSentTo( [ $message -> session -> instructor ] , createNewMessageForInstructor :: class , fn ( $notification , $channels , $notifiable ) =>
			( $channels                                                                 === [ 'App\Notifications\Classes\FireBaseChannel' ]          ) &&
			( $notification -> fireBaseCloudMessageingNotification( )[ 'click_action' ] === 'Instructor::SessionQuestionsAction'                     ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind'         ] === class_basename( createNewMessageForInstructor :: class ) ) &&
			( $notification -> fireBaseCloudMessageingData        ( )[ 'kind_number'  ] === 6                                                        ) &&
			true
		);

	}

	public function testMessageCreateFromInstructor( ) {

		$message = session_message::factory ( ) -> make ( ) ;

		session_message::factory ( 5 ) -> make ( ) -> map( fn( $msg ) => $message -> session -> session_messages( ) -> create( $msg -> toArray( ) ) ) ;

		$array = [ 'Instructor_name' => $message -> session -> Instructor -> name ];

		$session = $message -> session ;

		$msg = $this 
			-> passport     ( $session -> instructor , [ 'instructor' ] , 'instructor' )
			-> GraphQLFiles ( $this -> MessageCreate( ) , $var = $this -> messageVars ( $message ) )
			-> assertJson   ( [ 'data' => [ 'MessageCreate' => [
				'session' => [ 'count_un_seen_messages' => $session -> refresh( ) -> studentCountUnSeen ],
				'message' => [ 'message' => $var[ 'data' ] [ 'message' ] ]
			] ] ] )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.successfully' , true , 200 , [ ] , 'successfully' )
		;

		$msg -> assertMessagesLog( 'Notifications' , 'student' , $message -> session  -> student -> notifications( ) -> get( ) , createNewMessageForstudent::class , $array );

		$this -> assertTrue( count( $session -> messages -> whereNull( 'instructor_id' ) ) === $session -> studentCountUnSeen     );
		$this -> assertTrue( count( $session -> messages -> whereNull( 'student_id'    ) ) === $session -> instructorCountUnSeen  );

		$get = $this 
			-> passport     ( $session -> student , [ 'student' ] , 'student' )
			-> GraphQLFiles ( $this -> MessageGet( ) , [ 'id' => $msg -> json( 'data.MessageCreate.message.id' )  ])
			-> json( 'data.MessageGet.seen_at' )
		;

		$this -> assertNotNull( $get );

		$this
			-> passport                          ( $session -> instructor , [ 'instructor' ] , 'instructor'                                        )
			-> GraphQLFiles                      ( $this -> SessionsSeeMessages( ) , [ 'data' => [ 'id' => $session -> id ] ]                       )
			-> assertJson                        ( [ 'data' => [ 'sessionsSeeMessages' => [ 'session' => [  'count_un_seen_messages' => 0 ] ] ] ]   )
			-> assertResponseStatusMessageAndLog ( 'sessionsMutations.sessionsSeeMessages.Successfully' , true , 200 , [ ] , 'successfully' )
		;

		$session  -> refresh( );

		$this -> assertTrue( $session -> studentCountUnSeen === 0 );

		$this
			-> passport( $session -> instructor , [ 'instructor' ] , 'instructor' )
			-> GraphQLFiles ( $this -> MessageCreate( ) , $var = $this -> messageVars ( $message ) )
			-> assertJson   ( [ 'data'     => [ 'MessageCreate' => [
				'session' => [  'count_un_seen_messages' => $session  -> refresh( ) -> studentCountUnSeen ],
				'message' => [ 'message' => $var[ 'data' ] [ 'message' ] ]
			] ] ] )
			-> assertResponseStatusMessageAndLog ( 'MessageMutations.MessageCreate.successfully' , true , 200 , [ ] , 'successfully' )
			-> assertMessagesLog( 'Notifications' , 'student' , $message -> session  -> student -> notifications( ) -> get( ) , createNewMessageForstudent::class , $array )
			-> assertTrue( $session -> student -> Notifications ( ) -> for( $session , createNewMessageForStudent::class ) -> count( ) === 1 )
		;

	}

}
