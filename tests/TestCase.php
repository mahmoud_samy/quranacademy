<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;
use harby\services\tests\Traits\Teststrait ;
use harby\services\tests\Traits\testconsole ;
use Illuminate\Support\Facades\Http ;
use Illuminate\Testing\TestResponse ;

abstract class TestCase extends BaseTestCase {

    use Teststrait ,
        WithFaker
    ;

	public static string $ModelsRoute  =   'App\Models'   ;
	public static array  $ModelsTranck = [
		'certificate'        ,
		'File'               ,
		'image'              ,
		'instructor'         ,
		'instructorTransfer' ,
		'instructorView'     ,
		'letter'             ,
		'OauthAccessToken'   ,
		'Pincode'            ,
		'rating'             ,
		'recommendation'     ,
		'session_message'    ,
		'SpokenLanguageUser' ,
		'student'            ,
		'SubInstructor'      ,
		'variabledb'         ,
		'Pincode'            ,
		'session'            ,
		'notification'       ,
    ] ;
    
	public function login( $user ) : String {

		$response = $user -> MakeBearerToken( ) ;
		return $response [ 'token_type' ] . ' ' . $response [ 'access_token' ];

	}


	public function card( ) : array {
		return [
			'name'   => $this -> faker( ) -> name ,
			'number' => $this -> faker( ) -> creditCardNumber ,
			'date'   => $this -> faker( ) -> creditCardExpirationDateString ,
			'cvv'    => ( string ) $this -> faker( ) -> numberBetween( 100 , 999 ) ,
		] ;
	}

	protected function setUp( ) : void {
		parent :: setUp (  );
		Http   :: fake  ( [
			'https://fcm.googleapis.com/*' => Http::response( [ 'foo' => 'bar' ] , 200 , [ 'Headers' ] ) ,
		]);
		if ( ! static::$wasSetup ) {
			static::$console  = new testconsole ( ) ;
			static::$wasSetup = true                ;
		}
		static::$console -> updateCase( $this );
	}

}