<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\languageType;

class languageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        languageType::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'      => $id = 1,
                'context' => 'engilsh',
                'code'    => 'en',
            ],
            [
                'id'      => ++$id,
                'context' => 'arabic',
                'code'    => 'ar',
            ],
        ]);
    }
}
