<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CurrencyTypeSeeder::class);
        $this->call(ImageTypeSeeder::class);
        $this->call(variabledbSeeder::class);
        $this->call(courseTypeSeeder::class);
        $this->call(studentAgeTypeSeeder::class);
        $this->call(studentGenderTypeSeeder::class);
        $this->call(languageTypeSeeder::class);
        $this->call(RateMinutesTypeSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(faqSeeder::class);
        $this->call(PrivacyPolicySeeder::class);
        $this->call(TermSeeder::class);
        $this->call(AboutSeeder::class);
    }
}
