<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\about;

class AboutSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
        $faker = \Faker\Factory::create( );
		about::createOrUpdateArrayWithoutGlobalScopes( [
            [
                'id'     => $id = 1                ,
                'header' => null                   ,
                'lang'   => 'en'                   ,
                'body'   => '<p>' .
                    '<p>Do you want to recite the Quran fluently? Do you want to memorize the Quran brilliantly? Are you looking for someone to help you? Don\'t you have time to go to a place to memorize and recite the Quran? So, you need to know more about Quran Academy Application.<p><br>' . 
                    '<p>With the increasing demand for memorizing and reciting the Quran from various people and countries around the world, we launched Quran Academy Application. It is an online application that provides professional education for Quran Tahfeeth and Tajweed, with students all over the world and with distinguished instructors who have certificates "Ijazah" and experience in memorizing and reciting the Quran.<p><br>' . 
                    '<p>This application is supported by many innovative tools specifically designed to enable the student to communicate with the instructor, through online audio and video sessions. It is now easier to learn and memorize the Quran.<p><br>' . 
                    '<p>Our mission is to offer Quran education to those who need it and are eager to learn by providing modern teaching methods and dedicated instructors.<p><br>' . 
                    '<p>Whether you dream of memorizing the whole Quran, or a part of it, or just want to learn how to recite it correctly. Quran Academy is the most innovative application to help you learn the Holy Quran at your home, under the supervision of qualified Quran instructors.<p><br>' . 
                    '<p>As the Hadeeth (prophetic statement) in At-Tirmithi proves: “Whoever reads a letter from the Book of Allah, he will have a reward, and this reward will be multiplied by ten. I am not saying that \'Alif, Laam, Meem\' (a combination of letters frequently mentioned in the Holy Quran) is a letter, rather I am saying that ‘Alif’ is a letter, ‘Lam’ is a letter and ‘Mim’ is a letter.” [At-Tirmithi]. We hope this application encourages you to recite the Quran.<p><br>' . 
                '</p>'
            ],
            [
                'id'     => ++$id                  ,
                'header' => null                   ,
                'lang'   => 'ar'                   ,
                'body'   => '<p>' .
                    '<p>هل تريد أن تحفظ القرآن بطريقة صحيحة؟ هل تريد أن تتعلم التجويد و القراءات تنمي مهاراتك في الحفظ و القراءة؟<p><br>' . 
                    '<p>هل دائما ما تبحث عن شخص يساعدك في الحفظ و المراجعة؟<p><br>' . 
                    '<p>أنت الآن لم تعد بحاجة للذهاب لأي مكان, أنت فقط بحاجة أن تتعرف إلى قرآن اكاديمي.<p><br>' . 
                    '<p>مع ازدياد الطلب لحفظ القرآن الكريم ومراجعته, بدأنا تطبيق قرآن أكاديمي قرآن أكاديمي هو تطبيق اونلاين يساعدك تعلم و حفظ القرآن من أي مكان في العالم مع محفظين محترفين حاصلين على إجازات التحفيظ و لديهم ما يكفي من سنوات الخبرة للقيام بإرشادك. <p><br>' . 
                    '<p>التطبيق لديه كثير من الأدوات التي تساعدك على الحفظ و اختيار المحفظ المناسب, فأنت تستطيع ان تختار جنس و جنسية المحفظين و نوع الحصة ما اذا كانت تجويد او تحفيظ أو قراءات و أيضا يمكنك اختيار هل الحصة موجهة الى بالغين أم أطفال<p><br>' . 
                    '<p>بالإضافة إلى تسجيل كل الحصص في التطبيق بحيث تستطيع العودة اليها في اي وقت بعد الانتهاء للمراجعة<p><br>' . 
                    '<p>قال رسولُ الله صلى الله عليه وسلم: «مَن قرأَ حرفًا من كتابِ اللَّهِ فلَهُ بِهِ حسنةٌ، والحسنةُ بعشرِ أمثالِها، لا أقولُ آلم حرفٌ، ولَكِن ألِفٌ<p><br>' . 
                '</p>'
            ],
            /* [
                'id'     => ++$id                  ,
                'header' => null                   ,
                'lang'   => 'ar'                   ,
                'body'   => '<p>'+
                    '<p>هل تسائلت يومًا كيف يمكنك أن تتعلم القرآن الكريم من المنزل بطريقة ممتعة و فعالة؟ ستجد الإجابة دائما في تطبيق قرآن أكاديمي اذا كنت كبير أو صغيرا, رجلا أو امرأة, الآن, حفظ القرآن الكريم صار أسهل و أسرع بكثير من السابق خصوصا بعد دمج طرق التحفيظ بالتكنولوجيا, كل هذا يمكنك أن تجده في تطبيق قرآن اكاديمي.<p><br>' . 
                    '<p>ماهي المميزات التي تجعلك تثق في قرآن أكاديمي؟.<p><br>' . 
                    '<ol>' . 
                        '<li>تصميم سهل الإستخدام<li>' . 
                        '<li>تسجيل سريع<li>' . 
                        '<li>تستطيع ان تختار عن طريق (الجنسية - اللغة - السعر - مدة الحصة - التقييم)<li>' . 
                        '<li>حصص تجويد و تحفيظ و تعليم قراءات<li>' . 
                        '<li>دفع الكتروني عن طريق الكارت البنكي<li>' . 
                        '<li>حصص فردية<li>' . 
                        '<li>القدرة على تغيير الموعد في حالة الطوارئ<li>' . 
                        '<li>تذكير بمواعيد الحصص قبل بدايتها<li>' . 
                    '</ol>' . 
                '</p>'
            ], */
        ] );
    }
}
