<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\rateMinutesType;

class RateMinutesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        rateMinutesType::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'      => $id = 1,
                'context' => '15 min',
                'attr'    => 'rate_15_min',
                'value'   => 15,
                'type'    => 'MINUTE',
            ],
            [
                'id'      => ++$id,
                'context' => '30 min',
                'attr'    => 'rate_30_min',
                'value'   => 30,
                'type'    => 'MINUTE',
            ],
            [
                'id'      => ++$id,
                'context' => '60 min',
                'attr'    => 'rate_60_min',
                'value'   => 60,
                'type'    => 'MINUTE',
            ],
        ]);
    }
}
