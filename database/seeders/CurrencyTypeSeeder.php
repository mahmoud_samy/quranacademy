<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\currencyType;

class CurrencyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        currencyType::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'            => $id = 1,
                'code'          => 'EUR',
                'symbol'        => '€',
                'symbol_native' => '€',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
            [
                'id'             => ++$id,
                'code'          => 'USD',
                'symbol'        => '$',
                'symbol_native' => '$',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
            [
                'id'            => ++$id,
                'code'          => 'AED',
                'symbol'        => 'AED',
                'symbol_native' => 'د.إ.',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
            [
                'id'            => ++$id,
                'code'          => 'EGP',
                'symbol'        => 'EGP',
                'symbol_native' => 'ج.م.',
                'is_active'     => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
        ]);
    }
}
