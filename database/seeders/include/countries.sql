-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2017 at 12:13 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


INSERT INTO countries
    (`id`, `name`, `iso_code`, `country_code`, `is_active`)
VALUES
    (1,'{ "en":"Afghanistan", "ar":"أفغانستان" }',"AF",93,TRUE),
    (2,'{ "en":"Albania", "ar":"ألبانيا" }',"AL",355,TRUE),
    (3,'{ "en":"Algeria", "ar":"الجزائر" }',"DZ",213,TRUE),
    (4,'{ "en":"American Samoa", "ar":"ساموا الأمريكية" }',"AS",684,TRUE),
    (5,'{ "en":"Andorra", "ar":"أندورا" }',"AD",376,TRUE),
    (6,'{ "en":"Angola", "ar":"أنجولا" }',"AO",244,TRUE),
    (7,'{ "en":"Anguilla", "ar":"أنجويلا" }',"AI",1,TRUE),
    (8,'{ "en":"Antarctica", "ar":"القطب الجنوبي" }',"AQ",268,TRUE),
    (9,'{ "en":"Antigua and Barbuda", "ar":"أنتيجوا وبربودا" }',"AG",1,TRUE),
    (10,'{ "en":"Argentina", "ar":"الأرجنتين" }',"AR",54,TRUE),
    (11,'{ "en":"Armenia", "ar":"أرمينيا" }',"AM",374,TRUE),
    (12,'{ "en":"Aruba", "ar":"آروبا" }',"AW",297,TRUE),
    (13,'{ "en":"Australia", "ar":"أستراليا" }',"AU",61,TRUE),
    (14,'{ "en":"Austria", "ar":"النمسا" }',"AT",43,TRUE),
    (15,'{ "en":"Azerbaijan", "ar":"أذربيجان" }',"AZ",994,TRUE),
    (16,'{ "en":"Bahamas", "ar":"الباهاما" }',"BS",1,TRUE),
    (17,'{ "en":"Bahrain", "ar":"البحرين" }',"BH",973,TRUE),
    (18,'{ "en":"Bangladesh", "ar":"بنجلاديش" }',"BD",880,TRUE),
    (19,'{ "en":"Barbados", "ar":"بربادوس" }',"BB",1,TRUE),
    (20,'{ "en":"Belarus", "ar":"روسيا البيضاء" }',"BY",375,TRUE),
    (21,'{ "en":"Belgium", "ar":"بلجيكا" }',"BE",32,TRUE),
    (22,'{ "en":"Belize", "ar":"بليز" }',"BZ",501,TRUE),
    (23,'{ "en":"Benin", "ar":"بنين" }',"BJ",229,TRUE),
    (24,'{ "en":"Bermuda", "ar":"برمودا" }',"BM",1,TRUE),
    (25,'{ "en":"Bhutan", "ar":"بوتان" }',"BT",975,TRUE),
    (26,'{ "en":"Bolivia", "ar":"بوليفيا" }',"BO",591,TRUE),
    (27,'{ "en":"Bosnia and Herzegovina", "ar":"البوسنة والهرسك" }',"BA",387,TRUE),
    (28,'{ "en":"Botswana", "ar":"بتسوانا" }',"BW",267,TRUE),
    (29,'{ "en":"Bouvet Island", "ar":"جزيرة بوفيه" }',"BV",1,TRUE),
    (30,'{ "en":"Brazil", "ar":"البرازيل" }',"BR",55,TRUE),
    (31,'{ "en":"British Indian Ocean Territory", "ar":"المحيط الهندي البريطاني" }',"IO",246,TRUE),
    (32,'{ "en":"British Virgin Islands", "ar":"جزر فرجين البريطانية" }',"VG",1,TRUE),
    (33,'{ "en":"Brunei", "ar":"بروناي" }',"BN",1,TRUE),
    (34,'{ "en":"Bulgaria", "ar":"بلغاريا" }',"BG",359,TRUE),
    (35,'{ "en":"Burkina Faso", "ar":"بوركينا فاسو" }',"BF",226,TRUE),
    (36,'{ "en":"Burundi", "ar":"بوروندي" }',"BI",257,TRUE),
    (37,'{ "en":"Cambodia", "ar":"كمبوديا" }',"KH",855,TRUE),
    (38,'{ "en":"Cameroon", "ar":"الكاميرون" }',"CM",237,TRUE),
    (39,'{ "en":"Canada", "ar":"كندا" }',"CA",1,TRUE),
    (40,'{ "en":"Cape Verde", "ar":"الرأس الأخضر" }',"CV",238,TRUE),
    (41,'{ "en":"Cayman Islands", "ar":"جزر الكايمن" }',"KY",1,TRUE),
    (42,'{ "en":"Central African Republic", "ar":"جمهورية افريقيا الوسطى" }',"CF",236,TRUE),
    (43,'{ "en":"Chad", "ar":"تشاد" }',"TD",235,TRUE),
    (44,'{ "en":"Chile", "ar":"شيلي" }',"CL",56,TRUE),
    (45,'{ "en":"China", "ar":"الصين" }',"CN",86,TRUE),
    (46,'{ "en":"Christmas Island", "ar":"جزيرة الكريسماس" }',"CX",16,TRUE),
    (47,'{ "en":"Cocos [Keeling] Islands", "ar":"جزر كوكوس" }',"CC",16,TRUE),
    (48,'{ "en":"Colombia", "ar":"كولومبيا" }',"CO",57,TRUE),
    (49,'{ "en":"Comoros", "ar":"جزر القمر" }',"KM",269,TRUE),
    (50,'{ "en":"Congo - Brazzaville", "ar":"الكونغو - برازافيل" }',"CG",242,TRUE),
    (51,'{ "en":"Congo - Kinshasa", "ar":"جمهورية الكونغو الديمقراطية" }',"CD",243,TRUE),
    (52,'{ "en":"Cook Islands", "ar":"جزر كوك" }',"CK",682,TRUE),
    (53,'{ "en":"Costa Rica", "ar":"كوستاريكا" }',"CR",506,TRUE),
    (54,'{ "en":"Croatia", "ar":"كرواتيا" }',"HR",385,TRUE),
    (55,'{ "en":"Cuba", "ar":"كوبا" }',"CU",53,TRUE),
    (56,'{ "en":"Cyprus", "ar":"قبرص" }',"CY",357,TRUE),
    (57,'{ "en":"Czech Republic", "ar":"جمهورية التشيك" }',"CZ",420,TRUE),
    (58,'{ "en":"Côte d’Ivoire", "ar":"ساحل العاج" }',"CI",225,TRUE),
    (59,'{ "en":"Denmark", "ar":"الدانمرك" }',"DK",45,TRUE),
    (60,'{ "en":"Djibouti", "ar":"جيبوتي" }',"DJ",253,TRUE),
    (61,'{ "en":"Dominica", "ar":"دومينيكا" }',"DM",1,TRUE),
    (62,'{ "en":"Dominican Republic", "ar":"جمهورية الدومينيك" }',"DO",1,TRUE),
    (63,'{ "en":"Ecuador", "ar":"الاكوادور" }',"EC",593,TRUE),
    (64,'{ "en":"Egypt", "ar":"مصر" }',"EG",20,TRUE),
    (65,'{ "en":"El Salvador", "ar":"السلفادور" }',"SV",503,TRUE),
    (66,'{ "en":"Equatorial Guinea", "ar":"غينيا الاستوائية" }',"GQ",240,TRUE),
    (67,'{ "en":"Eritrea", "ar":"اريتريا" }',"ER",291,TRUE),
    (68,'{ "en":"Estonia", "ar":"استونيا" }',"EE",372,TRUE),
    (69,'{ "en":"Ethiopia", "ar":"اثيوبيا" }',"ET",251,TRUE),
    (70,'{ "en":"Falkland Islands", "ar":"جزر فوكلاند" }',"FK",500,TRUE),
    (71,'{ "en":"Faroe Islands", "ar":"جزر فارو" }',"FO",298,TRUE),
    (72,'{ "en":"Fiji", "ar":"فيجي" }',"FJ",679,TRUE),
    (73,'{ "en":"Finland", "ar":"فنلندا" }',"FI",358,TRUE),
    (74,'{ "en":"France", "ar":"فرنسا" }',"FR",33,TRUE),
    (75,'{ "en":"French Guiana", "ar":"غويانا" }',"GF",594,TRUE),
    (76,'{ "en":"French Polynesia", "ar":"بولينيزيا الفرنسية" }',"PF",689,TRUE),
    (77,'{ "en":"French Southern Territories", "ar":"المقاطعات الجنوبية الفرنسية" }',"TF",1,TRUE),
    (78,'{ "en":"Gabon", "ar":"الجابون" }',"GA",241,TRUE),
    (79,'{ "en":"Gambia", "ar":"غامبيا" }',"GM",220,TRUE),
    (80,'{ "en":"Georgia", "ar":"جورجيا" }',"GE",995,TRUE),
    (81,'{ "en":"Germany", "ar":"ألمانيا" }',"DE",49,TRUE),
    (82,'{ "en":"Ghana", "ar":"غانا" }',"GH",233,TRUE),
    (83,'{ "en":"Gibraltar", "ar":"جبل طارق" }',"GI",350,TRUE),
    (84,'{ "en":"Greece", "ar":"اليونان" }',"GR",30,TRUE),
    (85,'{ "en":"Greenland", "ar":"جرينلاند" }',"GL",299,TRUE),
    (86,'{ "en":"Grenada", "ar":"جرينادا" }',"GD",1,TRUE),
    (87,'{ "en":"Guadeloupe", "ar":"جوادلوب" }',"GP",590,TRUE),
    (88,'{ "en":"Guam", "ar":"جوام" }',"GU",1,TRUE),
    (89,'{ "en":"Guatemala", "ar":"جواتيمالا" }',"GT",502,TRUE),
    (90,'{ "en":"Guinea", "ar":"غينيا" }',"GN",224,TRUE),
    (91,'{ "en":"Guinea-Bissau", "ar":"غينيا بيساو" }',"GW",245,TRUE),
    (92,'{ "en":"Guyana", "ar":"غيانا" }',"GY",592,TRUE),
    (93,'{ "en":"Haiti", "ar":"هايتي" }',"HT",509,TRUE),
    (94,'{ "en":"Heard Island and McDonald Islands", "ar":"جزيرة هيرد وماكدونالد" }',"HM",1,TRUE),
    (95,'{ "en":"Honduras", "ar":"هندوراس" }',"HN",504,TRUE),
    (96,'{ "en":"Hong Kong SAR China", "ar":"هونج كونج الصينية" }',"HK",852,TRUE),
    (97,'{ "en":"Hungary", "ar":"المجر" }',"HU",36,TRUE),
    (98,'{ "en":"Iceland", "ar":"أيسلندا" }',"IS",354,TRUE),
    (99,'{ "en":"India", "ar":"الهند" }',"IN",91,TRUE),
    (100,'{ "en":"Indonesia", "ar":"اندونيسيا" }',"ID",62,TRUE),
    (101,'{ "en":"Iran", "ar":"ايران" }',"IR",98,TRUE),
    (102,'{ "en":"Iraq", "ar":"العراق" }',"IQ",964,TRUE),
    (103,'{ "en":"Ireland", "ar":"أيرلندا" }',"IE",353,TRUE),
    (104,'{ "en":"Isle of Man", "ar":"جزيرة مان" }',"IM",1,TRUE),
    (106,'{ "en":"Italy", "ar":"ايطاليا" }',"IT",39,TRUE),
    (107,'{ "en":"Jamaica", "ar":"جامايكا" }',"JM",1,TRUE),
    (108,'{ "en":"Japan", "ar":"اليابان" }',"JP",81,TRUE),
    (109,'{ "en":"Jersey", "ar":"جيرسي" }',"JE",1,TRUE),
    (110,'{ "en":"Jordan", "ar":"الأردن" }',"JO",962,TRUE),
    (111,'{ "en":"Kazakhstan", "ar":"كازاخستان" }',"KZ",7,TRUE),
    (112,'{ "en":"Kenya", "ar":"كينيا" }',"KE",254,TRUE),
    (113,'{ "en":"Kiribati", "ar":"كيريباتي" }',"KI",686,TRUE),
    (114,'{ "en":"Kuwait", "ar":"الكويت" }',"KW",59,TRUE),
    (115,'{ "en":"Kyrgyzstan", "ar":"قرغيزستان" }',"KG",996,TRUE),
    (116,'{ "en":"Laos", "ar":"لاوس" }',"LA",856,TRUE),
    (117,'{ "en":"Latvia", "ar":"لاتفيا" }',"LV",371,TRUE),
    (118,'{ "en":"Lebanon", "ar":"لبنان" }',"LB",961,TRUE),
    (119,'{ "en":"Lesotho", "ar":"ليسوتو" }',"LS",266,TRUE),
    (120,'{ "en":"Liberia", "ar":"ليبيريا" }',"LR",231,TRUE),
    (121,'{ "en":"Libya", "ar":"ليبيا" }',"LY",218,TRUE),
    (122,'{ "en":"Liechtenstein", "ar":"ليختنشتاين" }',"LI",243,TRUE),
    (123,'{ "en":"Lithuania", "ar":"ليتوانيا" }',"LT",370,TRUE),
    (124,'{ "en":"Luxembourg", "ar":"لوكسمبورج" }',"LU",352,TRUE),
    (125,'{ "en":"Macau SAR China", "ar":"ماكاو الصينية" }',"MO",853,TRUE),
    (126,'{ "en":"Macedonia", "ar":"مقدونيا" }',"MK",389,TRUE),
    (127,'{ "en":"Madagascar", "ar":"مدغشقر" }',"MG",261,TRUE),
    (128,'{ "en":"Malawi", "ar":"ملاوي" }',"MW",265,TRUE),
    (129,'{ "en":"Malaysia", "ar":"ماليزيا" }',"MY",60,TRUE),
    (130,'{ "en":"Maldives", "ar":"جزر الملديف" }',"MV",960,TRUE),
    (131,'{ "en":"Mali", "ar":"مالي" }',"ML",223,TRUE),
    (132,'{ "en":"Malta", "ar":"مالطا" }',"MT",356,TRUE),
    (133,'{ "en":"Marshall Islands", "ar":"جزر المارشال" }',"MH",692,TRUE),
    (134,'{ "en":"Martinique", "ar":"مارتينيك" }',"MQ",596,TRUE),
    (135,'{ "en":"Mauritania", "ar":"موريتانيا" }',"MR",222,TRUE),
    (136,'{ "en":"Mauritius", "ar":"موريشيوس" }',"MU",230,TRUE),
    (137,'{ "en":"Mayotte", "ar":"مايوت" }',"YT",262,TRUE),
    (138,'{ "en":"Mexico", "ar":"المكسيك" }',"MX",52,TRUE),
    (139,'{ "en":"Micronesia", "ar":"ميكرونيزيا" }',"FM",691,TRUE),
    (140,'{ "en":"Moldova", "ar":"مولدافيا" }',"MD",373,TRUE),
    (141,'{ "en":"Monaco", "ar":"موناكو" }',"MC",377,TRUE),
    (142,'{ "en":"Mongolia", "ar":"منغوليا" }',"MN",976,TRUE),
    (143,'{ "en":"Montenegro", "ar":"الجبل الأسود" }',"ME",382,TRUE),
    (144,'{ "en":"Montserrat", "ar":"مونتسرات" }',"MS",1,TRUE),
    (145,'{ "en":"Morocco", "ar":"المغرب" }',"MA",212,TRUE),
    (146,'{ "en":"Mozambique", "ar":"موزمبيق" }',"MZ",258,TRUE),
    (147,'{ "en":"Myanmar [Burma]", "ar":"ميانمار" }',"MM",95,TRUE),
    (148,'{ "en":"Namibia", "ar":"ناميبيا" }',"NA",264,TRUE),
    (149,'{ "en":"Nauru", "ar":"نورو" }',"NR",674,TRUE),
    (150,'{ "en":"Nepal", "ar":"نيبال" }',"NP",977,TRUE),
    (151,'{ "en":"Netherlands", "ar":"هولندا" }',"NL",31,TRUE),
    (152,'{ "en":"Netherlands Antilles", "ar":"جزر الأنتيل الهولندية" }',"AN",599,TRUE),
    (153,'{ "en":"New Caledonia", "ar":"كاليدونيا الجديدة" }',"NC",687,TRUE),
    (154,'{ "en":"New Zealand", "ar":"نيوزيلاندا" }',"NZ",64,TRUE),
    (155,'{ "en":"Nicaragua", "ar":"نيكاراجوا" }',"NI",505,TRUE),
    (156,'{ "en":"Niger", "ar":"النيجر" }',"NE",227,TRUE),
    (157,'{ "en":"Nigeria", "ar":"نيجيريا" }',"NG",234,TRUE),
    (158,'{ "en":"Niue", "ar":"نيوي" }',"NU",683,TRUE),
    (159,'{ "en":"Norfolk Island", "ar":"جزيرة نورفوك" }',"NF",672,TRUE),
    (160,'{ "en":"North Korea", "ar":"كوريا الشمالية" }',"KP",850,TRUE),
    (161,'{ "en":"Northern Mariana Islands", "ar":"جزر ماريانا الشمالية" }',"MP",1,TRUE),
    (162,'{ "en":"Norway", "ar":"النرويج" }',"NO",47,TRUE),
    (163,'{ "en":"Oman", "ar":"عمان" }',"OM",968,TRUE),
    (164,'{ "en":"Pakistan", "ar":"باكستان" }',"PK",92,TRUE),
    (165,'{ "en":"Palau", "ar":"بالاو" }',"PW",680,TRUE),
    (166,'{ "en":"Palestinian Territories", "ar":"فلسطين" }',"PS",970,TRUE),
    (167,'{ "en":"Panama", "ar":"بنما" }',"PA",507,TRUE),
    (168,'{ "en":"Papua New Guinea", "ar":"بابوا غينيا الجديدة" }',"PG",675,TRUE),
    (169,'{ "en":"Paraguay", "ar":"باراجواي" }',"PY",595,TRUE),
    (170,'{ "en":"Peru", "ar":"بيرو" }',"PE",51,TRUE),
    (171,'{ "en":"Philippines", "ar":"الفيلبين" }',"PH",63,TRUE),
    (172,'{ "en":"Pitcairn Islands", "ar":"بتكايرن" }',"PN",870,TRUE),
    (173,'{ "en":"Poland", "ar":"بولندا" }',"PL",48,TRUE),
    (174,'{ "en":"Portugal", "ar":"البرتغال" }',"PT",351,TRUE),
    (175,'{ "en":"Puerto Rico", "ar":"بورتوريكو" }',"PR",1,TRUE),
    (176,'{ "en":"Qatar", "ar":"قطر" }',"QA",974,TRUE),
    (177,'{ "en":"Romania", "ar":"رومانيا" }',"RO",40,TRUE),
    (178,'{ "en":"Russia", "ar":"روسيا" }',"RU",7,TRUE),
    (179,'{ "en":"Rwanda", "ar":"رواندا" }',"RW",250,TRUE),
    (180,'{ "en":"Réunion", "ar":"روينيون" }',"RE",262,TRUE),
    (181,'{ "en":"Saint Helena", "ar":"سانت هيلنا" }',"SH",290,TRUE),
    (182,'{ "en":"Saint Kitts and Nevis", "ar":"سانت كيتس ونيفيس" }',"KN",1,TRUE),
    (183,'{ "en":"Saint Lucia", "ar":"سانت لوسيا" }',"LC",1,TRUE),
    (184,'{ "en":"Saint Martin", "ar":"سانت مارتين" }',"MF",1,TRUE),
    (185,'{ "en":"Saint Pierre and Miquelon", "ar":"سانت بيير وميكولون" }',"PM",508,TRUE),
    (186,'{ "en":"Saint Vincent and the Grenadines", "ar":"سانت فنسنت وغرنادين" }',"VC",1,TRUE),
    (187,'{ "en":"Samoa", "ar":"ساموا" }',"WS",685,TRUE),
    (188,'{ "en":"San Marino", "ar":"سان مارينو" }',"SM",378,TRUE),
    (189,'{ "en":"Saudi Arabia", "ar":"المملكة العربية السعودية" }',"SA",966,TRUE),
    (190,'{ "en":"Senegal", "ar":"السنغال" }',"SN",221,TRUE),
    (191,'{ "en":"Serbia", "ar":"صربيا" }',"RS",381,TRUE),
    (192,'{ "en":"Serbia and Montenegro", "ar":"صربيا والجبل الأسود" }',"CS",1,TRUE),
    (193,'{ "en":"Seychelles", "ar":"سيشل" }',"SC",248,TRUE),
    (194,'{ "en":"Sierra Leone", "ar":"سيراليون" }',"SL",232,TRUE),
    (195,'{ "en":"Singapore", "ar":"سنغافورة" }',"SG",65,TRUE),
    (196,'{ "en":"Slovakia", "ar":"سلوفاكيا" }',"SK",421,TRUE),
    (197,'{ "en":"Slovenia", "ar":"سلوفينيا" }',"SI",386,TRUE),
    (198,'{ "en":"Solomon Islands", "ar":"جزر سليمان" }',"SB",677,TRUE),
    (199,'{ "en":"Somalia", "ar":"الصومال" }',"SO",252,TRUE),
    (200,'{ "en":"South Africa", "ar":"جمهورية جنوب افريقيا" }',"ZA",27,TRUE),
    (201,'{ "en":"South Georgia and the South Sandwich Islands", "ar":"جورجيا الجنوبية وجزر ساندويتش الجنوبية" }',"GS",1,TRUE),
    (202,'{ "en":"South Korea", "ar":"كوريا الجنوبية" }',"KR",82,TRUE),
    (203,'{ "en":"Spain", "ar":"أسبانيا" }',"ES",34,TRUE),
    (204,'{ "en":"Sri Lanka", "ar":"سريلانكا" }',"LK",94,TRUE),
    (205,'{ "en":"Sudan", "ar":"السودان" }',"SD",249,TRUE),
    (206,'{ "en":"Suriname", "ar":"سورينام" }',"SR",597,TRUE),
    (207,'{ "en":"Svalbard and Jan Mayen", "ar":"سفالبارد وجان مايان" }',"SJ",47,TRUE),
    (208,'{ "en":"Swaziland", "ar":"سوازيلاند" }',"SZ",268,TRUE),
    (209,'{ "en":"Sweden", "ar":"السويد" }',"SE",46,TRUE),
    (210,'{ "en":"Switzerland", "ar":"سويسرا" }',"CH",41,TRUE),
    (211,'{ "en":"Syria", "ar":"سوريا" }',"SY",963,TRUE),
    (212,'{ "en":"São Tomé and Príncipe", "ar":"ساو تومي وبرينسيبي" }',"ST",1,TRUE),
    (213,'{ "en":"Taiwan", "ar":"تايوان" }',"TW",886,TRUE),
    (214,'{ "en":"Tajikistan", "ar":"طاجكستان" }',"TJ",992,TRUE),
    (215,'{ "en":"Tanzania", "ar":"تانزانيا" }',"TZ",255,TRUE),
    (216,'{ "en":"Thailand", "ar":"تايلند" }',"TH",66,TRUE),
    (217,'{ "en":"Timor-Leste", "ar":"تيمور الشرقية" }',"TL",670,TRUE),
    (218,'{ "en":"Togo", "ar":"توجو" }',"TG",228,TRUE),
    (219,'{ "en":"Tokelau", "ar":"توكيلو" }',"TK",690,TRUE),
    (220,'{ "en":"Tonga", "ar":"تونجا" }',"TO",676,TRUE),
    (221,'{ "en":"Trinidad and Tobago", "ar":"ترينيداد وتوباغو" }',"TT",1,TRUE),
    (222,'{ "en":"Tunisia", "ar":"تونس" }',"TN",216,TRUE),
    (223,'{ "en":"Turkey", "ar":"تركيا" }',"TR",90,TRUE),
    (224,'{ "en":"Turkmenistan", "ar":"تركمانستان" }',"TM",993,TRUE),
    (225,'{ "en":"Turks and Caicos Islands", "ar":"جزر الترك وجايكوس" }',"TC",1,TRUE),
    (226,'{ "en":"Tuvalu", "ar":"توفالو" }',"TV",688,TRUE),
    (227,'{ "en":"U.S. Minor Outlying Islands", "ar":"جزر الولايات المتحدة البعيدة الصغيرة" }',"UM",1,TRUE),
    (228,'{ "en":"U.S. Virgin Islands", "ar":"جزر فرجين الأمريكية" }',"VI",1,TRUE),
    (229,'{ "en":"Uganda", "ar":"أوغندا" }',"UG",256,TRUE),
    (230,'{ "en":"Ukraine", "ar":"أوكرانيا" }',"UA",380,TRUE),
    (231,'{ "en":"United Arab Emirates", "ar":"الامارات العربية المتحدة" }',"AE",971,TRUE),
    (232,'{ "en":"United Kingdom", "ar":"المملكة المتحدة" }',"GB",44,TRUE),
    (233,'{ "en":"United States", "ar":"الولايات المتحدة الأمريكية" }',"US",1,TRUE),
    (234,'{ "en":"Unknown or Invalid Region", "ar":"منطقة غير معرفة" }',"ZZ",1,TRUE),
    (235,'{ "en":"Uruguay", "ar":"أورجواي" }',"UY",598,TRUE),
    (236,'{ "en":"Uzbekistan", "ar":"أوزبكستان" }',"UZ",998,TRUE),
    (237,'{ "en":"Vanuatu", "ar":"فانواتو" }',"VU",678,TRUE),
    (238,'{ "en":"Vatican City", "ar":"الفاتيكان" }',"VA",379,TRUE),
    (239,'{ "en":"Venezuela", "ar":"فنزويلا" }',"VE",58,TRUE),
    (240,'{ "en":"Vietnam", "ar":"فيتنام" }',"VN",84,TRUE),
    (241,'{ "en":"Wallis and Futuna", "ar":"جزر والس وفوتونا" }',"WF",681,TRUE),
    (242,'{ "en":"Western Sahara", "ar":"الصحراء الغربية" }',"EH",212,TRUE),
    (243,'{ "en":"Yemen", "ar":"اليمن" }',"YE",967,TRUE),
    (244,'{ "en":"Zambia", "ar":"زامبيا" }',"ZM",260,TRUE),
    (245,'{ "en":"Zimbabwe", "ar":"زيمبابوي" }',"ZW",236,TRUE),
    (246,'{ "en":"Åland Islands", "ar":"جزر أولان" }',"AX",358,1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
