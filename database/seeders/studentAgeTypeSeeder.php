<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\studentAgeType;

class studentAgeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        studentAgeType::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'      => $id = 1,
                'context' => 'Adults Only',
            ],
            [
                'id'      => ++$id,
                'context' => 'Children Only',
            ],
            [
                'id'      => ++$id,
                'context' => 'Adults & Children',
            ],
        ]);
    }
}
