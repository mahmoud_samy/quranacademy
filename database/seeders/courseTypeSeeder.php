<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\courseType;

class courseTypeSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
        courseType::createOrUpdateArrayWithoutGlobalScopes([
			[
				'id'      => $id = 1    ,
				'context' => 'Tahfeeth' ,
			],
			[
				'id'      => ++ $id    ,
				'context' => 'Tajweed' ,
			],
			[
				'id'      => ++ $id ,
				'context' => 'both' ,
			],
		]);
	}
}