<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\studentGenderType;

class studentGenderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        studentGenderType::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'      => $id = 1,
                'context' => 'Female Only',
            ],
            [
                'id'      => ++$id,
                'context' => 'Male Only',
            ],
            [
                'id'      => ++$id,
                'context' => 'Male & Female',
            ],
        ]);
    }
}
