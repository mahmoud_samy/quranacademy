<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\faq;

class faqSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
        $faker = \Faker\Factory::create( );
		faq::createOrUpdateArrayWithoutGlobalScopes([
			[
				'id'     => $id    = 1                 ,
				'header' => '1- What is Quran Academy?' ,
				'body'   => 'Quran Academy is an application designed for learning the Quran by instructors with experience and proficiency in memorizing and reciting the Holy Quran.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '2- Who can use Quran Academy?' ,
				'body'   => 'Quran Academy is available to people of all ages and from all over the world.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '3- Can I memorize the Quran easily?' ,
				'body'   => 'Yes, the main purpose of Quran Academy is to help you memorize the whole Quran easily under the direct supervision of an instructor dedicated to you.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '4- Can I use Quran Academy to help me recite the Quran only?' ,
				'body'   => 'Yes, Quran Academy can help you learn proper recitation. You will learn perfectly to pronounce the Quran\'s words and verses (Tajweed & Tarteel).' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '5- How Can I Start?' ,
				'body'   => 'You can start by clicking on search, picking the instructor, and scheduling a suitable time for you. You can also use the filter for better results, and then you can pay with a Visa card.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '6- What is the duration of each class?' ,
				'body'   => 'Classes are divided between half and a quarter of an hour per class, according to your needs.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '7- Can I choose my instructor?' ,
				'body'   => 'Yes, you can choose your instructor by filter based on Gender, Nationality, Language, Course Type, Price Range, Duration, and Rating.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '8- Can I choose session timings?' ,
				'body'   => 'Yes, you can. Each tutor’s schedule is displayed on their profile page, and you can select any time for your session.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '9- What are the instructor\'s qualifications?' ,
				'body'   => 'All instructors have at least a Quran recitation certified "Sanad" or more than two years of experience in memorization. You can look over each instructor\'s profile before you book.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '10- Do you have female instructors on the application?' ,
				'body'   => 'Yes, we have instructors from all over the world who teach students. You can filter tutor profiles based on gender.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '11- What are the refund conditions?' ,
				'body'   => 'You can get your money back if you cancel your session within 3 hours of the start time or if the instructor does not attend the session.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '12- Is there a notice period required if I decide not to continue with a tutor?' ,
				'body'   => 'You can cancel the session 3 hours before it starts, or you can reschedule the time.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '13- How can I start?' ,
				'body'   => 'You can start by adding your available appointments on the application for a specific time and waiting until the reservation is confirmed, and a notification when there is a reservation.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '14- How can I receive my money?' ,
				'body'   => 'You can submit your bank account information via the Earnings button, and you will receive the amount assigned to you within 5-8 working days.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '15- How do I appear invisible to the students?' ,
				'body'   => 'You can make your account visible or invisible to students from your profile.' ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => '16- Where can I find out who applied to my session?' ,
				'body'   => 'When a student chooses you, a notification appears on your dashboard. To see this, you must be logged in. We also send you an email when you receive an invitation.' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '1- ما هو قران اكاديمي؟' ,
				'body'   => 'تطبيق قرآن اكاديمي يمكنك من التدريب على حفظ القرآن الكريم وتعلم التجويد و القراءات مع أفضل المحفظين الحاصلين على إجازات التحفيظ.' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '2- من يستطيع استخدام قران أكاديمي؟' ,
				'body'   => 'قرآن اكاديمي متاح لجميع الاعمار من كل بلاد العالم.' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '3- هل يمكنني الحفظ بسهولة؟ وكيف؟' ,
				'body'   => 'يمكنك القيام بذلك بكل سهولة عن طريق الادوات المتاحة خلال التطبيق و المتابعة المستمرة مع المحفظين.' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '4- هل هو تطبيق للحفظ فقط؟' ,
				'body'   => 'لا, قران اكاديمي تطبيق يمكنك من حفظ و تجويد القران الكريم بالاضافة الى تعلم القراءات.' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '5- كيف يمكنني البدء؟' ,
				'body'   => '.<p>يمكنك البدء كالتالي:</p>' .
					'<ol>' . 
						'<li>الضغط علي زر البحث<li>' . 
						'<li>اختيار المحفظ المناسب<li>' .
						'<li>الحجز من المواعيد المتاحة<li>' .
						'<li>الدفع عن طريق البطاقة البنكية<li>' .
						'<li>فتح التطبيق في ميعاد الحصة و الضغط علي زر الانضمام<li>' .
					'</ol>'
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '6- ما هي مدة الحصة؟' ,
				'body'   => '.هناك أكثر من اختيار لمدة الحصة حسب احتياجك و هي من ربع ساعة الي نصف ساعة الي ساعة' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '7- هل يمكنني اختيار المحفظ؟' ,
				'body'   => '.نعم يمكنك ذلك و يمكنك أيضا اختيار جنسه و جنسيته و السعر المناسب' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '8- هل يمكن اختيار التوقيت؟' ,
				'body'   => '.يمكنك اختيار من بين توقيتات متعددة لدى المحفظ' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '9- هل لديكم محفظات نساء علي التطبيق؟' ,
				'body'   => '.نعم يوجد العديد من المحفظات علي التطبيق للاختيار من بينهم' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '10- ما هي مؤهلات المحفظين و المحفظات؟' ,
				'body'   => '.كل المحفظين و المحفظات علي التطبيق لديهم اجازات في تحفيظ القران بالاضافة الى عدد من سنين الخبرة' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '11- هل يمكنني الغاء الحصة و استرداد الاموال؟' ,
				'body'   => '.نعم يمكنك ذلك في أي وقت ماعدا 3 ساعات من بداية الحصة حينها لا يمكنك استرداد المبلغ و لا يمكن الالغاء او تغيير الميعاد' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '12- هل هناك مدة معينة لا يسمح فيها بالغاء الحجز؟' ,
				'body'   => '.نعم, لا يمكنك الغاء الحجز او تغيير الميعاد في خلال 3 ساعات من قبل موعد الحصة' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '13- كيف يمكنني البدء؟' ,
				'body'   => '.<p>يمكنك البدء عن طريق الخطوات الاتية:</p>' .
					'<ol>' . 
						'<li>الضغط على زر + في اسفل منتصف الشاشة<li>' . 
						'<li>اختيار نوع الحصة و المعاد المناسب لك<li>' .
						'<li>يمكنك تكرار هذا الميعاد لمدة أقصاها 8 أسابيع<li>' .
						'<li>عندما يتم حجز احد المواعيد سيتم إرسال إشعار بتفاصيل الحجز<li>' .
					'</ol>'
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '14- ?كيف يمكنني استلام اموالي' ,
				'body'   => '.عن طريق ادراج بيانات حسابك البنكي في الجزء الخاص بالارباح في التطبيق وسيتم تحويل الاموال الى حسابك البنكي في خلال 5-8 أيام عمل' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '15- ?هل يمكنني التوقف عن الظهور للطلبة' ,
				'body'   => '.نعم يمكنك ان تخفي نفسك من الظهور و الحجز عن طريق الزر الخاص بالظهور في الصفحة الشخصية الخاصة بك' ,
			],
			[
				'id'     => ++ $id                     ,
				'lang'   => 'ar'                     ,
				'header' => '16- أين يمكنني ان أجد الحصة المحجوزة علي التطبيق؟' ,
				'body'   => '.يمكنك ان تجد الحصص في الزر الخاص ب (حصصي) في التطبيق وستجد الحصص القديمة و الحصص القادمة' ,
			],
		]);
    }
}
