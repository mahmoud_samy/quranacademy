<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\imageType;

class ImageTypeSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
		imageType::createOrUpdateArrayWithoutGlobalScopes([
			[
				'id'         => 1            ,
				'mime_type'  => 'image/jpeg' ,
				'extension'  => 'jpg'        ,
				'name'       => 'jpg'        ,
				'is_active'  => 1            ,
				'created_at' => now( )       ,
				'updated_at' => now( )       ,
			],
			[
				'id'         => 2            ,
				'mime_type'  => 'image/png' ,
				'extension'  => 'png'       ,
				'name'       => 'png'       ,
				'is_active'  => 1           ,
				'created_at' => now( )      ,
				'updated_at' => now( )      ,
			],
		]);
    }
}
