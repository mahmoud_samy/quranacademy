<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\term;

class TermSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run( ) {
        $faker = \Faker\Factory::create( );
		term::createOrUpdateArrayWithoutGlobalScopes([
			[
				'id'     => $id    = 1                 ,
				'header' => $faker -> realText ( 191 ) ,
				'body'   => $faker -> realText (     ) ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => $faker -> realText ( 191 ) ,
				'body'   => $faker -> realText (     ) ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => $faker -> realText ( 191 ) ,
				'body'   => $faker -> realText (     ) ,
			],
			[
				'id'     => ++ $id                     ,
				'header' => $faker -> realText ( 191 ) ,
				'body'   => $faker -> realText (     ) ,
			],
		]);
    }
}
