<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\privacyPolicy;

class PrivacyPolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        privacyPolicy::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'     => $id    = 1,
                'header' => $faker->realText(191),
                'body'   => $faker->realText(),
            ],
            [
                'id'     => ++$id,
                'header' => null,
                'body'   => $faker->realText(),
            ],
            [
                'id'     => ++$id,
                'header' => $faker->realText(191),
                'body'    => '<li><p dir="rtl" style="margin-top: 0.19in; margin-bottom: 0in; line-height: 100%" align="right"><a href="#" target="_blank"><font color="#0000ff"><u><font face="Times New Roman, serif"><font style="font-size: 12pt" size="3">test html tags</font></font></u></font></a></p> </li><li><p dir="rtl" style="margin-bottom: 0.19in; line-height: 100%" align="right"><a href="#" target="_blank"><font color="#0000ff"><font face="Times New Roman, serif"><font style="font-size: 12pt" size="3"><u>test html tags</u></font></font></font></a></p></li>',
            ],
            [
                'id'     => ++$id,
                'header' => $faker->realText(191),
                'body'   => $faker->realText(),
            ],
        ]);
    }
}
