<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\variabledb;

class variabledbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        variabledb::createOrUpdateArrayWithoutGlobalScopes([
            [
                'id'         => $id = 1,
                'key'        => 'TIME_EXPIRE_ACCESS_TOKEN',
                'value'      => 15,
                'note'       => 'time expire access token',
                'forAdmin'   => 1,
            ],
            [
                'id'         => ++$id,
                'key'        => 'TIME_EXPIRE_REFRESH_TOKEN',
                'value'      => 30,
                'note'       => 'time expire refresh token',
                'forAdmin'   => 1,
            ],
            [
                'id'         => ++$id,
                'key'        => 'EXPIRE_PIN_CODE_TOKEN_BY_MINUTES',
                'value'      => 3,
                'note'       => 'expire pin code token by minutes',
                'forAdmin'   => 1,
            ],
            [
                'id'         => ++$id,
                'key'        => 'LENGTH_PIN_CODE_TOKEN',
                'value'      => 5,
                'note'       => 'length pin code token',
                'forAdmin'   => 1,
            ],
            [
                'id'         => ++$id,
                'key'        => 'IF_ADMIN_NOT_NEED_APPROVE_INSTRUCTORST',
                'value'      => 1,
                'note'       => 'IF_ADMIN_NOT_NEED_APPROVE_INSTRUCTORST',
                'forAdmin'   => 1,
            ],
            [
                'id'         => ++$id,
                'key'        => 'TIME_CAN_SESSIONS_EDIT_WITHIN_BY_HOURS',
                'value'      => 3,
                'note'       => 'TIME_CAN_SESSIONS_EDIT_WITHIN_BY_HOURS',
                'forAdmin'   => 0,
            ],
        ]);
    }
}
