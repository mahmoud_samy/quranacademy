<?php

namespace Database\Factories;

use App\Models\SubInstructor     ;
use App\Models\courseType        ;
use App\Models\studentGenderType ;
use App\Models\studentAgeType    ;
use App\Models\currencyType      ;
use App\Models\image             ;
use App\Models\languageType      ;

class SubInstructorFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubInstructor::class ;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'first_name'              => $this -> faker -> unique( ) -> name                                    ,
            'last_name'               => $this -> faker -> unique( ) -> name                                    ,
            'email'                   => rand( 0 , 999999999999999 ) . $this -> faker -> unique( ) -> safeEmail ,
            'Locale'                  => 'en'                                                           ,
            'gender'                  => $this -> faker -> boolean( )                                           ,
            'date_of_birth'           => now( )                                                         ,
            'is_active'               => 1                                                              ,
            'is_blocked'              => 0                                                              ,
            'deleted_at'              => null                                                           ,
            'description'             => $this -> faker -> unique( ) -> name                                    ,
            'created_at'              => now( )                                                         ,
            'updated_at'              => now( )                                                         ,
            'password'                => '$2y$04$2hQ6qx9/olDScTBquglOy.AQOtdcqGmDGPqjnADQmRp3bpK3Sghnm' , 
            // password1D
            'years_of_experience'     => $this -> faker -> unique( ) -> randomNumber                            ,
            'description'             => $this -> faker -> unique( ) -> text                                    ,
        ];
    }

    public function configure( ) {
        return $this -> afterCreating( function ( SubInstructor $SubInstructor ) {
            $SubInstructor -> update_Spoken_Language_User( [
                languageType::RandemOne( ) -> id , 
                languageType::RandemOne( ) -> id , 
                languageType::RandemOne( ) -> id , 
            ]);
        });
    }
}