<?php

namespace Database\Factories;

use App\Models\image;
use App\Models\instructor;

class imageFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = image::class ;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'uplouder_type' => instructor::class ,
            'uplouder_id'   => 1                 ,
            'image_type_id' => 1                 ,
        ];
    }
}