<?php

namespace Database\Factories;

use App\Models\instructor;
use App\Models\courseType;
use App\Models\studentGenderType;
use App\Models\studentAgeType;
use App\Models\currencyType;
use App\Models\image;
use App\Models\File;
use App\Models\Country;
use App\Models\languageType;

use Illuminate\Http\UploadedFile;

class instructorFactory extends factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = instructor::class;

    public function definition(): array
    {
        return [
            'gender'                  => $this->faker->boolean(),
            'first_name'              => $this->faker->unique()->name,
            'last_name'               => $this->faker->unique()->name,
            'description'             => $this->faker->unique()->name,
            'email'                   => $this->faker->unique()->safeEmail . rand(0, 99999),
            'date_of_birth'           => now(),
            'is_Approval'             => 1,
            'password'                => '$2y$04$2hQ6qx9/olDScTBquglOy.AQOtdcqGmDGPqjnADQmRp3bpK3Sghnm', // password1D
            'years_of_experience'     => $this->faker->randomNumber(),
            'description'             => $this->faker->text(),
            'is_profile_public'       => $this->faker->boolean(),
            'rate_15_min'             => $this->faker->numberBetween(5, 10),
            'rate_30_min'             => $this->faker->numberBetween(15, 20),
            'rate_60_min'             => $this->faker->numberBetween(25, 30),
            'country_id'              => Country::RandemOne(),
            'courses_types_id'        => courseType::RandemOne(),
            'currency_type_id'        => currencyType::RandemOne(),
            'student_age_types_id'    => studentAgeType::RandemOne(),
            'student_gender_types_id' => studentGenderType::RandemOne(),
            'profile_image_id'        => image::factory()->create(),
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (instructor $instructor) {
            $instructor->profile_vidoe_id = File::createFromFile(UploadedFile::FromPath('/app/public/logo'))->id;
            $instructor->update_Spoken_Language_User([
                languageType::RandemOne()->id,
                languageType::RandemOne()->id
            ]);
            $instructor->save();
        });
    }
}
