<?php

namespace Database\Factories;

use App\Models\rating;
use App\Models\session;
use App\Models\student;

class ratingFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = rating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition( ){
        $student = student :: factory ( ) -> create ( ) ;
        $session = session :: factory ( ) -> create ( [ 'student_id' => $student -> id ] ) ;
        return [
            'session_id'    => $session -> id                                    ,
            'instructor_id' => $session -> instructor -> id                      ,
            'student_id'    => $session -> student    -> id                      ,
            'rating'        => $this    -> faker      -> numberBetween ( 1 , 5 ) ,
            'reviews'       => $this    -> faker      -> text          (  191  ) ,
        ];
    }
}
