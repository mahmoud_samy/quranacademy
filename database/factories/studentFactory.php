<?php

namespace Database\Factories;

use App\Models\student;

class studentFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = student::class ;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        return [
            'first_name'    => $this -> faker -> unique( ) -> name                                    ,
            'last_name'     => $this -> faker -> unique( ) -> name                                    ,
            'email'         => rand( 0 , 999999999999999 ) . $this -> faker -> unique( ) -> safeEmail ,
            'Locale'        => 'en'                                                           ,
            'is_active'     => 1                                                              ,
            'gender'        => $this -> faker -> boolean                                              ,
            'is_blocked'    => 0                                                              ,
            'deleted_at'    => null                                                           ,
            'date_of_birth' => now( )                                                         ,
            'created_at'    => now( )                                                         ,
            'updated_at'    => now( )                                                         ,
            'password'      => '$2y$04$2hQ6qx9/olDScTBquglOy.AQOtdcqGmDGPqjnADQmRp3bpK3Sghnm' , 
            // password1D
        ];
    }
}