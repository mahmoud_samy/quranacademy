<?php

namespace Database\Factories;

use App\Models\session_message;
use App\Models\instructor;
use App\Models\student;
use App\Models\session;

class session_messageFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = session_message::class ;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $session = session :: factory ( ) -> create ( [ 'student_id' =>  student::factory ( ) -> create ( ) ] );
        $boolean = $this -> faker -> boolean( );
        return [
            'session_id'    =>   $session                                 ,
            'instructor_id' =>   $boolean ? $session -> instructor : null ,
            'student_id'    => ! $boolean ? $session -> student    : null ,
            'message'       =>   $this -> faker -> unique( ) -> text( 190 )
        ];
    }
}
