<?php

namespace Database\Factories;

use App\Models\session;
use App\Models\courseType;
use App\Models\rateMinutesType;
use App\Models\instructor;

use Illuminate\Http\UploadedFile;

class sessionFactory extends factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = session::class ;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $instructor           = instructor ::factory ( ) -> create ( ) ;
        $rate_minutes_type_id = rateMinutesType::RandemOne( )                ;
        return [
            'instructor_id'        => $instructor -> id                            ,
            'course_type_id'       => courseType:: RandemOne( ) -> id              ,
            'rate_minutes_type_id' => $rate_minutes_type_id -> id                  ,
            'date'                 => now( ) -> addDays( 1 ) -> format( 'Y-m-d' )  ,
            'time'                 => now( ) -> addDays( 1 ) -> format( 'H:i:s' )  ,
            'rate_salary'          => $instructor[ $rate_minutes_type_id -> attr ] ,
            'currency_type_id'     => $instructor -> currency_type_id              ,
        ];
    }

    public function configure( ) {
        return $this -> afterCreating( function ( session $session ) {
            $session -> addMaterial ( UploadedFile::FromPath ( '/app/public/logo' ) );
            $session -> addMaterial ( UploadedFile::FromPath ( '/app/public/logo' ) );
        });
    }
}