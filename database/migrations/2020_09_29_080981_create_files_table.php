<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'files' , function ( Blueprint $table ) {
            $table -> id        (                 )                                                                                                                  ;
			$table -> string    ( 'type'          )                                                                                                                  ;
			$table -> integer   ( 'size'          ) -> default( 0 )                                                                                                  ;
			$table -> boolean   ( 'is_active'     ) -> default( 1 )                                                                                                  ;
			$table -> timestamp ( 'created_at'    ) -> nullable ( ) -> useCurrent( )                                                                                 ;
			$table -> timestamp ( 'updated_at'    ) -> nullable ( ) -> useCurrent( )                                                                                 ;
			$table -> foreignId ( 'instructor_id' ) -> nullable ( ) -> references( 'id' ) -> on( 'instructors' ) -> constrained( ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'files' );
	}
}
