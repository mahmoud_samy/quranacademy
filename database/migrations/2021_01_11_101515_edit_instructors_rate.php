<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditInstructorsRate extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'instructors' , function ( Blueprint $table ) {
            $table -> bigInteger ( 'rate_15_min' ) -> nullable( ) -> change( ) ;
            $table -> bigInteger ( 'rate_30_min' ) -> nullable( ) -> change( ) ;
            $table -> bigInteger ( 'rate_60_min' ) -> nullable( ) -> change( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::table( 'instructors' , function ( Blueprint $table ) {
            $table -> bigInteger ( 'rate_15_min' ) -> change( ) ;
            $table -> bigInteger ( 'rate_30_min' ) -> change( ) ;
            $table -> bigInteger ( 'rate_60_min' ) -> change( ) ;
        });
    }
}
