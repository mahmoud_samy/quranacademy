<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditOauthAccessTokensFirebaseId extends Migration {

    public function up( ) {
        Schema::table( 'oauth_access_tokens' , function ( Blueprint $table ) {
		    $table -> string( 'firebaseId' ) -> nullable( ) ;
        });
    }

    public function down( ) {
        Schema::table( 'oauth_access_tokens' , function ( Blueprint $table ) {
			$table -> dropColumn ( 'firebaseId' ) ;
        });
    }
}
