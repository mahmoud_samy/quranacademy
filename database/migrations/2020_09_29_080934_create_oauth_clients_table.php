<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthClientsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'oauth_clients' , function ( Blueprint $table ) {
			$table -> id        (                          )                                  ;
			$table -> foreignId ( 'user_id'                ) -> nullable( ) -> index( )       ;
			$table -> string    ( 'name'                   )                                  ;
			$table -> string    ( 'secret' , 100           ) -> nullable( )                   ;
            $table -> string    ( 'provider'               ) -> nullable( )                   ;
			$table -> text      ( 'redirect'               )                                  ;
			$table -> boolean   ( 'personal_access_client' )                                  ;
			$table -> boolean   ( 'password_client'        )                                  ;
			$table -> boolean   ( 'revoked'                )                                  ;
			$table -> timestamp ( 'created_at'             ) -> nullable ( ) -> useCurrent( ) ;
			$table -> timestamp ( 'updated_at'             ) -> nullable ( ) -> useCurrent( ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'oauth_clients' );
	}
}
