<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveProfileImageConstraintFromSubInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_instructors', function (Blueprint $table) {
            $table->dropForeign(['profile_image_id']);
            $table->foreignId('profile_image_id')
                ->nullable()
                ->change()
                ->references('id')
                ->on('images')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_instructors', function (Blueprint $table) {
            $table->dropForeign(['profile_image_id']);
            $table->foreignId('profile_image_id')
                ->change()
                ->references('id')
                ->on('images')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
}
