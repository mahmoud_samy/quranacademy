<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditSeenUsers extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'session_messages' , function ( Blueprint $table ) {
            $table -> timestamp ( 'seen_at' ) -> nullable ( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'session_messages' , function ( Blueprint $table ) {
			$table -> dropColumn ( 'seen_at' ) ;
        });
    }
}
