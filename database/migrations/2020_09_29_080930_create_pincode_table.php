<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePincodeTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'pincodes' , function ( Blueprint $table ) {
			$table -> string    ( 'email'      ) -> unique(	) -> index    ( )                  ;
			$table -> text      ( 'token'      ) ->              nullable ( )                  ;
			$table -> timestamp ( 'created_at' ) ->              nullable ( ) -> useCurrent( ) ;
			$table -> timestamp ( 'updated_at' ) ->              nullable ( ) -> useCurrent( ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'pincodes' );
	}
}
