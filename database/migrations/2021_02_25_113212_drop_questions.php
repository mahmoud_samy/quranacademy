<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::dropIfExists( 'questions' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::create( 'questions' , function ( Blueprint $table ) {
            $table -> id         (                 )                 ;
            $table -> string     ( 'title'         )                 ;
            $table -> boolean    ( 'is_active'     ) -> default( 1 ) ;
            $table -> timestamps (                 )                 ;
            $table -> foreignId  ( 'instructor_id' ) -> nullable ( ) -> references( 'id' ) -> on( 'instructors' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId  ( 'student_id'    ) -> nullable ( ) -> references( 'id' ) -> on( 'students'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId  ( 'session_id'    ) -> nullable ( ) -> references( 'id' ) -> on( 'sessions'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
        });
    }
}
