<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'notifications' , function ( Blueprint $table ) {
            $table -> uuid        ( 'id'           ) -> primary( )                    ;
            $table -> string      ( 'type'         )                                  ;
            $table -> morphs      ( 'notifiable'   )                                  ;
            $table -> morphs      ( 'related_with' )                                  ;
            $table -> text        ( 'data'         )                                  ;
            $table -> timestamp   ( 'read_at'      ) -> nullable( )                   ;
            $table -> boolean     ( 'is_active'    ) -> default ( 1 )                 ;
            $table -> softDeletes ( 'deleted_at'   )                                  ;
            $table -> timestamp   ( 'created_at'   ) -> nullable ( ) -> useCurrent( ) ;
            $table -> timestamp   ( 'updated_at'   ) -> nullable ( ) -> useCurrent( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'notifications' );
    }
}
