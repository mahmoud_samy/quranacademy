<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'certificates' , function ( Blueprint $table ) {
            $table -> id        (                 )                                                                                                                  ;
            $table -> boolean   ( 'is_active'     ) -> default( 1 )                                                                                                  ;
            $table -> timestamp ( 'created_at'    ) -> nullable ( ) -> useCurrent( )                                                                                 ;
            $table -> timestamp ( 'updated_at'    ) -> nullable ( ) -> useCurrent( )                                                                                 ;
            $table -> foreignId ( 'instructor_id' ) -> references( 'id' ) -> on( 'instructors' ) -> constrained( ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId ( 'file_id'       ) -> references( 'id' ) -> on( 'files'       ) -> constrained( ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'certificates' );
	}
}
