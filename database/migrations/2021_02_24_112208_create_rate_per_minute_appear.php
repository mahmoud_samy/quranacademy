<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatePerMinuteAppear extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'sub_instructors' , function ( Blueprint $table ) {
            $table -> foreignId ( 'public_rate_minutes_type_id' ) -> nullable( ) -> references( 'id' ) -> on( 'rate_minutes_types' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
        });
        Schema::table( 'instructors' , function ( Blueprint $table ) {
            $table -> foreignId ( 'public_rate_minutes_type_id' ) -> nullable( ) -> references( 'id' ) -> on( 'rate_minutes_types' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::table( 'sub_instructors' , function ( Blueprint $table ) {
            $table -> dropForeign ( 'sub_instructors_public_rate_minutes_type_id_foreign' ) ;
            $table -> dropColumn  ( 'public_rate_minutes_type_id'                         ) ;
        });
        Schema::table( 'instructors' , function ( Blueprint $table ) {
            $table -> dropForeign ( 'instructors_public_rate_minutes_type_id_foreign' ) ;
            $table -> dropColumn  ( 'public_rate_minutes_type_id'                     ) ;
        });
    }
}
