<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditFilesToable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'files' , function ( Blueprint $table ) {
            $table -> nullableMorphs ( 'to' ) ;
            $table -> string ( 'extension' ) -> nullable ( ) ;
            $table -> string ( 'mime_type' ) -> nullable ( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::table( 'files' , function ( Blueprint $table ) {
            $table -> dropMorphs (   'to'                        ) ;
			$table -> dropColumn ( [ 'extension' , 'mime_type' ] ) ;
        });
    }
}
