<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->boolean('visibility');
            $table->integer('package_id');
            $table->integer('instructor_level_id')->nullable();
            $table->string('title');
            $table->string('name');
            $table->integer('hours_limitation');
            $table->float('price');
            $table->float('discount')->default(0);
            $table->float('net_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
