<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpokenLanguageUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'spoken_language_users' , function ( Blueprint $table ) {
			$table -> id        (                     )                                                                     ;
			$table -> foreignId ( 'language_types_id' ) -> constrained( ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
			$table -> morphs    ( 'spokenable'        )                                                                     ;
			$table -> boolean   ( 'is_active'         ) -> default( 1 )                                                     ;
			$table -> timestamp ( 'created_at'        ) -> nullable ( ) -> useCurrent( )                                    ;
			$table -> timestamp ( 'updated_at'        ) -> nullable ( ) -> useCurrent( )                                    ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'spoken_language_users' );
	}
}
