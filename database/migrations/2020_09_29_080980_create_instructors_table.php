<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructors', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('Locale')->default('en');
            $table->boolean('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('password')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('is_blocked')->default(0);
            $table->softDeletes('deleted_at');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->bigInteger('years_of_experience');
            $table->text('description');
            $table->boolean('is_Approval')->default(0);
            $table->boolean('is_profile_public');
            $table->bigInteger('rate_15_min');
            $table->bigInteger('rate_30_min');
            $table->bigInteger('rate_60_min');
            $table->foreignId('profile_vidoe_id')->nullable();
            $table->foreignId('profile_image_id')->references('id')->on('images')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('courses_types_id')->references('id')->on('course_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('student_gender_types_id')->references('id')->on('student_gender_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('student_age_types_id')->references('id')->on('student_age_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('currency_type_id')->references('id')->on('currency_types')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructors');
    }
}
