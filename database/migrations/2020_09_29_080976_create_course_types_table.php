<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTypesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ){
		Schema::create( 'course_types' , function ( Blueprint $table ) {
            $table -> id        (              )                                  ;
			$table -> string    ( 'context'    )                                  ;
			$table -> boolean   ( 'is_active'  ) -> default( 1 )                  ;
			$table -> timestamp ( 'created_at' ) -> nullable ( ) -> useCurrent( ) ;
			$table -> timestamp ( 'updated_at' ) -> nullable ( ) -> useCurrent( ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'course_types' );
	}
}
