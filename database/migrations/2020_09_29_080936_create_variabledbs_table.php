<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariabledbsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ) {
		Schema::create( 'variabledbs' , function ( Blueprint $table ) {
			$table -> id          (              )                                  ;
			$table -> string      ( 'key'        )                                  ;
			$table -> string      ( 'value'      )                                  ;
			$table -> string      ( 'note'       ) -> nullable( )                   ;
			$table -> boolean     ( 'forAdmin'   ) -> default( 1 )                  ;
			$table -> boolean     ( 'is_active'  ) -> default ( 1 )                 ;
			$table -> softDeletes ( 'deleted_at' )                                  ;
			$table -> timestamp   ( 'created_at' ) -> nullable ( ) -> useCurrent( ) ;
			$table -> timestamp   ( 'updated_at' ) -> nullable ( ) -> useCurrent( ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'variabledbs' );
	}
}
