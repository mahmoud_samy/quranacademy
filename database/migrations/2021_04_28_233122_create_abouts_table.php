<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'abouts' , function ( Blueprint $table ) {
            $table -> id         (              )                                    ;
            $table -> string     ( 'header'     ) -> nullable( )                     ;
            $table -> text       ( 'body'       )                                    ;
            $table -> string     ( 'lang'       ) -> nullable ( ) -> default( 'en' ) ;
            $table -> boolean    ( 'is_active'  )                 -> default( 1    ) ;
            $table -> boolean    ( 'is_visible' )                 -> default( 1    ) ;
            $table -> timestamps (              )                                    ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'abouts' );
    }
}
