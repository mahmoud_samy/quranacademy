<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropQuestionReplays extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::dropIfExists( 'question_replays' );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::create( 'question_replays' , function ( Blueprint $table ) {
            $table -> id         (                 )                 ;
            $table -> string     ( 'replay'        )                 ;
            $table -> boolean    ( 'is_active'     ) -> default( 1 ) ;
            $table -> timestamps (                 )                 ;
            $table -> foreignId  ( 'instructor_id' ) -> nullable ( ) -> references( 'id' ) -> on( 'instructors' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId  ( 'student_id'    ) -> nullable ( ) -> references( 'id' ) -> on( 'students'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId  ( 'question_id'   ) -> nullable ( ) -> references( 'id' ) -> on( 'questions'   ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
        });
    }
}
