<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'currency_types' , function ( Blueprint $table ) {
            $table -> id        (                 )                  ;
			$table -> string    ( 'code'          )                  ;
			$table -> string    ( 'symbol'        )                  ;
			$table -> string    ( 'symbol_native' )                  ;
			$table -> boolean   ( 'is_active'     ) -> default( 1 )  ;
			$table -> timestamp ( 'created_at'    ) -> useCurrent( ) ;
			$table -> timestamp ( 'updated_at'    ) -> useCurrent( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'currency_types' );
    }
}
