<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('instructor_id')->references('id')->on('instructors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('rate_minutes_type_id')->references('id')->on('rate_minutes_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('currency_type_id')->references('id')->on('currency_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('course_type_id')->references('id')->on('course_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('student_id')->nullable()->references('id')->on('students')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('rate_salary')->default(0)->nullable();
            $table->boolean('is_active')->default(1);
            $table->longText('options');
            $table->date('date')->nullable()->useCurrent();
            $table->time('time')->nullable()->useCurrent();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
