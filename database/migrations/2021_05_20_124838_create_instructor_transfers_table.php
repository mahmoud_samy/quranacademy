<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;

class CreateInstructorTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor_transfers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('instructor_id')->nullable()->references('id')->on('instructors')->onDelete('cascade')->onUpdate('cascade');
            $table->json('quotes')->nullable();
            $table->json('accounts')->nullable();
            $table->json('transfers')->nullable();
            $table->json('fund')->nullable();
            $table->unsignedInteger('amount');
            $table->string('status');
            $table->boolean('is_active')->default(1);
            $table->timestamp('estimatedDeliveryDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructor_transfers');
    }
}
