<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveProfileImageConstraintFormStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign(['profile_image_id']);
            $table->foreign('profile_image_id')->references('id')->on('images')->onDelete('set null')->onUpdate('cascade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign(['profile_image_id']);
            $table->foreign('profile_image_id')->nullable()->references('id')->on('images')->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
