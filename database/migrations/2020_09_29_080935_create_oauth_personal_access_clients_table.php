<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthPersonalAccessClientsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'oauth_personal_access_clients' , function ( Blueprint $table ) {
            $table -> id        (              )                                  ;
            $table -> foreignId ( 'client_id'  )                                  ;
            $table -> timestamp ( 'created_at' ) -> nullable ( ) -> useCurrent( ) ;
            $table -> timestamp ( 'updated_at' ) -> nullable ( ) -> useCurrent( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'oauth_personal_access_clients' );
    }
}
