<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditSessionsDeleteFor extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'sessions' , function ( Blueprint $table ) {
            $table -> timestamp ( 'delete_for_instructors' ) -> nullable ( ) ;
            $table -> timestamp ( 'delete_for_students'    ) -> nullable ( ) ;
            $table -> timestamp ( 'cancel_at'              ) -> nullable ( ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::table( 'sessions' , function ( Blueprint $table ) {
			$table -> dropColumn ( 'delete_for_instructors' , 'delete_for_students' , 'cancel_at' ) ;
        });
    }
}
