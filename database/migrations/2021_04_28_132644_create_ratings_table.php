<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'ratings' , function ( Blueprint $table ) {
            $table -> id                  (                  )                                                                                                                  ;
            $table -> foreignId           ( 'instructor_id'  ) -> nullable (   ) -> references( 'id' ) -> on( 'instructors' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId           ( 'student_id'     ) -> nullable (   ) -> references( 'id' ) -> on( 'students'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId           ( 'session_id'     ) -> nullable (   ) -> references( 'id' ) -> on( 'sessions'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> string              ( 'reviews'        ) -> nullable (   )                                                                                                ;
            $table -> unsignedTinyInteger ( 'rating'         )                                                                                                                  ;
            $table -> boolean             ( 'is_active'      ) -> default  ( 1 )                                                                                                ;
            $table -> timestamps          (                  )                                                                                                                  ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'ratings' );
    }
}
