<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUserForCountries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->foreignId('country_id')->nullable()->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('sub_instructors', function (Blueprint $table) {
            $table->foreignId('country_id')->nullable()->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('instructors', function (Blueprint $table) {
            $table->foreignId('country_id')->nullable()->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_country_id_foreign');
            $table->dropColumn('country_id');
        });
        Schema::table('sub_instructors', function (Blueprint $table) {
            $table->dropForeign('sub_instructors_country_id_foreign');
            $table->dropColumn('country_id');
        });
        Schema::table('instructors', function (Blueprint $table) {
            $table->dropForeign('instructors_country_id_foreign');
            $table->dropColumn('country_id');
        });
    }
}
