<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditNotifySession extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::table( 'sessions' , function ( Blueprint $table ) {
            $table -> boolean ( 'one_hour_before_notify'     ) -> default( 0 ) ;
            $table -> boolean ( 'five_Minutes_before_notify' ) -> default( 0 ) ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::table( 'sessions' , function ( Blueprint $table ) {
            $table -> dropColumn ( 'one_hour_before_notify' , 'five_Minutes_before_notify' ) ;
        });
    }
}
