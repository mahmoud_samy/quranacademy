<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageTypesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up( ){
		Schema::create( 'image_types' , function ( Blueprint $table ) {
            $table -> id        (              )                                  ;
			$table -> string    ( 'mime_type'  )                                  ;
			$table -> string    ( 'extension'  )                                  ;
			$table -> string    ( 'name'       )                                  ;
			$table -> boolean   ( 'is_active'  ) -> default( 1 )                  ;
			$table -> timestamp ( 'created_at' ) -> nullable ( ) -> useCurrent( ) ;
			$table -> timestamp ( 'updated_at' ) -> nullable ( ) -> useCurrent( ) ;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down( ) {
		Schema::dropIfExists( 'image_types' );
	}
}
