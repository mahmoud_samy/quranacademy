<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_access_tokens', function (Blueprint $table) {
            $table->string('id', 100)->primary();
            $table->foreignId('user_id')->index()->nullable();
            $table->foreignId('client_id');
            $table->string('name')->nullable();
            $table->text('scopes')->nullable();
            $table->boolean('revoked');
            $table->dateTime('expires_at')->nullable();
            // $table -> ipAddress  ( 'ip'                 ) -> nullable( )             ;
            // $table -> string     ( 'firebaseId'         ) -> nullable( )             ;
            // $table -> string     ( 'device'             ) -> nullable( )             ;
            // $table -> string     ( 'browser'            ) -> nullable( )             ;
            // $table -> string     ( 'operatingSystem'    ) -> nullable( )             ;
            // $table -> decimal    ( 'Latitude'  , 10 , 7 ) -> nullable( )             ;
            // $table -> decimal    ( 'Longitude' , 10 , 7 ) -> nullable( )             ;
            // $table -> string     ( 'cityName'           ) -> nullable( )             ;
            // $table -> string     ( 'countryName'        ) -> nullable( )             ;
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_access_tokens');
    }
}
