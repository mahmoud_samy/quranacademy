<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstructorViewsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
        Schema::create( 'instructor_views' , function ( Blueprint $table ) {
            $table -> id        ( );
            $table -> foreignId ( 'instructor_id' ) -> references ( 'id' ) -> on( 'instructors' ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> foreignId ( 'student_id'    ) -> references ( 'id' ) -> on( 'students'    ) -> onDelete( 'cascade' ) -> onUpdate( 'cascade' ) ;
            $table -> boolean   ( 'is_active'     ) -> default    ( 1    ) ;
            $table -> timestamps( );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
        Schema::dropIfExists( 'instructor_views' );
    }
}
